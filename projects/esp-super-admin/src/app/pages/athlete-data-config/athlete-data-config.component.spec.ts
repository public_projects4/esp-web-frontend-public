import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AthleteDataConfigComponent } from './athlete-data-config.component';

describe('AthleteDataConfigComponent', () => {
  let component: AthleteDataConfigComponent;
  let fixture: ComponentFixture<AthleteDataConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AthleteDataConfigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AthleteDataConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
