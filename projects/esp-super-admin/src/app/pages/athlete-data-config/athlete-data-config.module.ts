import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AthleteDataConfigComponent } from './athlete-data-config.component';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';





const routes: Route[] = [
  {
    path: '',
    component: AthleteDataConfigComponent
  }
];

@NgModule({
  declarations: [AthleteDataConfigComponent],
  imports: [
    CommonModule,
    EspSharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ]
})
export class AthleteDataConfigModule { }
