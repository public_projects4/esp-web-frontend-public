import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
declare var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({});
  returnUrl = '/esp-super-admin/dashboard';
  success = false;
  warning = false;
  somethingWrong = false;
  Email = '';
  Password = '';
  respondData: any;
  incorrect = false;
  constructor(
    private formBuilder: FormBuilder,
    private Api: AllApiService,
    private routes: Router
  ) {}
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userName: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      password: ['', [Validators.required]],
    });
  }
  get f() {
    return this.loginForm.controls;
  }
  onSubmit(data: any) {
    this.Api.getLogin(this.Email, this.Password).subscribe((data) => {
      this.respondData = data;
      if (this.respondData.message == 'Login Successfull') {
        this.routes.navigate([this.returnUrl]);
      }
      else {
        this.incorrect = true;
      }
      localStorage.setItem('superAdminName', this.respondData.userDetails.name);
      localStorage.setItem('ID', this.respondData.userDetails.id);
    });
  }
  eightCharLength = false;
  upperCase = false;
  lowerCase = false;
  numberCase = false;
  specialCase = false;
  isDisplay: boolean = false;
  validatePassword(data: any) {
    var valData = data.target.value;
    if (valData.length > 7) {
      this.eightCharLength = true;
    } else {
      this.eightCharLength = false;
    }
    if (valData.search(/[A-Z]/) < 0) {
      this.upperCase = false;
    } else {
      this.upperCase = true;
    }
    if (valData.search(/[a-z]/) < 0) {
      this.lowerCase = false;
    } else {
      this.lowerCase = true;
    }
    if (valData.search(/[0-9]/) < 0) {
      this.numberCase = false;
    } else {
      this.numberCase = true;
    }
    if (valData.search(/[!@#$%^&*]/) < 0) {
      this.specialCase = false;
    } else {
      this.specialCase = true;
    }
  }
  showBox(data: any) {
    this.isDisplay = true;
  }
  hideBox(data: any) {
    this.isDisplay = false;
  }
  fieldTextType: boolean;
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
}
