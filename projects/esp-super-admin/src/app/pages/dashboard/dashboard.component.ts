import { Component, OnInit } from '@angular/core';
import { StatisticBlockProperties } from 'projects/esp-shared/src/lib/models';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  orgIcon = 'settings_accessibility';
  activeIcon = 'task_alt';
  inactiveIcon = 'personal_injury';
  orgCount: any;
  activeCount: any;
  inactiveCount: any;
  color_class = 'dashtext-1';
  color_class1 = 'dashtext-4';
  color_class2 = 'dashtext-2';
  progress_color_class = 'dashbg-1';
  progress_color_class1 = 'dashbg-4';
  progress_color_class2 = 'dashbg-2';
  progress_percentage: any;
  progress_percentage1: any;
  progress_percentage2: any;
  constructor(private api: AllApiService) {}
  ngOnInit(): void {
    this.getOrganisationCount();
  }
  getOrganisationCount() {
    this.api.getOrganisationCount().subscribe((data: any) => {
      this.orgCount = data.totalOrganisations;
      this.activeCount = data.activeOrgainsations;
      this.inactiveCount = data.inActiveOrgainsations;
      this.progress_percentage = data.totalOrganisations;
      this.progress_percentage1 = data.activeOrgainsations;
      this.progress_percentage2 = data.inActiveOrgainsations;
    });
  }
}
