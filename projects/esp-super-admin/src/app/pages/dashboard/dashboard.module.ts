import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
const routes: Route[] = [
  {
    path: '',
    component: DashboardComponent
  }
];
@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    RouterModule.forChild(routes)
  ]
})
export class DashboardModule { }
