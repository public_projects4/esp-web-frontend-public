

import { newArray } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';

declare var $: any;

@Component({
  selector: 'app-manage-roles',
  templateUrl: './manage-roles.component.html',
  styleUrls: ['./manage-roles.component.css']
})
export class ManageRolesComponent implements OnInit {
  isCreates:any=[]
  test:any
  button=true
  isActive:any = []
  myForm: FormGroup = new FormGroup({});
  createCheckedArrayss:any = []
  viewCheckedArrays:any = []
  deleteCheckedArrays:any =[]
  editCheckedArrays:any = []
  createCheckedArrays:any = []
  description: any;
  roleName: any;
  parent : any = [];
  checkedArray: any = [];
  testArray: any = [];
  createCheckedArray : any = [];
  viewCheckedArray: any = [];
  editCheckedArray: any = [];
  deleteCheckedArray: any = [];
  title = "Configuration";
  createvalue:any
  viewvalue:any
  editvalue:any
  deletevalue:any
  getRoles:any = [];
  showButtonArray: any = [];
  loggedInName:any;
  data : any = []
  arr: any[];
  x: any[];

  constructor(private Api:AllApiService,private formBuilder: FormBuilder) {


    this.myForm = this.formBuilder.group({
      title: ['']
      // password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]]
    });
  }

  ngOnInit(): void {
    this.getDetails();
  }
  disabledCreateCheckedArrays:any=[]
  disabledViewCheckedArrays:any=[]
  disabledEditCheckedArrays:any=[]
  disabledDeleteCheckedArrays:any=[]
  getDetails() {
    this.Api.getAllRoles().subscribe((res: any) => {
      this.getRoles=res;
      console.log(this.getRoles);

      this.getRoles.forEach((element: any, key: number) => {
        this.checkedArray[key] = new Array(element.parentNode.length).fill(true);
        this.testArray = new Array(element.parentNode)
        console.log(this.checkedArray[key]);
        console.log(this.testArray);
        for (var i = 0; i < this.testArray.length; i++) {
          console.log(this.testArray[i]);

        }

        this.disabledCreateCheckedArrays = new Array(element.parentNode.length).fill(true);
        this.disabledDeleteCheckedArrays = new Array(element.parentNode.length).fill(false);
        this.disabledEditCheckedArrays = new Array(element.parentNode.length).fill(false);
        this.disabledViewCheckedArrays = new Array(element.parentNode.length).fill(false);

        this.x = new Array(this.getRoles.length)

for (var i = 0; i < this.x.length; i++) {
  this.x[i] = new Array(12).fill(true);
  // this.x[i] = new Array(this.testArray).fill(true);
  // console.log(this.x[i]);

  // this.x[i] = new Array(12).fill(element.parentNode[i].isActive);
  // this.x[i] = element.parentNode[i].isActive
}

this.getRoles.forEach((element: any, key: number) => {
  console.log(element);

  element.parentNode.forEach((element2 :any, key2 :number) => {
    // console.log(element2);
    // console.log(element2.isActive);

    this.x[key][key2] = !element2.isActive;

  });


})

console.log(this.x);




        if(element.parentNode.name == 'Dashboard' || element.parentNode.name == 'Reports' ||
        element.parentNode.name == 'Settings' || element.parentNode.name == 'Help & Support') {
          this.disabledCreateCheckedArrays[key] = true;
          this.disabledEditCheckedArrays[key] = true;
          this.disabledViewCheckedArrays[key] = false;
          this.disabledDeleteCheckedArrays[key] = true;
        }

        else if(element.parentNode.name == 'Manage Teams'){
          this.disabledDeleteCheckedArrays[key] = true;

        }
        else {
          this.disabledCreateCheckedArrays[key] = false;
          this.disabledEditCheckedArrays[key] = false;
          this.disabledViewCheckedArrays[key] = false;
          this.disabledDeleteCheckedArrays[key] = false;
        }
        let createTempArray: any = [];
        let editTempArray: any = [];
        let viewTempArray: any = [];
        let deleteTempArray: any = [];
        let activeTempArray: any = [];

        element.parentNode.forEach((parentNode: any, index: number) => {
          createTempArray.push(parentNode.isCreate);
          editTempArray.push(parentNode.isEdit);
          viewTempArray.push(parentNode.isView);
          deleteTempArray.push(parentNode.isDelete);
          activeTempArray.push(parentNode.isActive);
        });
        this.createCheckedArray[key] = createTempArray;
        this.editCheckedArray[key] = editTempArray;
        this.viewCheckedArray[key] = viewTempArray;
        this.deleteCheckedArray[key] = deleteTempArray;
        this.isActive[key] = activeTempArray;
      });
    });
  }

  // abc:any;
  // cfd:any

  onToggleClick(index: number, subIndex: number){
    console.log(this.checkedArray[index][subIndex]);

    // if(this.abc == index && this.cfd == subIndex){
    (this.checkedArray[index][subIndex]) ? this.x[index][subIndex] = true : this.x[index][subIndex] = false;

    // }
    // else{
    //   this.abc = index;
    //   this.cfd = subIndex;
    // (!this.checkedArray[index][subIndex]) ? this.x[index][subIndex] = true : this.x[index][subIndex] = false;

    // }
    this.checkedArray[index][subIndex] = !this.checkedArray[index][subIndex];
    if(this.checkedArray[index][subIndex]){

    }
    else{
      if(this.getRoles[index].parentNode[subIndex].isCreate){
        this.getRoles[index].parentNode[subIndex].isCreate = !this.getRoles[index].parentNode[subIndex].isCreate
      }
      if(this.getRoles[index].parentNode[subIndex].isView){
        this.getRoles[index].parentNode[subIndex].isView = !this.getRoles[index].parentNode[subIndex].isView
      }
      if(this.getRoles[index].parentNode[subIndex].isEdit){
        this.getRoles[index].parentNode[subIndex].isEdit = !this.getRoles[index].parentNode[subIndex].isEdit
      }
      if(this.getRoles[index].parentNode[subIndex].isDelete){
        this.getRoles[index].parentNode[subIndex].isDelete = !this.getRoles[index].parentNode[subIndex].isDelete
      }
    }


    // console.log("this check",this.getRoles[index].parentNode[subIndex]);


    // console.log(!this.checkedArray[index][subIndex], index, subIndex);

    (this.checkedArray[index][subIndex]) ? this.isActive[index][subIndex] = true : this.isActive[index][subIndex] = false;
    // console.log(this.disabledCreateCheckedArrays[subIndex]);


    // console.log(this.checkedArray[index][subIndex]);


    // console.log((this.checkedArray[index][subIndex]) ? this.disabledCreateCheckedArrays[index][subIndex] = true : this.disabledCreateCheckedArrays[index][subIndex] = false);

    // (this.checkedArray[index][subIndex]) ? this.x[index][subIndex] = true : this.x[index][subIndex] = false;
    // (this.checkedArray[index][subIndex]) ? this.disabledViewCheckedArrays[index][subIndex] = true : this.disabledViewCheckedArrays[index][subIndex] = false;
    // (this.checkedArray[index][subIndex]) ? this.disabledDeleteCheckedArrays[index][subIndex] = true : this.disabledDeleteCheckedArrays[index][subIndex] = false;

    // if(this.checkedArray[index][subIndex]){
    //   this.createCheckedArrays[index] = !this.createCheckedArrays[index];
    //   this.editCheckedArrays[index] = !this.editCheckedArrays[index];
    //   this.deleteCheckedArrays[index] = !this.deleteCheckedArrays[index];
    //   this.viewCheckedArrays[index] = !this.viewCheckedArrays[index];
    // }
    // else{
    //   this.createCheckedArrays[index] = !this.createCheckedArrays[index];
    //   this.editCheckedArrays[index] = !this.editCheckedArrays[index];
    //   this.deleteCheckedArrays[index] = !this.deleteCheckedArrays[index];
    //   this.viewCheckedArrays[index] = !this.viewCheckedArrays[index];
    // }
  }

  onButtonClick(){
  }



  checkboxClick(status: string, topIndex: number, subIndex: number){
    if(status === 'create') {
      this.getRoles[topIndex].parentNode[subIndex].isCreate = !this.getRoles[topIndex].parentNode[subIndex].isCreate




      let tempArray = this.createCheckedArray[topIndex];
      tempArray[subIndex] = !tempArray[subIndex];
      this.createCheckedArray[topIndex] = tempArray;
    } else if(status === 'view') {
      // this.viewCheckedArray[topIndex][subIndex]=[this.getRoles[topIndex].parentNode[subIndex].isView = !this.getRoles[topIndex].parentNode[subIndex].isView]
      this.getRoles[topIndex].parentNode[subIndex].isView = !this.getRoles[topIndex].parentNode[subIndex].isView

      let tempArray = this.viewCheckedArray[topIndex];
      tempArray[subIndex] = !tempArray[subIndex];
      this.viewCheckedArray[topIndex] = tempArray;
    } else if(status === 'edit') {
      this.getRoles[topIndex].parentNode[subIndex].isEdit = !this.getRoles[topIndex].parentNode[subIndex].isEdit

      let tempArray = this.editCheckedArray[topIndex];
      tempArray[subIndex] = !tempArray[subIndex];
      this.editCheckedArray[topIndex] = tempArray;
    } else {
      this.getRoles[topIndex].parentNode[subIndex].isDelete = !this.getRoles[topIndex].parentNode[subIndex].isDelete
      let tempArray = this.deleteCheckedArray[topIndex];
      tempArray[subIndex] = !tempArray[subIndex];
      this.deleteCheckedArray[topIndex] = tempArray;
    }
  }




  onSubmit(data: any, index :number){
    console.log(data);

    let postData = this.getRoles[index];
    console.log(postData);

    let array: any = [];
    postData.parentNode.forEach((element: any, key: number) => {
        element.isActive = this.isActive[index][key],
        element.isEdit = this.editCheckedArray[index][key];
        element.isActive=  this.isActive[index][key]

        element.isView = this.viewCheckedArray[index][key];
        element.isCreate = this.createCheckedArray[index][key];
        element.isDelete = this.deleteCheckedArray[index][key];
        array.push(element);
    });
    postData.parentNode = array;
    console.log(postData.parentNode);

    this.Api.updateRoles(postData).subscribe((data:any)=>{
      console.log('getting final response',data);
      this.getDetails()

      if(data.message=="Role Updated Successfully"){
        $("#result").modal('show');
      }
    })
  }

  Open(data:any){
// console.log(data);

  }


  click(data:any){
// console.log(data);

  }
}
