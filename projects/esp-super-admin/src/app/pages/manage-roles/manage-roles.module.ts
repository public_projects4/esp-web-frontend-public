import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { ManageRolesComponent } from './manage-roles.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const routes: Route[] = [
  {
    path: '',
    component: ManageRolesComponent
  }
];


@NgModule({
  declarations: [
    ManageRolesComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class ManageRolesModule { }
