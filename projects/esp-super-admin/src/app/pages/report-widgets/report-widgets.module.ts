import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { Route, RouterModule } from '@angular/router';
import { ReportWidgetsComponent } from './report-widgets.component';


const routes: Route[] = [
  {
    path: '',
    component: ReportWidgetsComponent
  }
];

@NgModule({
  declarations: [
    ReportWidgetsComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    RouterModule.forChild(routes)
  ]
})
export class ReportWidgetsModule { }
