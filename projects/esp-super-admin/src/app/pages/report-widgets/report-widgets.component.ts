import { Component, OnInit } from '@angular/core';
import { ReportWidgetsData } from 'projects/esp-shared/src/lib/models/Super-Admin-Models/report-widgets-data';

@Component({
  selector: 'app-report-widgets',
  templateUrl: './report-widgets.component.html',
  styleUrls: ['./report-widgets.component.css']
})
export class ReportWidgetsComponent implements OnInit {

  selected_title:string = "General Report Widgets"

  block1 = true;
  block2 = true;
  block3 = true;

  reportwidgetsdata : ReportWidgetsData [] = [{
    label:'Report widget 1',
    descrip_text:'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing'
  },
  {
    label:'Report widget 2',
    descrip_text:'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing'
  },
 {
    label:'Report widget 1',
    descrip_text:'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing'
  },
  {
    label:'Report widget 2',
    descrip_text:'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing'
  },
  {
    label:'Report widget 1',
    descrip_text:'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing'
  },
  {
    label:'Report widget 2',
    descrip_text:'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing'
  },
];

  constructor() { }

  ngOnInit(): void {
  }

  onChange(data:any){
    // console.log("ameen",data.target.value)
    if(data.target.value == "org1"){
      this.block1 = true;
      this.block2 = false;
      this.block3 = false;
      this.selected_title = "Organisation-1";
    }
    else if(data.target.value == "org2"){
      this.block1 = false;
      this.block2 = true;
      this.block3 = false;
      this.selected_title = "Organisation-2";
    }
    else if(data.target.value == "org3"){
      this.block1 = false;
      this.block2 = true;
      this.block3 = true;
      this.selected_title = "Organisation-3";
    }
    else if(data.target.value == "default"){
      this.block1 = true;
      this.block2 = true;
      this.block3 = true;
      this.selected_title = "General Report Widgets";
    }

  }

     // enable button code starts here
  // https://www.netjstech.com/2020/04/angular-disable-button-example.html#DisableButtonAfterClick  -- refer this link for click enable button
  click : boolean = true;
  count=0;
  onButtonClick(){
    this.count++
    if(this.count==1)
    this.click = !this.click;
  }
  onKey(event: KeyboardEvent) {
    // if value is not empty the set click to false otherwise true
    this.click = (event.target as HTMLInputElement).value === '' ? true:false;
  }

  // enable button code ends here


}
