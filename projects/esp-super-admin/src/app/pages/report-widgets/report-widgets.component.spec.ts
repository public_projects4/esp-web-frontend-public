import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportWidgetsComponent } from './report-widgets.component';

describe('ReportWidgetsComponent', () => {
  let component: ReportWidgetsComponent;
  let fixture: ComponentFixture<ReportWidgetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportWidgetsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportWidgetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
