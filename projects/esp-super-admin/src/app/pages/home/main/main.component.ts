import { transition } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";


@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})



export class MainComponent implements OnInit {


  isCollapsed = true;
  isCollapsed_1 = true;
  isCollapsed_10 = true;

  isCollapsed_2 = true;
  myFun1(){
      this.isCollapsed=true;
    this.isCollapsed_1=true;
    this.isCollapsed_2=true;
   this.isCollapsed_10 = true;

  }

  myFun2(){
    this.isCollapsed=false;
    this.isCollapsed_1=true;
    this.isCollapsed_10 = true;

    this.isCollapsed_2=true;
  }

  myFun3(){
    this.isCollapsed=true;
    this.isCollapsed_1=false;
    this.isCollapsed_10 = true;

    this.isCollapsed_2=true;
  }

  myFun4(){
    this.isCollapsed=true;
    this.isCollapsed_1=true;
    this.isCollapsed_10 = true;

    this.isCollapsed_2=false;
  }


name:any
  ngOnInit(): void {
this.name=localStorage.getItem('superAdminName')

  }






// upload image code starts here
// [code link] = https://stackblitz.com/edit/angular-file-upload-preview-k6dnu5?file=app%2Fapp.component.html

url:any = '';
onSelectFile(event:any) {
  if (event.target.files && event.target.files[0]) {
    var reader = new FileReader();

    reader.readAsDataURL(event.target.files[0]); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.url = reader.result;
    }
  }
}


// upload image code ends here



}
