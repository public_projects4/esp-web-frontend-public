import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { Route, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MessagesComponent } from 'projects/esp-shared/src/lib/components/messages/messages.component';
import { AuthenticationGuard } from 'projects/esp/src/app/authentication.guard';

const routes: Route[] = [
  {
    path: '',
    component: HomeComponent,canActivate:[AuthenticationGuard],
    children: [
      {
       path: 'dashboard',
       loadChildren: () =>  import('../dashboard/dashboard.module').then(o => o.DashboardModule)
      },
       {
        path: 'new-organisations',
        loadChildren: () =>  import('../new-oraganisations/new-oraganisations.module').then(o => o.NewOraganisationsModule)
       },

       {
        path: 'list-of-organisations',
        loadChildren: () =>  import('../list-of-oraganisations/list-of-oraganisations.module').then(o => o.ListOfOraganisationsModule)
       },
       {
        path:  'manage-roles',
        loadChildren: () =>  import('../manage-roles/manage-roles.module').then(o => o.ManageRolesModule)
       },
       {
        path:  'report-widgets',
        loadChildren: () =>  import('../report-widgets/report-widgets.module').then(o => o.ReportWidgetsModule)
       },
       {
        path:  'manage-athlete-device',
        loadChildren: () =>  import('../manage-athlete-device/manage-athlete-device.module').then(o => o.ManageAthleteDeviceModule)
       },
       {
        path: 'manage-widgets',
        loadChildren: () => import('../manage-widgets/manage-widgets.module').then(o => o.ManageWidgetsModule)
       },
       {
        path: 'create-new-role',
        loadChildren: () => import('../create-new-role/create-new-role.module').then(o => o.CreateNewRoleModule)
       },
       {
        path: 'messages',
        loadChildren: () => import('../messages/messages.module').then(o => o.MessagesModule)
       },

       {
        path: 'settings',
        loadChildren: () => import('../settings/settings.module').then(o => o.SettingsModule)
       },
       {
        path: 'athlete-data-config',
        loadChildren: () => import('../athlete-data-config/athlete-data-config.module').then(o => o.AthleteDataConfigModule)
       },
       {
        path: 'super-admin-reports',
        loadChildren: () => import('../super-admin-reports/super-admin-reports.module').then(o => o.SuperAdminReportsModule)
       }
    ]
  }
];


@NgModule({
  declarations: [
    HomeComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    RouterModule.forChild(routes),
    NgbModule
  ]
})
export class HomeModule { }
