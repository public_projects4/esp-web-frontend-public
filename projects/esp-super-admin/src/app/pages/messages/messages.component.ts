import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { ModalCommanService } from 'projects/esp-shared/src/lib/models/modal-comman-service'
import { AngularFireMessaging } from '@angular/fire/messaging';

declare var $: any;


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  modelDetails :ModalCommanService []=[

  ]
  selectedInbox = true;

  pushedData  :any =[];
  arrPush:string='';
  replayUploadedNewMsgBtn:boolean = false;
  replayFileUploadEnabled = false
  file: any;
  description: any;
  hideUploadNewMsgBlock = false;
  fileName:any;
  pushedDataNewMsg  :any =[];
  fileUploadEnabled = false;
  secondBlock = false;
  firstBlock = true;
  fileNameCoach:any;
  pushedDataCoach  :any =[];
  uploadedFile: string = '';
  uploadedNewMsgBtn: boolean =true;
  subject: string = '';
  sentButton: Boolean = false;
  isEnableMsgHistory: boolean = false;
  replyMessageData :any;
  fileTypeName: any = '';
  adminsList: any[];
  uploadedFileArray: any = [];
  superAdminName: string;
  adminId: any;
  superAdminNewMsgForm: FormGroup = new FormGroup({});
  isReplyMsg: any;
  inboxList: any[];
  sentList: any[];
  msgCount: number;
  selectedMessageId: any;
  superAdminId: string;
  isRowSelected: boolean;
  messageIdsList: any = [];
  selectedMessage: any[];
  isDelete: boolean;
  readColor:any
  senderToken: any;
  isExists: boolean;
  tokenObj: any;
  receiverToken: string;

  constructor(private Api: AllApiService, private formBuilder: FormBuilder, private datepipe: DatePipe,private angularFireMessaging: AngularFireMessaging) {
    this.superAdminNewMsgForm = this.formBuilder.group({
      recieverId: ['', [Validators.required]],
      description: ['', [Validators.required]],
      subject: ['', [Validators.required]],
      senderId: '',
      attachment: this.formBuilder.array([]),

    });
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
      _messaging.onMessage = _messaging.onMessage.bind(_messaging);
      _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
      )
  }

  ngOnInit(): void {
    this.requestPermission()
    this.receiveMessage();
    if(localStorage.getItem('superAdminName')!=null){
    this.superAdminName = localStorage.getItem('superAdminName');
    this.superAdminId = localStorage.getItem('ID');
    }
    this.getAllOrganizations();
    this.getMessagesByReceiverId(this.superAdminId);
    this.getTokenById(this.superAdminId);
    //this.saveOrUpdateToken();
  }


//   this.Api.uploadMessageFile(this.orgName, receiverId, this.testData).subscribe(result => {
//     this.uploadedFile = result;
//     this.uploadStatus = 'Successfully Uploaded';
// this.hideUploadNewMsgBlock = false;
// this.fileUploadEnabled = false;


//     this.pushDataNewMsg()
//     this.pushDataCoach()
//     this.pushData()
//   }

  get f() {
    return this.superAdminNewMsgForm.controls;
  }

  newSkill(): FormGroup {
    return this.formBuilder.group({
      file: '',
      fileNameToDisplay: '',
      btnUpload: [false],
      cancelBtn: [false],
    });
  }

  addSkills() {
    this.abc.push(this.newSkill());
  }

  removeSkill(i: number) {
    this.abc.removeAt(i);
  }

  get abc(): FormArray {
    return this.superAdminNewMsgForm.get('attachment') as FormArray;
  }



  uploadFileApi = (files: any, i: any) => {

    console.log(files);

    console.log(files[0].name);



    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);

    this.Api.uploadMessageFile("61654db8985824ac061823b1", '616551d6217c6d98238fd958', formData).subscribe((data: any) => {
      console.log('upload treatmernt con', data);



      this.fileTypeName = data;

      let a: any = this.superAdminNewMsgForm.controls['attachment'];

      a['controls'][i].patchValue({
        file: this.fileTypeName,
        btnUpload: true,
        cancelBtn:true
      });
      console.log('this.upFileName', this.fileTypeName);

      a['controls'][i].patchValue({
        fileNameToDisplay:files[0].name
      });


    });




  };




  cancelArray(i:any) {
    let a: any = this.superAdminNewMsgForm.controls['attachment'];
    console.log('this.upFileName', this.fileTypeName);
        a['controls'][i].patchValue({
          fileNameToDisplay: [''],
          btnUpload: false,
          cancelBtn:false
        });


  }

  // clicked(){

  // }


  onClickReply(data:any) {
    this.resetFormData();
    this.isReplyMsg = true;
    this.replyMessageData = data[0];
    console.log(this.replyMessageData);
    $('#replyModal').modal('show');
  }



  onClickBackToList() {
    this.isEnableMsgHistory = false;

    if(this.selectedInbox){
      this.showInbox();
      }
      else{
      // this.showSentMessages();
      }

  }

  showSentMessages() {
    this.selectedInbox = false;

    this.sentButton = true;
    this.isEnableMsgHistory = false;
    this.getMessagesBySenderId(this.superAdminId);
  }
  showInbox() {
    this.selectedInbox = true;

    this.sentButton = false;
    this.isEnableMsgHistory = false;
    this.getMessagesByReceiverId(this.superAdminId);
  }


  myFun() {
    this.firstBlock = true;
    this.secondBlock = false;
    this.resetFormData();
  }

  secondFun() {
    this.firstBlock = false;
    this.secondBlock = true;
    this.resetFormData();
  }

  onClickNewMessage() {
    this.firstBlock = true;
    this.secondBlock = false;
    this.resetFormData();
    // this.isDelete = false;
    $('#newMessageModal').modal('show');


  }

  uploadFilePinCoach = (files: any) => {


    this.fileUploadEnabled = true;


    console.log(files);

    console.log(files[0].name);



    let fileToUploadCoach = <File>files[0];
    console.log(fileToUploadCoach.name);

    this.file = fileToUploadCoach
    this.fileNameCoach = fileToUploadCoach.name

    console.log(this.file);
  }

  uploadFilePin = (files: any) => {

    this.hideUploadNewMsgBlock = true;


    console.log(files);

    console.log(files[0].name);



    let fileToUpload = <File>files[0];
    console.log(fileToUpload.name);

    this.file = fileToUpload
    this.fileName = fileToUpload.name

    console.log(this.file);
  }

  pushDataNewMsg(){
    this.pushedDataNewMsg.push(this.fileName)

    console.log(this.pushedDataNewMsg);
    this.fileName = '';

    // this.replayUploadedNewMsgBtn = false;

  }


  uploadFile() {
    console.log('its working');

    var receiverId = '';

    if (this.file != undefined) {
      console.log(this.file);
      console.log(this.file.name);
      var testData = new FormData();
      testData.append('file', this.file, this.file.name);
      console.log(testData);

      this.Api.uploadMessageFile(this.superAdminName, receiverId, testData).subscribe(result => {
        this.uploadedFile = result;
        this.hideUploadNewMsgBlock = false;
        this.fileUploadEnabled = false;
        //this.pushDataNewMsg()
        //this.pushDataCoach()
        this.pushData()
      },
        error => {
          //this.uploadStatus = JSON.parse(error.error).message;
          console.log(error)
        })
    }
  }


  testUploadFilePin = (files: any) => {

    this.replayUploadedNewMsgBtn = true;


    console.log(files);

    console.log(files[0].name);
    this.replayFileUploadEnabled= true;



    let fileToUpload = <File>files[0];
    console.log(fileToUpload.name);
    this.file = fileToUpload

    this.arrPush = fileToUpload.name

    console.log( this.arrPush);



  }

  resetFormData() {
    this.pushedData = [];
    this.pushedDataNewMsg = [];
    this.pushedDataCoach = []
    this.subject = '';
    this.uploadedFileArray = [];
    this.hideUploadNewMsgBlock = false;
    this.fileName =''
    this.uploadedFile = ''
    this.description = '';



  }

  pushData(){
    this.pushedData.push(this.fileName)
    this.uploadedFileArray.push(this.uploadedFile);
    console.log(this.pushedData);
    this.fileName = '';

    this.replayUploadedNewMsgBtn = false;
  }

  cancelReplayField(){
    this.arrPush = '';
    this.replayUploadedNewMsgBtn = false;

  }

  cancelReplay(i:any){
    console.log(i);
  this.pushedData.splice(i, 1);
  }

  sendMessage(){
    var messageData = Object.create({});
    messageData.createdDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString(),
    messageData.updatedDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString(),
    messageData.subject = this.subject;
    messageData.description = this.description;
    messageData.date = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString(),
    messageData.attachment = this.uploadedFileArray;
    messageData.createdBy = this.superAdminName // admin name or coach name
    messageData.senderName = this.superAdminName;
    messageData.isDeleted = false;
    messageData.recieverId = this.adminId;
    messageData.senderId = this.superAdminId;
    if(this.isReplyMsg){
    messageData.replyId = this.selectedMessageId;
    messageData.recieverId = this.replyMessageData.senderId;
    messageData.subject = this.replyMessageData.subject;
    }
    console.log(messageData);
    this.Api.sendMessage(messageData).subscribe((data: any) => {
      if (data.message === 'Message sent successfully') {
        $('#messageSentSuccessModal').modal('show');
        this.pushedData = []
        this.pushedDataNewMsg = []
        this.hideUploadNewMsgBlock = false;
        this.getMessagesBySenderId(this.superAdminId);
        this.getMessagesByReceiverId(this.superAdminId);
        this.getReceiverTokenById(messageData.recieverId);
      }
   })
    // this.pushedData = [];
    // this.pushedDataNewMsg = [];
    // this.hideUploadNewMsgBlock = false;
  }

  cancel() {
    this.file =''
    this.fileName =''
   this.uploadedNewMsgBtn = false;
   this.hideUploadNewMsgBlock = false;
    this.uploadedFile = '';

  }

  cancelNewMsgUpload(i:any){
    console.log(i);
  //this.pushedDataNewMsg.splice(i, 1);
  this.pushedData.splice(i,1);
  this.uploadedFileArray.splice(i,1);
  }

  pushDataCoach(){
    this.pushedDataCoach.push(this.fileNameCoach)

    console.log(this.pushedData);
    this.fileNameCoach = '';

    this.fileUploadEnabled = false;

  }

  cancelCoachData() {
    this.fileUploadEnabled = false;
    this.fileNameCoach = '';
    this.file = '';
   }

   cancelCoach(i:any){
    console.log(i);
  this.pushedDataCoach.splice(i, 1);
  }
  getAllOrganizations(){
    this.Api.getAllOrganization().subscribe((data:any)=>{
      this.adminsList = [];
      data.forEach(element => {
        var admin = Object.create({});
        admin.name = element.organisationName;
        admin.id = element.id;
        this.adminsList.push(admin);
      });
    })
  }
  handleAdminChange(data:any){
    this.adminId = data.target.value;
  }
  getMessagesByReceiverId(id: any) {
    this.sentList = [];
    this.Api.getMessagesByReceiveId(id).subscribe((data: any) => {
      console.log(data);
      this.inboxList = [];
      data.forEach(element => {
        var message = Object.create({});
        message.name = element.recieverName;
        message.teamName = element.teamName;
        message.description = element.description;
        message.subject = element.subject;
        message.date = element.date;
        message.dateToDisplay = this.datepipe.transform(new Date(), element.date);
        message.id = element.id;
        message.createdBy = element.createdBy;
        message.attachment = element.attachment;
        message.recieverId = element.recieverId;
        message.senderId = element.senderId;
        message.createdDate = element.createdDate;
        message.days = element.days;
        message.image = element.image;
        message.isArchieved = element.isArchieved;
        message.isDeleted = element.isDeleted;
        message.isRead = element.isRead;
        message.isReciverDeleted = element.isReciverDeleted;
        message.isSenderDeleted = element.isSenderDeleted;
        message.isSent = element.isSent;
        message.recieverName = element.recieverName;
        message.replyId = element.replyId;
        message.senderName = element.senderName;
        message.teamId = element.teamId;
        message.updatedBy = element.updatedBy;
        message.updatedDate = element.updatedDate;
        this.inboxList.unshift(message);
      })
      this.msgCount = this.inboxList.length;
    })
  }
  getMessagesBySenderId(id) {
    // this.inboxList = [];
    this.Api.getMessagesBySentId(id).subscribe((data: any) => {
      console.log(data);
      this.sentList = [];
      data.forEach(element => {
        var message = Object.create({});
        message.name = element.recieverName;
        message.teamName = element.teamName;
        message.description = element.description;
        message.subject = element.subject;
        message.date = element.date;
        message.dateToDisplay = this.datepipe.transform(new Date(), element.date);
        message.id = element.id;
        message.createdBy = element.createdBy;
        message.attachment = element.attachment;
        message.recieverId = element.recieverId;
        message.senderId = element.senderId;
        message.createdDate = element.createdDate;
        message.days = element.days;
        message.image = element.image;
        message.isArchieved = element.isArchieved;
        message.isDeleted = element.isDeleted;
        message.isRead = element.isRead;
        message.isReciverDeleted = element.isReciverDeleted;
        message.isSenderDeleted = element.isSenderDeleted;
        message.isSent = element.isSent;
        message.recieverName = element.recieverName;
        message.replyId = element.replyId;
        message.senderName = element.senderName;
        message.teamId = element.teamId;
        message.updatedBy = element.updatedBy;
        message.updatedDate = element.updatedDate;
        this.sentList.unshift(message);
      })
    })
  }
  onRowSelected(dataItem: any, event, id: any) {
    console.log(id);
    this.isRowSelected = false;
    this.selectedMessageId = id;
    dataItem.isSelected = event.target.checked;
    if(dataItem.isSelected)
    this.messageIdsList.push(dataItem.id);
    if(dataItem.isSelected ==false)
      this.messageIdsList = this.messageIdsList.filter(messageId => messageId !==dataItem.id)
    if (this.inboxList.filter((x) => x.isSelected == true).length > 0) {
      this.isRowSelected = true;
    }
    if (this.sentList.filter((x) => x.isSelected == true).length > 0) {
      this.isRowSelected = true;
    }
  }
  onClickMsgHistory(row: any) {

    if(row.isRead == true){
      this.readColor = true;
    }
    else{
      this.readColor = false;

    }
    console.log(row);




    this.selectedMessage = [];
    this.isEnableMsgHistory = true;
    this.selectedMessageId = row.id;
    var currentMessage = Object.create({});
    var files = [];
    currentMessage.id = row.id;
    currentMessage.image = row.image;
    currentMessage.description = row.description;
    currentMessage.date = this.datepipe.transform(row.date, 'dd MMM yyyy, hh:mm:a');
    // currentMessage.roleName = row.name;
    currentMessage.roleName = this.sentButton == true ? row.name : row.senderName;

    currentMessage.subject = row.subject;
    row.attachment.forEach(attachment=>{
      var fileObj = Object.create({});
      fileObj.fileName = attachment.split('/')[4];
      fileObj.fileLocation = attachment;
      files.push(fileObj);
    })
    currentMessage.files= files;
    currentMessage.teamName = row.teamName;
    currentMessage.senderId = row.senderId;
    currentMessage.recieverId = row.recieverId;
    currentMessage.dateToDisplay = row.dateToDisplay;
    this.selectedMessage.push(currentMessage);
    console.log(this.selectedMessage);

    localStorage.setItem('printDate',currentMessage.date);
    localStorage.setItem('printSubject',row.subject);
    localStorage.setItem('printDescription',row.description);
    localStorage.setItem('printName',row.name);

    const updateMessageRead = {
      createdBy: row.createdBy,
      createdDate: row.createdDate,
      updatedBy: row.updatedBy,
      updatedDate: row.updatedDate,
      isDeleted: row.isDeleted,
      senderId: row.senderId,
      recieverId: row.recieverId,
      teamId: row.teamId,
      replyId: row.replyId,
      subject: row.subject,
      description: row.description,
      id:row.id,
      date: row.date,
      attachment: row.attachment,
      isSent: row.isSent,
      isRead: true,
      isArchieved: row.isArchieved,
      senderName: row.senderName,
      recieverName: row.recieverName,
      image: row.image,
      teamName: row.teamName,
      days: row.days,
      isSenderDeleted: row.isSenderDeleted,
      isReciverDeleted: row.isReciverDeleted
    }

    console.log(updateMessageRead);

    this.Api.updateMessage(updateMessageRead).subscribe( (data: any) => {
      console.log(data);
    })
  }
  openAttachment(url: any) {
    window.open(url);
  }
  deleteMessage() {
    this.isDelete = true;
    console.log(this.messageIdsList.length)
    if(this.messageIdsList.length <=1){
    this.Api.deleteMessageById(this.selectedMessageId,this.superAdminId).subscribe((data: any) => {
      if (data.success ==true) {
        if (this.sentButton)
          this.getMessagesBySenderId(this.superAdminId);
        else
          this.getMessagesByReceiverId(this.superAdminId);
        $('#messageSuccessfullyDeletedModal').modal('show')
        this.isRowSelected = false;
        this.isEnableMsgHistory = false;
      }
    })
  }else{
    this.Api.deleteMultipleMessages(this.superAdminId,this.messageIdsList).subscribe((data: any) => {
      if (data.success === true) {
        if (this.sentButton)
          this.getMessagesBySenderId(this.superAdminId);
        else
          this.getMessagesByReceiverId(this.superAdminId);
        $('#messageSuccessfullyDeletedModal').modal('show')
        this.isRowSelected = false;
        this.isEnableMsgHistory = false;
      }
    })
  }
  this.messageIdsList = [];
  }

  printPage(){

    var a = window.open(
       '/#/print-page'
     )

     a.window.print()

   }
   requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
        this.senderToken = token;
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.log("new message received. ", payload);
      })
  }
  getTokenById(id:any){
    this.Api.getTokenById(id).subscribe((data:any)=>{
      if(data.length ==0)
      this.isExists = false;
      else{
      this.isExists = true;
      this.tokenObj = data;
      }
      this.saveOrUpdateToken();
  });
  }
  saveOrUpdateToken(){
    console.log(this.senderToken);
    var tokenInfo = Object.create({});
    if(this.isExists ==false){
      console.log("if");
    tokenInfo.createdBy = this.superAdminName;
    tokenInfo.createdDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString();
    tokenInfo.updatedDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString();
    tokenInfo.isDeleted = false;
    tokenInfo.tokenPasserId = this.superAdminId;
    tokenInfo.fcmToken = this.senderToken;
    tokenInfo.date = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString();
    if(tokenInfo.fcmToken !=undefined && tokenInfo.tokenPasserId !=undefined)
    this.Api.saveToken(tokenInfo).subscribe((data)=>{
      console.log(data);
    })
  }
    else{
      tokenInfo.id = this.tokenObj[0].id;
      tokenInfo.createdBy = this.tokenObj[0].createdBy;
      tokenInfo.createdDate = this.tokenObj[0].createdDate;
      tokenInfo.updatedDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString();
      tokenInfo.isDeleted = false;
      tokenInfo.tokenPasserId = this.tokenObj[0].tokenPasserId;
      tokenInfo.fcmToken = this.senderToken;
      tokenInfo.date = this.tokenObj[0].createdDate;
      tokenInfo.updatedBy =  this.superAdminName;
      console.log(tokenInfo);
      if(tokenInfo.fcmToken !=undefined && tokenInfo.tokenPasserId !=undefined)
      this.Api.updateToken(tokenInfo).subscribe((data)=>{
        console.log(data);
      })
    }
  }
  sendPushNotification(){
    console.log("push method" + this.receiverToken);
    var data =
    {
      "notification": {
       "title": this.subject,
       "body": this.description,
       "click_action": 'http://localhost:4200/#/esp-super-admin/messages'
      },
      "to" : this.receiverToken,
     }
     if(this.receiverToken!=undefined)
  this.Api.sendPushNotification(data).subscribe((data)=>{
    console.log(data);
  })
}
getReceiverTokenById(id:any){
  this.Api.getTokenById(id).subscribe((data:any)=>{
    console.log(data.length);
    if(data.length === 0){
    //this.errorMessage = "receiver don't had valid token to send push notification";
    //console.log(this.errorMessage);
  }
    else{
    this.receiverToken = data[0].fcmToken;
    this.sendPushNotification();
    }
  })
}

confirmDelete(){
  $('#removeConfirm').modal('show');
}

}
