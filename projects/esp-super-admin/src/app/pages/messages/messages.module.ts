import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessagesComponent } from './messages.component';



const routes: Route[] = [
  {
    path: '',
    component: MessagesComponent
  }
];


@NgModule({
  declarations: [
    MessagesComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EspSharedModule,
    FormsModule,

    RouterModule.forChild(routes)
  ],
})
export class MessagesModule { }
