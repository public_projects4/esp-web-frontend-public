import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperAdminReportsComponent } from './super-admin-reports.component';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Route[] = [
  {
    path: '',
    component: SuperAdminReportsComponent
  }
];

@NgModule({
  declarations: [SuperAdminReportsComponent],
  imports: [
    CommonModule,
    EspSharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),

  ]
})
export class SuperAdminReportsModule { }
