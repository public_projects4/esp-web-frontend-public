import { Component, OnInit } from '@angular/core';
import { ManageWidgets } from 'projects/esp-shared/src/lib/models/Super-Admin-Models/manage-widgets';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';

import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';
declare var $: any;
@Component({
  selector: 'app-manage-widgets',
  templateUrl: './manage-widgets.component.html',
  styleUrls: ['./manage-widgets.component.css'],
})
export class ManageWidgetsComponent implements OnInit {


  modelDetails :ModalCommanService []=[
    {
      modelName: "Widget"
    },
    {
      modelName: "Dataset"
    },
  ]

  // managewidgets: ManageWidgets[] = [
  //   {
  //     label: 'Dashboard widget 1',
  //     description:
  //       'Lorem ipsum is placeholder text commonly used in the graphic, print and publishing',
  //     widgetImage: './assets/img/widgets/widget1.svg',
  //     click: 'click',
  //   },
  // ];
  color2: any = '';
  bordercolor: any = '';
  clickedBox = true;
  checkedDataset  = true;
  collectionData: any = [];
  widgetsTypeData: any = [];
  dataSetArray: any = [];
  datasets: any = [];
  collectionId: string = '';
  dataSetsCreatedDate: string = '';
  dataSetsId: string = '';
  myAddWidgetForm: FormGroup = new FormGroup({});
  constructor(private Api: AllApiService, private formBuilder: FormBuilder) {
    this.myAddWidgetForm = this.formBuilder.group({
      createdDate: '',
      createdBy: '',
      widgetName: ['', [Validators.required]],
      widetDescription: ['', [Validators.required]],
      widgetTypeId: ['', [Validators.required]],
      collectionName: ['', [Validators.required]],
    });
  }
  selectedBox: number;
  ngOnInit(): void {
    this.getCollections();
    this.getWidgetsType();
  }
  onBoxSelect(data, i) {
    this.selectedBox = i;

    this.myAddWidgetForm.patchValue({
      widgetTypeId: data,
    });
    this.clickedBox = false;
  }

  greenTicActiveOne = false;
  blueRadioBtnOne = true;
  greyTicActiveTwo = true;
  greenTicActiveTwo = false;
  blueRadioBtnTwo = false;
  selectedSecondTab() {
    this.greenTicActiveOne = true;
    this.blueRadioBtnOne = false;
    this.blueRadioBtnTwo = true;
    this.greyTicActiveTwo = false;
  }
  selectedFirstTab() {
    this.greenTicActiveOne = false;
    this.greenTicActiveTwo = false;
    this.blueRadioBtnOne = false;
    this.greyTicActiveTwo = true;
  }
  selectedPreviewTab(){
    this.greenTicActiveTwo = true;
    this.blueRadioBtnTwo = false
    this.greyTicActiveTwo = false
  }
  getCollections() {
    this.Api.getWidgetCollections().subscribe((data: any) => {
      this.collectionData = data;
    });
  }
  getWidgetsType() {
    this.Api.getAllWidgetTypes().subscribe((data: any) => {
      this.widgetsTypeData = data;
    });
  }
  onSubmit() {
    const data = this.myAddWidgetForm.value;

    this.collectionId = data.collectionName;

    data['createdBy'] = localStorage.getItem('Name');
    data['createdDate'] = new Date();

    this.Api.addWidgets(data).subscribe((res: any) => {
      if (res.message == 'AddWidget Added Successfully') {

    $('#successModal').modal('show');
        this.myAddWidgetForm.reset({
          collectionName: '',
          widgetTypeId: '',
        });
        this.selectedBox = null;
        this.moveToSecondTab();
      }
      else{
        $('#modelError').modal('show');
      }
    });
  }

  moveToSecondTab() {
    console.log(this.collectionId);

    this.Api.getDataSet(this.collectionId).subscribe((data: any) => {
      console.log(data[0].datasets);
      console.log(data[0]);
      this.datasets = data[0].datasets;
      console.log(this.datasets);
      this.dataSetsId = data[0].id;
      this.dataSetsCreatedDate = data[0].createdDate;
      console.log(this.dataSetsId);
    });
    this.selectedSecondTab()
    $('.nav-tabs a[href="#select_column_filter"]').tab('show');
  }

  onCheckedDataSet(data: any) {
    this.checkedDataset = false;
    console.log(data);
    if (data.isSelected == true) {
      this.dataSetArray = {
        id: this.dataSetsId,
        createdBy: localStorage.getItem('Name'),
        createdDate: this.dataSetsCreatedDate,
        updatedBy: localStorage.getItem('Name'),
        updatedDate: new Date(),
        isDeleted: true,
        collectionId: this.collectionId,
        datasets: [
          {
            datasetName: data.datasetName,
            datasetId: data.datasetId,
            isSelected: data.isSelected,
          },
        ],
      };
    } else {
      this.dataSetArray = {
        id: this.dataSetsId,
        createdBy: localStorage.getItem('Name'),
        createdDate: this.dataSetsCreatedDate,
        updatedBy: localStorage.getItem('Name'),
        updatedDate: new Date(),
        isDeleted: true,
        collectionId: this.collectionId,
        datasets: [
          {
            datasetName: data.datasetName,
            datasetId: data.datasetId,
            isSelected: true,
          },
        ],
      };

      console.log(this.dataSetArray);
    }
  }

  goToWidgetPreviewTab() {
    this.Api.updateDataSet(this.dataSetArray).subscribe((res: any) => {
      console.log(res);

      if(res.message == "Dataset Updated Successfully"){
        $('#updateModal').modal('show');
        this.moveToWidgetPreviewTab()
      }
      else{
        $('#modelError').modal('show');
      }
    });
  }

  moveToWidgetPreviewTab() {
    this.selectedPreviewTab()

    $('.nav-tabs a[href="#widget_preview"]').tab('show');
  }


}
