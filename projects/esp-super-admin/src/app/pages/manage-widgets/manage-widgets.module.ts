import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { ManageWidgetsComponent } from './manage-widgets.component';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Route[] = [
  {
    path: '',
    component: ManageWidgetsComponent
  }
];

@NgModule({
  declarations: [
    ManageWidgetsComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class ManageWidgetsModule { }
