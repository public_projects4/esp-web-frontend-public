import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';

declare var $: any;

@Component({
  selector: 'app-create-new-role',
  templateUrl: './create-new-role.component.html',
  styleUrls: ['./create-new-role.component.css'],
})
export class CreateNewRoleComponent implements OnInit {
  disable1 = false;
  disable2 = false;
  disable3 = false;
  disable4 = false;
  
  test: any;
  isActive: any = [];
  myForm: FormGroup = new FormGroup({});
  demo = false;
  success = false;
  exist = false;
  disabled = false;
  selected_title: string = 'New Role';
  
  //Disabled & Enabled
  disabledCreateCheckedArrays: any = [];
  disabledDeleteCheckedArrays: any = [];
  disabledEditCheckedArrays: any = [];
  disabledViewCheckedArrays: any = [];
  //Checkbox
  createCheckedArray: any = [];
  viewCheckedArray: any = [];
  editCheckedArray: any = [];
  deleteCheckedArray: any = [];

  loggedInName: any;
  Description: any;
  RoleName: any;
  parent: any = [];
  checkedArray: any = [];
  roleName: any;
  description: any;

  constructor(
    private Api: AllApiService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder
  ) {
    this.myForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getParentNode();
    this.loggedInName = localStorage.getItem('Name');
  }
  get f() {
    return this.myForm.controls;
  }

  // enable button code starts here
  // https://www.netjstech.com/2020/04/angular-disable-button-example.html#DisableButtonAfterClick  -- refer this link for click enable button
  onToggleClick(index: number, parent: string) {
    this.checkedArray[index] = !this.checkedArray[index];
    (this.checkedArray[index]) ? this.isActive[index]=true : this.isActive[index]=false;
    if(parent != 'Dashboard' && parent != 'Settings' && parent != 'Help & Support' && parent != 'Reports') {
      this.disabledCreateCheckedArrays[index] = !this.disabledCreateCheckedArrays[index];
      this.disabledEditCheckedArrays[index] = !this.disabledEditCheckedArrays[index];
      this.disabledDeleteCheckedArrays[index] = !this.disabledDeleteCheckedArrays[index];
      this.disabledViewCheckedArrays[index] = !this.disabledViewCheckedArrays[index];

    } 

 
    else {
      this.disabledViewCheckedArrays[index] = !this.disabledViewCheckedArrays[index];

    }

  
  }

  checkboxClick(status: string, index: number) {
    if (status === 'create') {
      this.createCheckedArray[index] = !this.createCheckedArray[index];
    } else if (status === 'view') {
      this.viewCheckedArray[index] = !this.viewCheckedArray[index];
    } else if (status === 'edit') {
      this.editCheckedArray[index] = !this.editCheckedArray[index];
    } else {
      this.deleteCheckedArray[index] = !this.deleteCheckedArray[index];
    }
  }

  onSubmit() {
    let parentData: any = [];
    this.parent.forEach((element: any, key: any) => {
      // if(this.checkedArray[key]) {
      parentData.push({
        id: element.id,
        name: element.parentNodeName,
        isActive: this.isActive[key],
        isView: this.viewCheckedArray[key],
        isCreate: this.createCheckedArray[key],
        isEdit: this.editCheckedArray[key],
        isDelete: this.deleteCheckedArray[key],
      });
      // }
    });
    const postData = {
      createdBy: this.loggedInName,
      roleName: this.roleName,
      parentNode: parentData,
      description: this.description,
    };
    console.log(postData);
    this.spinner.show()
    this.Api.addRoles(postData)
    .subscribe((data:any)=>{
      console.log('final response',postData)

      if(data.message=='Role Added Successfuly'){
        $('#result').modal('show')
        this.success=true
        this.exist=false
      }
    else  if(data.message=='Role Exsits'){
        $('#result').modal('show')
        this.exist=true

        this.success=false
      }
      this.spinner.hide()
    })
  }

  getParentNode() {
    this.spinner.show();
    this.Api.getAllParentNode().subscribe((res: any) => {
      this.parent = res;
      console.log(this.parent);
      
      this.isActive = new Array(this.parent.length).fill(true);
      this.checkedArray = new Array(this.parent.length).fill(true);
      this.createCheckedArray = new Array(this.parent.length).fill(false);
      this.editCheckedArray = new Array(this.parent.length).fill(false);
      this.viewCheckedArray = new Array(this.parent.length).fill(false);
      this.deleteCheckedArray = new Array(this.parent.length).fill(false);
      this.disabledCreateCheckedArrays = new Array(this.parent.length).fill(false);
      this.disabledDeleteCheckedArrays = new Array(this.parent.length).fill(false);
      this.disabledEditCheckedArrays = new Array(this.parent.length).fill(false);
      this.disabledViewCheckedArrays = new Array(this.parent.length).fill(false);
      this.spinner.hide();
      this.parent.forEach((element, key) => {
        if(element.parentNodeName == 'Dashboard' || element.parentNodeName == 'Reports' ||
        element.parentNodeName == 'Settings' || element.parentNodeName == 'Help & Support') {
          this.disabledCreateCheckedArrays[key] = true;
          this.disabledEditCheckedArrays[key] = true;
          this.disabledViewCheckedArrays[key] = false;
          this.disabledDeleteCheckedArrays[key] = true;
        } 
        
        else if(element.parentNodeName == 'Manage Teams'){
          this.disabledDeleteCheckedArrays[key] = true;

        }
        else {
          this.disabledCreateCheckedArrays[key] = false;
          this.disabledEditCheckedArrays[key] = false;
          this.disabledViewCheckedArrays[key] = false;
          this.disabledDeleteCheckedArrays[key] = false;
        }
      });
      console.log(this.disabledCreateCheckedArrays);
    },
    (error) => {}
    );
  }

  Open(data: any) {
    console.log(data, 'rashid');
  }
}
