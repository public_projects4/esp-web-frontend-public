import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { CreateNewRoleComponent } from './create-new-role.component';
import { Route, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';

const routes: Route[] = [
  {
    path: '',
    component: CreateNewRoleComponent
  }
];

@NgModule({
  declarations: [
    CreateNewRoleComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    
    RouterModule.forChild(routes),
  ]
})
export class CreateNewRoleModule { }
