import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { Route, RouterModule } from '@angular/router';
import { ManageAthleteDeviceComponent } from './manage-athlete-device.component';

const routes: Route[] = [
  {
    path: '',
    component: ManageAthleteDeviceComponent
  }
];

@NgModule({
  declarations: [
    ManageAthleteDeviceComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    RouterModule.forChild(routes)
  ]
})
export class ManageAthleteDeviceModule { }
