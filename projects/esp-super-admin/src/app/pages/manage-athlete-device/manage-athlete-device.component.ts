import { Component, OnInit } from '@angular/core';
import { ManageAthleteDeviceData } from 'projects/esp-shared/src/lib/models/Super-Admin-Models/manage-athlete-device-data';

@Component({
  selector: 'app-manage-athlete-device',
  templateUrl: './manage-athlete-device.component.html',
  styleUrls: ['./manage-athlete-device.component.css']
})
export class ManageAthleteDeviceComponent implements OnInit {

  selected_title:string = "Athlete General Devices";
  block1 = true;
  block2 = true;
  block3 = true;

  manageathletedevicedata : ManageAthleteDeviceData [] = [{
    label:'Dynosense',
    widgetImage:'./assets/img/dynosense.png',
    title:'Dyno 100',
    descrip_text:'Disclaimer: This health monitoring system is approved by FDA, CFDA, HSA and CE for vital signs only as well as registered in accordance with the Medical Device Regulations, Section 36 of Health Canada (License number: 97962).',
    list1:'Lorem ipsum dolor sit amet, consectetur',
    list2:'Adipiscing elit, sed do eiusmod tempor incididunt',
    list3:'Quis nostrud exercitation',
    list4:'Consectetur adipiscing',
    themeId:'1'
  },
  {
    label:'Dynosense',
    widgetImage:'./assets/img/dynosense.png',
    title:'Dyno 200',
    descrip_text:'Disclaimer: This health monitoring system is approved by FDA, CFDA, HSA and CE for vital signs only as well as registered in accordance with the Medical Device Regulations, Section 36 of Health Canada (License number: 97962).',
    list1:'Lorem ipsum dolor sit amet, consectetur',
    list2:'Adipiscing elit, sed do eiusmod tempor incididunt',
    list3:'Quis nostrud exercitation',
    list4:'Consectetur adipiscing',
    themeId:'2'
  },
  {
    label:'Dynosense',
    widgetImage:'./assets/img/dynosense.png',
    title:'Dyno 300',
    descrip_text:'Disclaimer: This health monitoring system is approved by FDA, CFDA, HSA and CE for vital signs only as well as registered in accordance with the Medical Device Regulations, Section 36 of Health Canada (License number: 97962).',
    list1:'Lorem ipsum dolor sit amet, consectetur',
    list2:'Adipiscing elit, sed do eiusmod tempor incididunt',
    list3:'Quis nostrud exercitation',
    list4:'Consectetur adipiscing',
    themeId:'3'
  },
  {
    label:'Dynosense',
    widgetImage:'./assets/img/dynosense.png',
    title:'Dyno 100',
    descrip_text:'Disclaimer: This health monitoring system is approved by FDA, CFDA, HSA and CE for vital signs only as well as registered in accordance with the Medical Device Regulations, Section 36 of Health Canada (License number: 97962).',
    list1:'Lorem ipsum dolor sit amet, consectetur',
    list2:'Adipiscing elit, sed do eiusmod tempor incididunt',
    list3:'Quis nostrud exercitation',
    list4:'Consectetur adipiscing',
    themeId:'4'
  },
  {
    label:'Dynosense',
    widgetImage:'./assets/img/dynosense.png',
    title:'Dyno 100',
    descrip_text:'Disclaimer: This health monitoring system is approved by FDA, CFDA, HSA and CE for vital signs only as well as registered in accordance with the Medical Device Regulations, Section 36 of Health Canada (License number: 97962).',
    list1:'Lorem ipsum dolor sit amet, consectetur',
    list2:'Adipiscing elit, sed do eiusmod tempor incididunt',
    list3:'Quis nostrud exercitation',
    list4:'Consectetur adipiscing',
    themeId:'5'
  },
];


  onChange(data:any){
    // console.log("ameen",data.target.value)
    if(data.target.value == "org1"){
      this.block1 = true;
      this.block2 = false;
      this.block3 = false;
      this.selected_title = "Athlete Organisation-1 Devices";
    }
    else if(data.target.value == "org2"){
      this.block1 = false;
      this.block2 = true;
      this.block3 = false;
      this.selected_title = "Athlete Organisation-2 Devices";
    }
    else if(data.target.value == "org3"){
      this.block1 = false;
      this.block2 = false;
      this.block3 = true;
      this.selected_title = "Athlete Organisation-3 Devices";
    }
    else if(data.target.value == "default"){
      this.block1 = true;
      this.block2 = true;
      this.block3 = true;
      this.selected_title = "Athlete General Devices";
    }

  }

  constructor() { }

  ngOnInit(): void {
  }

}
