import { Component, Inject, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { PostcodeService } from 'projects/esp-shared/src/lib/services/postcode.service';
import { __values } from 'tslib';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { DOCUMENT } from '@angular/common';
import jsPDF from 'jspdf';
import { NgxSpinnerService } from 'ngx-spinner';
import countries from 'projects/esp-shared/src/lib/services/callingCodes.json';
export interface data {
  columnName: string;
  value: string;
}
declare var $: any;
@Component({
  selector: 'app-list-of-oraganisations',
  templateUrl: './list-of-oraganisations.component.html',
  styleUrls: ['./list-of-oraganisations.component.css'],
})
export class ListOfOraganisationsComponent implements OnInit {
  Pdf: any;
  tests:any = []
  alert1=false
  excel: any;
  alert = false;
  zipcode = '';
  state = '';
  city = '';
  countryCode: any;
  text: any;
  pdf: any;
  searchText: string = '';
  inputChipBox1: boolean = false;
  inputChipBox2: boolean = false;
  removemark: any;
  test: any;
  others: any;
  success = true;
  wrong = false;
  myReactiveForms: FormGroup = new FormGroup({});
  icon = true;
  Edit = false;
  View = false;
  Table = true;
  router: any;
  constructor(
    private postservice: PostcodeService,
    private Api: AllApiService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    @Inject(DOCUMENT) private _document: Document
  ) {
    this.myReactiveForms = this.formBuilder.group({
      zipcode: ['', [Validators.required]],
      state: ['', [Validators.required]],
      city: ['', [Validators.required]],
      countryCode: ['91', [Validators.required]],
      organisationName: ['', [Validators.required]],
      subDomain: ['', [Validators.required]],
      emailAddress: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      addressline1: ['', [Validators.required]],
      addressline2: ['', [Validators.required]],
      ADD: ['', [Validators.required]],
      Others: [''],
      sportsCategory: this.formBuilder.array([]),
      createdBy:'',
updatedBy:''
    });
  }
  ngOnInit(): void {
    this.getDetails();
  }
  get sportsCategory(): FormArray {
    return this.myReactiveForms.get('sportsCategory') as FormArray;
  }
  formats: data[] = [
    {
      columnName: 'EXCEL',
      value: 'EXCEL',
    },
    {
      columnName: 'PDF',
      value: 'PDF',
    },
  ];
  oraganisationslist2: any[] = [];
  public edit: any = {
    organisationName: '',
    subDomain: '',
    emailAddress: '',
    addressline1: '',
    addressline2: '',
    city: '',
    state: '',
    countryCode: '',
    zipCode: '',
    sportsCategory: '',
    id: '',
    createdBy: '',
    createdDate: '',
    expiryDate: '',
    isDeleted: '',
    onBoardedDate: '',
    status: '',
    updatedBy: '',
    updatedDate: '',
  };
  getDetails() {
    this.spinner.show()
    this.Api.getAllOrganization().subscribe((res: any) => {

      this.oraganisationslist2 = res;
      this.tests=res
      this.spinner.hide()
    });
  }
  get f() {
    return this.myReactiveForms.controls;
  }
  inputs: any[] = [];
  openEditPage(data: any) {
    this.icon = true;
    this.removable = false;
    this.inputs = data.sportsCategory;

    this.edit = data;
    this.inputs.forEach((element) => {
      this.fruits.push({ sportsCategory: element });
    });
    this.inputChipBox1 = true;
    this.inputChipBox2 = false;
    this.myReactiveForms = this.formBuilder.group({
      zipcode: this.edit.zipCode,
      state: this.edit.state,
      city: this.edit.city,
      countryCode: this.edit.countryCode,
      organisationName: this.edit.organisationName,
      subDomain: this.edit.subDomain,
      emailAddress: this.edit.emailAddress,
      addressline1: this.edit.addressline1,
      addressline2: this.edit.addressline2,
      id: this.edit.id,
      sportsCategory: this.formBuilder.array([]),
      createdBy: this.edit.createdBy,
      createdDate: this.edit.createdDate,
      expiryDate: this.edit.expiryDate,
      isDeleted: this.edit.isDeleted,
      onBoardedDate: this.edit.onBoardedDate,
      status: this.edit.status,
      updatedDate: this.edit.updatedDate,
    });
    this.city= this.edit.city;
    this.state= this.edit.state;
    this.zipcode = this.edit.zipCode
    this.Edit = true;
    this.View = false;
    this.Table = false;
  }
  Type: any;
  opens(data: any) {
    this.Type = data.target.value;

  }
  openViewPage(data: any) {
    this.inputs = data.sportsCategory;

    this.edit = data;
    this.inputs.forEach((element) => {
      this.fruits.push({ sportsCategory: element });
    });
    this.inputChipBox1 = true;
    this.inputChipBox2 = false;
    this.myReactiveForms = this.formBuilder.group({
      zipcode: this.edit.zipCode,
      state: this.edit.state,
      city: this.edit.city,
      countryCode: this.edit.countryCode,
      organisationName: this.edit.organisationName,
      subDomain: this.edit.subDomain,
      emailAddress: this.edit.emailAddress,
      addressline1: this.edit.addressline1,
      addressline2: this.edit.addressline2,
      id: this.edit.id,
      sportsCategory: this.formBuilder.array([]),
    });
    for (let i = 0; i < this.edit.sportsCategory.length; i++) {
      this.sportsCategory.push(
        this.formBuilder.group({
          sportsCategory: this.edit.sportsCategory[i],
        })
      );
    }
    this.Edit = false;
    this.View = true;
    this.Table = false;
  }
  closeView() {
    this.fruits = [];
    this.Edit = false;
    this.View = false;
    this.Table = true;
    this.getDetails();
  }
  updateItems(data: any) {
    const lunch = data;

    let sportsCategory: any = [];
    lunch.sportsCategory.map((temp: any) => {
      sportsCategory.push(temp.sportsCategory);
    });
    lunch['sportsCategory'] = sportsCategory;
    data['updatedBy']=localStorage.getItem('superAdminName');
    this.Api.updateOrganization(data).subscribe((data: any) => {

      this.Edit = false;
      this.View = false;
      this.Table = true;
      this.getDetails();
    });
  }
  listid: any;
  removePage(data: any) {
    this.listid = data;

  }
  deletePage() {
    this.Api.deleteOrganization(this.listid).subscribe((data: any) => {
      this.getDetails();
      if (data) {
        $('#result').modal('show');
      }
    });
  }
  type: any;
  Id: any;
  download(data: any) {
    this.Id = data;
  }
  Name = 'ORGANISATION';
  openPage() {
    this.getDetails();
  }
  check(data: any) {
    this.pdf = data;
  }
  onSubmit() {
    const data = this.myReactiveForms.value;
    this.fruits.forEach((element) => {
      data.sportsCategory.push(element);

    });

    let sportsCategory: any = [];
    this.myReactiveForms.value.sportsCategory.map((temp: any) => {
      sportsCategory.push(temp.sportsCategory);
    });
    data['sportsCategory'] = sportsCategory;
   localStorage.setItem('Name',data.organisationName)

this.spinner.show()
    this.Api.updateOrganization(data).subscribe((data: any) => {
      if (data.message == 'Organisation Updated Successfully') {
        $('#Updated').modal('show');
        this.success=true
      }

      this.refresh();
      this.fruits = [];
      this.spinner.hide()
    });
  }
  refresh() {
    this.Edit = false;
    this.View = false;
    this.Table = true;
    this.getDetails();
  }
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  fruits: any[] = [
  ];
  pushData() {
    this.add;
  }
  add(event: MatChipInputEvent): void {
    const value = (event.value.replace(/\b\w/g, (l: any) => l.toUpperCase()) || '').trim();
    if (value) {
      if (!this.fruits.find((o) => o.sportsCategory === value)) {

        this.alert = false;
        this.fruits.push({ sportsCategory: value, showRemove: true });
      } else {

        this.alert = true;
      }
    }
    event.chipInput!.clear();
  }
  remove(fruit: any): void {
    const index = this.fruits.indexOf(fruit);
    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }
  inputBox(data: any) {
    this.icon = false;

    const dataValue = data.target.value;
    this.inputChipBox1 = true;
    if (dataValue == 'others') {
      this.inputChipBox2 = true;
      this.test = '';
      this.alert1=false
      this.alert=false
    }
    else {
      this.inputChipBox1 = true;
      this.inputChipBox2 = false;
      this.alert = false;
      this.alert1=false
      if (dataValue) {
        if (!this.fruits.find((o) => o.sportsCategory === dataValue))
        {
          this.fruits.push({ sportsCategory: dataValue,showRemove:true });

          this.alert1=false
        }
        else{
          this.alert1=true;
          this.success = false;

        }
      }
    }
  }
  keyPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z  ]/.test(inp)) {
      let name1: string = this.myReactiveForms.value.organisationName;
      name1 && name1.length > 0
        ? this.myReactiveForms.patchValue({
            organisationName: name1[0].toUpperCase() + name1.substring(1),
          })
        : '';
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9  ]/.test(inp)) {
      let name1: string = this.myReactiveForms.value.organisationName;
      name1 && name1.length > 0
        ? this.myReactiveForms.patchValue({
            organisationName: name1[0].toUpperCase() + name1.substring(1),
          })
        : '';
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  EXCEL() {
    this.excel = 'EXCEL';

    this.Api.downloadOrganization(this.Id, this.excel, this.Name).subscribe(
      (data: any) => {

        if (data) {
          window.location.href = data;
        }
      }
    );
  }
  PDF() {

    const doc = new jsPDF();
    doc.text(`\n OrganisationName : ${this.pdf.organisationName}`, 5, 17);
    doc.text(`\n SubDomain : ${this.pdf.subDomain}`, 5, 27);
    doc.text(`\n EmailAddress : ${this.pdf.emailAddress}`, 5, 37);
    doc.text(`\n Addressline1 : ${this.pdf.addressline1}`, 5, 47);
    doc.text(`\n Addressline2 : ${this.pdf.addressline2}`, 5, 57);
    doc.text(`\n ZipCode : ${this.pdf.zipCode}`, 5, 67);
    doc.text(`\n City : ${this.pdf.city}`, 5, 77);
    doc.text(`\n State : ${this.pdf.state}`, 5, 87);
    doc.text(`\n CountryCode : ${this.pdf.countryCode}`, 5, 97);
    doc.text(`\n SportsCategory : ${this.pdf.sportsCategory}`, 5, 107);
    doc.save('Evvosports.pdf');
  }
  keysPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z  ]/.test(inp)) {
      let name3: string = this.myReactiveForms.value.ADD;
      name3 && name3.length > 0
        ? this.myReactiveForms.patchValue({
            ADD: name3[0].toUpperCase() + name3.substring(1),
          })
        : '';
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  input() {
    let name2: string = this.myReactiveForms.value.Sport;
    name2 && name2.length > 0
      ? this.myReactiveForms.patchValue({
          Sport: name2[0].toUpperCase() + name2.substring(1),
        })
      : '';
  }
  keysPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z  ]/.test(inp)) {
      let name2: string = this.myReactiveForms.value.Others;
      name2 && name2.length > 0
        ? this.myReactiveForms.patchValue({
            Others: name2[0].toUpperCase() + name2.substring(1),
          })
        : '';
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  exportToExcel(){
    this.Api.exportToExcel().subscribe((data:any)=>{

      if(data){
window.location.href=data
      }
    })
  }
  ele:any
  exportToPdf(){
    this.tests.forEach((element:any)=>{
      const doc = new jsPDF();
      doc.text(`\n OrganisationName : ${element.organisationName}`, 5, 17);
      doc.text(`\n SubDomain : ${element.subDomain}`, 5, 27);
      doc.text(`\n EmailAddress : ${element.emailAddress}`, 5, 37);
      doc.text(`\n Addressline1 : ${element.addressline1}`, 5, 47);
      doc.text(`\n Addressline2 : ${element.addressline2}`, 5, 57);
      doc.text(`\n ZipCode : ${element.zipCode}`, 5, 67);
      doc.text(`\n City : ${element.city}`, 5, 77);
      doc.text(`\n State : ${element.state}`, 5, 87);
      doc.text(`\n CountryCode : ${element.countryCode}`, 5, 97);
      doc.text(`\n SportsCategory : ${element.sportsCategory}`, 5, 107);
      doc.save('Evvosports.pdf');
    })
  }
  zipcodee = () => {
    this.zipcode.length > 4 ? this.getZip() : '';
    if (this.zipcode.length > 5) {
      this.countryCode = "91";
    }
  };
  getZipcode() {
    this.postservice.getPost(this.zipcode).subscribe((res: any) => {
      this.state = res[0].PostOffice[0].State;
      this.city = res[0].PostOffice[0].District;


    });
  }

  counrtyList: { dial_code: string; code: string; name: string }[] = countries;

  getZip(){
    console.log(this.zipcode);

    this.Api.getZipCodeBase(this.zipcode).subscribe((data:any) => {
      console.log(data);

      console.log(data.results[this.zipcode][0]);

      let arr1 =data.results[this.zipcode][0]

      console.log(arr1.country_code);
      console.log(this.counrtyList);

      this.counrtyList.forEach((element:any) => {

        if(arr1.country_code == element.code){
           console.log(element.dial_code);
           this.state = arr1.state;
      this.city = arr1.city;
          this.countryCode = element.dial_code
         }

      } )




    })
  }
}
