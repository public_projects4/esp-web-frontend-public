import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { NewOraganisationsComponent } from './new-oraganisations.component';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { PostcodeService } from 'projects/esp-shared/src/lib/services/postcode.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatChipsModule} from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import { NgxSpinnerModule } from 'ngx-spinner';
const routes: Route[] = [
  {
    path: '',
    component: NewOraganisationsComponent
  }
];
@NgModule({
  declarations: [
    NewOraganisationsComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    EspSharedModule,
    MatChipsModule,
    FormsModule,
    NgxSpinnerModule,
    RouterModule.forChild(routes)
  ],
  providers: [PostcodeService]
})
export class NewOraganisationsModule { }
