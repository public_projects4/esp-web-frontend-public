import { Component, OnInit } from '@angular/core';
import { Team } from 'projects/esp-shared/src/lib/models';
import { PostcodeService } from 'projects/esp-shared/src/lib/services/postcode.service';
import { Observable } from 'rxjs';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import countries from 'projects/esp-shared/src/lib/services/callingCodes.json';
declare var $: any;
@Component({
  selector: 'app-new-oraganisations',
  templateUrl: './new-oraganisations.component.html',
  styleUrls: ['./new-oraganisations.component.css'],
})
export class NewOraganisationsComponent implements OnInit {
  fruits=[];
  box:any
  alert = false;
  Box=false
  test: any;
  teams$?: Observable<Team[]>;
  countryCode: any;
  zipcode = '';
  state = '';
  city = '';
  success = true;
  exist = false;
  returnUrl = '/esp-super-admin/list-of-organisations';
  myDate = this.datepipe.transform(new Date(), 'yyyy-MM-ddT00:00:46.217Z');
  myReactiveForms: FormGroup = new FormGroup({});
  inputChipBox1: boolean = false;
  inputChipBox2: boolean = false;
  constructor(
    private postservice: PostcodeService,
    private Api: AllApiService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private datepipe: DatePipe
  ) {
    this.myReactiveForms = this.formBuilder.group({
      zipcode: ['', [Validators.required]],
      state: ['', [Validators.required]],
      city: ['', [Validators.required]],
      countryCode: ['91', [Validators.required]],
      organisationName: ['', [Validators.required]],
      subDomain: ['', [Validators.required]],
      emailAddress: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      addressline1: ['', [Validators.required]],
      addressline2: ['', [Validators.required]],
      Others: [''],
      createdDate: this.myDate,
      sportsCategory: this.formBuilder.array([]),
      createdBy:'',
      justForValidation :  ['', [Validators.required]],
    });
  }
  ngOnInit(): void {
    this.alert=false
  }
  get sportsCategory(): FormArray {
    return this.myReactiveForms.get('sportsCategory') as FormArray;
  }
  newSkill(): FormGroup {
    return this.formBuilder.group({
      sportsCategory: ''
    });
  }
  addSkills() {
    this.sportsCategory.push(this.newSkill());
  }
  removeSkill(i: number) {
    this.sportsCategory.removeAt(i);
  }
  get f() {
    return this.myReactiveForms.controls;
  }
  onSubmit() {
    const data = this.myReactiveForms.value;

    this.fruits.forEach((element) => {
      data.sportsCategory.push(element);

    });
    let sportsCategory: any = [];
    this.myReactiveForms.value.sportsCategory.map((temp: any) => {
      sportsCategory.push(temp.sportsCategory);
    });
    data['sportsCategory'] = sportsCategory;
    data['createdBy'] = localStorage.getItem('Name');
this.spinner.show()
    this.Api.addorganization(data).subscribe((data: any) => {

      if (data.message == 'Organisation Added Successfully') {
        $('#result').modal('show');
        this.success = true;
        this.exist = false;
        this.alert = false;
      } else if (data.message == 'Organisation Exsits') {
        $('#result').modal('show');
        this.exist = true;
        this.success = false;
        this.alert = false;
      }
      this.myReactiveForms.reset();
      this.fruits = [];
      this.spinner.hide()
    });
  }


  getZipcode() {
    this.postservice.getPost(this.zipcode).subscribe((res: any) => {
      this.state = res[0].PostOffice[0].State;
      this.city = res[0].PostOffice[0].District;
    });
  }

  counrtyList: { dial_code: string; code: string; name: string }[] = countries;

  getZip(){
    console.log(this.zipcode);

    this.Api.getZipCodeBase(this.zipcode).subscribe((data:any) => {
      console.log(data);

      console.log(data.results[this.zipcode][0]);

      let arr1 =data.results[this.zipcode][0]

      console.log(arr1.country_code);
      console.log(this.counrtyList);

      this.counrtyList.forEach((element:any) => {

        if(arr1.country_code == element.code){
           console.log(element.dial_code);
           this.state = arr1.state;
      this.city = arr1.city;
          this.countryCode = element.dial_code
         }

      } )




    })
  }
  zipcodee = () => {
    this.zipcode.length > 4 ? this.getZip() : '';
    console.log(this.zipcode);

    // if (this.zipcode.length > 5) {
    //   this.countryCode = "91";
    // }
  };
  path = window.location.host.slice(0, 4);
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  pushData() {
    this.add;
    this.test = '';
  }
  add(event: MatChipInputEvent): void {
    this.test = '';
    this.Box=false
    const value = (event.value.replace(/\b\w/g, (l: any) => l.toUpperCase()) || '').trim();
    if (value) {
      if (!this.fruits.find((o) => o.sportsCategory === value))
      {
        this.fruits.push({ sportsCategory: value });

        this.alert=false
      }
      else{
        this.alert=true;
        this.exist = false;
        this.success = false;

      }
    }
    event.chipInput!.clear();
  }
  remove(fruit: any): void {
    const index = this.fruits.indexOf(fruit);
    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
    if(this.fruits.length==0){
this.Box=true
this.inputChipBox2=false
this.alert1=false
this.myReactiveForms.patchValue({
  justForValidation:''
})
    }
  }
  arr: any[] = [];
  alert1=false
  inputBox(data: any) {
    this.Box=false

    this.alert = false;
    const dataValue = data.target.value;
    this.inputChipBox1 = true;
    if (dataValue == 'others') {
      this.inputChipBox2 = true;
      this.test = '';
      this.alert = false;
      this.alert1=false
    } else {
      this.inputChipBox1 = true;
      this.inputChipBox2 = false;
      this.alert = false;
      this.alert1=false
      if (dataValue) {
        if (!this.fruits.find((o) => o.sportsCategory === dataValue))
        {
          this.fruits.push({ sportsCategory: dataValue });

          this.alert1=false
        }
        else{
          this.alert1=true;
          this.exist = false;
          this.success = false;

        }
      }
    }
  }
  keyPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z @",'-_#$%&*  ]/.test(inp)) {
      let name1: string = this.myReactiveForms.value.subDomain;
      name1 && name1.length > 0
        ? this.myReactiveForms.patchValue({
            subDomain: name1[0].toUpperCase() + name1.substring(1),
          })
        : '';
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9 a-zA-Z @",'-_#$%&*   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keysPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9 a-zA-Z  ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  // mat chips code ends here
  keysPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z  ]/.test(inp)) {
      let name2: string = this.myReactiveForms.value.Others;
      name2 && name2.length > 0
        ? this.myReactiveForms.patchValue({
            Others: name2[0].toUpperCase() + name2.substring(1),
          })
        : '';
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
}
