import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // {
  //   path: 'esp-super-admin/login',
  //   loadChildren: () => import('./pages/login/login.module').then(o => o.LoginModule)
  // },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(o => o.HomeModule)
  },
  // {
  //   path: '404',
  //   loadChildren: () => import('./pages/404/404.module').then(o => o.NotFoundModule)
  // },
  {
    path: '',
    redirectTo: 'login-super-admin',
    pathMatch: 'full'
  },
  // {
  //   path: 'esp-super-admin',
  //   loadChildren: () => import('../../../esp-super-admin/src/app/pages/home/home.module').then(o => o.HomeModule)
  // },
  {
    path: '**',
    redirectTo: 'esp-super-admin'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
