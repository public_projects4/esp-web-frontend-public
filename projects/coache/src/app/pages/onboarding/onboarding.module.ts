import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';
import { OnboardingComponent } from './onboarding.component';
import { BrowserModule } from '@angular/platform-browser';

const routes: Route[] = [
  {
    path: '',
    component: OnboardingComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class OnboardingModule { }
