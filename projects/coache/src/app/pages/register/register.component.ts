import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
 import { FormGroup,FormBuilder, FormControl,Validators} from '@angular/forms';
 import { ConfirmedValidator } from './confirmed.validator';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import * as CryptoJS from 'crypto-js';  

declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  conversionEncryptOutput:any
  conversionDecryptOutput:any
    otp:any='';
    pass:any='';
    returnUrl='/login';
  registerForm: FormGroup = new FormGroup({});
  submitted:boolean = false;
   decryptedUrl:any
  constructor(private formBuilder: FormBuilder, private Api:AllApiService,  private routes:Router) { }
  
  ngOnInit() {
      this.registerForm = this.formBuilder.group({
          password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}'),Validators.minLength(8)]],
          confirmPassword: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}'),Validators.minLength(8)]],
          code: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6),Validators.pattern('^[0-9]*$')]],
          // emailAddress: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      },
      {
        validator: ConfirmedValidator('password', 'confirmPassword')
      }
      );


  
      const url =window.location.href.split('=')[1]
      //   console.log('first',url);        
      // var rawString = url;
      // var b64decoded = btoa(rawString);
      // console.log('second',b64decoded);
         this.decryptedUrl =atob(url)
        
     this.conversionDecryptOutput = CryptoJS.AES.decrypt(url.trim(), url.trim()).toString(CryptoJS.enc.Utf8);  
   console.log('decrypt',this.conversionDecryptOutput);
   

  }
 


  
  
  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }
  
  onSubmit() {
    const data = this.registerForm.value
    console.log('inputData',data);
    
    this.Api.registerApi(data.code, this.decryptedUrl, data.confirmPassword)
    // this.Api.registerApi('147852', "syedameenabrar786@gmail.com", "Evvo@12345")
 .subscribe((res:any)=>{
   console.log(res);
   if(res.message=="Password Created Successfully"){
         $('#result').modal('show');

   }
   
 })
      
    
  }

    // (err) => {
      //   if (err.error.text === 'Password Created Successfully') {
      //     $('#result').modal('show');
      //   }
      //   else if (err.error.text == 'Object reference not set to an instance of an object.') {
      //     $('#modelWarning').modal('show');
      //   }
      //   else {
      //     $('#modelError').modal('show');
      //   }  
      //   })

  show_button: Boolean = false
  show_button1: Boolean = false

  show_eye: Boolean = false

  showPassword() {
    this.show_button = !this.show_button
    this.show_eye = !this.show_eye
  }

  showPasswords() {
    this.show_button1 = !this.show_button1
    this.show_eye = !this.show_eye
  }

  keysPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[0-9   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
}
