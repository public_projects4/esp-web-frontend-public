import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DataInjuryDetailsComponent } from './data-injury-details.component';

const routes: Route[] = [
  {
    path: '',
    component: DataInjuryDetailsComponent
  }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
    NgxSpinnerModule,



    RouterModule.forChild(routes)
  ]
})
export class DataInjuryDetailsModule { }
