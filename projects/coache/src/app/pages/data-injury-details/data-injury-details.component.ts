import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import {
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
} from '@angular/forms';
import Swal from 'sweetalert2';
import { Subject } from 'rxjs';
import jsPDF from 'jspdf';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';

import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';


export interface data {
  columnName: string;

  value: string;
}

declare var $: any;
@Component({
  selector: 'app-data-injury-details',
  templateUrl: './data-injury-details.component.html',
  styleUrls: ['./data-injury-details.component.scss']
})
export class DataInjuryDetailsComponent implements OnInit {

  modelDetails :ModalCommanService []=[
    {
    modelName:'Injury details'
  }
  ]

  @Input() atheleteId:any;
  myInjuryForm: FormGroup = new FormGroup({});

  practitioner:any[]=[]
  id:any
  injury = true;
  viewInjurySection = false;
  editInjurySection = false;
  injuryData:any;
  recData:any;
  injuryDeleteId:any;


  constructor(private Api: AllApiService, private formBuilder: FormBuilder,private spinner: NgxSpinnerService ,public datepipe: DatePipe) {
    this.myInjuryForm = this.formBuilder.group({
      injuryAssessments: this.formBuilder.array([]),
      injuryType: ['', [Validators.required]],
      dateOfInjury: ['', [Validators.required]],
      timeOfInjury: ['', [Validators.required]],
      event: ['', [Validators.required]],
      diagnosticCode: ['', [Validators.required]],
      injuryOnset: ['', [Validators.required]],
      referredTo: ['', [Validators.required]],
      incidentType: ['', [Validators.required]],
      level1Injury: ['', [Validators.required]],
      level2Injury: ['', [Validators.required]],
      swelling: ['', [Validators.required]],
      stability: ['', [Validators.required]],
      strength: ['', [Validators.required]],
      medicalCondition: ['', [Validators.required]],
      injuryDetails: ['', [Validators.required]],


      continueToPlay: ['', [Validators.required]],
      treatmentStatus: ['', [Validators.required]],
      expectedDateOfReturn: ['', [Validators.required]],
      createdBy:'',

updatedBy:''
    });
   }

  ngOnInit(): void {
    this.id = localStorage.getItem('OrgID');
    this.getPractitioner()
    this.addSkillsInjury()
  }

  ngOnChanges(changes:SimpleChanges) :void {
    this.getInjuryByAthleteId();
  }

  newSkillsInjury(): FormGroup {
    return this.formBuilder.group({
      danger: false,
      commentToDanger: '',
      response: false,
      commentToResponse: '',
      airway: false,
      commentToAirway: '',
      breathing: false,
      commentToBreathing: '',
      circulation: false,
      commentToCirculation: '',
    });
  }

  get abcInjury(): FormArray {
    return this.myInjuryForm.get('injuryAssessments') as FormArray;
  }

  addSkillsInjury() {
    this.abcInjury.push(this.newSkillsInjury());
  }

  getPractitioner() {
    this.Api.getPractitionerByOrganisationId(this.id)
    .subscribe((data:any)=>{
      console.log('final response => (getPractitioner)',data)
      this.practitioner=data
    })

  }

  getInjuryByAthleteId() {
    this.Api.getInjuryById(this.atheleteId).subscribe((data) => {
      this.injuryData = data;
      console.log('this.injuryData',this.injuryData);

    });
  }

  cancel() {
    this.injury = true;
    this.viewInjurySection = false;
    this.editInjurySection = false;
  }

  view(data: any) {
    console.log(data);

    this.viewInjurySection = true;
    this.injury = false;
    this.editInjurySection = false;

    // this.edit = data;



data.injuryAssessments.forEach((element:any) => {
    console.log('dataReturn',element );
    this.recData = element

});



  const lessonForm = this.formBuilder.group({
    airway: this.recData.airway,
  breathing: this.recData.breathing,
  circulation: this.recData.circulation,
  commentToAirway: this.recData.commentToAirway,
  commentToBreathing: this.recData.commentToBreathing,
  commentToCirculation: this.recData.commentToCirculation,
  commentToDanger: this.recData.commentToDanger,
  commentToResponse: this.recData.commentToResponse,
  danger: this.recData.danger,
  response: this.recData.response
  });





this.myInjuryForm = this.formBuilder.group({

  injuryAssessments: this.formBuilder.array([lessonForm]),
  injuryType: data.injuryType,
  dateOfInjury:  this.datepipe.transform(data.dateOfInjury,'dd/MM/yyyy'),
  timeOfInjury: this.datepipe.transform(data.timeOfInjury,'h:mm'),
  event: data.event,
  diagnosticCode: data.diagnosticCode,
  injuryOnset: data.injuryOnset,
  referredTo: data.referredTo,
  incidentType: data.incidentType,
  level1Injury: data.level1Injury,
  level2Injury: data.level2Injury,
  swelling: data.swelling,
  stability: data.stability,
  strength: data.strength,
  medicalCondition: data.medicalCondition,
  injuryDetails: data.injuryDetails,


  continueToPlay: data.continueToPlay,
  treatmentStatus: data.treatmentStatus,
  expectedDateOfReturn: this.datepipe.transform(data.expectedDateOfReturn,'dd/MM/yyyy'),
});

console.log(this.myInjuryForm);

  }



  editInjury(data:any){

    this.viewInjurySection = false;
    this.injury = false;
    this.editInjurySection = true;



    data.injuryAssessments.forEach((element:any) => {
      console.log('dataReturn',element );
      this.recData = element
  });


    const lessonForm = this.formBuilder.group({
      airway: this.recData.airway,
    breathing: this.recData.breathing,
    circulation: this.recData.circulation,
    commentToAirway: this.recData.commentToAirway,
    commentToBreathing: this.recData.commentToBreathing,
    commentToCirculation: this.recData.commentToCirculation,
    commentToDanger: this.recData.commentToDanger,
    commentToResponse: this.recData.commentToResponse,
    danger: this.recData.danger,
    response: this.recData.response
    });

    // var daattee = this.datepipe.transform(data.dateOfInjury,'dd/MM/yyyy')

  this.myInjuryForm = this.formBuilder.group({
    injuryAssessments: this.formBuilder.array([lessonForm]),
    injuryType: data.injuryType,
    // dateOfInjury:  this.datepipe.parse(data.dateOfInjury),
    timeOfInjury: this.datepipe.transform(data.timeOfInjury,'hh:mm'),
    dateOfInjury:  this.datepipe.transform(data.dateOfInjury,'dd/MM/yyyy')?.split('/').reverse().join('-'),
    event: data.event,
    diagnosticCode: data.diagnosticCode,
    injuryOnset: data.injuryOnset,
    id : data.id,
    referredTo: data.referredTo,
    incidentType: data.incidentType,
    level1Injury: data.level1Injury,
    level2Injury: data.level2Injury,
    swelling: data.swelling,
    stability: data.stability,
    strength: data.strength,
    medicalCondition: data.medicalCondition,
    injuryDetails: data.injuryDetails,


    continueToPlay: data.continueToPlay,
    treatmentStatus: data.treatmentStatus,
    expectedDateOfReturn: this.datepipe.transform(data.expectedDateOfReturn,'dd/MM/yyyy')?.split('/').reverse().join('-'),
    createdBy:data.createdBy
  });

  console.log('this.myInjuryForm',this.myInjuryForm );


  }


  onInjuryUpdate(){


    let data = this.myInjuryForm.value;

    data['updatedBy']=localStorage.getItem('Name');
    data['athleteId'] = this.atheleteId,

    console.log(data);

    this.Api.updateInjury(data).subscribe(
      (data: any) => {
        console.log('final update response', data);
        if(data.message == "InjuryDetails Updated Successfully"){
          $("#updateModal").modal('show');
          this.getInjuryByAthleteId()
          this.cancel()
        }
        else{
          $("#modelError").modal('show');
        }
      },
      // (err) => {
      //   if (err.message === 'InjuryDetails Updated Successfully') {

      //     alert("updated successfully")
      //     $('#createInjurydata').modal('show');
      //   } else {
      //     alert("Un successfully")
      //     $('#modelError').modal('show');
      //   }
      // }
    );

  }

  deleteTreatInfo(id:any){

    this.injuryDeleteId = id

   }

  removeInjury(){
    this.Api.deleteInjury(this.injuryDeleteId).subscribe((res:any) => {
      if(res.message == "InjuryDetails Deleted Successfully"){
        $("#successfullyDeletedModal").modal('show');
        this. getInjuryByAthleteId()
      }

    })
  }



  keyPressAlphaNumeric(event: any) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z  ]/.test(inp)) {
      let name1: string = this.myInjuryForm.value.organisationName;
      name1 && name1.length > 0 ? this.myInjuryForm.patchValue({
        organisationName: name1[0].toUpperCase() + name1.substring(1)
      }) : '';
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }



}
