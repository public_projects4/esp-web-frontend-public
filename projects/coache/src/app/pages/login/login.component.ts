import { Component, HostListener, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, FormControl,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup = new FormGroup({});
  returnUrl='/coach/dashboard';


returnurl='/coach-onboarding'
incorrect=false;

    constructor(private formBuilder:FormBuilder,  private Api:AllApiService, private routes:Router) { }



    ngOnInit() {
    this.loginForm = this.formBuilder.group({

        userName: ['', [Validators.required]],
        password: ['', [Validators.required]],
        // password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]]

    });
}

// convenience getter for easy access to form fields
get f() { return this.loginForm.controls; }





  onSubmit(){
    const data = this.loginForm.value
  console.log(data);

  this.Api.getAdminLogin(data.userName, data.password)
  .subscribe((data:any)=>{
    console.log('final response login',data);
    console.log(data.message);

    if(data.message == 'Please Check your Email or Password'){
      this.incorrect=true
    }
    else if(data.message == 'User is Upto Date'){
      this.incorrect=false
      this.routes.navigate([this.returnurl])

    }

    localStorage.setItem('OrgID',data.userDetails.organizationId)
    localStorage.setItem('Names',data.userDetails.name)

    sessionStorage.setItem('roleId',data.userDetails.roleId)
    localStorage.setItem('OrganizationId',data.userDetails.organizationId)
    sessionStorage.setItem('coachId',data.userDetails.coachId)
    sessionStorage.setItem('createdBy',data.userDetails.createdBy)
    localStorage.setItem('coach-createdBy',data.userDetails.createdBy)
    localStorage.setItem('Email',data.userDetails.email)
    localStorage.setItem('coachMobileNo',data.userDetails.mobile)
    localStorage.setItem('coachRoleId',data.userDetails.roleId)
    localStorage.setItem('Name',data.userDetails.name)
    localStorage.setItem('coachId',data.userDetails.coachId)
    localStorage.setItem('RoleId',data.userDetails.roleId)
    localStorage.setItem('ID',data.userDetails.id)
    // sessionStorage.setItem('name',data.userDetails.name)
    // sessionStorage.setItem('coachRole',data.userDetails.createdBy)
    // sessionStorage.setItem('coachRole',data.userDetails.createdBy)







   if(data.userDetails.isProfileCompleted==true){
      this.routes.navigate([this.returnUrl])


          }
  else  if(data.userDetails.isProfileCompleted==false){
      this.routes.navigate([this.returnurl])

          }




  }, (err:any) => {
    console.log(err);

  })

}
// show_button: Boolean = false
//   show_eye: Boolean = false

//   showPassword() {
//     this.show_button = !this.show_button
//     this.show_eye = !this.show_eye
//   }


  fieldTextType: boolean;


  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }


}
