import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { AssessmentsComponent } from './assessments.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';



const routes: Route[] = [
  {
    path: '',
    component: AssessmentsComponent
  }
];


@NgModule({
  declarations: [
    AssessmentsComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    RouterModule.forChild(routes)
  ]
})
export class AssessmentsModule { }
