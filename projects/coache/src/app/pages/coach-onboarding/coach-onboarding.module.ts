import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';
import { CoachOnboardingComponent } from './coach-onboarding.component';

const routes: Route[] = [
  {
    path: '',
    component: CoachOnboardingComponent
  }
];

@NgModule({
  declarations: [CoachOnboardingComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class CoachOnboardingModule { }
