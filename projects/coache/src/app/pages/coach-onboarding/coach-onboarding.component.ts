import { DatePipe } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';

@Component({
  selector: 'app-coach-onboarding',
  templateUrl: './coach-onboarding.component.html',
  styleUrls: ['./coach-onboarding.component.scss'],
})
export class CoachOnboardingComponent implements OnInit {
  coachId: any;
  Image: any;
  url: any = 'assets/img/imgPlaceholder.svg';
  demo = 'rashid';
  coachForm: FormGroup;
  returnUrl = '/coach/dashboard';
  myDate = this.datepipe.transform(new Date(), 'yyyy-MM-ddT00:00:46.217Z');
  orgId: any;
  orgName: any;
  constructor(
    private formBuilder: FormBuilder,
    private datepipe: DatePipe,
    private api: AllApiService,
    private routes: Router
  ) {
    this.coachForm = this.formBuilder.group({
      coacheName: ['', [Validators.required]],
      email: [''],
      mobile: ['', [Validators.required]],
      address: ['', [Validators.required]],
      dob: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      image: '',
      biography:['', [Validators.required]],

      id: sessionStorage.getItem('coachId'),
      createdBy: sessionStorage.getItem('createdBy'),
      createdDate: this.myDate,
      updatedBy: '',
      updatedDate: this.myDate,
      roleId: sessionStorage.getItem('roleId'),

      organisationId: localStorage.getItem('OrganizationId'),
      sportsCategory: localStorage.getItem('sportsCategory'),

      // password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]]
    });
  }
  ngOnInit(): void {

    this.coachForm.patchValue({
      coacheName:localStorage.getItem('Names'),
      email:localStorage.getItem('Email'),
      mobile:localStorage.getItem('coachMobileNo')
    })
    this.coachId = sessionStorage.getItem('coachId');

    this.orgId = localStorage.getItem('OrganizationId');

    this.api.getOrgById(this.orgId).subscribe((data: any) => {
      console.log(data);
      if (this.orgId == data.id) {
        console.log('yesss');
        this.orgName = data.organisationName;
      }
      console.log(this.orgName);
    });
  }

  get f() {
    return this.coachForm.controls;
  }



  fileToUpload: any = null;
  onSelectFile(file: FileList) {
    this.fileToUpload = file.item(0);

    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.url = event.target.result;
    };

    reader.readAsDataURL(this.fileToUpload);

    this.api
      .uploadImage(this.orgName, this.coachId, this.fileToUpload)
      .subscribe((data: any) => {
if(data.message=="Select correct file format"){
  this.url='assets/img/imgPlaceholder.svg'
alert('please select valid file')

}

else{
  this.Image = data.message;
  console.log(this.Image);
 this.coachForm.patchValue({
   image:data.message
 })
}



      });

  }

  submitCoach() {
    let data = this.coachForm.value;
    data['updatedBy']=data.coacheName
    // this.coachForm.patchValue({
    //   updatedBy:data.coacheName
    // })
    console.log(data);

    this.api.addCoachOnboard(data).subscribe((data: any) => {
      console.log('final response', data);

      if (data) {
        // localStorage.setItem('Names',data.coacheName)
        this.routes.navigate([this.returnUrl]);
        this.coachForm.reset()
      }
    });
  }


  keyPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[0-9   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  keyPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z  ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphaNumericS(event: any) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[0-9/]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
}
