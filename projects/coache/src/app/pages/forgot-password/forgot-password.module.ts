import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { ForgotPasswordComponent } from './forgot-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Route[] = [
  {
    path: '',
    component: ForgotPasswordComponent
  }
];


@NgModule({
  declarations: [ForgotPasswordComponent],
  imports: [
    CommonModule,
     FormsModule,
     ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class ForgotPasswordModule { }
