import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
// import * as crypto from 'crypto-js';
// import { Injectable } from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })


declare var $: any;

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  // secretKey = "YourSecretKeyForEncryption&Descryption";


  // encrypt(value : string) : string{
  //   return CryptoJS.AES.encrypt(value, this.secretKey.trim()).toString();
  // }

  // decrypt(textToDecrypt : string){
  //   return CryptoJS.AES.decrypt(textToDecrypt, this.secretKey.trim()).toString(CryptoJS.enc.Utf8);
  // }

  registerForm: FormGroup = new FormGroup({});
  submitted: boolean = false;
  returnUrl = '/home/login';

  constructor(private formBuilder: FormBuilder, private Api: AllApiService, private routes: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],

    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    const data = this.registerForm.value

    this.Api
      .forgetPassword(data.email)
      .subscribe((data: any) => {
        console.log('data', data);


        if (data.message == "OTP sent successfully") {
          Swal.fire({
            title: "<h4 style='color:white'>" + 'Check your email inbox <br> We have send you information to change your password' + "</h4>",
            background: '#2D3035',
            icon: 'success',
            confirmButtonColor: '#13c9ca',
            cancelButtonColor: '#ff8084',
            confirmButtonText: 'OK',

          }).then(()=>{
            window.location.href = this.returnUrl;


          })
        }

        // else if (data.message == 'Login Successfull'){
        //   this.routes.navigate([this.returnUrl]);

        // }
        else if (data.message == "Failed to send the OTP Please Check your Email") {
          // alert('Failed to send the OTP Please Check your Email');
          Swal.fire({
            title: "<h4 style='color:white'>" + 'Failed to send the OTP Please Check your Email ' + "</h4>",
            background: '#2D3035',
            icon: 'question',
            confirmButtonColor: '#13c9ca',
            cancelButtonColor: '#ff8084',
            confirmButtonText: 'OK',
          })
        }
        else {
          // alert('Something went wrong');
          Swal.fire({
            title: "<h4 style='color:white'>" + 'Something went wrong ' + "</h4>",
            background: '#2D3035',
            icon: 'question',
            confirmButtonColor: '#13c9ca',
            cancelButtonColor: '#ff8084',
            confirmButtonText: 'OK',
          })
        }
      });

  }


  // message : Failed to send the OTP Please Check your Email




}
