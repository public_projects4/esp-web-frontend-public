import { Team } from 'projects/esp-shared/src/lib/models';
import { Observable, Subject } from 'rxjs';
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
} from '@angular/core';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarView,
} from 'angular-calendar';
import { data } from 'jquery';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { DatePipe } from '@angular/common';
import { addDays, addHours, endOfMonth, setHours, setMinutes, startOfDay, subDays,sub } from 'date-fns';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};
export interface data {
  columnName: string;

  value: string;
}
declare var $: any;

@Component({
  selector: 'app-trainings',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './trainings.component.html',
  styleUrls: ['./trainings.component.scss'],
})
export class TrainingsComponent implements OnInit {
  getTeamList = [];
  teamId: any;
  teamList: any = [];
  teams$?: Observable<Team[]>;
  form1 = false;
  form2 = false;
  form3 = false;
  form4 = false;
  isDelete = false;
  t: string = 'hi';
  training = true;
  Event = false;
  editTrainingRadio = false;
  editEventRadio = false;
  disableEventRadio:any;
  disableTrainingRadio :any;
  orgId: any;
  orgName: any;
  monthNames = ['January', 'February', 'March', 'April', 'May', 'June','July', 'August', 'September', 'October', 'November', 'December'];
  days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  currentMonthName: String = this.monthNames[new Date().getMonth()];
  currentYear = new Date().getFullYear();
  currentDate = this.datepipe.transform(new Date(),"yyyy-MM-dd");
  fromDate:any;
  toDate : any;
  coachId:any
  public Training = {
    title: '',
    date: '',
    endDate :'',
    endtime: '',
    starttime: '',
    location: '',
    trainingDescription: '',
    id: '',
  };
  public editTraining = {
    id: '',
    title: '',
    date: '',
    endDate :'',
    endtime: '',
    starttime: '',
    location: '',
    trainingDescription: '',
    teamId: '',
  };
  public Eventsec = {
    title: '',
    date: '',
    endDate :'',
    endtime: '',
    starttime: '',
    area: '',
    id: '',
  };
  public editEvent = {
    id: '',
    title: '',
    date: '',
    endDate :'',
    endtime: '',
    starttime: '',
    area: '',
    teamId: '',
  };
  start : Date;
  end:Date;
  ngOnInit(): void {
    this.orgId = localStorage.getItem('OrgID');
    this.orgName = localStorage.getItem('Name');
    this.coachId=sessionStorage.getItem('coachId')

    this.setFromAndToDate(new Date());
    this.getTeam();
    this.getAllTraining();
    this.getAllEvents();
    this.getAllTrainingsByDay();
    this.getAllEventsByDay();
    this.getAllTrainingsByWeekly();
    this.getAllEventsByWeekly();
  }
  setFromAndToDate(date){
    var fromDate = new Date();
    var toDate = new Date();
    var dayName = this.days[date.getDay()];
    var i=0;
    while(dayName !='Sunday'){
      i++;
     dayName = this.days[date.getDay()-i];
    }
    fromDate.setDate(date.getDate()-i);
    toDate.setDate(fromDate.getDate()+6);
    this.fromDate = this.datepipe.transform(fromDate,"yyyy-MM-dd");
    this.toDate = this.datepipe.transform(toDate,"yyy-MM-dd");
  }
  getAllTrainingsByWeekly(){
    this.weeklyEvents = [];
    this.api.getAllTrainingsByWeeklyByCoachId(this.coachId, this.fromDate,this.toDate).subscribe((data: any) => {
      console.log('getweekss',data);
      data.forEach((training: any) => {
        var Difference_In_Time =new Date(training.endDate).getTime() - new Date(training.startDate).getTime();
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
        console.log(Difference_In_Days,training);
        for(var i=0;i<=Difference_In_Days;i++){
          var calenderEvent = Object.create({});
          var startHours = parseInt(training.starttime.split(':')[0]);
          var startMins = parseInt(training.starttime.split(':')[1]);
          var endHours = parseInt(training.endtime.split(':')[0]);
          var endMins = parseInt(training.endtime.split(':')[1]);
          calenderEvent.id = training.id;
          this.start = setHours(setMinutes(new Date(training.startDate).setDate(new Date(training.startDate).getDate() + i), startMins), startHours);
          this.end = setHours(setMinutes(new Date(training.startDate).setDate(new Date(training.startDate).getDate() + i), endMins), endHours);
          calenderEvent.start = this.start;
          calenderEvent.end = this.end;
          calenderEvent.startDate = training.startDate;
          calenderEvent.endDate = training.endDate
          calenderEvent.title = training.trainingTitle;
          calenderEvent.starttime = training.starttime;
          calenderEvent.endtime = training.endtime;
          calenderEvent.location = training.location;
          calenderEvent.teamId = training.teamId;
          calenderEvent.trainingDescription = training.trainingDescription;
          calenderEvent.color = colors.blue,
          calenderEvent.draggable = true;
          calenderEvent.actions = this.actions;
          this.weeklyEvents.push(calenderEvent);
          this.refresh.next();
          console.log(this.weeklyEvents);
        }
      });
    });
  }
  getAllEventsByWeekly(){
    this.api.getAllEventsByWeeklyByCoachId(this.coachId, this.fromDate,this.toDate).subscribe((data: any) => {
      data.forEach((training: any) => {
        console.log(training);
        var date1 = new Date(this.convertDate(training.startDate));
        var date2 = new Date(this.convertDate(training.endDate));
        var Difference_In_Time = date2.getTime() - date1.getTime();
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
        for(var i=0;i<=Difference_In_Days;i++){
        var calenderEvent = Object.create({});
        var startHours = parseInt(this.convertTime(training.startTime).split(':')[0]);
        var startMins = parseInt(this.convertTime(training.startTime).split(':')[1]);
        var endHours = parseInt(this.convertTime(training.endTime).split(':')[0]);
        var endMins = parseInt(this.convertTime(training.endTime).split(':')[1]);
        calenderEvent.id = training.id;
        calenderEvent.start = setHours(setMinutes(new Date(this.convert(training.startDate)).setDate(new Date(this.convert(training.startDate)).getDate() + i), startMins), startHours);
        calenderEvent.end = setHours(setMinutes(new Date(this.convert(training.startDate)).setDate(new Date(this.convert(training.startDate)).getDate() + i), endMins), endHours);
        calenderEvent.startDate = new Date(this.convert(training.startDate));
        calenderEvent.endDate = new Date(this.convert(training.endDate));
        calenderEvent.title = training.title;
        calenderEvent.starttime = this.convertTime(training.startTime);
        calenderEvent.endtime = this.convertTime(training.endTime);
        calenderEvent.area = training.area;
        calenderEvent.teamId = training.teamId;
        calenderEvent.color = colors.yellow,
        calenderEvent.draggable = true;
        calenderEvent.actions = this.actions;
        this.weeklyEvents.push(calenderEvent);
        this.refresh.next();
        console.log(this.weeklyEvents);
        }
      });
    });
  }
  getAllTrainingsByDay() {
    this.dayEvents = [];
    this.api.getAllTrainingsByDayByCoachId(this.coachId, this.currentDate).subscribe((data: any) => {
      data.forEach((training: any) => {
        var calenderEvent = Object.create({});
        var startHours = parseInt(training.starttime.split(':')[0]);
        var startMins = parseInt(training.starttime.split(':')[1]);
        var endHours = parseInt(training.endtime.split(':')[0]);
        var endMins = parseInt(training.endtime.split(':')[1]);
        calenderEvent.id = training.id;
        calenderEvent.start = setHours(setMinutes(new Date(training.date), startMins), startHours);
        calenderEvent.end = setHours(setMinutes(new Date(training.date), endMins), endHours);
        calenderEvent.title = training.trainingTitle;
        calenderEvent.starttime = training.starttime;
        calenderEvent.endtime = training.endtime;
        calenderEvent.location = training.location;
        calenderEvent.teamId = training.teamId;
        calenderEvent.trainingDescription = training.trainingDescription;
        calenderEvent.color = colors.blue,
        calenderEvent.draggable = true;
        calenderEvent.actions = this.actions;
        this.dayEvents.push(calenderEvent);
        this.refresh.next();
      });
    });
  }
  openDialog(data:any){
    this.training = true;
    this.Event = false;
    this.form3 = false;
    this.form4= false;
    this.form2 = false;
    this.form1 = true;
    this.Training.title = '';
    this.Training.endtime = '';
    this.Training.starttime = '';
    this.Training.location = '';
    this.Training.trainingDescription = '';
    this.Training.id = '';
    this.Eventsec.title = '';
    this.Eventsec.endtime = '';
    this.Eventsec.starttime = '';
    this.Eventsec.area = '';
    this.Eventsec.id = '';
    this.Training.endDate = '';
    this.Eventsec.endDate = '';
    if(this.view =='month'){
      this.Training.date = data.day.date;
      this.Eventsec.date = data.day.date;
    }else{
    this.Training.date = data.date;
    this.Eventsec.date = data.date;
    } 

    console.log(this.training);
    $('#addTraingAndEvent').modal('show');
  }
  getAllEventsByDay() {
    this.api.getAllEventsByDayByCoachId(this.coachId, this.currentDate).subscribe((data: any) => {
      data.forEach((event: any) => {
        var calenderEvent = Object.create({});
        var startHours = parseInt(event.startTime.split(':')[0]);
        var startMins = parseInt(event.startTime.split(':')[1]);
        var endHours = parseInt(event.endTime.split(':')[0]);
        var endMins = parseInt(event.endTime.split(':')[1]);
        calenderEvent.id = event.id;
        calenderEvent.start = setHours(setMinutes(new Date(event.date), startMins), startHours);
        calenderEvent.end = setHours(setMinutes(new Date(event.date), endMins), endHours);
        calenderEvent.title = event.title;
        calenderEvent.starttime = event.startTime;
        calenderEvent.endtime = event.endTime;
        calenderEvent.area = event.area;
        calenderEvent.teamId = event.teamId;
        calenderEvent.color = colors.yellow,
        calenderEvent.draggable = true;
        calenderEvent.actions = this.actions;
        this.dayEvents.push(calenderEvent);
        this.refresh.next();
      });
    });
  }
  getAllTraining() {
    this.monthlyEvents = [];
    this.api.getAllTrainingByCoachId(this.coachId, this.currentMonthName + ',' + this.currentYear).subscribe((data: any) => {
      console.log('gettraining', data);
      data.forEach((training: any) => {
        var calenderEvent = Object.create({});
        calenderEvent.id = training.id;
        calenderEvent.start = new Date(training.startDate);
        calenderEvent.end = new Date(training.endDate);
        calenderEvent.title = training.trainingTitle;
        calenderEvent.color = colors.blue;
        calenderEvent.draggable = true;
        calenderEvent.starttime = training.starttime;
        calenderEvent.endtime = training.endtime;
        calenderEvent.location = training.location;
        calenderEvent.trainingDescription = training.trainingDescription;
        calenderEvent.teamId = training.teamId;
        this.monthlyEvents.push(calenderEvent);
        this.refresh.next();
      });
    })
  }
  getAllEvents() {
    this.monthlyEvents = [];
    this.api.getAllEventsByCoachId(this.coachId, this.currentMonthName + ',' + this.currentYear).subscribe((data: any) => {
      console.log('getevents', data);
      data.forEach((event: any) => {
        var calenderEvent = Object.create({});
        calenderEvent.id = event.id;
        calenderEvent.start = new Date(this.convert(event.startDate));
        calenderEvent.end = new Date(this.convert(event.endDate));
        calenderEvent.title = event.title;
        calenderEvent.color = colors.yellow;
        calenderEvent.draggable = true;
        calenderEvent.starttime = this.convertTime(event.startTime);
        calenderEvent.endtime = this.convertTime(event.endTime);
        calenderEvent.area = event.area;
        calenderEvent.teamId = event.teamId;
        this.monthlyEvents.push(calenderEvent);
        this.refresh.next();
      });
    })
  }

  getTeam() {
    this.api.getTeamBySportsByCoachId(this.coachId).subscribe((data: any) => {
      this.teamList = data;
      this.getTeamList = data;
    });
  }

  traing: any;
  check(data: any) {
    this.traing = data.target.value;
    console.log(data.target.value);
    this.training = true;
    this.Event = false;
    this.form1 = true;
    this.form2 = false;
    this.form3 = false;
    this.form4 = false;
  }
  EVENT: any;
  check1(data: any) {
    this.EVENT = data.target.value;
    console.log(data.target.value);
    this.Event = true;
    this.training = false;
    this.form1 = false;
    this.form2 = true;
    this.form3 = false;
    this.form4 = true;
  }

  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData!: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    // {
    //   label: '<i class="fas fa-fw fa-trash-alt"></i>',
    //   a11yLabel: 'Delete',
    //   onClick: ({ event }: { event: CalendarEvent }): void => {
    //     this.dayEvents = this.dayEvents.filter((iEvent) => iEvent !== event);
    //     this.handleEvent('Deleted', event);
    //   },
    // },
  ];

  refresh: Subject<any> = new Subject();
  monthlyEvents: CalendarEvent[] = [];
  dayEvents :CalendarEvent[] = [];
  weeklyEvents : CalendarEvent[]=[];
  activeDayIsOpen: boolean = false;

  constructor(
    private api: AllApiService,
    public datepipe: DatePipe
  ) { }

  Clicked() {
    this.display = 'block';
  }

  test = '';
  dayClicked(data: any) {
    // if (isSameMonth(date, this.viewDate)) {
    //   if (
    //     (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
    //     events.length === 0
    //   ) {
    //     this.activeDayIsOpen = false;
    //   } else {
    //     this.activeDayIsOpen = true;
    //   }
    //   this.viewDate = date;
    // }
    console.log('datatat', data);
  }
  display = 'none';
  // eventTimesChanged({
  //   event,
  //   newStart,
  //   newEnd,
  // }: CalendarEventTimesChangedEvent): void {
  //   this.events = this.events.map((iEvent) => {
  //     if (iEvent === event) {
  //       return {
  //         ...event,
  //         start: newStart,
  //         end: newEnd,
  //       };
  //     }
  //     return iEvent;
  //   });
  //   this.handleEvent('Dropped or resized', event);
  // }
  setDataToEditEventForm(event:any){
    console.log(event);
    this.editTrainingRadio = false;
    this.editEventRadio = true;
    this.form1 = false;
    this.form2 = false;
    this.form3 = false;
    this.form4 = true;
    this.editEvent.id = event.id,
    this.editEvent.title = event.title;
    this.editEvent.date = event.start;
    this.editEvent.endDate = event.end;
    if(event.startDate !=undefined && event.endDate !=undefined){
      this.editEvent.date = event.startDate;
      this.editEvent.endDate = event.endDate;
    }
    this.editEvent.endtime = event.endtime;
    this.editEvent.starttime = event.starttime;
    this.editEvent.area = event.area;
    this.editEvent.teamId = event.teamId;
  }
  setDataToEditTrainingForm(event:any){
    this.editTrainingRadio = true;
    this.editEventRadio = false;
    this.form1 = false;
    this.form2 = false;
    this.form3 = true;
    this.form4 = false;
    this.editTraining.id = event.id;
    if(event.title ==undefined)
    this.editTraining.title = event.title;
    else
    this.editTraining.title = event.title;
    this.editTraining.starttime = event.starttime;
    this.editTraining.endtime = event.endtime;
    this.editTraining.date = event.start;
    this.editTraining.endDate = event.end;
    if(event.startDate !=undefined && event.endDate !=undefined){
      this.editTraining.date = event.startDate;
      this.editTraining.endDate = event.endDate;
    }
    this.editTraining.location = event.location;
    this.editTraining.trainingDescription = event.trainingDescription;
    this.editTraining.teamId = event.teamId;
  }
  handleEvent(action: string, event: any): void {
    this.modalData = { event, action }
    if(event.location ==undefined){
      this.disableTrainingRadio =false;
      this.disableEventRadio = null;
      this.setDataToEditEventForm(event);
    }else{
      this.disableTrainingRadio =null;
      this.disableEventRadio = false;
      this.setDataToEditTrainingForm(event);
    }
    $('#editTraingAndEvent').modal('show');
  }
  deleteTraining(data:any){
    this.api.deleteTraining(data.id).subscribe(
      result => console.log(result),
      err => {
        if(err.status ==200){
          this.isDelete = true;
          this.refreshCalanderDataBasedOnView();
            $('#deleteModel').modal('show')
        }}
    );
  }
  deleteEvent(data:any){
    this.api.deleteEvent(data.id).subscribe(
      result => console.log(result),
      err => {
        if(err.status ==200){
          this.isDelete = false;
          this.refreshCalanderDataBasedOnView();
            $('#deleteModel').modal('show')
        }}
    );
  }
  Events = '';
  event = '';

  setView(view: CalendarView) {
    this.view = view;
    this.refreshCalanderDataBasedOnView();
  }

  closeOpenMonthViewDay(data: any) {
    this.currentDate = this.datepipe.transform(new Date(data),"yyyy-MM-dd");
    this.activeDayIsOpen = false;
    this.currentMonthName = this.monthNames[new Date(data).getMonth()];
    this.currentYear = new Date(data).getFullYear();
    var toDate = new Date(data);
    var fromDate = new Date(data).setDate(toDate.getDate()-6);
    this.fromDate = this.datepipe.transform(fromDate,"yyyy-MM-dd");
    this.toDate = this.datepipe.transform(toDate,"yyy-MM-dd");
    this.refreshCalanderDataBasedOnView();
  }

  change(data: any) {
    console.log(data.target.value);
  }
  Date: any;
  saveForm(data: any) {
    console.log(data);
    this.Date = this.datepipe.transform(data.date, 'yyyy-MM-ddT00:00:00.483Z');
    var endDate = this.datepipe.transform(data.endDate, 'yyyy-MM-ddT00:00:00.483Z');
    const datatosend = {
      createdBy: this.orgName,
      startDate: this.Date,
      endDate : endDate,
      trainingTitle: data.title,
      location: data.location,
      trainingDescription: data.trainingDescription,
      teamId: data.id,
      starttime: data.starttime,
      endtime: data.endtime,
    };
    console.log(datatosend);
    this.api.addTraining(datatosend).subscribe((data: any) => {
      if (data.message == 'Training Added Successfully') {
        $('#result').modal('show')
        this.refreshCalanderDataBasedOnView();
      }
    });
  }
  updateTraining(data: any) {
    this.form1 = false;
    this.form3 = true;
    this.Date = this.datepipe.transform(data.date, 'yyyy-MM-ddTh:mm:ss.483Z');
    var endDate = this.datepipe.transform(data.endDate, 'yyyy-MM-ddTh:mm:ss.483Z');
    const datatosend = {
      id: data.id,
      createdBy: this.orgName,
      startDate: this.Date,
      endDate : endDate,
      trainingTitle: data.title,
      location: data.location,
      trainingDescription: data.trainingDescription,
      teamId: data.teamId,
      starttime: data.starttime,
      endtime: data.endtime,
      updatedBy: this.orgName,
      updatedDate: new Date(),
    };
    console.log(datatosend);
    this.api.updateTraining(datatosend).subscribe((data: any) => {
      if (data.message == 'Training Updated Successfully') {
        $('#result').modal('show')}
        this.refreshCalanderDataBasedOnView();
    });
  }
  refreshCalanderDataBasedOnView(){
    if(this.view =='month'){
      this.getAllTraining();
      this.getAllEvents();
      }
      else if(this.view =="day"){
      this.getAllTrainingsByDay();
      this.getAllEventsByDay();
      }
     else if(this.view =="week"){
      this.getAllTrainingsByWeekly();
      this.getAllEventsByWeekly();
      }
  }
  updateEvents(data: any) {
    this.form1 = false;
    this.form2 = false;
    this.form3 = false;
    this.form4 = true;
    this.eventData = this.datepipe.transform(data.date, 'dd-MM-yyy');
    var endDate = this.datepipe.transform(data.endDate, 'dd-MM-yyyy');
    const datatosend = {
      id: data.id,
      createdBy: this.orgName,
      createdDate: this.Date,
      startDate : this.eventData,
      endDate : endDate,
      title: data.title,
      area: data.area,
      teamId: data.teamId,
      endtime: this.tConv24(data.endtime),
      starttime: this.tConv24(data.starttime),
      updatedBy: this.orgName,
      updatedDate: this.datepipe.transform(new Date(), 'yyyy-MM-ddT06:17:51.483Z'),
    };
    console.log(datatosend);
    this.api.updateEvent(datatosend).subscribe((data: any) => {
      if (data.message == 'Events Updated Successfully') {
        $('#result').modal('show')}
        this.refreshCalanderDataBasedOnView();
    });
  }
  eventData: any;
  saveEvents(data: any) {
    this.form1 = false;
    this.form2 = true;
    this.form3 = false;
    this.form4 = false;
    console.log(data);
    this.eventData = this.datepipe.transform(data.date, 'dd-MM-yyyy');
    var endDate = this.datepipe.transform(data.endDate, 'dd-MM-yyyy')
    const postData = {
      title: data.title,
      startDate: this.eventData,
      endDate : endDate,
      endtime: this.tConv24(data.endtime),
      starttime: this.tConv24(data.starttime),
      area: data.area,
      teamId: data.id,
      createdBy: this.orgName,
    };
    console.log(postData);
    this.api.addEvents(postData).subscribe((data: any) => {
      if (data.message == 'Events Added Successfully') {
        $('#result').modal('show')
        this.refreshCalanderDataBasedOnView();
    };
  })
}

  Teams(data: any) {
    this.teamId = data;
    console.log(this.teamId);
  }

  types: data[] = [
    {
      columnName: 'Trainings',
      value: 'Trainings',
    },
    {
      columnName: 'My Events',
      value: 'My Events',
    },
  ];
  tConv24(time24) {
    var ts = time24;
    var H = +ts.substr(0, 2);
    var h:any = (H % 12) || 12;
    h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
    var ampm = H < 12 ? " AM" : " PM";
    ts = h + ts.substr(2, 3) + ampm;
    return ts;
  };
  convertTime(timeStr) {
    const [time, modifier] = timeStr.split(' ');
    let [hours, minutes] = time.split(':');
    if (hours === '12') {
       hours = '00';
    }
    if (modifier === 'PM') {
       hours = parseInt(hours, 10) + 12;
    }
    return `${hours}:${minutes}`;
 };
 convert(str){
  return str.split('-')[1]+'-'+str.split('-')[0]+'-'+str.split('-')[2];
 }
 convertDate(str){
  return str.split('-')[1]+'/'+str.split('-')[0]+'-'+str.split('-')[2];
 }
}
