import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import {
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
} from '@angular/forms';
import Swal from 'sweetalert2';
import { Subject } from 'rxjs';
import jsPDF from 'jspdf';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';


export interface data {
  columnName: string;

  value: string;
}

declare var $: any;


@Component({
  selector: 'app-data-treatment-info',
  templateUrl: './data-treatment-info.component.html',
  styleUrls: ['./data-treatment-info.component.scss']
})
export class DataTreatmentInfoComponent implements OnInit {

  modelDetails :ModalCommanService []=[
    {
    modelName:'Treatment information'
  }
  ]

  @Input() atheleteId: any;
  myTreatmentForm: FormGroup = new FormGroup({});
  treatmentInfoData: any;
  treatment = true;
  editTreatSection = false;
  viewTreatSection = false;
  practitioner: any[] = [];
  id: any;
  treatDeleteId: any;

  test: any = [];
  demo: any = [];
  notTouchedUploadBtn = true;
  touchedUploadBtn = false;
ameen:any=[]
orgName:any

docType:any
docFileName:any

uploadButtonGreen = '#f79b3e';
  uploadButtonOrange = '#207e04';

  fileTypeName: any = '';

  constructor(
    private Api: AllApiService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    public datepipe: DatePipe
  ) {
    this.myTreatmentForm = this.formBuilder.group({
      consultationType: ['', [Validators.required]],
      disease: ['', [Validators.required]],
      treatmentType: ['', [Validators.required]],
      location: ['', [Validators.required]],
      practitionerID: ['', [Validators.required]],
      consultationDate: ['', [Validators.required]],
      consultationTime: ['', [Validators.required]],
      complaints: ['', [Validators.required]],
      examination: ['', [Validators.required]],
      athleteId: this.atheleteId,
      documents: this.formBuilder.array([]),
      createdBy: '',

      updatedBy: '',
    });
  }

  ngOnInit(): void {
    this.id = localStorage.getItem('OrgID');
    this.getPractitioner();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getTreatmentDataByAthleteId();
  }

  newSkill(): FormGroup {
    return this.formBuilder.group({
      documentType: '',
      fileName: '',
      fileName2: '',
      fileNameToDisplay: [''],
      btnUpload: [false],
      cancelBtn: [false],
    });
  }

  removeSkill(i: number) {
    this.abc.removeAt(i);
  }

  get abc(): FormArray {
    return this.myTreatmentForm.get('documents') as FormArray;
  }

  addSkills() {
    this.abc.push(this.newSkill());
  }

  treatBack() {
    this.editTreatSection = false;
    this.viewTreatSection = false;
    this.treatment = true;
    this.demo = [];
    this.ameen = []

    this.notTouchedUploadBtn = true;
    this.touchedUploadBtn = false;
  }

  recDocData: any;

  viewTreatInfo(data: any) {
    console.log('dataIs Coming', data);
    this.editTreatSection = false;
    this.viewTreatSection = true;
    this.treatment = false;
    this.test = data.documents;

    data.documents.forEach((element: any) => {
      console.log('data Doc Return', element);
      this.recDocData = element;
    });


    this.test.forEach((element: any) => {
      console.log(element.fileName);
      this.demo.push(element);
    });

    this.demo.forEach((obj:any) => {
      this.ameen.push(
      this.formBuilder.group({
        documentType: obj.documentType,
      fileName: obj.fileName.replace('C:\\fakepath\\', ''),
      fileNameToDisplay: obj.fileName.replace('C:\\fakepath\\', ''),
      btnUpload: true,
      cancelBtn: true,
      })
    );
  })


    this.myTreatmentForm = this.formBuilder.group({
      consultationType: data.consultationType,
      disease: data.disease,
      treatmentType: data.treatmentType,
      location: data.location,
      practitionerID: data.practitionerID,
      consultationDate: this.datepipe
        .transform(data.consultationDate, 'dd/MM/yyyy')
        ?.split('/')
        .reverse()
        .join('-'),
      consultationTime: this.datepipe.transform(data.consultationTime, 'hh:mm'),
      complaints: data.complaints,
      examination: data.examination,
      athleteId: this.atheleteId,
      documents: this.formBuilder.array(this.ameen),
    });

    console.log('treatment info', this.myTreatmentForm);

    this.getTreatmentDataByAthleteId();
  }

  clickedUpload(i:any) {
    this.notTouchedUploadBtn = false;
    this.touchedUploadBtn = true;
  }

  uploadFile = (files: any, i: any) => {
    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);

    this.Api.uploadTreatmentFile(
      'Ameenabrar',
      this.atheleteId,
      formData
    ).subscribe((data: any) => {
      console.log('upload treatmernt con', data);

      this.fileTypeName = data;
      if (data) {

        let a: any = this.myTreatmentForm.controls['documents'];
        console.log(
          "this.myTreatmentForm.controls['documents']",
          a['controls'][i].value.btnUpload
        );
        console.log('this.upFileName', this.fileTypeName);
        a['controls'][i].patchValue({
          fileName: this.fileTypeName,
          btnUpload: true,
          cancelBtn:true
        });
        console.log('this.upFileName', this.fileTypeName);
        a['controls'][i].patchValue({
          fileNameToDisplay:files[0].name
        });

        console.log(
          "this.myTreatmentForm.controls['documents']",
          a['controls'][i].value
        );

      }
    });
  };


  downloadDoc(i:any){

    let a: any = this.myTreatmentForm.controls['documents']

    this.docType = a['controls'][i].value.documentType;
    this.docFileName = a['controls'][i].value.fileName;

        console.log(this.docType, this.docFileName);

        this.orgName = localStorage.getItem('Name'),
        console.log(this.orgName);


        this.Api.downloadTreatmentInfo(this.docFileName,this.orgName.split(' ').join(''),this.atheleteId).subscribe((res:any) => {
          console.log(res);

          if(res){
            window.open(
              res
            )
          }

        })
  }


  editTreatInfo(data: any) {
    console.log('editTreatment', data);

    this.editTreatSection = true;
    this.viewTreatSection = false;
    this.treatment = false;

    this.test = data.documents;
    this.test.forEach((element: any) => {
      console.log(element.fileName);
      this.demo.push(element);
    });


    this.demo.forEach((obj:any) => {
        this.ameen.push(
        this.formBuilder.group({
          documentType: obj.documentType,
        fileName: obj.fileName.replace('C:\\fakepath\\', ''),
        fileNameToDisplay: obj.fileName.replace('C:\\fakepath\\', ''),
              btnUpload: true,
          cancelBtn:true
        })
      );
    })

    // let a: any = this.myTreatmentForm.controls['documents'];

    //     a['controls'].patchValue({
    //       btnUpload: false,
    //       cancelBtn:false
    //     });


    this.myTreatmentForm = this.formBuilder.group({
      consultationType: data.consultationType,
      disease: data.disease,
      id: data.id,
      treatmentType: data.treatmentType,
      location: data.location,
      practitionerID: data.practitionerID,
      consultationDate: this.datepipe
        .transform(data.consultationDate, 'dd/MM/yyyy')
        ?.split('/')
        .reverse()
        .join('-'),
      consultationTime: this.datepipe.transform(data.consultationTime, 'hh:mm'),
      complaints: data.complaints,
      documents: this.formBuilder.array(this.ameen),
      examination: data.examination,
      athleteId: this.atheleteId,
      createdBy: localStorage.getItem('Name'),
    });

    console.log('this.myTreatmentForm',this.myTreatmentForm);



    this.getTreatmentDataByAthleteId();

    console.log('treatment info', this.myTreatmentForm);
  }

  updateTreatInfo() {
    let data = this.myTreatmentForm.value;
    data['updatedBy'] = localStorage.getItem('Name');

    console.log(data);

    this.Api.updateTreatmentInfo(data).subscribe(
      (data: any) => {
        console.log('final update response', data);
        if (data.message == 'Treatmentinfo Updated Successfully') {
          this.getTreatmentDataByAthleteId();
          this.treatBack();
          $('#updateTreatment').modal('show');
        } else {
          $('#modelError').modal('show');
        }
      }
      // (err) => {
      //   if (err.message === 'InjuryDetails Updated Successfully') {

      //     alert("updated successfully")
      //     $('#createInjurydata').modal('show');
      //   } else {
      //     alert("Un successfully")
      //     $('#modelError').modal('show');
      //   }
      // }
    );
  }

  deleteTreatInfo(id: any) {
    this.treatDeleteId = id;
  }

  deleteData() {
    this.Api.deleteTreatmentInfo(this.treatDeleteId).subscribe((res: any) => {
      if (res.message == 'TreatmentInfo Deleted Successfully') {
        $('#deleteTreatment').modal('show');
        this.getTreatmentDataByAthleteId();
      } else {
        $('#modelError').modal('show');
      }
    });
  }

  getTreatmentDataByAthleteId() {
    this.Api.getTreatmentInfoByAthleteId(this.atheleteId).subscribe((data) => {
      console.log('treatmentInfoData', data);

      this.treatmentInfoData = data;
    });
  }

  getPractitioner() {
    this.Api.getPractitionerByOrganisationId(
      localStorage.getItem('OrgID')
    ).subscribe((data: any) => {
      console.log('final response => (getPractitioner)', data);
      this.practitioner = data;
    });
  }


  cancel(i:any) {
    let a: any = this.myTreatmentForm.controls['documents'];
    console.log('this.upFileName', this.fileTypeName);
        a['controls'][i].patchValue({
          fileNameToDisplay: '',
          btnUpload: false,
          cancelBtn:false
        });


  }
}
