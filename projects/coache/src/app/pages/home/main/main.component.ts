import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})



export class MainComponent implements OnInit {
  name:any
  orgId:any
  getRoleId:any

  constructor(private api: AllApiService) { }

    ngOnInit(): void {
  // this.name=localStorage.getItem('Names')
  this.orgId=sessionStorage.getItem('coachId')
  this.getRoleId=localStorage.getItem('coachRoleId')
  console.log(this.orgId);
  this.get()
  this.getSideNavItems()
    }
    itemsList:any=[]
    getSideNavItems(){
    this.api.getSideNavbarItems(this.getRoleId).subscribe((data:any)=>{
      console.log('sidebar',data);
      this.itemsList=data
    })
    }

  myFun(){

  }
  isCollapsed = true;
  isCollapsed_1 = true;
  isCollapsed_2 = true;
  myFun1(){
      this.isCollapsed=true;
    this.isCollapsed_1=true;
    this.isCollapsed_2=true;
  }

  myFun2(){
    this.isCollapsed=false;
    this.isCollapsed_1=true;
    this.isCollapsed_2=true;
  }

  myFun3(){
    this.isCollapsed=true;
    this.isCollapsed_1=false;
    this.isCollapsed_2=true;
  }clickCount = 0;




//   ready(){
//     this.clickCount++;
// }












// upload image code starts here
// [code link] = https://stackblitz.com/edit/angular-file-upload-preview-k6dnu5?file=app%2Fapp.component.html

url:any = '';
onSelectFile(event:any) {
  if (event.target.files && event.target.files[0]) {
    var reader = new FileReader();

    reader.readAsDataURL(event.target.files[0]); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.url = reader.result;
    }
  }
}


// upload image code ends here











 SidebarCollapse () {
  $('.menu-collapsed').toggleClass('d-none');
  $('.sidebar-submenu').toggleClass('d-none');
  $('.submenu-icon').toggleClass('d-none');
  $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');

  // Treating d-flex/d-none on separators with title
  var SeparatorTitle = $('.sidebar-separator-title');
  if ( SeparatorTitle.hasClass('d-flex') ) {
      SeparatorTitle.removeClass('d-flex');
  } else {
      SeparatorTitle.addClass('d-flex');
  }

  // Collapse/Expand icon
  $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
}




  get(){
    this.api.getCoachById(this.orgId).subscribe((data:any)=>{
      console.log('get',data);
      this.name=data.coacheName
      this.url=data.image

    })
  }


}
function SidebarCollapse() {
  throw new Error('Function not implemented.');
}

function click(arg0: () => void): JQuery.TypeEventHandlers<HTMLElement, undefined, HTMLElement, HTMLElement> {
  throw new Error('Function not implemented.');
}

