import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { MainComponent } from './main/main.component';
import { Route, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from '../header/header.component';
import { MessagesComponent } from 'projects/esp-shared/src/lib/components/messages/messages.component';
import { AuthenticationGuard } from 'projects/esp/src/app/authentication.guard';

const routes: Route[] = [
  {
    path: '',
    component: HomeComponent,canActivate:[AuthenticationGuard],
    children: [
      {
       path: 'dashboard',
       loadChildren: () =>  import('../dashboard/dashboard.module').then(o => o.DashboardModule)
      },
      {
        path:'teams',
        loadChildren: ()=> import('../teams/teams.module').then(o=>o.TeamsModule)
      },

       {
        path: 'coach-attendence',
        loadChildren: () =>  import('../attendence/attendence.module').then(o => o.AttendenceModule)
       },

       {
        path: 'coach-assessments',
        loadChildren: () =>  import('../assessments/assessments.module').then(o => o.AssessmentsModule)
       },
       {
         path : 'coach-trainings',
         loadChildren : () => import ('../trainings/trainings.module').then(o => o.TrainingsModule)
       },
       {
        path: 'create-team-details',
        loadChildren: () => import('../create-team-details/create-team-details.module').then(o => o.CreateTeamDetailsModule)
       },

       {
         path : 'athlete-data',
         loadChildren : () => import ('../athlete-data/athlete-data.module').then(o => o.AthleteDataModule)
       },

       {
         path : 'view-questionnaire',
         loadChildren : () => import ('../data-view-questionnaire/data-view-questionnaire.module').then(o => o.DataViewQuestionnaireModule)
       },
      // {
      //  path: 'header',
      //  loadChildren: () =>  import('../header/header.module').then(o => o.HeaderModule)
      // },




       {
        path: 'forgot-password',
        loadChildren: () => import('../forgot-password/forgot-password.module').then(o => o.ForgotPasswordModule)
       },
       {
         path:'messages',
         component:MessagesComponent
       }



      // {
      //   path : 'messages',
      //   loadChildren : () => import ('../messages/messages.module').then(o => o.MessagesModule)
      // },



      //  {
      //    path : 'login-super-admin',
      //    loadChildren : () => import ('../../../../../esp-super-admin/src/app/pages/login/login.module').then(o => o.LoginModule)
      //  }
    ]
  }
];


@NgModule({
  declarations: [
    HomeComponent,
    MainComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,

    RouterModule.forChild(routes),
    NgbModule
  ]
})
export class HomeModule { }
