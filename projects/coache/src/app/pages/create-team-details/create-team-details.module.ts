import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateTeamDetailsComponent } from './create-team-details.component';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
// import { AtheleteDetailsComponent } from '../athelete-details/athelete-details.component';

const routes: Route[] = [
  {
    path: '',
    component: CreateTeamDetailsComponent
  }
];

@NgModule({
  declarations: [
    CreateTeamDetailsComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    NgxIntlTelInputModule,
    // AtheleteDetailsComponent,
    NgMultiSelectDropDownModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class CreateTeamDetailsModule { }
