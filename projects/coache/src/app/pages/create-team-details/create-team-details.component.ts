import { Component, OnInit } from '@angular/core';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import Swal from 'sweetalert2';
import countries from 'projects/esp-shared/src/lib/services/callingCodes.json'
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';

import {
  FormArray,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';

import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';


declare var $: any;
@Component({
  selector: 'app-create-team-details',
  templateUrl: './create-team-details.component.html',
  styleUrls: ['./create-team-details.component.scss']
})
export class CreateTeamDetailsComponent implements OnInit {

  modelDetails :ModalCommanService []=[
    {
    modelName:'Coach'
  }
  ]
  dropdownList: any = [];
  selectedItems: any = [];
  dropdownSettings: any = [];
  countriesEnglish = [
    // { item_id: 1, item_text: '+91' },

    { item_id: 1, item_text: '+358' },

    { item_id: 2, item_text: '+355' },

    { item_id: 3, item_text: '+213' },
    { item_id: 4, item_text: '+1684' },
    { item_id: 5, item_text: '+376' },

    { item_id: 6, item_text: '+244' },

    { item_id: 7, item_text: '+355' },

    { item_id: 8, item_text: '+1264' },
    { item_id: 9, item_text: '+672' },
    { item_id: 10, item_text: '+1268' },
    { item_id: 11, item_text: '+54' },
    { item_id: 12, item_text: '+91' },
    { item_id: 13, item_text: '+65' },
    { item_id: 14, item_text: '+60' },
  ];

  separateDialCode = false;
	SearchCountryField = SearchCountryField;
	CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
	preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];

  counrtyList:{dial_code:string, name:string}[] = countries

  myReactiveForms: FormGroup = new FormGroup({});
  myCreateTeamForms: FormGroup = new FormGroup({});
  // inviteAtheleteForm: FormGroup = new FormGroup({});



  phoneCode: any[] = []
  teamId: any = ''
  coaches: any[] = []
  athiid: any
  // filteredCoach: any[] = []
  coachId = ''
  resId: any = ''
  orgId: any = ''
  secondBlock = false;
  firstBlock = true;
  byID: any;
  statusAthlete: any
  atheletById: any[] = []
  logedInName:any



  constructor(private Api: AllApiService, private formBuilder: FormBuilder) {

    this.myReactiveForms = this.formBuilder.group({

      athleteName: '',
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      mobile: ['', [Validators.required]],
      coacheName: ['', [Validators.required]],

      mobileNumber: ['', [Validators.required]],
      image: 'assets/img/coache-avatar.png',
      updatedBy:''

    });

    this.myCreateTeamForms = this.formBuilder.group({

      teamName: ['', [Validators.required]],
      sportCategory: ['', [Validators.required]],
      teamSize: ['', [Validators.required]],
      coachId: ['', [Validators.required]],
      createdBy: ''
    });

    // this.inviteAtheleteForm = this.formBuilder.group({



    //   email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    //   mobileNo: ['', [Validators.required]],
    //   mobileNumber: ['', [Validators.required]],
    //   athleteName: ['', [Validators.required]],
    //   organizationId: '',
    //   teamId: '',
    //   createdBy: ''
    // });

  }

  ngOnInit(): void {
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      // selectAllText: 'Select All',
      // unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true

    };
    this.orgId = localStorage.getItem('OrgID')
    this.resId = localStorage.getItem('ID')
    this.teamId = localStorage.getItem('teamID')
    this.getOrgById()
    this.getAthlete()
    this.getCoaches()
    this.athleteByTeamId()
    this.getCallingCodes()
  }

  getCoaches() {
    this.orgId = localStorage.getItem('OrgID')
    this.Api
      .getAllCoachBySportsCategory(this.orgId)
      .subscribe((res: any) => {
        this.coaches = res;
        console.log('lkg', res);

      });
  }

  athleteByTeamId() {

    this.teamId = localStorage.getItem('teamID')


    this.Api
      .getAthleteByTeamId(this.teamId)
      .subscribe((res: any) => {
        console.log('getAthleteByTeamId', res);
        this.atheletById = res;




        // console.log('atheletById', this.atheletById.status);

      });
  }








  myFun() {
    this.firstBlock = true;
    this.secondBlock = false;
  }

  secondFun() {
    this.firstBlock = false;
    this.secondBlock = true;
  }



  getOrgById() {
    this.Api.getTeamById(this.teamId).subscribe((data: any) => {
      if (data) {
        this.byID = data;
        console.log('this.byID ', this.byID);
        // this.byID.forEach((element: any) => {
        //   console.log('element.sportCategory', element.sportCategory);
        // });
      }
    });
  }


  getAthlete() {


    this.Api.getAthleteStatus(this.teamId).subscribe((data) => {
      console.log('status', data);

      this.statusAthlete = data




    });
  }


  // add: any =
  //   {


  //     "email": "",
  //     "mobile": "",
  //     "athleteName": "",
  //     "id": "",
  //     "createdBy": "",
  //     "updatedBy": "",
  //     "organizationId": "",
  //     "mobileNumber": "",

  //   }



    numCode:any

    onItemSelect(item: any) {
      console.log(item.item_text);
   this.numCode=item.item_text
    }

  // onSubmit() {

  //   this.logedInName = localStorage.getItem("Name")

  //   const data = this.inviteAtheleteForm.value;
  //   data.mobileNumber=this.numCode
  //   console.log('original', data);


  //   const datatosend = {
  //     "email": data.email,
  //     "mobileNo": data.mobileNumber + '-' + data.mobileNo,
  //     "athleteName": data.athleteName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
  //     "teamId": this.teamId,
  //     "organizationId": this.orgId,
  //     "createdBy": this.logedInName,
  //   }

  //   console.log('Athelete', datatosend);

  //   this.Api.inviteAthlete(datatosend).subscribe((data: any) => {

  //     this.athiid = data
  //     console.log('final response => inviteathlete', this.athiid)
  //     this.athleteByTeamId();

  //     window.location.href = window.location.href

  //   }
  //   , (err) => {

  //     this.getOrgById()
  //     this.getAthlete()
  //     this.getCoaches()
  //     this.athleteByTeamId()

  //     if (err.error.text === "Invited Succesfully") {


  //       $('#createdSuccessAthlete').modal('show');
  //       this.inviteAtheleteForm.reset({
  //         mobileNumber:''
  //       });

  //     }
  //     else if (err.error.text === "Entered data already Exists ") {

  //       $('#modelWarningAthlete').modal('show');
  //     }
  //     else {


  //       $('#modelError').modal('show');

  //     }
  //     console.log('final response => inviteathlete', err);
  //     console.log('final response', err.error.text);





  //   }
  //   )

  // }

  get f() { return this.myReactiveForms.controls; }
  // get fa() { return this.inviteAtheleteForm.controls; }



  addDetails(sc: any) {
    const data = this.myReactiveForms.value;
    console.log('data image', data, sc);
    data.mobileNumber=this.numCode
    const update = {
      coacheName: data.coacheName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      email: data.email,
      mobile: data.mobileNumber + '-' + data.mobile,
      sportsCategory: sc,
      image: 'assets/img/coache-avatar.png',
      organisationId: this.orgId,
      createdBy: localStorage.getItem('Name')
      // id: data.id,
    }

    console.log('data coach', update);

    this.Api
      .addCoaches(update)
      .subscribe((res: any) => {
        console.log('final-response1', res);
        if (res.message == "Coach Exsits") {
          $('#modelWarning').modal('show');
        }
        else if (res.message == 'Coach Added Successfully') {

          $('#successModal').modal('show');
          this.getCoaches()
        }
        else {
          $('#modelError').modal('show');
        }
        this.myReactiveForms.reset()
      });
  }






  addCoach() {


    const data = this.myCreateTeamForms.value;
    console.log('updateCoach===', data);

    const updateCoach = {
      organisationId: this.orgId,
      teamName: this.byID.teamName.replace(/\b\w/g, (l:any) => l.toUpperCase()) ,
      teamSize: this.byID.teamSize,
      sportCategory: this.byID.sportCategory,
      coachId: data.coachId,
      id: this.byID.id,
      createdDate: this.byID.createdDate,
      updatedBy: localStorage.getItem('Name')
    }

    console.log('updateCoach', updateCoach);




    this.Api.updateTeam(updateCoach)
      .subscribe((res: any) => {
        console.log('get all teams data', res);
        this.getOrgById()

      });

  }


  TeamsRecived: any[] = []
  getTeams() {
    this.Api
      .getAllTeam()
      .subscribe((res: any) => {
        console.log('get all teams data11', res);
        this.TeamsRecived = res;


      });
  }

  keyPressAlphaNumeric(event: any) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z  ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }





  // editTeam(data: any) {
  //   this.coaches.forEach((ele) => {
  //     if (data.sportCategory == ele.category) {
  //       this.filteredCoach = ele.coach
  //     }
  //   })
  // }



  getCallingCodes() {
    this.Api
      .callingCodesApi()
      .subscribe((res: any) => {
        console.log('callingCodes', res);


        this.phoneCode = Object.values(res[0]);

        console.log('result', this.phoneCode);

      });
  }

  changePreferredCountries() {
		this.preferredCountries = [CountryISO.India, CountryISO.Canada];
	}
  keyPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[0-9   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

}
