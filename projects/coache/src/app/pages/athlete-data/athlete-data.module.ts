import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { ChartModule } from 'angular-highcharts';
import {HighchartsChartModule} from 'highcharts-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AthleteDataComponent } from './athlete-data.component';
import { DataAssessmentsComponent } from '../data-assessments/data-assessments.component';
import { DataInjuryDetailsComponent } from '../data-injury-details/data-injury-details.component';
import { DataTreatmentInfoComponent } from '../data-treatment-info/data-treatment-info.component';

const routes: Route[] = [
  {
    path: '',
    component: AthleteDataComponent
  }
];


@NgModule({
  declarations: [
    AthleteDataComponent,
    DataTreatmentInfoComponent,
    DataInjuryDetailsComponent,
    DataAssessmentsComponent

  ],
  imports: [
    CommonModule,
    EspSharedModule,
    ChartModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    HighchartsChartModule,
  ]
})
export class AthleteDataModule { }
