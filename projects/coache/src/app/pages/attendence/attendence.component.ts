import { Component, OnInit } from '@angular/core';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';

@Component({
  selector: 'app-attendence',
  templateUrl: './attendence.component.html',
  styleUrls: ['./attendence.component.scss'],
})
export class AttendenceComponent implements OnInit {
  trainingList:any = []
  eventList:any =[]
  trainingFiled = false;
  eventFiled = false;
  orgData: any = [];
  category: any;
teamList:any=[]
  id: any;
  teamId:any
  Type:any
  constructor(private Api: AllApiService) {}

  ngOnInit(): void {
    this.id = localStorage.getItem('OrgID');

    this.getOrganisation();
 this.getTeamBySPorts()
 this.getEventTraining()
  }
  changeFileds(data: any) {
this.Type=data
    if (data == 'Events') {
      this.eventFiled = true;
      this.trainingFiled = false;
    } else if (data == 'Trainings') {
      this.trainingFiled = true;

      this.eventFiled = false;
    } else {
      this.trainingFiled = false;
      this.eventFiled = false;
    }
    this.getEventTraining()

  }

  getOrganisation() {
    this.Api.getAllOrganizationById(this.id).subscribe((data) => {
      this.orgData = data;
      console.log(this.orgData);
    });
  }

  sports(data: any) {
    this.category = data.target.value;

    this.getTeamBySPorts()
  }
  getTeamBySPorts(){
    this.Api.getTeamBySportsCat(this.id,this.category).subscribe((data:any)=>{
      console.log('getteam',data);
      this.teamList=data
    })
  }
  changeTeam(data:any){
console.log(data);
this.teamId=data
this.getEventTraining()
}

getEventTraining(){
  this.Api.getListEventTraining(this.teamId,this.Type).subscribe((data:any)=>{
    console.log('getlistt',data);
    console.log(this.teamId,this.Type);
    this.eventList=data
    this.trainingList=data
    
  })
}
changeEvent(data:any){
console.log(data);

}
onSearch(){
}


}
