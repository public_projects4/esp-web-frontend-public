import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DataViewQuestionnaireComponent } from './data-view-questionnaire.component';

const routes: Route[] = [
  {
    path: '',
    component: DataViewQuestionnaireComponent
  }
];

@NgModule({
  declarations: [DataViewQuestionnaireComponent],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule,


    RouterModule.forChild(routes)
  ]
})
export class DataViewQuestionnaireModule { }
