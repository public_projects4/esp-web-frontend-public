import { Component, OnInit } from '@angular/core';
import { Team } from 'projects/esp-shared/src/lib/models';
import { TeamsInfoBar } from 'projects/esp-shared/src/lib/models/teamsinfobar';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { TeamService } from 'projects/esp-shared/src/public-api';
import { Observable } from 'rxjs';

import {
  FormArray,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';


declare var $: any;
@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {

  modelDetails :ModalCommanService []=[
    {
    modelName:'Team'
  },
  {
    modelName:'New team'
  },
  ]

  teams$?: Observable<Team[]>;
  team = "TEAM"
  Id:any
  myCreateTeamForms: FormGroup = new FormGroup({});
  myUpdateTeamForms: FormGroup = new FormGroup({});

  TeamsRecived: any[] = []
  dataById: any[] = []
  coaches: any;

  gotId = '';
  orgData: any;
  orgId: any = ''
excel="EXCEL"
filteredCoach: any[] = []
// filteredCoach2: any[] = []
success = false;
warning = false;
somethingWrong = false;
updateResponse:any
coachModuleId:any



  constructor(private teamservice: TeamService, private Api: AllApiService, private formBuilder: FormBuilder, private datepipe: DatePipe,    private spinner: NgxSpinnerService
    ) {
    this.myCreateTeamForms = this.formBuilder.group({

      teamName: ['', [Validators.required]],
      sportCategory: '',

      teamSize: ['', [Validators.required]],
      coachId: '',
      coachName: '',
      createdDate: new Date(Date.now()),
      createdBy:''
    });

    this.myUpdateTeamForms = this.formBuilder.group({
      teamName: ['', [Validators.required]],
      sportCategory: ['', [Validators.required]],
      teamSize: ['', [Validators.required]],
      coachId: ['', [Validators.required]],
      coachName: ['', [Validators.required]],
      createdDate: new Date(Date.now()),
      updatedBy:''
    });
  }

  ngOnInit(): void {
    this.teams$ = this.teamservice.getTeams();
    this.getTeamsCategory()
    this.getCoaches()
    this.orgId = localStorage.getItem('OrgID')
    this.getOrgById()
    this.coachModuleId=sessionStorage.getItem('coachId')

  }

  get f() { return this.myCreateTeamForms.controls; }


  selectCoach(data: any, i: number, ii: number) {
    let temp = this.TeamsRecived;
    temp[i]['team'][ii]['coachSelected'] = data.target.value;
    this.TeamsRecived = temp;
  }



  getTeamsCategory() {
    this.spinner.show()
    this.coachModuleId=sessionStorage.getItem('coachId')

    this.Api
      .getAllTeamSportsCategoryByCoachId(this.coachModuleId)
      .subscribe((res: any) => {
        this.TeamsRecived = res;
        this.spinner.hide()
        console.log('this.TeamsRecived',this.TeamsRecived);

      });
  }



  UpldateTeams(data: any) {

    this.Api.updateTeam(data)
      .subscribe((res: any) => {
      });
  }


//   onSubmit(sCategory: any) {


//     var data = this.myCreateTeamForms.value
//     data['createdDate']= new Date(Date.now())


//     const datatosend = {
//       organisationId: this.orgId,
//       teamName: data.teamName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
//       teamSize: data.teamSize,
//       sportCategory: sCategory,
//       coachId: data.coachId,
//       coachName: data.coachName,
//       createdDate: this.datepipe.transform(new Date(data.createdDate), 'yyyy-MM-ddT06:17:51.483Z'),
//       createdBy: localStorage.getItem('Name')
//     }
// this.spinner.show()
//     this.Api.createTeam(datatosend)
//       .subscribe((data: any) => {
//         this.gotId = data.id;
//         localStorage.setItem('ID', this.gotId)
//         this.getTeamsCategory()
//         $('#successModal').modal('show');
//         this.myCreateTeamForms.reset({
//           coachId:"",
//         })
//         this.spinner.hide()
//       })

//   }



// createTeam(data:any){
//   this.coaches.forEach((ele:any)=>{
//     if(data.category == ele.category){
//       this.filteredCoach2 = ele.coach
//     }
//   })
// }

  editTeam(data: any) {
    console.log('edit',data);

    this.myUpdateTeamForms = this.formBuilder.group({
      teamName: data.teamName,
      sportCategory: data.sportCategory,
      teamSize: data.teamSize,
      coachId: data.coachId,
      coachName: data.coachName,
      id: data.id,
      createdDate: data.createdDate
    });

    this.coaches.forEach((ele:any)=>{
      if(data.sportCategory == ele.category){
        this.filteredCoach = ele.coach
      }
    })
  }





  updateItems(data: any, sCategory: any) {

    const datatosend = {
      organisationId: this.orgId,
      teamName: data.teamName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      teamSize: data.teamSize,
      sportCategory: data.sportCategory,
      coachId: data.coachId,
      coachName: data.coachName,
      id: data.id,
      createdDate: this.datepipe.transform(new Date(data.createdDate), 'yyyy-MM-ddT06:17:51.483Z'),
      updatedBy: localStorage.getItem('Name')
    }

    this.Api.updateTeam(datatosend)
      .subscribe((data) => {
        this.updateResponse = data
        if(this.updateResponse.message == "Team Updated Successfully"){
          $('#updateModal').modal('show');
        this.success = true;
        this.warning = false;
        this.somethingWrong = false;
        this.getTeamsCategory()
        }
        else{
          $('#updateTeam').modal('show');
        this.success = false;
        this.warning = false;
        this.somethingWrong = true;
        }
      })
  }

  onSet() {
    const data = this.myUpdateTeamForms.value;
    console.log('createdata',data);


    const coachUpdate = {
      organisationId: this.orgId,
      teamName: data.teamName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      teamSize: data.teamSize,
      sportCategory: data.sportCategory,
      coachId: data.coachId,
      coachName: data.coachName,
      id: data.id,
      createdDate: data.createdDate,
      updatedBy: localStorage.getItem('Name')

    }
    this.Api.updateTeam(coachUpdate)
      .subscribe((data: any) => {
        this.getTeamsCategory()
        this.myUpdateTeamForms.reset()
      })
  }





  getCoaches() {
    this.orgId = localStorage.getItem('OrgID')
    this.Api
      .getAllCoachBySportsCategory(this.orgId)
      .subscribe((res: any) => {
        this.coaches = res;
      });
  }


  getOrgById() {


    this.Api.getAllOrganizationById(this.orgId).subscribe((data) => {
      this.orgData = data

    });
  }




  downloadTeam(ID: any) {
this.Id=ID
    // this.Api.downloadApi(id, this.excel, this.team).subscribe((data: any) => {

    //   if (data) {
    //     window.location.href = data
    //   }

    // });
  }


  configPage(getByID: any) {
    localStorage.setItem('teamID', getByID.id)
  }



  addCoach(model_id: any) {
    $(`'#coach_modal${model_id}`).modal('show');
  }


  EXCEL(){
     this.Api.downloadApi(this.Id,this.excel,this.team).subscribe((data: any) => {
          if(data){
    window.location.href=data
          }
        });
      }

      checking(){
        console.log("its working");

      }

      keyPressAlphaNumerics(event: any) {
        var inp = String.fromCharCode(event.keyCode);

        if (/[0-9   ]/.test(inp)) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }

}


