import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(o => o.HomeModule)
  },
  // {
  //   path: 'code-verification',
  //   loadChildren: () => import('./pages/code-verification/code-verification.module').then(o => o.CodeVerificationModule)
  // },
  
  //   path: 'register',
  //   loadChildren: () => import('./pages/register/register.module').then(o => o.RegisterModule)
  // },
 
 
  // {
  //   path: 'coache',
  //   loadChildren: () => import('../../../coache/src/app/pages/home/home.module').then(o => o.HomeModule)
  // },
 
 
  // {
  //   path: 'login-coache',
  //   loadChildren: () => import('../../../coache/src/app/pages/login/login.module').then(o => o.LoginModule)
  // },
  // {
  //   path: 'register-coache',
  //   loadChildren: () => import('../../../coache/src/app/pages/register/register.module').then(o => o.RegisterModule)
  // },
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    { useHash: true }
  )],
  exports: [RouterModule],
})
export class AppRoutingModule { }
// [
//   RouterModule.forRoot(
//     appRoutes,
//     { enableTracing: true } // <-- debugging purposes only
//   )
//   // other imports here
// ],
