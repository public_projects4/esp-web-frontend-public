import { Component, Input, OnInit } from '@angular/core';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import {
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;
@Component({
  selector: 'app-treatment-information-form',
  templateUrl: './treatment-information-form.component.html',
  styleUrls: ['./treatment-information-form.component.scss'],
})
export class TreatmentInformationFormComponent implements OnInit {
isCreate=true
isView=true
isDelete=true
isEdit=true
  @Input() atheleteId: any;
  myTreatmentForm: FormGroup = new FormGroup({});
  uploadButtonGreen = '#f79b3e';
  uploadButtonOrange = '#207e04';
  practitioner: any[] = [];
  pid: string = '';
  newPractitioner: any[] = [];
  testUpload: any[] = [];
  fileTypeName: any = '';
  orgId: any = '';
  res: any;
  documentName: any;
  arrayList: any;
  labelImport: any;
  fileToUpload: any = null;
  binding = 'upload ';
  viewFile: any = 'hi';
  finalResponse: any;
  constructor(
    private Api: AllApiService,
    private formBuilder: FormBuilder,
    public datepipe: DatePipe,
    private spinner: NgxSpinnerService
  ) {
    this.myTreatmentForm = this.formBuilder.group({
      consultationType: ['', [Validators.required]],
      disease: ['', [Validators.required]],
      treatmentType: ['', [Validators.required]],
      location: ['', [Validators.required]],
      consultationDate: ['', [Validators.required]],
      consultationTime: ['', [Validators.required]],
      complaints: ['', [Validators.required]],
      examination: ['', [Validators.required]],
      practitionerID: ['', [Validators.required]],
      athleteId: this.atheleteId,
      documents: this.formBuilder.array(
        [this.newSkill()],
        [Validators.required]
      ),
    });
  }
  getRoleId:any
  ngOnInit() {
    this.getPractitioner();
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();
  }
  itemsList: any = [];
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {
      this.itemsList = data;
      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Athlete Data') {
          if (ele.isDelete == true) {
            this.isDelete = true;
          } else {
            this.isDelete = false;
          }
          if (ele.isEdit == true) {
            this.isEdit = true;
          } else {
            this.isEdit = false;
          }
          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }
          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
          }
        }
      });
    });
  }
  get f() {
    return this.myTreatmentForm.controls;
  }
  newSkill(): FormGroup {
    return this.formBuilder.group({
      documentType: ['', [Validators.required]],
      fileName: [''],
      fileNameToDisplay: ['', [Validators.required]],
      btnUpload: [false],
      cancelBtn: [false],
    });
  }
  get abc(): FormArray {
    return this.myTreatmentForm.get('documents') as FormArray;
  }
  addSkills() {
    this.abc.push(this.newSkill());
  }
  removeSkill(i: number) {
    this.abc.removeAt(i);
  }
  passData(data: any) {
    this.pid = data.target.value;
    this.newPractitioner = [];
    this.practitioner.forEach((element) => {
      if (element.id == data.target.value) {
        this.newPractitioner.push(element);
      }
    });
  }
  uploadFile = (files: any, i: any) => {
    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    this.Api.uploadTreatmentFile(
      'Ameenabrar',
      this.atheleteId,
      formData
    ).subscribe((data: any) => {
      this.fileTypeName = data;
      if (data) {
        let a: any = this.myTreatmentForm.controls['documents'];
        console.log(
          "this.myTreatmentForm.controls['documents']",
          a['controls'][i].value.btnUpload
        );
        a['controls'][i].patchValue({
          fileName: this.fileTypeName,
          btnUpload: true,
          cancelBtn:true
        });
        a['controls'][i].patchValue({
          fileNameToDisplay:files[0].name
        });
        console.log(
          "this.myTreatmentForm.controls['documents']",
          a['controls'][i].value
        );
      }
    });
  };
  notTouchedUploadBtn = true;
  touchedUploadBtn = false;
  clickedUpload(i:any) {
    this.notTouchedUploadBtn = false;
    this.touchedUploadBtn = true;
  }
  onSubmit() {
    const data = this.myTreatmentForm.value;
    data.athleteId = this.atheleteId;
    data['consultationDate'] = this.datepipe.transform(
      new Date(data.consultationDate),
      'yyyy-MM-ddT06:17:51.483Z'
    );
    data['createdBy'] = localStorage.getItem('Name');
    this.spinner.show();
    this.Api.addTreatmentInfo(data).subscribe(
      (data) => {
        this.finalResponse = data;
        if (
          this.finalResponse.message ==
          'Treatment Information Added Successfully'
        ) {
          $('#createTreatmentInfo').modal('show');
          this.newPractitioner = [];
          this.myTreatmentForm.reset({
            practitionerID: '',
            documents: '',
          }),
            (this.myTreatmentForm.get('documents') as FormArray).clear();
            this.addSkills()
        }
        this.spinner.hide();
      },
      (err) => {
        $('#modelErrorTreatmentInfo').modal('show');
      }
    );
  }
  getPractitioner() {
    this.orgId = localStorage.getItem('OrgID');
    this.Api.getPractitionerByOrganisationId(this.orgId).subscribe(
      (data: any) => {
        this.practitioner = data;
      }
    );
  }
  treatmentDownload() {
  }
  cancel(i:any) {
    let a: any = this.myTreatmentForm.controls['documents'];
        a['controls'][i].patchValue({
          fileNameToDisplay: [''],
          btnUpload: false,
          cancelBtn:false
        });
  }
}
