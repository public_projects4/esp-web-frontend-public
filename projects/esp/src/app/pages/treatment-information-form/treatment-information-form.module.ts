import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TreatmentInformationFormComponent } from './treatment-information-form.component';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgxSpinnerModule } from 'ngx-spinner';
const routes: Route[] = [
  {
    path: '',
    component: TreatmentInformationFormComponent
  }
];
@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule,
    BrowserModule,
    RouterModule.forChild(routes)
  ]
})
export class TreatmentInformationFormModule { }
