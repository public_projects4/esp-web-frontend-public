import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { CreateTeamComponent } from './create-team.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';


const routes: Route[] = [
  {
    path: '',
    component: CreateTeamComponent
  }
];

@NgModule({
  declarations: [CreateTeamComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    EspSharedModule,
    NgxSpinnerModule,
    NgMultiSelectDropDownModule,

    RouterModule.forChild(routes)
  ]
})
export class CreateTeamModule { }
