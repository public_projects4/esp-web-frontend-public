import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import Swal from 'sweetalert2';
import {
  FormArray,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';


declare var $: any;

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss']
})
export class CreateTeamComponent implements OnInit {
isCreate=true
isView=true
isDelete=true
isEdit=true


  modelSucess :ModalCommanService []=[{
    modelName:'New Team',
  }]


  myCreateTeamForms: FormGroup = new FormGroup({});

  coaches: any[] = []
  filteredCoach: any[] = []
  orgData: any;
  orgId: any = ''
  gotId = '';

  constructor(private toastr: ToastrService, private Api: AllApiService, private formBuilder: FormBuilder, private datepipe:DatePipe,    private spinner: NgxSpinnerService
    ) {
    this.myCreateTeamForms = this.formBuilder.group({
      teamName: ['', [Validators.required]],
      sportCategory: ['', [Validators.required]],
      teamSize: ['', [Validators.required]],
      coachId: '',
      createdDate: new Date(Date.now()),
      createdBy:''

    });
  }
  getRoleId:any
  ngOnInit(): void {
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();
    this.getCoaches()
    // this.getOrganizationCatagory()
    this.orgId = localStorage.getItem('OrgID')
    console.log(this.orgId);

    this.getOrgById()
  }

  get f() { return this.myCreateTeamForms.controls; }

 itemsList: any = [];
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {
      console.log('sidebar', data);
      this.itemsList = data;
      console.log(this.itemsList);
      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Manage Teams') {
        

          if (ele.isEdit == true) {
            this.isEdit = true;
          } else {
            this.isEdit = false;
          }

          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }

          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
          }
        }
      });
    });
  }

  onClicked() {
    this.toastr.success("Team Created")
  }


  onSubmit() {
    const data = this.myCreateTeamForms.value;



    const datatosend = {
      organisationId: this.orgId,
      teamName: data.teamName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      teamSize: data.teamSize,
      sportCategory: data.sportCategory,
      coachId: data.coachId,
      createdDate: this.datepipe.transform(new Date(data.createdDate), 'yyyy-MM-ddT06:17:51.483Z'),
      createdBy: localStorage.getItem('Name')
    }
    console.log('lkg', datatosend);
    this.spinner.show()
    this.Api.createTeam(datatosend)
      .subscribe((data: any) => {
        console.log('finalresponse', data);
        this.gotId = data.id;
        localStorage.setItem('ID', this.gotId)

        if(data.message == "Team Added Successfully"){
          $('#SuccessCreateModal').modal('show');
        }

        else if(data.message == "Team Exsits"){
          $('#modelWarningAthlete').modal('show');
        }

        else{
          $('#modelError').modal('show');
        }
        this.spinner.hide()
      })
  }


  getCoaches() {
    this.orgId = localStorage.getItem('OrgID')
    this.Api
      .getAllCoachBySportsCategory(this.orgId)
      .subscribe((res: any) => {
        this.coaches = res;
        console.log('getCoachesByID', this.coaches);


      });
  }


  getOrgById() {

    this.Api.getAllOrganizationById(this.orgId).subscribe((data) => {
      console.log('final response => orgIdd', data)
      this.orgData = data
      console.log('abc', this.orgData.sportsCategory);
    });
  }

  categorySelected(data:any){
    console.log('categorySelected',data.target.value);
    this.filteredCoach =[]
    this.coaches.forEach((ele)=>{
      if(data.target.value == ele.category){


        this.filteredCoach = ele.coach
        console.log('lllkkk',this.filteredCoach);
      }
    })

  }


  keyPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[0-9   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

}
