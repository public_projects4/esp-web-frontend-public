import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { MainComponent } from './main/main.component';
import { Route, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from '../header/header.component';
import { MessagesComponent } from 'projects/esp-shared/src/lib/components/messages/messages.component';
import { AuthenticationGuard } from '../../authentication.guard';
const routes: Route[] = [
  {
    path: '',
    component: HomeComponent,canActivate:[AuthenticationGuard],
    children: [
      {
       path: 'dashboard',
       loadChildren: () =>  import('../dashboard/dashboard.module').then(o => o.DashboardModule)
      },
      {
        path: 'coaches',
        loadChildren: () =>  import('../coaches/coaches.module').then(o => o.CoachesModule)
       },
       {
        path: 'attendance',
        loadChildren: () =>  import('../attendence/attendence.module').then(o => o.AttendenceModule)
       },
       {
        path: 'teams',
        loadChildren: () =>  import('../teams/teams.module').then(o => o.TeamsModule)
       },
       {
        path: 'team-details',
        loadChildren: () =>  import('../team-details/team-details.module').then(o => o.TeamDetailsModule)
       },
       {
        path: 'athelete-details',
        loadChildren: () =>  import('../athelete-details/athelete-details.module').then(o => o.AtheleteDetailsModule)
       },
       {
        path: 'assessments',
        loadChildren: () =>  import('../assessments/assessments.module').then(o => o.AssessmentsModule)
       },
       {
        path: 'createassessments',
        loadChildren: () =>  import('../createassessments/createassessments.module').then(o => o.CreateassessmentsModule)
       },
       {
        path: 'createteam',
        loadChildren: () => import('../create-team/create-team.module').then(o => o.CreateTeamModule)
       },
       {
        path: 'create-team-details',
        loadChildren: () => import('../create-team-details/create-team-details.module').then(o => o.CreateTeamDetailsModule)
       },
       {
        path: 'forgot-password',
        loadChildren: () => import('../forgot-password/forgot-password.module').then(o => o.ForgotPasswordModule)
       },
       {
         path : 'athelete-data',
         loadChildren : () => import ('../athelete-data/athelete-data.module').then(o => o.AtheleteDataModule)
       },
       {
         path : 'trainings',
         loadChildren : () => import ('../trainings/trainings.module').then(o => o.TrainingsModule)
       },
       {
         path : 'create-practitioner',
         loadChildren : () => import ('../create-practitioner/create-practitioner.module').then(o => o.CreatePractitionerModule)
       },
       {
         path : 'practitioner-list',
         loadChildren : () => import ('../practitioner-list/practitioner-list.module').then(o => o.PractitionerListModule)
       },
       {
         path : 'reports',
         loadChildren : () => import ('../reports/reports.module').then(o => o.ReportsModule)
       },
       {
         path : 'settings',
         loadChildren : () => import ('../settings/settings.module').then(o => o.SettingsModule)
       },
       {
         path : 'help-and-support',
         loadChildren : () => import ('../help-and-support/help-and-support.module').then(o => o.HelpAndSupportModule)
       },
      {
        path : 'messages',
        component : MessagesComponent
      },
    ]
  }
];
@NgModule({
  declarations: [
    HomeComponent,
    MainComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    RouterModule.forChild(routes),
    NgbModule
  ]
})
export class HomeModule { }
