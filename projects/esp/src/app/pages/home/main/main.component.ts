import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  getRoleId: any;
  profilePicPath: any;
  dummyPic = 'projects/esp/src/assets/img/coache-avatar.png';
  name: any;
  isCreate = true;
  isDelete = true;
  isView = true;
  large=true
  lesser=false;
  enablePic:boolean = true
  isEdit = SVGComponentTransferFunctionElement;
  constructor(private Api: AllApiService) {}
  ngOnInit(): void {
      setInterval(() => {
        this.name = localStorage.getItem('Name');
        }, 1000);
    this.getRoleId = localStorage.getItem('RoleId');
    this.profilePicPath = localStorage.getItem('onboard-profilePic');
    if(this.profilePicPath == ''){
      this.enablePic = true;
    }
    else{
      this.enablePic = false;
    }
    this.getSideNavItems();
  }
  itemsList: any = [];
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {
      this.itemsList = data;
      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Manage Teams') {
          if (ele.isCreate == false) {
            this.itemsList.forEach((ele: any) => {
              if (ele.parentNodeName == 'Manage Teams') {
                ele.permission.splice(0, 1);
              }
            });
          }
        }
        if (ele.parentNodeName == 'Manage Practitioner') {
          if (ele.isCreate == false) {
            this.itemsList.forEach((ele: any) => {
              if (ele.parentNodeName == 'Manage Practitioner') {
                ele.permission.splice(0, 1);
              }
            });
          }
        }
      });
    });
  }
  myFun() {}
  isCollapsed_10 = true;
  isCollapsed = true;
  isCollapsed10 = true;
  isCollapsed_2 = true;
  myFun1(data: any) {
    this.isCollapsed = !this.isCollapsed;
    this.isCollapsed10 = true;
    this.isCollapsed_2 = true;
  }
  myFun2() {
    this.isCollapsed = false;
    this.isCollapsed10 = true;
    this.isCollapsed_2 = true;
  }
  myFun3() {
    this.isCollapsed_10 = true;
    this.isCollapsed = true;
    this.isCollapsed10 = false;
    this.isCollapsed_2 = true;
  }
  clickCount = 0;
  url: any = '';
  onSelectFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.url = reader.result;
      };
    }
  }
  SidebarCollapse() {
    $('.menu-collapsed').toggleClass('d-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    var SeparatorTitle = $('.sidebar-separator-title');
    if (SeparatorTitle.hasClass('d-flex')) {
      SeparatorTitle.removeClass('d-flex');
    } else {
      SeparatorTitle.addClass('d-flex');
    }
    $('#collapse-icon').toggleClass(
      'fa-angle-double-left fa-angle-double-right'
    );
  }
}
function SidebarCollapse() {
  throw new Error('Function not implemented.');
}
function click(
  arg0: () => void
): JQuery.TypeEventHandlers<HTMLElement, undefined, HTMLElement, HTMLElement> {
  throw new Error('Function not implemented.');
}
