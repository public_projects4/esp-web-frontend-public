import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsComponent } from './teams.component';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';


const routes: Route[] = [
  {
    path: '',
    component: TeamsComponent
  }
];


@NgModule({
  declarations: [
    TeamsComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    FormsModule,
    NgxSpinnerModule,

    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class TeamsModule { }
