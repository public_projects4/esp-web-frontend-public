import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Route[] = [
  {
    path: '',
    component: ReportsComponent
  }
];

@NgModule({
  declarations: [ReportsComponent],
  imports: [
    CommonModule,
    EspSharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)

  ]
})
export class ReportsModule { }
