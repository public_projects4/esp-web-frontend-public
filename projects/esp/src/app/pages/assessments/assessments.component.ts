import { Component, OnInit } from '@angular/core';
import { AssessmentInfoBar } from 'projects/esp-shared/src/lib/models/assessment-info-bar';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import {
  FormArray,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';


// import { EspPagesService } from '../../esp-pages.service';



declare var $: any;
@Component({
  selector: 'app-assessments',
  templateUrl: './assessments.component.html',
  styleUrls: ['./assessments.component.scss']
})
export class AssessmentsComponent implements OnInit {

isCreate=true
isView=true
isDelete=true
isEdit=true
  modelDetails :ModalCommanService []=[
    {
    modelName:'Assessment'
  }
  ]

  createAssessmentForm: FormGroup = new FormGroup({});

  dropdownList:any= [];
  testList:any= [];
  selectedItems = [];
  dropdownSettings = {};


  orgData: any;
  orgId: any = ''
  creatBy: any = ''
  respondData: any
  teamsRecived: any[] = []
  teamList: any[] = []
  teamListByOrgId: any[] = []
  // abcd:any='testing1123';
  assListId:string="123uyht";
  demo=[];
  removeId:any;

  assessmentinfo: AssessmentInfoBar [] = [{
    label: 'assessments created (5)'
  }];




  constructor(private Api: AllApiService, private formBuilder: FormBuilder,    private spinner: NgxSpinnerService
    ) {
    this.createAssessmentForm = this.formBuilder.group({
      createdBy: this.creatBy,
      title: ['', [Validators.required]],
      teamId: ['', [Validators.required]],
      organizationId: '',
      orderBy: ['', [Validators.required]],
      id: ''
    });
   }
   getRoleId:any
  ngOnInit(): void {
    this.getOrgById()
    this.getTeamsCategory()
    this.getAssessmentListByOrgId()
  localStorage.setItem('viewAssement','false');
  this.teamList=[]

  this.getRoleId = localStorage.getItem('RoleId');
  this.getSideNavItems();

  }

  itemsList: any = [];
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {
      console.log('sidebar', data);
      this.itemsList = data;
      console.log(this.itemsList);
      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Assessments') {
          if (ele.isDelete == true) {
            this.isDelete = true;
          } else {
            this.isDelete = false;
          }

          if (ele.isEdit == true) {
            this.isEdit = true;
          } else {
            this.isEdit = false;
          }

          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }

          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
          }
        }
      });
    });
  }
  selectedCategory(data:any){
    console.log(data.target.value);
    const getCategory = data.target.value
      this.teamList =[]

    this.teamsRecived.forEach((ele) =>{
      this.dropdownList =[]
      this.demo =[]
      // console.log(ele);




    if(getCategory == ele.category){


      this.dropdownList =[]
      this.teamList =[]
      this.demo =[]



      this.teamList = ele.team
      console.log(this.teamList);



      this.teamList.forEach((res:any) => {
        console.log(res);
        var abc ={}
  abc = {
          id : res.id, teamName : res.teamName
        }

        this.testList.push(abc);

        console.log(this.testList);


        // this.dropdownList = [
        //   { item_id: res.id, item_text: res.teamName  }
        // ];







        // this.dropdownList.push({ item_id: res.id, item_text: res.teamName })
        // this.dropdownList.push( res )

        console.log(this.dropdownList);


      })
      // this.teamList =[]
      // this.dropdownList =[]





      // this.dropdownList = [
      //   { item_id: 1, item_text: 'Mumbai' },
      //   { item_id: 2, item_text: 'Bangaluru' },
      //   { item_id: 3, item_text: 'Pune' },
      //   { item_id: 4, item_text: 'Navsari' },
      //   { item_id: 5, item_text: 'New Delhi' }
      // ];
      // this.selectedItems = [
      //   { item_id: 3, item_text: 'Pune' },
      //   { item_id: 4, item_text: 'Navsari' }
      // ];

      this.dropdownSettings = {
        singleSelection: false,
        idField: 'id',
        textField: 'teamName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 6,
        allowSearchFilter: false
      };



    }



    }
    )
  }

  getOrgById() {
    this.spinner.show()
    this.orgId = localStorage.getItem('OrgID')

    this.Api.getAllOrganizationById(this.orgId).subscribe((data) => {
      this.orgData = data
      this.spinner.hide()
    });
  }

  getTeamsCategory() {
    this.Api
      .getAllTeamBySportsCategory(this.orgId)
      .subscribe((res: any) => {
        this.teamsRecived = res;
      });
}

  getAssessmentListByOrgId() {
    this.spinner.show()
    this.Api
      .getAssessmentByOrgId(this.orgId)
      .subscribe((res: any) => {
        this.teamListByOrgId = res;
        this.spinner.hide()
        console.log('this.teamListByOrgId',this.teamListByOrgId);

      });
}

get f() {
  return this.createAssessmentForm.controls;
}



onSubmit(){
  const data = this.createAssessmentForm.value

  this.orgId = localStorage.getItem('OrgID')
  this.creatBy = localStorage.getItem('Name')

  var idTeam=[]
  data.teamId.map((ele:any) => {
    console.log(ele.id)
    idTeam.push(ele.id)

  })

  const dataToAdd ={

    createdBy: this.creatBy ,
      title: data.title.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      teamId: idTeam,
      organizationId: this.orgId,
      // id: ''

  }


  console.log('datatoAdd',dataToAdd );

this.spinner.show()
  this.Api
      .addAssessment(dataToAdd)
      .subscribe((res: any) => {
        console.log('final-response', res);
        this.getAssessmentListByOrgId()


        this.respondData = data
        console.log(this.respondData.message);


          $('#successModal').modal('show');
          this.createAssessmentForm.reset({
            teamId : '',
            orderBy : '',

          })

  this.teamList=[]


        this.spinner.hide()
      },(err) => {


          $('#modelError').modal('show');

        }
        );

}

dublicateAssessment(data:any){
  console.log('dublicate data', data);


  const dataToDublicate ={

    createdBy: data.creatBy ,
      title: data.title,
      teamId: data.teamId,
      organizationId: data.organizationId,
  }

  this.spinner.show()
  this.Api
      .addAssessment(dataToDublicate)
      .subscribe((res: any) => {
        console.log('final-response', res);
        this.getAssessmentListByOrgId()
        // this.createAssessmentForm.reset()


        this.respondData = data
        console.log(this.respondData.message);


          $('#duplicateSuccessAssessment').modal('show');
          // this.createAssessmentForm.reset({
          //   teamId : '',
          //   orderBy : '',

          // })

        this.spinner.hide()
      },(err) => {


          $('#modelError').modal('show');

        }
        );

}

deleteAssess(){
  this.Api
      .deleteAssessment(this.removeId)
      .subscribe((res: any) => {
      console.log('assessment deletes',res);

      },(err) => {
        if(err.error.text === "Assessment Deleted Successfully"){
          $('#successfullyDeletedModal').modal('show');
      this.getOrgById()
      this.getAssessmentListByOrgId()
        }
      });
}

viewQue(data:any){
  console.log(data);
  localStorage.setItem('viewAssement','true');
  localStorage.setItem('resultAssement','false');
  localStorage.setItem('QuesAssmentId',data)


}

editQue(data:any){
  console.log(data);
  localStorage.setItem('viewAssement','false');
  localStorage.setItem('resultAssement','false');
  localStorage.setItem('QuesAssmentId',data)
}

resultQue(data:any){
  console.log(data);
  localStorage.setItem('resultAssement','true');
  localStorage.setItem('viewAssement','false');
  localStorage.setItem('QuesAssmentId',data)
}

removeQue(id:any){
  this.removeId= id;
}


onItemSelect(data:any){
  console.log(data);
}

}

