import { Component, OnInit } from '@angular/core';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { LoadingBarState } from '@ngx-loading-bar/core/loading-bar.state';
import { ToastrService } from 'ngx-toastr';
import {  PageInfoProperty, StatisticBlockProperties } from 'projects/esp-shared/src/lib/models';
import { DashboardLeaderBoard } from 'projects/esp-shared/src/lib/models/dashboard-leader-board';
import { Chart } from 'angular-highcharts';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
orgId=localStorage.getItem('OrgID')
abc={};
icon1='settings_accessibility'
icon2='task_alt'
icon3='personal_injury'
icon4='running_with_errors'
progress_color_class='dashbg-1'
progress_color_class1='dashbg-4'
progress_color_class2='dashbg-2'
progress_color_class3='dashbg-3'
totalLabel:any
totalActive:any
injury:any
risk:any
progress_percentage:any
progress_percentage1:any
progress_percentage2:any
progress_percentage3:any
  columnchart = new Chart ({
    chart: {
      backgroundColor: 'transparent',
      zoomType: 'xy',
      height:'400px',
      width:370,
      marginBottom:80,
    },
    navigation:{
      menuStyle:{
        backgroundColor:'#666666',
      },
      menuItemStyle:{
        color:'white',
      },
      buttonOptions:{
        symbolStroke:'white',
        enabled:false,
        theme:{
          fill:'#666666',
        }
      }
    },
    title: {
      text: "Team Key Performance Indicator"
    },
    credits: {
      enabled: false,
    },
    xAxis: {
      labels: {
       rotation:-45
      },
      categories: ["Thomas Hector", "Alexander Shelby", "Arther Kooper", "Ranjith", "Jhon Dee", "David",
        "Harsith", "Swapna"]
    },
    yAxis: {
      title: {
        text: ""
      },
      gridLineColor:'#666666',
    },
    legend:{
      layout:'horizontal',
      itemMarginTop:-10,
      itemMarginBottom:-10,
      itemStyle:{
        color:"blue",
      },
      enabled:false
    },
    series: [{
      type: 'column',
      color: '#506ef9',
      data: [
        { y: 20.9 ,borderColor:'none',},
        { y: 71.5,borderColor:'none', },
        { y: 106.4,borderColor:'none', },
        { y: 129.2,borderColor:'none', },
        { y: 144.0, color: 'green',borderColor:'none', },
        { y: 176.0 ,borderColor:'none',},
        { y: 148.5 ,borderColor:'none',},
        { y: 216.4, color: '#fc5185',borderColor:'none', },
      ],
    }],
  });
  areachart = new Chart ({
    chart: {
      type: 'area',
      width: 370,
      inverted: false,
      zoomType: 'xy',
      backgroundColor: 'transparent',
    },
    title: {
      text: 'Optimum Work Load'
    },
    credits: {
      enabled: false,
    },
    accessibility: {
      keyboardNavigation: {
        seriesNavigation: {
          mode: 'serialize'
        }
      }
    },
    navigation:{
      menuStyle:{
        backgroundColor:'#666666',
      },
      menuItemStyle:{
        color:'white',
      },
      buttonOptions:{
        symbolStroke:'white',
        enabled:false,
        theme:{
          fill:'#666666',
        }
      }
    },
    legend: {
      layout: 'horizontal',
      align: 'right',
      verticalAlign: 'top',
      x: -150,
      y: 100,
      floating: true,
      borderWidth: 1,
      backgroundColor: 'white'
    },
    xAxis: {
      categories: [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
      ]
    },
    yAxis: {
      title: {
        text: '',
      },
      gridLineColor:'#666666',
      allowDecimals: false,
      min: 0
    },
    plotOptions: {
      area: {
        fillOpacity: 0.8
      }
    },
    series: [{
      type: 'area',
      name: 'John',
      data: [{y:3}, {y:4}, {y:3}, {y:5}, {y:4}, {y:10}, {y:12}]
    },
     {
       type: 'area',
      name: 'Jane',
      data: [{y:1}, {y:3}, {y:4}, {y:3}, {y:3}, {y:5}, {y:4}]
    }
  ]
  });
  statistics: StatisticBlockProperties[] = [{
    label: 'Total Athletes',
    statistic: 33,
    icon_name : 'settings_accessibility',
    progress_percentage: 30,
    progress_color_class: 'dashbg-1',
    color_class:  'dashtext-1',
  },
  {
    label: 'Active Athletes',
    statistic: 5,
    icon_name : 'task_alt',
    progress_percentage: 30,
    progress_color_class: 'dashbg-4',
    color_class: 'dashtext-4',
  },
  {
    label: 'Total Injuries',
    statistic: 4,
    icon_name : 'personal_injury',
    progress_percentage: 70,
    progress_color_class: 'dashbg-2',
    color_class: 'dashtext-2',
  },
  {
    label: 'Athletes Under Risks',
    statistic: 3,
    icon_name : 'running_with_errors',
    progress_percentage: 55,
    progress_color_class: 'dashbg-3',
    color_class: 'dashtext-3',
  }];
  userdetails: DashboardLeaderBoard[] = [
    {
      position:"1st",
      name: 'Tomas Hecktor',
      which_coach:"@Coacher",
      which_team:'Firefox Team',
      item_1:110,
      item_2:200,
      item_3:100,
      avatar:'assets/img/avatar-1.jpg',
      color_class: ''
    },
    {
      position:"2nd",
      name: 'Alexander Shelby',
      which_coach:"@Coacher",
      which_team:'Blue Tigers Team',
      item_1:150,
      item_2:120,
      item_3:50,
      avatar:'assets/img/avatar-2.jpg',
      color_class: ''
    },
    {
      position:"3rd",
      name: 'Arther Kooper',
      which_coach:"@Coacher",
      which_team:'True Kings Team',
      item_1:60,
      item_2:70,
      item_3:40,
      avatar:'assets/img/avatar-6.jpg',
      color_class: ''
    },
  ];
  constructor(private api: AllApiService) { }
  ngOnInit(): void {
    this.getCountByOrgId()
  }
getCountByOrgId(){
  this.api.getCountByOrgId(this.orgId).subscribe((data: any)=>{
    this.totalLabel=data.totalAthletes
    this.totalActive=data.activeAthletes
    this.injury=data.totalAthletesInjured
    this.risk=data.athletesUnderRisk
    this.progress_percentage=data.totalAthletes
    this.progress_percentage1=data.activeAthletes
    this.progress_percentage2=data.totalAthletesInjured
    this.progress_percentage3=data.athletesUnderRisk
  })
}
}
