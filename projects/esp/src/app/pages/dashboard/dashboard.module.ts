import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { Route, RouterModule } from '@angular/router';
import { DashChartsComponent } from '../dash-charts/dash-charts.component';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule } from '@angular/forms';

const routes: Route[] = [
  {
    path: '',
    component: DashboardComponent
  }
];


@NgModule({
  declarations: [
    DashboardComponent,
    DashChartsComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    RouterModule.forChild(routes)
  ]
})
export class DashboardModule { }
