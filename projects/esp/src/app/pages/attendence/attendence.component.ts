import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';

@Component({
  selector: 'app-attendence',
  templateUrl: './attendence.component.html',
  styleUrls: ['./attendence.component.scss'],
})
export class AttendenceComponent implements OnInit {
  myDate = this.datepipe.transform(new Date(), 'yyyy-MM-ddThh:mm:ss.217Z');
  // today=this.datepipe.transform(new Date(), 'yyyy-MM-dd');

  orgName = localStorage.getItem('Name');
  length: any;
  attendenceList: any = [];
  headerList: any = [];

  table = false;
  dateField = false;
  trainingList: any = [];
  eventList: any = [];
  trainingFiled = false;
  eventFiled = false;
  orgData: any = [];
  category: any;
  teamList: any = [];
  id: any;
  teamId: any;
  Type: any;
  countries = [
    {
      value: 'xxx-0',
      viewValue: 'xxx (+xx)',
      code: 'xx',
      icon: '../../../assets/img/icons/absent.svg',
    },
    {
      value: 'yyy-0',
      viewValue: 'yyy (+yy)',
      code: 'yy',
      icon: '../../assets/svg/icon-cc-add-blue.svg',
    },
  ];
  selected: any;
  constructor(
    private Api: AllApiService,
    public datepipe: DatePipe,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.id = localStorage.getItem('OrgID');
    //this.getAthleteAttendance();
    this.getOrganisation();
    this.getTeamBySPorts();
    this.getEventTraining();
    this.length = this.attendenceList.length;
    // this.get()
  }
  dates: any;
  // get(){
  //   this.Api.get().subscribe(data =>{
  //     console.log(data);

  //   })
  // }
  Date: any;
  List: any = [];
  getAthleteAttendance() {
    this.headerList = [];
    this.List = [];
    this.Api.getAthleteAttendance(
      this.teamId,
      this.typeId,
      this.startDate,
      this.endDate
    ).subscribe((data: any) => {
      console.log(data);
      this.attendenceList = data;
      this.length = this.attendenceList.length;
      console.log(this.attendenceList);
      this.attendenceList[0].data.forEach((ele: any) => {
        console.log(ele);
        //  var rashid= Object.create({})

        this.Date = this.datepipe.transform(ele.dates, 'dd-MM-yyyy');
        // rashid.dates=this.datepipe.transform(ele.dates, 'dd-MM-yyyy');
        // rashid.isPresent=ele.isPresent
        console.log(this.Date);

        this.headerList.push(this.Date);
        // this.headerList.push(rashid)
      });
      this.attendenceList.forEach((ele1: any) => {
        var arr = [];

        ele1.data.forEach((ele3: any) => {
          arr.push(ele3);
        });
        this.List.push(arr);
      });
      console.log('header', this.headerList);
      console.log(this.List, 'liststststs');

      console.log('getallfilters', data);
    });
  }
  changeFileds(data: any) {
    this.Type = data;
    if (data == 'Events') {
      this.eventFiled = true;
      this.trainingFiled = false;
    } else if (data == 'Trainings') {
      this.trainingFiled = true;

      this.eventFiled = false;
    } else {
      this.trainingFiled = false;
      this.eventFiled = false;
    }
    this.getEventTraining();
  }

  getOrganisation() {
    this.Api.getAllOrganizationById(this.id).subscribe((data) => {
      this.orgData = data;
      console.log(this.orgData);
    });
  }

  sports(data: any) {
    this.category = data.target.value;

    this.getTeamBySPorts();
  }
  getTeamBySPorts() {
    this.Api.getTeamBySportsCat(this.id, this.category).subscribe(
      (data: any) => {
        // console.log('getteam', data);
        this.teamList = data;
      }
    );
  }
  changeTeam(data: any) {
    console.log('teamsssss', data);
    this.teamId = data;
    this.getEventTraining();
  }

  getEventTraining() {
    this.Api.getListEventTraining(this.teamId, this.Type).subscribe(
      (data: any) => {
        // console.log('getlistt', data);
        console.log(this.teamId, this.Type);
        this.eventList = data;
        this.trainingList = data;
      }
    );
  }
  typeId: any;
  changeEvent(data: any) {
    console.log('typeidddddd', data);
    this.typeId = data;
    //  this.getAthleteAttendance();
  }

  dateClick(data: any) {
    if (data == 'customDates') {
      this.dateField = true;
    } else {
      this.dateField = false;
    }
  }
  changeTraining(data: any) {
    console.log('typeid', data);
    this.typeId = data;
    //  this.getAthleteAttendance();
  }

  startDate: any;
  startConversionDate: any;
  startDateEvent(data: any) {
    this.startDate = this.datepipe.transform(data.target.value, 'yyyy-MM-dd');

    console.log(this.startDate);

    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 550);
    // this.getInjury();
    //  this.getAthleteAttendance();
  }
  endDate: any;
  convertDate: any;
  endDateEvent(data: any) {
    // this.endDate = new Date()
    console.log(this.endDate);
    this.endDate = this.datepipe.transform(data.target.value, 'yyyy-MM-dd');
    console.log(this.endDate);

    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 550);
    //  this.getAthleteAttendance();
    this.table = true;
  }

  onSearch() {
    this.attendenceList = [];
    this.getAthleteAttendance();
  }
  is = false;
  Add() {
    console.log(this.attendenceList);
    this.attendenceList.forEach((ele: any) => {
      ele.teamID = this.teamId;
      ele.createdDate = this.myDate;
      ele.typeId = this.typeId;
      ele.createdBy = this.orgName;
      delete ele.id;
      delete ele.isDeleted;
      ele.data.forEach((ele2: any) => {
        ele2.dates = ele2.dates.split('T')[0];
      });
      console.log(ele.data);
    });

    console.log(this.attendenceList);

    this.Api.addAttendance(this.attendenceList).subscribe((data: any) => {
      console.log('addattendance', data);
      this.attendenceList = [];
    });
    console.log(this.attendenceList);
  }
  sendingDate: any;
  dropSelect(data: any, i: any, j: any, hi: any) {
    this.selected = data;
    console.log(data, i, j, hi);
    this.sendingDate = j;
    console.log(this.sendingDate);
    let currentAttendenceList = this.attendenceList;
    currentAttendenceList[i].data[hi]['isPresent'] = (data=='false')?false:true;
    currentAttendenceList[i].updatedDate = this.datepipe.transform(new Date(), 'yyyy-MM-ddThh:mm:ss.217Z');
    currentAttendenceList[i].updatedBy=this.orgName;
    console.log(currentAttendenceList[i].data[hi]);
    this.attendenceList = currentAttendenceList;
    // this.attendenceList[i].data[hi].isPresent = data;
  }
  getdailyList(data: any = []) {
    let tempArray: any = [];

    for (let i = 0; i < this.headerList.length; i++) {
      for (let index = 0; index < data.data.length; index++) {
        const element = data.data[index];
        if (
          this.headerList[i] ==
          this.datepipe.transform(element.dates, 'dd-MM-yyyy')
        ) {
          return [element.isPresent, !element.isPresent];
        }
        // else break;
      }
    }
  }

  update(){
    console.log(this.attendenceList);
    this.Api.updateAttendance(this.attendenceList).subscribe((data:any)=>{
      console.log(data);
      this.attendenceList = [];
    })
  }
}
