import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { AttendenceComponent } from './attendence.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';


const routes: Route[] = [
  {
    path: '',
    component: AttendenceComponent
  }
];
@NgModule({
  declarations: [AttendenceComponent],
  imports: [
    CommonModule,
    EspSharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class AttendenceModule { }
