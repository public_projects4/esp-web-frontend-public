import { Component, OnInit } from '@angular/core';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import Swal from 'sweetalert2';
import countries from 'projects/esp-shared/src/lib/services/callingCodes.json'
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import {
  FormArray,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';
import { DatePipe } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-create-team-details',
  templateUrl: './create-team-details.component.html',
  styleUrls: ['./create-team-details.component.scss']
})
export class CreateTeamDetailsComponent implements OnInit {
  modelDetails :ModalCommanService []=[
    {
    modelName:'Coach'
  }
  ]
  dropdownList: any = [];
  selectedItems: any = [];
  dropdownSettings: any = [];
  countriesEnglish = [
    { item_id: 1, item_text: '+358' },
    { item_id: 2, item_text: '+355' },
    { item_id: 3, item_text: '+213' },
    { item_id: 4, item_text: '+1684' },
    { item_id: 5, item_text: '+376' },
    { item_id: 6, item_text: '+244' },
    { item_id: 7, item_text: '+355' },
    { item_id: 8, item_text: '+1264' },
    { item_id: 9, item_text: '+672' },
    { item_id: 10, item_text: '+1268' },
    { item_id: 11, item_text: '+54' },
    { item_id: 12, item_text: '+91' },
    { item_id: 13, item_text: '+65' },
    { item_id: 14, item_text: '+60' },
  ];
  separateDialCode = false;
	SearchCountryField = SearchCountryField;
	CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
	preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  counrtyList:{dial_code:string, name:string}[] = countries
  myReactiveForms: FormGroup = new FormGroup({});
  myCreateTeamForms: FormGroup = new FormGroup({});
  inviteAtheleteForm: FormGroup = new FormGroup({});
  phoneCode: any[] = []
  teamId: any = ''
  coaches: any[] = []
  athiid: any
  filteredCoach: any[] = []
  coachId = ''
  resId: any = ''
  orgId: any = ''
  secondBlock = false;
  firstBlock = true;
  byID: any;
  statusAthlete: any
  atheletById: any[] = []
  logedInName:any
  form1 :Boolean = true;
  form2 :Boolean = false;
  teamName :any;
  orgName : any;
  eventData: string;
  training = true;
  Event = false;

  constructor(private Api: AllApiService, private formBuilder: FormBuilder, private datepipe: DatePipe) {
    this.myReactiveForms = this.formBuilder.group({
      athleteName: '',
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      mobile: ['', [Validators.required]],
      coacheName: ['', [Validators.required]],
      mobileNumber: ['', [Validators.required]],
      image: 'assets/img/coache-avatar.png',
      updatedBy:''
    });
    this.myCreateTeamForms = this.formBuilder.group({
      teamName: ['', [Validators.required]],
      sportCategory: ['', [Validators.required]],
      teamSize: ['', [Validators.required]],
      coachId: ['', [Validators.required]],
      createdBy: ''
    });
    this.inviteAtheleteForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      mobileNo: ['', [Validators.required]],
      mobileNumber: ['', [Validators.required]],
      athleteName: ['', [Validators.required]],
      organizationId: '',
      teamId: '',
      createdBy: '',
      createdDate: '',
    });
  }
  ngOnInit(): void {
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.orgId = localStorage.getItem('OrgID')
    this.resId = localStorage.getItem('ID')
    this.teamId = localStorage.getItem('teamID')
    this.orgName = localStorage.getItem('Name');
    this.getOrgById()
    this.getAthlete()
    this.getCoaches()
    this.athleteByTeamId()
    this.getCallingCodes()
  }
  getCoaches() {
    this.orgId = localStorage.getItem('OrgID')
    this.Api
      .getAllCoachBySportsCategory(this.orgId)
      .subscribe((res: any) => {
        this.coaches = res;

      });
  }
  athleteByTeamId() {
    this.teamId = localStorage.getItem('teamID')
    this.Api
      .getAthleteByTeamId(this.teamId)
      .subscribe((res: any) => {

        this.atheletById = res;
      });
  }
  myFun() {
    this.firstBlock = true;
    this.secondBlock = false;
  }
  secondFun() {
    this.firstBlock = false;
    this.secondBlock = true;
  }
  getOrgById() {
    this.Api.getTeamById(this.teamId).subscribe((data: any) => {
      if (data) {
        this.byID = data;

        this.teamName = this.byID.teamName;
      }
    });
  }
  getAthlete() {
    this.Api.getAthleteStatus(this.teamId).subscribe((data) => {

      this.statusAthlete = data
    });
  }
  add: any =
    {
      "email": "",
      "mobile": "",
      "athleteName": "",
      "id": "",
      "createdBy": "",
      "updatedBy": "",
      "organizationId": "",
      "mobileNumber": "",
    }
    numCode:any
    onItemSelect(item: any) {

   this.numCode=item.item_text
    }
  onSubmit() {
    const data = this.inviteAtheleteForm.value;
    data.mobileNumber=this.numCode

    data['createdDate']= new Date(Date.now())
    const datatosend = {
      "email": data.email,
      "mobileNo": data.mobileNumber + '-' + data.mobileNo,
      "athleteName": data.athleteName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      "teamId": this.teamId,
      "organizationId": this.orgId,
      "createdBy": localStorage.getItem("Name"),
      createdDate: this.datepipe.transform(new Date(data.createdDate), 'yyyy-MM-dd'),
    }

    this.Api.inviteAthlete(datatosend).subscribe((data: any) => {
      this.athiid = data

      this.athleteByTeamId();
      window.location.href = window.location.href
    }
    , (err) => {
      this.getOrgById()
      this.getAthlete()
      this.getCoaches()
      this.athleteByTeamId()
      if (err.error.text === "Invited Succesfully") {
        $('#createdSuccessAthlete').modal('show');
        this.inviteAtheleteForm.reset({
          mobileNumber:''
        });
      }
      else if (err.error.text === "Entered data already Exists ") {
        $('#modelWarningAthlete').modal('show');
      }
      else {
        $('#modelError').modal('show');
      }


    }
    )
  }
  get f() { return this.myReactiveForms.controls; }
  get fa() { return this.inviteAtheleteForm.controls; }
  addDetails(sc: any) {
    const data = this.myReactiveForms.value;

    data.mobileNumber=this.numCode
    const update = {
      coacheName: data.coacheName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      email: data.email,
      mobile: data.mobileNumber + '-' + data.mobile,
      sportsCategory: sc,
      image: 'assets/img/coache-avatar.png',
      organisationId: this.orgId,
      createdBy: localStorage.getItem('Name')
    }

    this.Api
      .addCoaches(update)
      .subscribe((res: any) => {

        if (res.message == "Coach Exsits") {
          $('#modelWarning').modal('show');
        }
        else if (res.message == 'Coach Added Successfully') {
          $('#successModal').modal('show');
          this.getCoaches()
        }
        else {
          $('#modelError').modal('show');
        }
        this.myReactiveForms.reset()
      });
  }
  addCoach() {
    const data = this.myCreateTeamForms.value;

    const updateCoach = {
      organisationId: this.orgId,
      teamName: this.byID.teamName.replace(/\b\w/g, (l:any) => l.toUpperCase()) ,
      teamSize: this.byID.teamSize,
      sportCategory: this.byID.sportCategory,
      coachId: data.coachId,
      id: this.byID.id,
      createdDate: this.byID.createdDate,
      updatedBy: localStorage.getItem('Name')
    }

    this.Api.updateTeam(updateCoach)
      .subscribe((res: any) => {

        this.getOrgById()
      });
  }
  TeamsRecived: any[] = []
  getTeams() {
    this.Api
      .getAllTeam()
      .subscribe((res: any) => {

        this.TeamsRecived = res;
      });
  }
  keyPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z  ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  editTeam(data: any) {


    this.coaches.forEach((ele) => {
      if (data.sportCategory == ele.category) {
        this.filteredCoach = ele.coach

      }
    })
  }
  getCallingCodes() {
    this.Api
      .callingCodesApi()
      .subscribe((res: any) => {

        this.phoneCode = Object.values(res[0]);

      });
  }
  changePreferredCountries() {
		this.preferredCountries = [CountryISO.India, CountryISO.Canada];
	}
  keyPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  openTrainingModal(){
    this.form1 = true;
    this.form2 = false;
    $('#addTraingAndEvent').modal('show');
  }
  check(data:any){
    this.form1 = true;
    this.form2 = false;
    this.Event = false;
    this.training = true;
    console.log(data);
  }
  check1(data:any){
    this.form1 = false;
    this.form2 = true;
    this.training = false;
    this.Event = true;
    console.log(data)
  }
  public Training = {
    title: '',
    date: '',
    endDate:'',
    endtime: '',
    starttime: '',
    location: '',
    trainingDescription: '',
    id: '',
  };
  public Eventsec = {
    title: '',
    date: '',
    endDate :'',
    endtime: '',
    starttime: '',
    area: '',
    id: '',
  };
  saveForm(data: any) {

    const datatosend = {
      createdBy: this.orgName,
      startDate: this.datepipe.transform(data.date, 'yyyy-MM-ddThh:mm:ss.483Z'),
      endDate : this.datepipe.transform(data.endDate, 'yyyy-MM-ddThh:mm:ss.483Z'),
      trainingTitle: data.title,
      location: data.location,
      trainingDescription: data.trainingDescription,
      teamId: this.teamId,
      starttime: data.starttime,
      endtime: data.endtime,
    };

    this.Api.addTraining(datatosend).subscribe((data: any) => {
      if (data.message == 'Training Added Successfully') {
        $('#result').modal('show')
        this.byID.startDate = new Date(data.date);
        this.byID.endDate = new Date(data.endDate);
      }
    });
  }
  saveEvents(data: any) {
    this.eventData = this.datepipe.transform(data.date, 'dd-MM-yyyy');
    var endDate = this.datepipe.transform(data.endDate, 'dd-MM-yyyy');
    const postData = {
      title: data.title,
      startDate: this.eventData,
      endDate : endDate,
      endtime: this.tConv24(data.endtime),
      starttime: this.tConv24(data.starttime),
      area: data.area,
      teamId: this.teamId,
      createdBy: this.orgName,
    };

    this.Api.addEvents(postData).subscribe((data: any) => {
      if (data.message == 'Events Added Successfully') {
        $('#result').modal('show')
    };
  })
}
tConv24(time24) {
  var ts = time24;
  var H = +ts.substr(0, 2);
  var h:any = (H % 12) || 12;
  h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
  var ampm = H < 12 ? " AM" : " PM";
  ts = h + ts.substr(2, 3) + ampm;
  return ts;
};
}
