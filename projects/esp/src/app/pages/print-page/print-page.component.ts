import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-print-page',
  templateUrl: './print-page.component.html',
  styleUrls: ['./print-page.component.scss']
})
export class PrintPageComponent implements OnInit {

  receivedSubject:any;
  receivedDescription:any;
  receivedName:any;
  receivedDate:any;

  constructor() { }

  ngOnInit(): void {
    this.receivedSubject = localStorage.getItem('printSubject');
    this.receivedDescription = localStorage.getItem('printDescription');
    this.receivedName = localStorage.getItem('printName');
    this.receivedDate = localStorage.getItem('printDate');

    // window.print();

  }


}
