import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { Route,RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PrintPageComponent } from './print-page.component';


const routes: Route[] = [
  {
    path: '',
    component: PrintPageComponent
  }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    EspSharedModule,
    NgxSpinnerModule,
    RouterModule.forChild(routes)
  ]
})
export class PrintPageModule {
  // PrintPageComponent
 }
