import { Component,Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {
  FormArray,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';

import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';


import { EspPagesService } from '../../esp-pages.service';


declare var $: any;
@Component({
  selector: 'app-view-questionnaire',
  templateUrl: './view-questionnaire.component.html',
  styleUrls: ['./view-questionnaire.component.scss']
})
export class ViewQuestionnaireComponent implements OnInit,OnChanges {
  @Input() refresh:any


  modelDetails :ModalCommanService []=[
    {
    modelName:'Questionnaire'
  }
  ]



  assessmId:any
  questionsList:any
  test: any = [];
  demo: any = [];
  delId:any
ameen:any=[]
boxCount = 0
yesOrNo=false
arrayBox=false
  editProfileTabs: any;
  count:any;
  editQuestionnaireForm: FormGroup = new FormGroup({});



  constructor(private formBuilder: FormBuilder, private Api: AllApiService, private eventEmitterService: EspPagesService ) {
    this.editQuestionnaireForm = this.formBuilder.group({
      assessmentId: this.assessmId,
      question: ['', [Validators.required]],
      type: ['', [Validators.required]],
      typedataValue: ['', [Validators.required]],
      data: this.formBuilder.array([]) ,
    })
   }

  ngOnInit(): void {

    if (this.eventEmitterService.subsVar==undefined) {
      console.log('im view working ng');

              this.eventEmitterService.subsVar = this.eventEmitterService.
              invokeFirstComponentFunction.subscribe((name:string) => {
                this.viwQuestionsById();
              });
            }

    this.assessmId = localStorage.getItem('QuesAssmentId');
    this.viwQuestionsById()
    this.count = localStorage.getItem('count')


  }

  ngOnChanges(changes:SimpleChanges): void{
    this.refresh;
    console.log(this.refresh);

    this.viwQuestionsById()
    this.count;

  //  if(true){
  //   if (this.eventEmitterService.subsVar==undefined) {
  //     console.log('im view working');

  //     this.eventEmitterService.subsVar = this.eventEmitterService.
  //     invokeFirstComponentFunction.subscribe((name:string) => {
  //       this.viwQuestionsById();
  //     });
  //   }
  //  }

  }


  get f() { return this.editQuestionnaireForm.controls; }


  get addInputs() : FormArray {
    return this.editQuestionnaireForm.get("data") as FormArray
  }

  newInput(): FormGroup {
    return this.formBuilder.group({
      value: '',
      name: '',
      selectList: [false],
    })
 }

 addSkills() {
  this.addInputs.push(this.newInput());
}



removeSkill(i:number) {
  this.addInputs.removeAt(i);
}



  selectedOption(data: any) {
    const val = data.target.value

    if(val=='yesOrNo'){
      this.yesOrNo = true
      this.arrayBox = false

      let a: any = this.editQuestionnaireForm.controls['data'];
    a['controls'].patchValue({
      name: 'yes',
      value: 1
    });
    }
    else{
      this.yesOrNo = false
      this.arrayBox = true
      this.boxCount++

      if(this.boxCount == 1){
        this.yesOrNo = false
        this.arrayBox = true
        this.addSkills()
      }
    }
  }

  viwQuestionsById(){
    this.Api.getQuestionsById(this.assessmId).subscribe((res:any) => {
      console.log('viewQuestinsList', res);
      this.questionsList =res
    })
  }




  editQues(data:any){
    console.log(data);
    this.test = data.data;
    this.ameen = []
    this.demo =[]

    this.test.forEach((element: any) => {
      console.log(element.fileName);
      this.demo.push(element);
    });

    console.log(this.demo);

    this.demo.forEach((obj:any) => {
      this.ameen.push(
      this.formBuilder.group({
      value: obj.value,
      name: obj.name,
      })
    );
  })


    this.editQuestionnaireForm = this.formBuilder.group({
      assessmentId: data.assessmentId,
      id:data.id,
      question: data.question,
      type: data.type,
      typedataValue: data.typedataValue,
      data: this.formBuilder.array(this.ameen) ,
    })

    console.log(this.editQuestionnaireForm.controls['typedataValue'].value);

    var conditions =this.editQuestionnaireForm.controls['typedataValue'].value

    if(conditions == "yesOrNo"){

      this.yesOrNo = true
      this.arrayBox = false
    }
    else{
      this.yesOrNo = false
      this.arrayBox = true
    }
  }


  onUpdate(){
    const data =this.editQuestionnaireForm.value
    console.log('beforeApi',data);


    this.Api.updateQuestionsById(data).subscribe( (res:any) =>{
      console.log('updated',res);

      if(res.message == "Questions Updated Successfully"){
        $('#updateModal').modal('show');
        this.viwQuestionsById()
      }
      else{
        $('#modelError').modal('show');
      }

    })

  }

  getQueListId(id:any){
    this.delId = id
  }

  deleteQueList(){
    this.Api.deleteQuessionById(this.delId).subscribe( (res:any) =>{
      console.log('deletedId',res);

      if(res.message == 'Questions Deleted Successfully'){
        $('#successfullyDeletedModal').modal('show');
        this.viwQuestionsById()
        console.log('deleted 1');
      }
      else{
        $('#modelError').modal('show');
      }
    }
    )
  }
}
