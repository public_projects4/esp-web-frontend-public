import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
declare var $: any;
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  registerForm: FormGroup = new FormGroup({});
  submitted: boolean = false;
  returnUrl = '/home/login';
  constructor(private formBuilder: FormBuilder, private Api: AllApiService, private routes: Router) { }
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    });
    setInterval(() => {
     }, 1000);
  }
  get f() { return this.registerForm.controls; }
  success=false;
  warning=false;
  somethingWrong=false;
  Email:any
  onSubmit() {
    const data = this.registerForm.value
this.Email=data.email
    this.Api
      .forgetPassword(data.email)
      .subscribe((data: any) => {

        if (data.message == "OTP sent successfully") {
            $('#successOtpSend').modal('show');
            this.success=true;
  this.warning=false;
  this.somethingWrong=false;
        }
        else if (data.message == "Failed to send the OTP Please Check your Email"){
          $('#errorLoginAdmin').modal('show');
          this.success=false;
  this.warning=true;
  this.somethingWrong=false;
        }
        else{
          $('#errorLoginAdmin').modal('show');
          this.success=false;
  this.warning=false;
  this.somethingWrong=true;
        }
      });
  }
}
