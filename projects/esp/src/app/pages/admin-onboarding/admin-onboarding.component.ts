import { DatePipe } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { PostcodeService } from 'projects/esp-shared/src/lib/services/postcode.service';
import countries from 'projects/esp-shared/src/lib/services/callingCodes.json';
@Component({
  selector: 'app-admin-onboarding',
  templateUrl: './admin-onboarding.component.html',
  styleUrls: ['./admin-onboarding.component.scss']
})
export class AdminOnboardingComponent implements OnInit {
  coachId: any;
  Image: any;
  url: any = 'assets/img/imgPlaceholder.svg';
  demo = 'rashid';
  returnUrl = '/home/dashboard';
  myDate = this.datepipe.transform(new Date(), 'yyyy-MM-ddT00:00:46.217Z');
  orgId: any;
  orgName: any;
  orgData:any=[];
  zipcode = '';
  state = '';
  countryCode: any;
  OnboardProfile= '';
  city = '';
  addressOne = '';
  addressTwo = '';
  coachForm: FormGroup = new FormGroup({});
  constructor(
    private formBuilder: FormBuilder,
    private postservice: PostcodeService,
    private datepipe: DatePipe,
    private api: AllApiService,
    private routes: Router
  ) {
    this.coachForm = this.formBuilder.group({
      emailAddress:  [localStorage.getItem('onboard-Email'), [Validators.required]],
      addressline1: ['', [Validators.required]],
      addressline2: ['', [Validators.required]],
      image: '',
      sportsCategory: '',
      subDomain: '',
      onBoardedDate: '',
      expiryDate: '',
      status: '',
      zipCode: ['', [Validators.required]],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      countryCode: ['', [Validators.required]],
      id: localStorage.getItem('onboard-ID'),
      createdBy: sessionStorage.getItem('onboard-createdBy'),
      createdDate: this.myDate,
      updatedBy: '',
      updatedDate: this.myDate,
      organisationName:  [localStorage.getItem('onboard-Names'), [Validators.required]],
    });
  }
  ngOnInit(): void {
    this.orgId = localStorage.getItem('onboard-ID');
    this.OnboardProfile = localStorage.getItem('onboard-profilePic');
    this.api.getOrgById(this.orgId).subscribe((data: any) => {
     this.orgData = data;
     this.city = data.city
     this.zipcode = data.zipCode
     this.addressOne = data.addressline1
     this.addressTwo = data.addressline2
     this.state = data.state
     this.countryCode = data.countryCode
    });
  }
  get f() {
    return this.coachForm.controls;
  }
  counrtyList: { dial_code: string; code: string; name: string }[] = countries;
  getZip(){
    this.api.getZipCodeBase(this.zipcode).subscribe((data:any) => {
      let arr1 =data.results[this.zipcode][0]
      this.counrtyList.forEach((element:any) => {
        if(arr1.country_code == element.code){
           this.state = arr1.state;
      this.city = arr1.city;
          this.countryCode = element.dial_code
         }
      } )
    })
  }
  zipcodee = () => {
    this.zipcode.length > 4 ? this.getZip() : '';
  };
  getZipcode() {
    this.postservice.getPost(this.zipcode).subscribe((res: any) => {
      this.state = res[0].PostOffice[0].State;
      this.city = res[0].PostOffice[0].District;
    });
  }
  fileToUpload: any = null;
  onSelectFile(file: FileList) {
    this.OnboardProfile =''
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.url = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
    this.api
      .uploadAdminImage(this.orgName, this.coachId, this.fileToUpload)
      .subscribe((data: any) => {
if(data.message=="Select correct file format"){
  this.url='assets/img/imgPlaceholder.svg'
alert('please select valid file')
}
else{
  this.Image = data.message;
 this.coachForm.patchValue({
   image:data.message
 })
}
      });
  }
  submitCoach() {
    let data = this.coachForm.value;
    data['updatedBy']=data.organisationName
    const dataToUpload = {
      emailAddress: data.emailAddress,
      addressline1: data.addressline1,
      addressline2: data.addressline2,
      image: data.image,
      sportsCategory: this.orgData.sportsCategory,
      subDomain: this.orgData.subDomain,
      onBoardedDate: this.orgData.onBoardedDate,
      expiryDate: this.orgData.expiryDate,
      status: this.orgData.status,
      zipCode: data.zipCode,
      city: data.city,
      state: data.state,
      countryCode: data.countryCode,
      id: data.id,
      createdBy: data.createdBy,
      createdDate: data.createdDate,
      updatedBy: this.orgData.updatedBy,
      updatedDate: data.updatedDate,
      organisationName: data.organisationName,
    }
    this.api.addAdminOnboard(dataToUpload).subscribe((data: any) => {
      if (data) {
        this.routes.navigate([this.returnUrl]);
        this.coachForm.reset()
      }
    });
  }
  keyPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9 a-zA-Z @",'-_#$%&*   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  // keyPressAlphaNumerics(event: any) {
  //   var inp = String.fromCharCode(event.keyCode);
  //   if (/[0-9   ]/.test(inp)) {
  //     return true;
  //   } else {
  //     event.preventDefault();
  //     return false;
  //   }
  // }
  keysPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9 a-zA-Z  ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z  ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphaNumericS(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9/]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
}
