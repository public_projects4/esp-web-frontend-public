import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';
import { AdminOnboardingComponent } from './admin-onboarding.component';

const routes: Route[] = [
  {
    path: '',
    component: AdminOnboardingComponent
  }
];

@NgModule({
  declarations: [AdminOnboardingComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class AdminOnboardingModule { }
