import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';

declare var $: any;



@Component({
  selector: 'app-athlete-message',
  templateUrl: './athlete-message.component.html',
  styleUrls: ['./athlete-message.component.scss']
})
export class AthleteMessageComponent implements OnInit {

  pushedData  :any =[];
  arrPush:string='';
  replayUploadedNewMsgBtn:boolean = false;
  replayFileUploadEnabled = false
  file: any;
  description: any;
  hideUploadNewMsgBlock = false;
  fileName:any;
  pushedDataNewMsg  :any =[];
  fileUploadEnabled = false;
  secondBlock = false;
  firstBlock = true;
  fileNameCoach:any;
  pushedDataCoach  :any =[];
  uploadedFile: string = '';
  uploadedNewMsgBtn: boolean =true;
  subject: string = '';
  sentButton: Boolean = false;
  isEnableMsgHistory: boolean = false;
  replyMessageData :any;
  fileTypeName: any = '';


  superAdminNewMsgForm: FormGroup = new FormGroup({});


















  constructor(private Api: AllApiService, private formBuilder: FormBuilder) {
    this.superAdminNewMsgForm = this.formBuilder.group({
      recieverId: ['', [Validators.required]],
      description: ['', [Validators.required]],
      subject: ['', [Validators.required]],
      senderId: '',
      attachment: this.formBuilder.array([]),

    });
  }

  ngOnInit(): void {

  }


//   this.Api.uploadMessageFile(this.orgName, receiverId, this.testData).subscribe(result => {
//     this.uploadedFile = result;
//     this.uploadStatus = 'Successfully Uploaded';
// this.hideUploadNewMsgBlock = false;
// this.fileUploadEnabled = false;


//     this.pushDataNewMsg()
//     this.pushDataCoach()
//     this.pushData()
//   }

  get f() {
    return this.superAdminNewMsgForm.controls;
  }

  newSkill(): FormGroup {
    return this.formBuilder.group({
      file: '',
      fileNameToDisplay: '',
      btnUpload: [false],
      cancelBtn: [false],
    });
  }

  addSkills() {
    this.abc.push(this.newSkill());
  }

  removeSkill(i: number) {
    this.abc.removeAt(i);
  }

  get abc(): FormArray {
    return this.superAdminNewMsgForm.get('attachment') as FormArray;
  }



  uploadFileApi = (files: any, i: any) => {

    console.log(files);

    console.log(files[0].name);



    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);

    this.Api.uploadMessageFile("61654db8985824ac061823b1", '616551d6217c6d98238fd958', formData).subscribe((data: any) => {
      console.log('upload treatmernt con', data);



      this.fileTypeName = data;

      let a: any = this.superAdminNewMsgForm.controls['attachment'];

      a['controls'][i].patchValue({
        file: this.fileTypeName,
        btnUpload: true,
        cancelBtn:true
      });
      console.log('this.upFileName', this.fileTypeName);

      a['controls'][i].patchValue({
        fileNameToDisplay:files[0].name
      });


    });




  };




  cancelArray(i:any) {
    let a: any = this.superAdminNewMsgForm.controls['attachment'];
    console.log('this.upFileName', this.fileTypeName);
        a['controls'][i].patchValue({
          fileNameToDisplay: [''],
          btnUpload: false,
          cancelBtn:false
        });


  }

  // clicked(){

  // }


  onClickReply(data:any) {
    this.resetFormData();

    this.replyMessageData = data[0];
    console.log(this.replyMessageData);
    $('#replyModal').modal('show');
  }



  onClickBackToList() {
    this.isEnableMsgHistory = false;
  }

  showSentMessages(e: any) {
    this.sentButton = true;
    this.isEnableMsgHistory = false;
  }

  showInbox(e: any) {
    this.sentButton = false;
    this.isEnableMsgHistory = false;

  }


  myFun() {
    this.firstBlock = true;
    this.secondBlock = false;
    this.resetFormData();
  }

  secondFun() {
    this.firstBlock = false;
    this.secondBlock = true;
    this.resetFormData();
  }

  onClickNewMessage() {
    this.firstBlock = true;
    this.secondBlock = false;
    this.resetFormData();
    // this.isDelete = false;
    $('#newMessageModal').modal('show');


  }

  uploadFilePinCoach = (files: any) => {


    this.fileUploadEnabled = true;


    console.log(files);

    console.log(files[0].name);



    let fileToUploadCoach = <File>files[0];
    console.log(fileToUploadCoach.name);

    this.file = fileToUploadCoach
    this.fileNameCoach = fileToUploadCoach.name

    console.log(this.file);
  }

  uploadFilePin = (files: any) => {

    this.hideUploadNewMsgBlock = true;


    console.log(files);

    console.log(files[0].name);



    let fileToUpload = <File>files[0];
    console.log(fileToUpload.name);

    this.file = fileToUpload
    this.fileName = fileToUpload.name

    console.log(this.file);
  }

  pushDataNewMsg(){
    this.pushedDataNewMsg.push(this.fileName)

    console.log(this.pushedDataNewMsg);
    this.fileName = '';

    // this.replayUploadedNewMsgBtn = false;

  }


  uploadFile() {
    console.log('its working');

    var receiverId = '';

    if (this.file != undefined) {
      console.log(this.file);
      console.log(this.file.name);

      // this.testData.append('file', this.file, this.file.name);
      // console.log(this.testData);

      // this.Api.uploadMessageFile(this.orgName, receiverId, this.testData).subscribe(result => {
      //   this.uploadedFile = result;
      //   this.uploadStatus = 'Successfully Uploaded';
  this.hideUploadNewMsgBlock = false;
  this.fileUploadEnabled = false;


        this.pushDataNewMsg()
        this.pushDataCoach()
        this.pushData()
      // },
      //   error => {
      //     this.uploadStatus = JSON.parse(error.error).message;
      //   })
      // setTimeout(() => {
      //   this.uploadStatus = '';
      // }, 5000);
    }
  }


  testUploadFilePin = (files: any) => {

    this.replayUploadedNewMsgBtn = true;


    console.log(files);

    console.log(files[0].name);
    this.replayFileUploadEnabled= true;



    let fileToUpload = <File>files[0];
    console.log(fileToUpload.name);
    this.file = fileToUpload

    this.arrPush = fileToUpload.name

    console.log( this.arrPush);



  }

  resetFormData() {
    this.pushedData = [];
    this.pushedDataNewMsg = [];
    this.pushedDataCoach = []


    this.hideUploadNewMsgBlock = false;
    this.fileName =''
    this.uploadedFile = ''




  }

  pushData(){
    this.pushedData.push(this.arrPush)

    console.log(this.pushedData);
    this.arrPush = '';

    this.replayUploadedNewMsgBtn = false;

  }

  cancelReplayField(){
    this.arrPush = '';
    this.replayUploadedNewMsgBtn = false;

  }

  cancelReplay(i:any){
    console.log(i);
  this.pushedData.splice(i, 1);
  }

  sendMessage(){
    const data = this.superAdminNewMsgForm.value;
    console.log(data);
    this.pushedData = [];
    this.pushedDataNewMsg = [];

  this.hideUploadNewMsgBlock = false;


  }

  cancel() {
    this.file =''
    this.fileName =''
   this.uploadedNewMsgBtn = false;
    this.uploadedFile = '';

  }

  cancelNewMsgUpload(i:any){
    console.log(i);
  this.pushedDataNewMsg.splice(i, 1);
  }

  pushDataCoach(){
    this.pushedDataCoach.push(this.fileNameCoach)

    console.log(this.pushedData);
    this.fileNameCoach = '';

    this.fileUploadEnabled = false;

  }

  cancelCoachData() {
    this.fileUploadEnabled = false;
    this.fileNameCoach = '';
    this.file = '';
   }

   cancelCoach(i:any){
    console.log(i);
  this.pushedDataCoach.splice(i, 1);
  }

}
