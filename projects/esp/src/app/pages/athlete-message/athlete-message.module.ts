import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AthleteMessageComponent } from './athlete-message.component';



const routes: Route[] = [
  {
    path: '',
    component: AthleteMessageComponent
  }
];


@NgModule({
  declarations: [
    AthleteMessageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EspSharedModule,
    FormsModule,

    RouterModule.forChild(routes)
  ],
})
export class AthleteMessageModule { }
