import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { AtheleteDataComponent } from './athelete-data.component';
import { TreatmentInformationFormComponent } from '../treatment-information-form/treatment-information-form.component';
import { AtheleteInjurydetailsComponent } from '../athelete-injurydetails/athelete-injurydetails.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AtheletePrescriptionsComponent } from '../athelete-prescriptions/athelete-prescriptions.component';
import { AtheleteVaccinationComponent } from '../athelete-vaccination/athelete-vaccination.component';
import { AtheleteMedicalHistoryComponent } from '../athelete-medical-history/athelete-medical-history.component';
import { NgxSpinnerModule } from 'ngx-spinner';
const routes: Route[] = [
  {
    path: '',
    component: AtheleteDataComponent
  }
];
@NgModule({
  declarations: [
    AtheleteDataComponent,
    TreatmentInformationFormComponent,
    AtheleteInjurydetailsComponent,
    AtheletePrescriptionsComponent,
    AtheleteVaccinationComponent,
    AtheleteMedicalHistoryComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    EspSharedModule,
    NgxSpinnerModule,
    RouterModule.forChild(routes)
  ]
})
export class AtheleteDataModule { }
