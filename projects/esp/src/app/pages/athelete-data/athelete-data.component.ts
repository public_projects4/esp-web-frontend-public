import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { th } from 'date-fns/locale';
import { jsPDF } from 'jspdf';
import { NgxSpinnerService } from 'ngx-spinner';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
declare var $: any;
@Component({
  selector: 'app-athelete-data',
  templateUrl: './athelete-data.component.html',
  styleUrls: ['./athelete-data.component.scss'],
})
export class AtheleteDataComponent implements OnInit {
  isCreate=true
  url:any
  isView=true
  isDelete=true
  isEdit=true
  uploadButton = '#f79b3e';
  After = false;
  Before = true;
  myForm: FormGroup = new FormGroup({});
  Cancel = false;
  Demo: any = [];
  AtheleteId: any;
  data: any;
  fileNames: any;
  tests: any;
  getAthid: any;
  t: any = [];
  r: any;
  roleId = localStorage.getItem('RoleId');
  documentNames: any;
  dropdown = false;
  dropdowns = true;
  doc: any;
  test: any;
  emails: any;
  mobileNo: any;
  athleteName: any;
  atheId: string = '';
  Uploaded = false;
  Delete = false;
  Success = false;
  Deleted = false;
  upload: any;
  file = false;
  deleted: any;
  delete = true;
  added: any;
  success: any;
  atheleteDocumentList: any = [];
  DocumentList: any = [];
  res: any;
  arrayList: any = [];
  URL: any;
  Category: any;
  sportsCategory: any = [];
  athlete: any[] = [];
  selectedAthelete: any = {};
  id: any;
  orgData: any = [];
  atheleteId: any;
  orgName: any;
  demoList: any = [];
  testList: any = [];
  itemsList:any=[]
  demos: any;
  status: any;
  public Athelete = {
    documentTitle: '',
    documentDescription: '',
  };
  constructor(
    private Api: AllApiService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService
  ) {
    this.myForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
  }
  getRoleId:any
  ngOnInit(): void {
    this.atheleteDocumentList = [];
    this.id = localStorage.getItem('OrgID');
    this.orgName = localStorage.getItem('Name');
    this.getOrganisation();
    this.getSports();
    this.getAthelete();
    this.DocumentList = [];
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();
  }
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {
      this.itemsList = data;
      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Athlete Data') {
          if (ele.isDelete == true) {
            this.isDelete = true;
          } else {
            this.isDelete = false;
          }
          if (ele.isEdit == true) {
            this.isEdit = true;
          } else {
            this.isEdit = false;
          }
          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }
          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
          }
        }
      });
    });
  }
  get f() {
    return this.myForm.controls;
  }
  rashid: any = [];
  ameen: any = [];
  ruman: any = [];
  getAthelete() {
    this.spinner.show();
    this.Api.getAtheleteDocument(this.atheId).subscribe((data: any) => {
      this.atheleteDocumentList = data;
      this.spinner.hide();
    });
  }
  getOrganisation() {
    this.Api.getAllOrganizationById(this.id).subscribe((data) => {
      this.orgData = data;
    });
  }
  inviteAthelete(data: any) {
    if (data.target.value == '0') {
      this.atheleteDocumentList = [];
    } else {
      this.atheId = data.target.value;
      this.getAthelete();
      var selectedInviteAthelete = data.target.value;
      this.athlete.forEach((element) => {
        if (selectedInviteAthelete == element.id) {
          this.URL=element.profilepic
          this.selectedAthelete = element;
          this.atheleteId = element.id;
          this.emails = element.email;
          this.athleteName = element.athleteName;
          this.mobileNo = element.mobileNo;
          this.status = element.status;
        }
      });
    }
  }
  sports(data: any) {
    this.Category = data.target.value;
    if (
      data.target.value == 'Cricket' ||
      data.target.value == 'FootBall' ||
      data.target.value == 'Tennis' ||
      data.target.value == 'nsd uunnn pff'
    ) {
      this.mobileNo = '';
      this.athleteName = '';
      this.emails = '';
      this.status = '';
    }
    this.getSports();
  }
  getSports() {
    this.Api.getSportsByCat(this.Category, this.id).subscribe((data) => {
      this.athlete = data;
    });
  }
  onSelectFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.URL = reader.result;
      };
    }
  }
  Save(data: any) {
    const dataToSend = {
      documentTitle: data.documentTitle,
      documentDescription: data.documentDescription,
      athleteId: this.atheleteId,
      roleId: this.roleId,
      documents: this.arrayList,
    };
    this.Api.addAtheleteDocument(dataToSend).subscribe((data: any) => {
      if (data.message == 'Document Uploaded Successfully') {
        $('#Results').modal('show');
        this.Success = true;
        this.added = true;
        this.Deleted = false;
        this.Delete = false;
        this.Uploaded = false;
        this.uploadButton='#f79b3e'
        this.getSports();
        this.getOrganisation();
        this.orgData = [];
        this.documentNames = '';
      }
      this.getAthelete();
      this.Athelete = {
        documentTitle: '',
        documentDescription: '',
      };
      this.Cancel = false;
      this.myForm.reset();
      this.arrayList = [];
      this.Before = true;
      this.After = false;
      this.Cancel = false;
    });
  }
  uploadFile = (files: any) => {
    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    this.Api.uploadFile(this.orgName, this.atheleteId, formData).subscribe(
      (data: any) => {
        if (data) {
          this.Cancel = true;
          this.After = true;
          this.uploadButton = '#207e04';
          this.Before = false;
        }
        this.getAthelete();
        this.Cancel = true;
        this.fileNames = data;
        this.res = data.split('.');
        this.documentNames = this.res;
        this.arrayList.push({
          documentType: this.res[1],
          fileName: this.res[0] + '.' + this.res[1],
        });
      }
    );
  };
  click(data: any) {
    this.AtheleteId = data;
  }
  deleteCard() {
    this.Api.deleteDocument(this.AtheleteId).subscribe((data: any) => {
      this.getAthelete();
      if (data) {
        $('#Results').modal('show');
        this.Success = true;
        this.added = false;
        this.Deleted = true;
        this.Delete = false;
      }
    });
  }
  Check(data: any) {
    this.getAthid = data.athleteId;
    this.t = data.documents;
    this.t.forEach((element: any) => {
      this.r = element.fileName;
    });
    this.Api.downloadDocument(this.r, this.orgName, this.getAthid).subscribe(
      (data: any) => {
        if (data) {
        window.open(
          data
        )
        }
      }
    );
  }
  EXCEL() {
  }
  PDF() {}
  cancel() {
    this.documentNames = '';
    this.After = false;
    this.Before = true;
    this.Cancel = false;
    this.uploadButton = '#f79b3e';
    this.arrayList = [];
  }
  getColumnContent(col: any) {
    return col[Object.keys(col)[0]];
  }
}
