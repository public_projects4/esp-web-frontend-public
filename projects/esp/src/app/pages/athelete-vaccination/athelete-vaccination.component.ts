import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { FormGroup, FormControl,FormArray, FormBuilder, Validators } from '@angular/forms'
import Swal from 'sweetalert2';
import { jsPDF } from 'jspdf';
import { NgxSpinnerService } from 'ngx-spinner';
import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';
export interface data {
  columnName: string;
  value: string;
}
declare var $: any;
@Component({
  selector: 'app-athelete-vaccination',
  templateUrl: './athelete-vaccination.component.html',
  styleUrls: ['./athelete-vaccination.component.scss']
})
export class AtheleteVaccinationComponent implements OnInit ,OnChanges{
isCreate=true
isView=true
isDelete=true
isEdit=true
  modelDetails :ModalCommanService []=[
    {
    modelName:'Vaccination',
  }
  ]
  @Input() athID:any;
  formats: data[] = [
    {
      columnName: 'EXCEL',
      value: 'EXCEL',
    },
    {
      columnName: 'PDF',
      value: 'PDF',
    }
  ];
  pdf:any
  getRoleId:any
  itemsList: any = [];
  excel:any
  myVaccinationForm: FormGroup = new FormGroup({});
  added=false
  vaccination:any[]=[]
  practitioner:any[]=[]
  pid:string=''
  update=false
  deleted=true
  orgId: any = ''
  constructor( private Api: AllApiService, private formBuilder: FormBuilder,private spinner: NgxSpinnerService,) {
    this.myVaccinationForm = this.formBuilder.group({
      vaccinationName: ['', [Validators.required]],
      practionerId: ['', [Validators.required]],
      vaccinationDate: ['', [Validators.required]],
      quantity: ['', [Validators.required]],
      description: ['', [Validators.required]],
      athleteId: '',
      createdBy: '',
      updatedBy: '',
    });
    }
  ngOnInit(): void {
    this.getPractitioner()
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();
  }
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {

      this.itemsList = data;

      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Athlete Data') {
          if (ele.isDelete == true) {
            this.isDelete = true;
          } else {
            this.isDelete = false;
          }
          if (ele.isEdit == true) {
            this.isEdit = true;
          } else {
            this.isEdit = false;
          }
          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }
          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
          }
        }
      });
    });
  }
  ngOnChanges(changes:SimpleChanges) :void {
    this.getAllVaccination()
  }
  get f() { return this.myVaccinationForm.controls; }
  Type:any
  opens(data:any){
this.Type=data.target.value

  }
 getPractitioner() {
    this.orgId = localStorage.getItem('OrgID')
    this.Api.getPractitionerByOrganisationId(this.orgId)
    .subscribe((data:any)=>{
      this.practitioner=data
    })
  }
  passData(data:any){
    this.pid=data.target.value
  }
  Id:any
  download(data:any){
    this.Id=data
      }
  Name="VACCINATION"
  openPage(){
    this.getAllVaccination()
    this.Api.downloadOrganization(this.Id,this.Type,this.Name).subscribe((data: any) => {

      if(data){
window.location.href=data
      }
    });
  }
  onSubmit() {
    const data = this.myVaccinationForm.value;
    const datatosend={
      vaccinationName: data.vaccinationName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      practionerId: this.pid,
      vaccinationDate: data.vaccinationDate,
      quantity: data.quantity,
      description: data.description.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      athleteId: this.athID,
      createdBy: localStorage.getItem('Name')
    }
    this.Api.addVaccination(datatosend)
    .subscribe((data:any)=>{

      if(data.message === "Vaccination created successfully"){

        $("#addVaccination").modal('show');
}
else{
  $("#modelError").modal('show');
}
      this.myVaccinationForm.reset();
      this.getAllVaccination();
this.spinner.hide();
    }
     )
  }
  getAllVaccination() {
this.spinner.show()
    this.Api.getVaccinationById(this.athID)
    .subscribe((data:any)=>{

      this.vaccination=data
this.spinner.hide()
    })
  }
  onEdit(){
    const edit = this.myVaccinationForm.value;
    const datatoupdate={
      vaccinationName: edit.vaccinationName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      practionerId: edit.practionerId,
      vaccinationDate: edit.vaccinationDate,
      quantity: edit.quantity,
      description: edit.description.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      id:edit.id,
      athleteId:this.athID,
      createdBy: edit.createdBy,
      updatedBy: localStorage.getItem('Name')
    }

this.spinner.show()
    this.Api.updateVaccination(datatoupdate)
    .subscribe((data:any)=>{

      this.myVaccinationForm.reset()
      this.getAllVaccination()
      if(data.message == "Vaccination updated successfully"){
          $("#updateVaccination").modal('show');
      }
      else{
  $("#modelError").modal('show');
      }
this.spinner.hide()
    })
  }
  editPres(data:any){

    this.myVaccinationForm = this.formBuilder.group({
      vaccinationName: data.vaccinationName,
      practionerId: data.practionerId,
      vaccinationDate: data.vaccinationDate,
      quantity: data.quantity,
      description: data.description,
      athleteId: this.athID,
      id:data.id,
      createdBy: data.createdBy
    });
  }
  Delete(){
    this.Api.deleteVaccination(this.deleteid).subscribe((data: any) => {

      if(data){
        $("#deleteVaccination").modal('show');
       }
        this.getAllVaccination()
      });
  }
  deleteid:any
  deletePres(data:any){
this.deleteid=data
  }
  EXCEL(){
    this.excel="EXCEL"

     this.Api.downloadOrganization(this.Id,this.excel,this.Name).subscribe((data: any) => {

          if(data){
    window.location.href=data
          }
        });
      }
      keyPressAlphaNumeric(event: any) {
        var inp = String.fromCharCode(event.keyCode);
        if (/[a-zA-Z  ]/.test(inp)) {
          let name1: string = this.myVaccinationForm.value.organisationName;
          name1 && name1.length > 0 ? this.myVaccinationForm.patchValue({
            organisationName: name1[0].toUpperCase() + name1.substring(1)
          }) : '';
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
      keyPressAlphaNumerics(event: any) {
        var inp = String.fromCharCode(event.keyCode);
        if (/[0-9   ]/.test(inp)) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
      check(data: any) {

        this.pdf = data;
      }
      PDF() {

        const doc = new jsPDF();
        doc.text(`\n vaccinationName : ${this.pdf.vaccinationName}`, 5, 17);
        doc.text(`\n description : ${this.pdf.description}`, 5, 27);
        doc.text(`\n quantity : ${this.pdf.quantity}`, 5, 37);
        doc.text(`\n vaccinationDate : ${this.pdf.vaccinationDate}`, 5, 47);
        doc.save('Vaccination.pdf');
      }
}
