import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreatePractitionerComponent } from './create-practitioner.component';
import { NgxSpinnerModule } from 'ngx-spinner';



const routes: Route[] = [
  {
    path: '',
    component: CreatePractitionerComponent
  }
];

@NgModule({
  declarations: [CreatePractitionerComponent],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule,
    RouterModule.forChild(routes)
  ]
})
export class CreatePractitionerModule { }
