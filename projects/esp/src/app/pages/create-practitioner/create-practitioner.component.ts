import { Component, OnInit } from '@angular/core';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { FormGroup, FormControl,FormArray, FormBuilder,
  Validators } from '@angular/forms'
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-create-practitioner',
  templateUrl: './create-practitioner.component.html',
  styleUrls: ['./create-practitioner.component.scss']
})
export class CreatePractitionerComponent implements OnInit {
isCreate=true
isView=true
  myPractitionerForm: FormGroup = new FormGroup({});
  orgId:any=''
  phoneCode:any[]=[]
  mobileNumber:string=''

  constructor(private Api: AllApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private datepipe: DatePipe) {
    this.myPractitionerForm = this.formBuilder.group({
      practitionerName: ['', [Validators.required]],
      practitionerRole: ['', [Validators.required]],
      contactNo: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      practiceName: ['', [Validators.required]],
      address: ['', [Validators.required]],
      orgainzationId: '',
      image:'assets/img/coache-avatar.png',
      mobileNumber:'',
      createdBy: '',
      createdDate: '',
    });

  }
  getRoleId:any
  ngOnInit(): void {
    this.orgId=localStorage.getItem('OrgID')
    console.log(this.orgId);

    this.getCallingCodes()
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();

  }
  itemsList:any=[]
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {
      console.log('sidebar', data);
      this.itemsList = data;
      console.log(this.itemsList);
      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Manage Practitioner') {
         

        

          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }

          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
          }
        }
      });
    });
  }
  get f() { return this.myPractitionerForm.controls; }

  onSubmit() {
    const data = this.myPractitionerForm.value;
    data['createdDate']= new Date(Date.now())


    this.orgId=localStorage.getItem('OrgID')
const datatosend = {
  practitionerName: data.practitionerName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
  practitionerRole: data.practitionerRole.replace(/\b\w/g, (l:any) => l.toUpperCase()),
  contactNo: data.contactNo,
  // mobile: data.mobileNumber + '-'+ data.mobile,
  email: data.email,
  practiceName: data.practiceName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
  address: data.address.replace(/\b\w/g, (l:any) => l.toUpperCase()),
  orgainzationId: this.orgId,
  image:data.image,

createdBy: localStorage.getItem('Name'),
createdDate: this.datepipe.transform(new Date(data.createdDate), 'yyyy-MM-dd'),


}
console.log("ruman",datatosend);

this.spinner.show()
    this.Api.addPractitioner(datatosend)
    .subscribe((data:any)=>{
      console.log('final response => (Practitioner)',data)
      this.myPractitionerForm.reset()
      this.spinner.hide()
    })

  }

  keyPressAlphaNumeric(event: any) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z  ]/.test(inp)) {
      let name1: string = this.myPractitionerForm.value.organisationName;
      name1 && name1.length > 0 ? this.myPractitionerForm.patchValue({
        organisationName: name1[0].toUpperCase() + name1.substring(1)
      }) : '';
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphaNumerics(event: any) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[0-9   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }


  getCallingCodes() {
    this.Api
      .callingCodesApi()
      .subscribe((res:any) => {
        console.log('callingCodes', res);


        this.phoneCode = Object.values(res[0]);

        console.log('result',this.phoneCode);


      });
  }

}

