import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DashChartsComponent } from './dash-charts.component';
import {Chart } from 'angular-highcharts'

const routes: Route[] = [
  {
    path: '',
    component: DashChartsComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EspSharedModule,
    FormsModule,
    Chart,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class DashChartsModule { }
