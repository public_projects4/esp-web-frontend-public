
import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
// import * as $ from "jquery";
// import { Chart } from 'highcharts';
import {Chart } from 'angular-highcharts'
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { HttpClient } from '@angular/common/http';
// import { DEFAULT_MAX_VERSION } from 'tls';

declare var require: any;
const More = require('highcharts/highcharts-more');
More(Highcharts);

const Exporting = require('highcharts/modules/exporting');
Exporting(Highcharts);

const ExportData = require('highcharts/modules/export-data');
ExportData(Highcharts);

const Accessibility = require('highcharts/modules/accessibility');
Accessibility(Highcharts);

@Component({
  selector: 'app-dash-charts',
  templateUrl: './dash-charts.component.html',
  styleUrls: ['./dash-charts.component.scss']
})
export class DashChartsComponent implements OnInit {


  public options: any = {
    chart: {
      // plotBackgroundColor: null,
      // plotBorderWidth: null,
      backgroundColor: 'none',
      plotShadow: false,
      type: 'pie',
      // marginTop:-50,
      // margin: [0, 0, 0, 0],
      //   spacingTop: 0,
      //   spacingBottom: 0,
      //   spacingLeft: 0,
      //   spacingRight: 0,
        height:'70%',



        // height:'100%' to fit the chart in perfect place  reference see at end of this page https://stackoverflow.com/questions/11735306/highcharts-make-the-pie-chart-100-of-the-div

      },


       //-------------------------- Button - Handberger Menu Styles (start)--------------------------//
    //https://api.highcharts.com/highcharts/
    navigation:{

      menuStyle:{ //menu pop-up background color
        backgroundColor:'#666666',


      },




      menuItemStyle:{//menu pop-up text color
        color:'white',

      },


      buttonOptions:{//handberger button settings
        symbolStroke:'white', //The color of the symbol's stroke or line.(inside the handbermenu)
        enabled:false, //enable and disable the menu button
        theme:{
          fill:'#666666', // for handberger button color change in "theme" in "fill"
        }

      }



    },

    //-------------------------- Button - Handberger Menu Styles (end)--------------------------//


    title: {
      text: ''
    },
    tooltip: {

      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        size: 90,
        colors: [
          '#723AC3',
          '#864DD9',
          '#9762E6',
          '#A678EB'
        ],
        borderWidth: 0 ,

        dataLabels: {
          enabled: false,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
          connectorColor: 'silver'
        }
      }
    },

    credits: {
      enabled: false
    },


    series: [{

      data: [{

          y: 60
        },
        {

          y: 10
        },
        {

          y: 15
        },
        {

          y: 15
        },
      ]
    }]
  }
































arr=[20,30,40,10,80,110,20,50,90]





 employeeName:any


  columnchart = new Chart ({


    // menuStyle:{
    //   border: "1px solid #999999", background: "#ffffff", padding: "5px 0",
    // },


    chart: {
      // https://www.highcharts.com/docs/chart-design-and-style/design-and-style
      backgroundColor: 'transparent',
      // zoomType: 'xy',
      height:'120',
      // width:'160'
      // width:370,
      // marginBottom:80,


    },


        //-------------------------- Button - Handberger Menu Styles (start)--------------------------//
    //https://api.highcharts.com/highcharts/
    navigation:{

      menuStyle:{ //menu pop-up background color
        backgroundColor:'#666666',


      },




      menuItemStyle:{//menu pop-up text color
        color:'white',

      },


      buttonOptions:{//handberger button settings
        symbolStroke:'white', //The color of the symbol's stroke or line.(inside the handbermenu)
        enabled:false, //enable and disable the menu button
        theme:{
          fill:'#666666', // for handberger button color change in "theme" in "fill"
        }

      }



    },

    //-------------------------- Button - Handberger Menu Styles (end)--------------------------//



    title: {
      text: ""
    },

    credits: {
      enabled: false,
    },
    xAxis: {
      gridLineWidth:0,
      visible:false,

      labels: {
       rotation:-45,
       enabled:false
      },



      // categories: ["Thomas Hector", "Alexander Shelby", "Arther Kooper", "Ranjith", "Jhon Dee", "David",
      //   "Harsith", "Swapna"]
    },


    yAxis: {



      visible:false,
      gridLineColor:'#666666',
      gridLineWidth:0,


      // height:'120px',
      // grid: {
      //   he
      // },

    },

    legend:{// chart inner lines gap (height) settings
      layout:'horizontal',
      itemMarginTop:-10,
      itemMarginBottom:-10,

      itemStyle:{
        color:"blue",
      },
      enabled:false // Make true  to enable it.

    },



    series: [
      {
      type: 'column',
      color: '#CF53F9',
      name:'evvo',

      pointWidth: 2,// pointWidth is what you require to set the width of the bars

      // http://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/column-drilldown/

      // data : this.employeeName
      // data: [
      //   { y: 60, borderColor:'none',name:'sports', drilldown: "Firefox"},
      //   { y: 135,borderColor:'none', },
      //   { y: 110,borderColor:'none', },
      //   { y: 40,borderColor:'none', },
      //   { y: 100,borderColor:'none', },
      //   { y: 70 ,borderColor:'none',},
      //   { y: 5 ,borderColor:'none',},
      //   { y: 50,borderColor:'none', },
      //   { y: 40,borderColor:'none', },
      //   { y: 126,borderColor:'none', },

      // ],

    }

  ],




  });






















  columnchart_2 = new Chart ({
    // menuStyle:{
    //   border: "1px solid #999999", background: "#ffffff", padding: "5px 0",
    // },


    chart: {
      // https://www.highcharts.com/docs/chart-design-and-style/design-and-style
      backgroundColor: 'transparent',
      // zoomType: 'xy',
      height:'120',
      // width:'160'
      // width:370,
      // marginBottom:80,


    },


        //-------------------------- Button - Handberger Menu Styles (start)--------------------------//
    //https://api.highcharts.com/highcharts/
    navigation:{

      menuStyle:{ //menu pop-up background color
        backgroundColor:'#666666',


      },




      menuItemStyle:{//menu pop-up text color
        color:'white',

      },


      buttonOptions:{//handberger button settings
        symbolStroke:'white', //The color of the symbol's stroke or line.(inside the handbermenu)
        enabled:false, //enable and disable the menu button
        theme:{
          fill:'#666666', // for handberger button color change in "theme" in "fill"
        }

      }



    },

    //-------------------------- Button - Handberger Menu Styles (end)--------------------------//



    title: {
      text: ""
    },

    credits: {
      enabled: false,
    },
    xAxis: {
      gridLineWidth:0,
      visible:false,

      labels: {
       rotation:-45,
       enabled:false
      },



      // categories: ["Thomas Hector", "Alexander Shelby", "Arther Kooper", "Ranjith", "Jhon Dee", "David",
      //   "Harsith", "Swapna"]
    },


    yAxis: {



      visible:false,
      gridLineColor:'#666666',
      gridLineWidth:0,


      // height:'120px',
      // grid: {
      //   he
      // },

    },

    legend:{// chart inner lines gap (height) settings
      layout:'horizontal',
      itemMarginTop:-10,
      itemMarginBottom:-10,

      itemStyle:{
        color:"blue",
      },
      enabled:false // Make true  to enable it.

    },



    series: [{
      type: 'column',
      color: '#EF8C99',
      pointWidth: 1,// pointWidth is what you require to set the width of the bars
      data: [
        {
           y: 60 ,borderColor:'none',

          },
        {
          y: 135,borderColor:'none',
        },
        {
          y: 110,borderColor:'none',
         },
        {
          y: 40,borderColor:'none',
         },
        {
          y: 100,borderColor:'none',
        },
        // { y: 144.0, color: '#ffe8df',borderColor:'none', }, use color inside to define other color
        {
           y: 70 ,borderColor:'none',
          },
        {
           y: 5 ,borderColor:'none',
          },
        {
           y: 50,borderColor:'none',
           },
        {
           y: 40,borderColor:'none',
           },
        {
          y: 126,borderColor:'none',
         },

      ],

    }],




  });







  constructor(private Api: AllApiService,private http:HttpClient) { }

  ngOnInit(): void {
    Highcharts.chart('container', this.options);
    this.getAthletesInviteCount()



  }
ras:any= []
recivedData :any
recivedInnerData :any

getAthletesInviteCount(){
  this.Api.getAthletesInviteCount().subscribe((data:any)=>{
    console.log('getwidgets',data);
    this.recivedData = data

    this.recivedData.forEach((element:any) => {
      // console.log('element',element.athleteInvited);
      this.recivedInnerData = element.athleteInvited;

      console.log('this.recivedInnerData',this.recivedInnerData);

    });
    console.log('this.recivedData',this.recivedData);
    // this.columnchart.
  })
}



}
