import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';

import { FormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { TrainingsComponent } from './trainings.component';
import { MatDialogModule } from '@angular/material/dialog';


const routes: Route[] = [
  {
    path: '',
    component: TrainingsComponent
  }
];



@NgModule({
  declarations: [
    TrainingsComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    NgbModalModule,
    MatDialogModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ]
})
export class TrainingsModule { }
