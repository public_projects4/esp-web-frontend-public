import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  inboxList: any[];
  inboxListCount :number=0;
  constructor(private Api: AllApiService,private datepipe: DatePipe) { }
  ngOnInit(): void {
    this.getMessagesByReceiverId(localStorage.getItem('OrgID'));
  }
  getMessagesByReceiverId(id: any) {
    this.inboxListCount = 0;
    this.Api.getMessagesByReceiveId(id).subscribe((data: any) => {
      this.inboxList = [];
      data.some(element => {
        if(element.isRead ==false){
        this.inboxListCount++;
        var message = Object.create({});
        message.name = element.senderName;
        message.description = element.description;
        message.time= this.datepipe.transform(new Date(), element.date);
        message.image = element.image;
        if(this.inboxList.length == 5)
        return true;
        this.inboxList.unshift(message);
        }
      })
    })
    setTimeout(()=>{
      this.getMessagesByReceiverId(localStorage.getItem('OrgID'))
    },60000)
  }
  clickCount = 0;
  ready(){
    this.clickCount++;
      if(this.clickCount===1){
        $('.sidebar-toggle').on('click', function () {
          $(this).toggleClass('active');
          $('#sidebar').toggleClass('shrinked');
          $('.page-content').toggleClass('active');
          $(document).trigger('sidebarChanged');
          if ($('.sidebar-toggle').hasClass('active')) {
              $('.navbar-brand .brand-sm').addClass('visible');
              $('.navbar-brand .brand-big').removeClass('visible');
              $(this).find('i').attr('class', 'fa fa-long-arrow-right');
          } else {
              $('.navbar-brand .brand-sm').removeClass('visible');
              $('.navbar-brand .brand-big').addClass('visible');
              $(this).find('i').attr('class', 'fa fa-long-arrow-left');
          }
      });
      }
   }
   logOut(){
     localStorage.clear()
   }
}
