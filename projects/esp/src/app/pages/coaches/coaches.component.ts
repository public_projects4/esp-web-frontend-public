import { Component, OnInit } from '@angular/core';
import { CoachInfoProperty } from 'projects/esp-shared/src/lib/models/coaches-info-bar';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import countries from 'projects/esp-shared/src/lib/services/callingCodes.json';
import {
  SearchCountryField,
  CountryISO,
  PhoneNumberFormat,
} from 'ngx-intl-tel-input';
import {
  FormArray,
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ModalCommanService } from 'projects/esp-shared/src/lib/models/modal-comman-service';
import { DatePipe } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-coaches',
  templateUrl: './coaches.component.html',
  styleUrls: ['./coaches.component.scss'],
})
export class CoachesComponent implements OnInit {
  isDelete = true;
  isEdit = true;
  isCreate=true
  isView=true
  dropdownList: any = [];
  selectedItems: any = [];
  dropdownSettings: any = [];
  countriesEnglish = [
    { item_id: 1, item_text: '+358' },
    { item_id: 2, item_text: '+355' },
    { item_id: 3, item_text: '+213' },
    { item_id: 4, item_text: '+1684' },
    { item_id: 5, item_text: '+376' },
    { item_id: 6, item_text: '+244' },
    { item_id: 7, item_text: '+355' },
    { item_id: 8, item_text: '+1264' },
    { item_id: 9, item_text: '+672' },
    { item_id: 10, item_text: '+1268' },
    { item_id: 11, item_text: '+54' },
    { item_id: 12, item_text: '+91' },
    { item_id: 13, item_text: '+65' },
    { item_id: 14, item_text: '+60' },
  ];
  modelDetails: ModalCommanService[] = [
    {
      modelName: 'Coach',
    },
  ];
  myDate = this.datepipe.transform(new Date(), 'yyyy-MM-ddT00:00:46.217Z');
  added = false;
  exist = false;
  url: any = 'assets/img/coache-avatar.png';
  separateDialCode = false;
  deleteId: any;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom,
  ];
  counrtyList: { dial_code: string; code: string; name: string }[] = countries;
  myReactiveForms: FormGroup = new FormGroup({});
  demo: any;
  searchText1: string = '';
  coacheslist: any[] = [];
  sCategory: any[] = [];
  orgData: any;
  orgData2: any;
  coachLength: any;
  orgId: any = '';
  createdUpdated: any = '';
  catch: any;
  phoneCode: any[] = [];
  mobileNumber: string = '';
  numCountryCode: any;
  numMobileNumber: any;
  numberValid = false;
  numberInvalid = false;
  pushedDataNewMsg  :any =[];
  fileName:any;
  hideUploadNewMsgBlock = false;
  phoneForm = new FormGroup({
    phone: new FormControl(undefined, [Validators.required]),
  });
  subject :any;
  description :any;
  errorMessage :string='';
  uploadStatus : string='';
  sportsType:string='';
  coachName :string='';
  file: any;
  testData = new FormData();
  coachId: any;
  uploadedFile: string;
  constructor(
    private Api: AllApiService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private datepipe: DatePipe
  ) {
    this.myReactiveForms = this.formBuilder.group({
      createdBy: '',
      createdDate: '',
      updatedBy: '',
      coacheName: ['', [Validators.required]],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      mobile: ['', [Validators.required]],
      sportsCategory: ['', [Validators.required]],
      image: '',
      organisationId: '',
      mobileNumber: ['', [Validators.required]],
    });
  }
  teamId: any;
  ShowFilter = false;
  getRoleId: any;
  ngOnInit(): void {
    this.dropdownList = [
      { item_id: 1, item_text: '+91' },
      { item_id: 2, item_text: 'Bangaluru' },
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' },
      { item_id: 5, item_text: 'New Delhi' },
    ];
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      itemsShowLimit: 3,
      allowSearchFilter: true,
    };
    this.getDetails();
    this.getSportsCategory();
    this.getTeams();
    this.getOrgById();
    this.getCallingCodes();
    this.getAllCoachesByOrgId();
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();
  }
  itemsList: any = [];
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {

      this.itemsList = data;

      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Coaches') {
          if (ele.isDelete == true) {
            this.isDelete = true;
          } else {
            this.isDelete = false;
          }
          if (ele.isEdit == true) {
            this.isEdit = true;
          } else {
            this.isEdit = false;
          }
          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }
          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
            this.isCreate=false
            this.isEdit=false
            this.isDelete=false
          }
        }
      });
    });
  }
  coachDetails: CoachInfoProperty[] = [
    {
      label: 'Total Coaches 4 ',
      addNew: 'Add New Coach',
    },
  ];
  get f() {
    return this.myReactiveForms.controls;
  }
  get a() {
    return this.phoneForm.controls;
  }
  refreshForm() {
    this.myReactiveForms.reset();
  }
  Status: any;
  test: any;
  mobileNu: any;
  array: any;
  teamList: any;
  mobilesNums: any;
  rolesIds: any;
  statusVar: any;
  updatedByy: any;
  openEditPage(data: any) {
    this.teamList = [];

    this.rolesIds = data.roleId;
    this.statusVar = data.status;
    this.updatedByy = data.updatedBy;
    this.mobilesNums = data.mobile;
    this.array = data;
    this.array.teamNames.forEach((ele: any) => {
      this.teamList.push(ele);
    });
    this.mobileNu = data.mobile;
    this.test = data.mobile.split('-')[0];
    this.selectedItems = [{ item_text: this.test }];
    this.Status = data.status;
    const mob = data.mobile.split('-');
    this.myReactiveForms = this.formBuilder.group({
      coacheName: data.coacheName,
      email: data.email,
      mobileNumber: mob[0],
      mobile: mob[1],
      sportsCategory: data.sportsCategory,
      image: data.image,
      id: data.id,
      organisationId: data.organisationId,
      createdBy: data.createdBy,
      createdDate: this.datepipe.transform(new Date(data.createdDate), 'yyyy-MM-dd'),
    });

  }
  closeView() {
    this.getDetails();
  }
  select: any;
  getDetails() {
    this.spinner.show();
    this.orgId = localStorage.getItem('OrgID');
    this.Api.getAllCoachBySportsCategory(this.orgId).subscribe((res: any) => {

      this.coacheslist = res;
      this.coacheslist.forEach((element, index, sourceArr) => {
        if (index == 0) {

          this.catch = element.coach;

          this.select = element.category;
        }
      });
      this.spinner.hide();
    });
  }
  numCode: any;
  pCode = false;
  onItemSelect(item: any) {
    this.pCode = true;
    this.numCode = item.item_text;
  }
  addDetails() {
    const data = this.myReactiveForms.value;
    data.mobileNumber = this.numCode;
    this.orgId = localStorage.getItem('OrgID');
    const update = {
      coacheName: data.coacheName.replace(/\b\w/g, (l: any) => l.toUpperCase()),
      email: data.email,
      mobile: data.mobileNumber + '-' + data.mobile,
      sportsCategory: data.sportsCategory,
      organisationId: localStorage.getItem('OrgID'),
      createdBy: localStorage.getItem('Name'),
      createdDate: this.datepipe.transform(new Date(Date.now()), 'yyyy-MM-dd'),
    };
    localStorage.setItem('sportsCategory', data.sportsCategory);

    this.spinner.show();
    this.Api.addCoaches(update).subscribe((res: any) => {

      if (res.message == 'Coach Added Successfully') {

        this.getDetails();
        this.getAllCoachesByOrgId();
        this.myReactiveForms.reset();
        $('#successModal').modal('show');
      } else if (res.message == 'Coach Exsits') {

        $('#alreadyExistModal').modal('show');
      }
      this.spinner.hide();
    });
  }
  testId(id: any) {

    this.deleteId = id;
  }
  deleteCoache() {
    this.Api.deleteCoaches(this.deleteId).subscribe((data) => {
      this.getDetails();
      this.getAllCoachesByOrgId();
      $('#successfullyDeletedModal').modal('show');
    });
  }
  filterCoach(cat: any) {
    this.catch = [];
    this.coacheslist.forEach((ele) => {
      if (cat == ele.category) {
        this.catch = ele.coach;

      } else {
      }
    });
  }
  Image: any;
  updateItems() {
    const data = this.myReactiveForms.value;


    data.mobileNumber = this.numCode;
    if (this.Status == 'Active') {
      if (this.pCode == true) {
        mob = this.numCode + '-' + data.mobile;
      } else if (this.pCode == false) {
        mob = this.mobileNu;
      }
      const update = {
        coacheName: data.coacheName.replace(/\b\w/g, (l: any) =>
          l.toUpperCase()
        ),
        email: data.email,
        mobile: mob,
        sportsCategory: data.sportsCategory,
        image: data.image,
        id: data.id,
        organisationId: data.organisationId,
        createdBy: data.createdBy,
        updatedBy:  localStorage.getItem('Name'),
      createdDate: this.myDate,
        status: this.statusVar,
        updatedDate: this.myDate,
        roleId: this.rolesIds,
        teamNames: this.teamList,
      };

      this.Api.updateCoaches(update).subscribe((data) => {

        $('#updateModal').modal('show');
        this.getDetails();
      });
    } else if (this.Status == 'Invited') {
      var mob: any;
      if (this.pCode == true) {
        mob = this.numCode + '-' + data.mobile;
      } else if (this.pCode == false) {
        mob = this.mobileNu;
      }
      const update = {
        coacheName: data.coacheName.replace(/\b\w/g, (l: any) =>
          l.toUpperCase()
        ),
        email: data.email,
        mobile: mob,
        sportsCategory: data.sportsCategory,
        image: 'assets/img/coache-avatar.png',
        id: data.id,
        organisationId: data.organisationId,
        createdBy: data.createdBy,
        updatedBy: localStorage.getItem('Name'),
      };

      this.Api.updateCoaches(update).subscribe((data) => {
        $('#updateModal').modal('show');
        this.getDetails();
      });
    }
  }
  getSportsCategory() {
    this.Api.getAllSportsCategory().subscribe((res: any) => {

      this.sCategory = res;
    });
  }
  getCallingCodes() {
    this.Api.callingCodesApi().subscribe((res: any) => {
      this.phoneCode = Object.values(res[0]);
    });
  }
  TeamsRecived: any[] = [];
  getTeams() {
    this.Api.getAllTeam().subscribe((res: any) => {
      this.TeamsRecived = res;
    });
  }
  getOrgById() {
    this.orgId = localStorage.getItem('OrgID');
    this.Api.getAllOrganizationById(this.orgId).subscribe((data) => {
      this.orgData = data;
      this.orgData2 = this.orgData;
    });
  }
  selectCode(data: any) {
    this.counrtyList.forEach((ele: any) => {
      if (ele.dial_code == data.target.value) {
        this.numCountryCode = ele.code;
      }
    });
  }
  inputNumber(data: any) {
    this.numMobileNumber = data.target.value;
  }
  getAllCoachesByOrgId() {
    this.orgId = localStorage.getItem('OrgID');
    this.Api.getCoachesByOrgId(this.orgId).subscribe((data) => {
      this.coachLength = data;

    });
  }
  keyPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z  ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  keyPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  changePreferredCountries() {
    this.preferredCountries = [CountryISO.India, CountryISO.Canada];
  }
  num: any;
  onSelectAll(items: any) {
  }
  openMsg(data:any){

    this.sportsType = data.sportsCategory;
    this.coachName = data.coacheName;
    this.coachId = data.id;
    $('#newMessageModal').modal('show');
  }
  openUploadModel(){
    $('#uploadModal').modal('show');
  }
  sendMessage(){
    var attachments = [];
    attachments.push(this.uploadedFile);
    var messageData = Object.create({});
    messageData.createdDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString(),
    messageData.updatedDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString(),
    messageData.subject = this.subject;
    messageData.description = this.description;
    messageData.date = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString(),
    messageData.attachment = attachments;
    messageData.createdBy = localStorage.getItem('Name');
    messageData.senderName = localStorage.getItem('Name');
    messageData.isDeleted = false;
    messageData.senderId = localStorage.getItem('OrgID');
    messageData.recieverId = this.coachId;

    this.Api.sendMessage(messageData).subscribe((data: any) => {
      if (data.message === 'Message sent successfully') {
        this.pushedDataNewMsg = [];
        this.description = '';
        this.subject = '';
        $('#messageSentSuccessModal').modal('show');
      }
    })
  }
  fileChangeEvent(data: any) {
    if (data.length > 0) {
      this.file = data[0];
    }
  }
  uploadFile() {
    if (this.file != undefined) {
      this.testData.append('file', this.file, this.file.name);
      this.Api.uploadMessageFile(localStorage.getItem('Name'),localStorage.getItem('orgId') , this.testData).subscribe(result => {
        this.uploadedFile = result;
        this.uploadStatus = 'Successfully Uploaded';
        this.pushDataNewMsg();
      },
        error => {
          this.uploadStatus = JSON.parse(error.error).message;
        })
      setTimeout(() => {
        this.uploadStatus = '';
      }, 5000);
    }
  }
  uploadFilePinn1 = (files: any) => {
    this.hideUploadNewMsgBlock = true;


    let fileToUpload = <File>files[0];

    this.file = fileToUpload
    this.fileName = fileToUpload.name

  }
  pushDataNewMsg(){
    this.pushedDataNewMsg.push(this.fileName)

    this.fileName = '';
    this.hideUploadNewMsgBlock = false;
  }
  cancelNewMsgUpload(i:any){

  this.pushedDataNewMsg.splice(i, 1);
  }
  cancel() {
    this.file =''
    this.fileName =''
  }
}
