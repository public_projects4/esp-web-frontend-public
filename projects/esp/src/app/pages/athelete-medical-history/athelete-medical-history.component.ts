import { DatePipe } from '@angular/common';
import { Component, OnInit, Input } from '@angular/core';
import {
  FormGroup,
  FormArray,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { jsPDF } from 'jspdf';
import { NgxSpinnerService } from 'ngx-spinner';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
export interface data {
  columnName: string;
  value: string;
}
@Component({
  selector: 'app-athelete-medical-history',
  templateUrl: './athelete-medical-history.component.html',
  styleUrls: ['./athelete-medical-history.component.scss'],
})
export class AtheleteMedicalHistoryComponent implements OnInit {
isCreate=true
isView=true
isEdit=true
isDelete=true
  @Input() atheleteId:any;
  myInjuryForm: FormGroup = new FormGroup({});
  editTreatSection = false;
  recData:any
  injuryDate:any
  practitioner:any[]=[]
  newPractitioner:any[]=[]
  injuryTime:any
  injuryReturn:any
  treatId: any;
  myForm: any = FormGroup;
  airway: any;
  danger: any;
  breathing: any;
  circulation: any;
  commentToAirway: any;
  commentToBreathing: any;
  commentToCirculation: any;
  commentToDanger: any;
  commentToResponse: any;
  testList: any = [];
  response: any;
  editSection = false;
  Id: any;
  Type: any;
  Name = 'INJURY';
  Ids: any;
  Types: any;
  Names = 'TREATMENT';
  type: any;
  excel: any;
  treatment = false;
  injury = true;
  treat = false;
  detailList: any = [];
  InjuryList: any[] = [];
  TreatList: any[] = [];
  injuryGetList = false;
  treatList = false;
  public docTreat = {
    documentType: '',
    fileName: '',
  };
  public editTreat = {
    complaints: 'test',
    consultationDate: '2021-11-04T16:00:00Z',
    consultationTime: '2021-11-16T10:20:00Z',
    consultationType: 'New Consultation',
    disease: '',
    examination: 'test',
    location: 'test',
    practitionerID: '',
    treatmentType: 'test',
  };
  public edit = {
    dateOfInjury: '',
  };
  formats: data[] = [
    {
      columnName: 'EXCEL',
      value: 'EXCEL',
    },
    {
      columnName: 'PDF',
      value: 'PDF',
    },
  ];
  myDate = this.datepipe.transform(new Date(), 'yyyy-MM-ddT00:00:46.217Z');
  constructor(private api: AllApiService,private formBuilder: FormBuilder, public datepipe: DatePipe,private spinner: NgxSpinnerService,) {
    this.myInjuryForm = this.formBuilder.group({
      injuryAssessments: this.formBuilder.array([]),
      injuryType: ['', [Validators.required]],
      dateOfInjury: ['', [Validators.required]],
      timeOfInjury: ['', [Validators.required]],
      event: ['', [Validators.required]],
      diagnosticCode: ['', [Validators.required]],
      injuryOnset: ['', [Validators.required]],
      referredTo: ['', [Validators.required]],
      incidentType: ['', [Validators.required]],
      level1Injury: ['', [Validators.required]],
      level2Injury: ['', [Validators.required]],
      swelling: ['', [Validators.required]],
      stability: ['', [Validators.required]],
      strength: ['', [Validators.required]],
      medicalCondition: ['', [Validators.required]],
      injuryDetails: ['', [Validators.required]],
      athleteId: '',
      createdDate: this.myDate,
      createdBy: localStorage.getItem('Name'),
      continueToPlay: ['', [Validators.required]],
      treatmentStatus: ['', [Validators.required]],
      expectedDateOfReturn: ['', [Validators.required]]
    });
  }
  getRoleId:any
  ngOnInit(): void {
    this.myInjuryForm.value.createdDate = this.myDate
    this.getInjury();
    this.getTreatment();
    this.getPractitioner();
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();
  }
  itemsList:any=[]
  getSideNavItems() {
    this.api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {

      this.itemsList = data;

      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Athlete Data') {
          if (ele.isDelete == true) {
            this.isDelete = true;
          } else {
            this.isDelete = false;
          }
          if (ele.isEdit == true) {
            this.isEdit = true;
          } else {
            this.isEdit = false;
          }
          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }
          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
          }
        }
      });
    });
  }
  newSkillsInjury(): FormGroup {
    return this.formBuilder.group({
      danger: false,
      commentToDanger: '',
      response: false,
      commentToResponse: '',
      airway: false,
      commentToAirway: '',
      breathing: false,
      commentToBreathing: '',
      circulation: false,
      commentToCirculation: '',
    });
  }
  get abcInjury(): FormArray {
    return this.myInjuryForm.get('injuryAssessments') as FormArray;
  }
  addSkillsInjury() {
    this.abcInjury.push(this.newSkillsInjury());
  }
  getInjury() {
    this.api
      .getMedicalHistory(
        this.atheleteId,
        this.startDate,
        this.endDate,
        this.type
      )
      .subscribe((data: any) => {
        this.detailList = data;
      });
  }
  getTreatment() {
    this.api.GetTreatment().subscribe((data: any) => {
      this.TreatList = data;
    });
  }
  openPage() {
    this.getInjury();
    this.api
      .downloadOrganization(this.Id, this.Type, this.Name)
      .subscribe((data: any) => {

        if (data) {
          window.location.href = data;
          if (data) {
          }
        }
      });
  }
  opens(data: any) {
    this.Type = data.target.value;

  }
  download(data: any) {
    this.Id = data;
  }
  Select(data: any) {
    if (data.target.value == 'Injury Data') {
      this.injuryGetList = true;
      this.treatList = false;
    } else {
      this.treatList = true;
      this.injuryGetList = false;
    }
    this.type = data.target.value;
    this.spinner.show()
    setTimeout(() => {
      this.spinner.hide();
    }, 550);
    this.getInjury();
  }
  OpenPage() {
    this.getTreatment();
    this.api
      .download(this.Ids, this.Types, this.Names)
      .subscribe((data: any) => {

        if (data) {
          window.location.href = data;
        }
      });
  }
  Opens(data: any) {
    this.Types = data.target.value;

  }
  Download(data: any) {
    this.Ids = data;
  }
  EXCEL() {
    this.excel = 'EXCEL';

    this.api
      .downloadOrganization(this.Id, this.excel, this.Name)
      .subscribe((data: any) => {

        if (data) {
          window.location.href = data;
        }
      });
  }
  excelTreat() {
    this.excel = 'EXCEL';

    this.api
      .download(this.treatId, this.excel, this.Names)
      .subscribe((data: any) => {

        if (data) {
          window.location.href = data;
        }
      });
  }
  startDate: any;
  endDate: any;
  startDateEvent(data: any) {
    this.startDate = this.datepipe.transform(data.target.value, 'MM-dd-yyyy');
    this.spinner.show()
    setTimeout(() => {
      this.spinner.hide();
    }, 550);
    this.getInjury();
  }
  endDateEvent(data: any) {
    this.endDate = this.datepipe.transform(data.target.value, 'MM-dd-yyyy');
    this.spinner.show()
    setTimeout(() => {
      this.spinner.hide();
    }, 550);
    this.getInjury();
  }
  Danger: any;
  dateOfInjury: any;
  view(data: any) {

    this.editSection = true;
    this.editTreatSection = false;
    this.injury = false;
    this.edit = data;
    data.injuryAssessments.forEach((element:any) => {

      this.recData = element
  });
    const lessonForm = this.formBuilder.group({
      airway: this.recData.airway,
    breathing: this.recData.breathing,
    circulation: this.recData.circulation,
    commentToAirway: this.recData.commentToAirway,
    commentToBreathing: this.recData.commentToBreathing,
    commentToCirculation: this.recData.commentToCirculation,
    commentToDanger: this.recData.commentToDanger,
    commentToResponse: this.recData.commentToResponse,
    danger: this.recData.danger,
    response: this.recData.response
    });
this.myInjuryForm = this.formBuilder.group({
  injuryAssessments: this.formBuilder.array([lessonForm]),
  injuryType: data.injuryType,
  dateOfInjury:  this.datepipe.transform(data.dateOfInjury,'dd/MM/yyyy'),
  timeOfInjury: this.datepipe.transform(data.timeOfInjury,'h:mm'),
  event: data.event,
  diagnosticCode: data.diagnosticCode,
  injuryOnset: data.injuryOnset,
  referredTo: data.referredTo,
  incidentType: data.incidentType,
  level1Injury: data.level1Injury,
  level2Injury: data.level2Injury,
  swelling: data.swelling,
  stability: data.stability,
  strength: data.strength,
  medicalCondition: data.medicalCondition,
  injuryDetails: data.injuryDetails,
  continueToPlay: data.continueToPlay,
  treatmentStatus: data.treatmentStatus,
  expectedDateOfReturn: this.datepipe.transform(data.expectedDateOfReturn,'dd/MM/yyyy'),
});
  }
  doc: any;
  file: any;
  cancel() {
    this.injury = true;
    this.editSection = false;
    this.editTreatSection = false;
  }
  downloadTreat(data: any) {
    this.treatId = data;
  }
  test:any=[]
  demo:any=[]
  myTreatmentForm: FormGroup = new FormGroup({});
    viewTreatInfo(data:any){

      this.editTreatSection = true;
    this.editSection = false;
    this.injury = false;
    this.demo=[]
      this.editTreat = data;
      this.test=data.documents
      this.test.forEach((element:any)=>{

  this.demo.push(element)
      })

      this.myTreatmentForm = this.formBuilder.group({
        consultationType: this.editTreat.consultationType,
        disease: this.editTreat.disease,
        treatmentType: data.treatmentType,
        location: data.location,
        practitionerID: data.practitionerID,
        consultationDate: this.datepipe.transform(data.consultationDate,'dd/MM/yyyy'),
        consultationTime: this.datepipe.transform(data.consultationTime,'h:mm'),
        complaints: data.complaints,
        examination: data.examination,
        athleteId: this.atheleteId,
      });
      this.showPractitionerData()
    }
    check(data:any){

this.pdf=data
    }
pdf:any
    PDF() {

      const doc = new jsPDF();
      doc.text(`\n injuryType : ${this.pdf.injuryType}`, 5, 17);
      doc.text(`\n strength : ${this.pdf.strength}`, 5, 27);
      doc.text(`\n event : ${this.pdf.event}`, 5, 37);
      doc.text(`\n incidentType : ${this.pdf.incidentType}`, 5, 47);
      doc.text(`\n dateOfInjury : ${this.pdf.dateOfInjury}`, 5, 57);
      doc.text(`\n diagnosticCode : ${this.pdf.diagnosticCode}`, 5, 67);
      doc.text(`\n injuryDetails : ${this.pdf.injuryDetails}`, 5, 77);
      doc.text(`\n injuryOnset : ${this.pdf.injuryOnset}`, 5, 87);
      doc.text(`\n level1Injury : ${this.pdf.level1Injury}`, 5, 97);
      doc.text(`\n timeOfInjury : ${this.pdf.timeOfInjury}`, 5, 107);
      doc.save('Injury Information.pdf');
    }
    PDFF(){

      const doc = new jsPDF();
      doc.text(`\n consultationType : ${this.pdf.consultationType}`, 5, 17);
      doc.text(`\n disease : ${this.pdf.disease}`, 5, 27);
      doc.text(`\n consultationDate : ${this.pdf.consultationDate}`, 5, 37);
      doc.text(`\n consultationTime : ${this.pdf.consultationTime}`, 5, 47);
      doc.text(`\n location : ${this.pdf.location}`, 5, 57);
      doc.text(`\n complaints : ${this.pdf.complaints}`, 5, 67);
      doc.text(`\n treatmentType : ${this.pdf.treatmentType}`, 5, 77);
      doc.text(`\n examination : ${this.pdf.examination}`, 5, 87);
      doc.save('Treatment Information.pdf');
    }
    OrgnizationID:any
    getPractitioner() {
      this.OrgnizationID = localStorage.getItem('OrgID');
      this.api.getPractitionerByOrganisationId(this.OrgnizationID)
      .subscribe((data:any)=>{
        this.practitioner=data
      })
    }
    showPractitionerData(){
      this.newPractitioner=[]
      this.practitioner.forEach((element) =>{
        if(element.id==this.editTreat.practitionerID){
          this.newPractitioner.push(element)

        }
      })
    }
}
