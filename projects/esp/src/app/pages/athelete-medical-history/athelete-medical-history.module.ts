import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import {  ReactiveFormsModule } from '@angular/forms';
import { AtheleteMedicalHistoryComponent } from './athelete-medical-history.component';
import { NgxSpinnerModule } from 'ngx-spinner';
const routes: Route[] = [
  {
    path: '',
    component: AtheleteMedicalHistoryComponent
  }
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    RouterModule.forChild(routes)
  ]
})
export class AtheleteMedicalHistoryModule { }
