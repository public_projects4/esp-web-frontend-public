import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { Route,RouterModule } from '@angular/router';
import { PractitionerListComponent } from './practitioner-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';


const routes: Route[] = [
  {
    path: '',
    component: PractitionerListComponent
  }
];


@NgModule({
  declarations: [PractitionerListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    EspSharedModule,
    NgxSpinnerModule,
    RouterModule.forChild(routes)
  ]
})
export class PractitionerListModule { }
