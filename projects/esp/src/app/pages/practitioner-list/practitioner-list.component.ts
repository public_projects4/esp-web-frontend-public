import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { CoachInfoProperty } from 'projects/esp-shared/src/lib/models/coaches-info-bar';
import { CoachList } from 'projects/esp-shared/src/lib/models/coach-list';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

import { ModalCommanService } from 'projects/esp-shared/src/lib/models/modal-comman-service';

declare var $: any;

@Component({
  selector: 'app-practitioner-list',
  templateUrl: './practitioner-list.component.html',
  styleUrls: ['./practitioner-list.component.scss'],
})
export class PractitionerListComponent implements OnInit, OnChanges {
  isCreate = true;
  isView = true;
  isDelete = true;
  isEdit = true;
  modelDetails: ModalCommanService[] = [
    {
      modelName: 'Practitioner',
    },
  ];

  myPractitionerForm: FormGroup = new FormGroup({});

  orgData: any[] = [];
  orgId: any = '';
  select: any;
  catch: any;
  phoneCode: any[] = [];
  mobileNumber: string = '';
  totalPractitioner: any;

  constructor(
    private Api: AllApiService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService
  ) {
    this.myPractitionerForm = this.formBuilder.group({
      id: '',
      practitionerName: '',
      practitionerRole: '',
      email: '',
      practiceName: '',
      address: '',
      orgainzationId: '',
      image: '',
      mobileNumber: '',
      contactNo: '',
      createdBy: '',
      createdDate: '',
      updatedBy: ''
    });
  }
  getRoleId: any;
  ngOnInit(): void {
    this.orgId = localStorage.getItem('OrgID');
    this.getOrgById();
    this.getCallingCodes();
    this.getPractitionerByOrgId();
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();
  }
  itemsList: any = [];
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {
      console.log('sidebar', data);
      this.itemsList = data;
      console.log(this.itemsList);
      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Manage Practitioner') {
          if (ele.isDelete == true) {
            this.isDelete = true;
          } else {
            this.isDelete = false;
          }

          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }

          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
          }
        }
      });
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.getOrgById();
    this.getPractitionerByOrgId();
  }

  openEditPage(data: any) {
    this.myPractitionerForm = this.formBuilder.group({
      id: data.id,
      practitionerName: data.practitionerName,
      practitionerRole: data.practitionerRole,
      contactNo: data.contactNo,
      email: data.email,
      practiceName: data.practiceName,
      address: data.address,
      orgainzationId: data.orgainzationId,
      image:data.image,
      createdBy: data.createdBy,
      createdDate: data.createdDate,
    });

    console.log('this.myPractitionerForm', this.myPractitionerForm);
  }

  onSubmit() {
    const data = this.myPractitionerForm.value;
    const datatosend = {
      practitionerName: data.practitionerName.replace(/\b\w/g, (l: any) =>
        l.toUpperCase()
      ),
      practitionerRole: data.practitionerRole.replace(/\b\w/g, (l: any) =>
        l.toUpperCase()
      ),
      contactNo: data.contactNo,
      email: data.email,
      practiceName: data.practiceName.replace(/\b\w/g, (l: any) =>
        l.toUpperCase()
      ),
      address: data.address.replace(/\b\w/g, (l: any) => l.toUpperCase()),
      id: data.id,
      image: data.image,
      orgainzationId: this.orgId,
      createdBy: localStorage.getItem('Name'),
      createdDate: data.createdDate,

      updatedBy: localStorage.getItem('Name'),
    };
    this.Api.UpdatePractitioner(datatosend).subscribe((data: any) => {
      this.getOrgById();
      this.getPractitionerByOrgId();
    });
  }

  getPractitionerByOrgId() {
    this.spinner.show();
    this.Api.getPractitionerByOrganisationId(this.orgId).subscribe(
      (data: any) => {
        this.totalPractitioner = data;
        this.spinner.hide();
      }
    );
  }

  getOrgById() {
    this.Api.getPractitionerByPractitionerRole(this.orgId).subscribe(
      (data: any) => {
        this.orgData = data;
        this.orgData.forEach((element, index, sourceArr) => {
          if (index == 0) {
            this.catch = element.practitioner;
            this.select = element.practitionerRole;
          }
        });
      }
    );
  }

  deletePractitioner(data: any) {
    this.Api.deletePractitionerData(data).subscribe((res) => {
      $('#successfullyDeletedModal').modal('show');

      console.log("finalResponse => deleted",res);


    }, (err) => {
      console.log('err prac',err);
      if(err.error.text == "Deleted Sucessfully"){
        console.log("workig");


        // window.location.reload(true)
        this.getOrgById()
      this.getPractitionerByOrgId()
      }

    });
  }

  pageReload() {
    window.location.reload();
  }

  filterCoach(cat: any) {
    this.catch = [];
    this.orgData.forEach((ele) => {
      if (cat == ele.practitionerRole) {
        this.catch = ele.practitioner;
      }
    });
  }

  getCallingCodes() {
    this.Api.callingCodesApi().subscribe((res: any) => {
      console.log('callingCodes', res);

      this.phoneCode = Object.values(res[0]);

      console.log('result', this.phoneCode);
    });
  }

  keyPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[0-9   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  keyPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z  ]/.test(inp)) {
      let name1: string = this.myPractitionerForm.value.organisationName;
      name1 && name1.length > 0
        ? this.myPractitionerForm.patchValue({
            organisationName: name1[0].toUpperCase() + name1.substring(1),
          })
        : '';
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
}
