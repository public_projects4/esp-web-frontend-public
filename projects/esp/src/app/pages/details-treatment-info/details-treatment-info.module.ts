import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DetailsTreatmentInfoComponent } from './details-treatment-info.component';

const routes: Route[] = [
  {
    path: '',
    component: DetailsTreatmentInfoComponent
  }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
    NgxSpinnerModule,



    RouterModule.forChild(routes)
  ]
})
export class DetailsTreatmentInfoModule { }
