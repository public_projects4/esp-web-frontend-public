import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { TeamDetailsComponent } from './team-details.component';
import { ChartModule } from 'angular-highcharts';
import { FormsModule } from '@angular/forms';

const routes: Route[] = [
  {
    path: '',
    component: TeamDetailsComponent
  }
];


@NgModule({
  declarations: [
    TeamDetailsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    EspSharedModule,
    ChartModule,
    RouterModule.forChild(routes)
  ]
})
export class TeamDetailsModule { }
