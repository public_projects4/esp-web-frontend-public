import { Component, OnInit } from '@angular/core';
import {  PageInfoProperty, StatisticBlockProperties } from 'projects/esp-shared/src/lib/models';
import { Chart } from 'angular-highcharts';
import { Atheleteblockproperties } from 'projects/esp-shared/src/lib/models/athelete-block';
import { NONE_TYPE } from '@angular/compiler';
import { ViewEncapsulation } from '@angular/compiler/src/compiler_facade_interface';
import { chart, color, reduce } from 'highcharts';
import { withLatestFrom } from 'rxjs/operators';
import { AUTO_STYLE } from '@angular/animations';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.scss']
})
export class TeamDetailsComponent implements OnInit {


  columnchart = new Chart ({
    // menuStyle:{
    //   border: "1px solid #999999", background: "#ffffff", padding: "5px 0",
    // },


    chart: {
      // https://www.highcharts.com/docs/chart-design-and-style/design-and-style
      backgroundColor: 'transparent',
      zoomType: 'xy',
      height:'400px',
      width:370,
      marginBottom:80,
      // marginLeft:70,
      // marginRight:-10

    },


        //-------------------------- Button - Handberger Menu Styles (start)--------------------------//
    //https://api.highcharts.com/highcharts/
    navigation:{

      menuStyle:{ //menu pop-up background color
        backgroundColor:'#666666',


      },




      menuItemStyle:{//menu pop-up text color
        color:'white',

      },


      buttonOptions:{//handberger button settings
        symbolStroke:'white', //The color of the symbol's stroke or line.(inside the handbermenu)
        enabled:false, //enable and disable the menu button
        theme:{
          fill:'#666666', // for handberger button color change in "theme" in "fill"
        }

      }



    },

    //-------------------------- Button - Handberger Menu Styles (end)--------------------------//



    title: {
      text: "Team Key Performance Indicator"
    },

    credits: {
      enabled: false,
    },
    xAxis: {
      labels: {
       rotation:-45
      },


      categories: ["Thomas Hector", "Alexander Shelby", "Arther Kooper", "Ranjith", "Jhon Dee", "David",
        "Harsith", "Swapna"]
    },


    yAxis: {
      title: {
        text: ""
      },



      gridLineColor:'#666666',


      // height:'120px',
      // grid: {
      //   he
      // },

    },

    legend:{// chart inner lines gap (height) settings
      layout:'horizontal',
      itemMarginTop:-10,
      itemMarginBottom:-10,

      itemStyle:{
        color:"blue",
      },
      enabled:false // Make true  to enable it.

    },



    series: [{
      type: 'column',
      color: '#506ef9',

      data: [
        { y: 20.9 ,borderColor:'none',},
        { y: 71.5,borderColor:'none', },
        { y: 106.4,borderColor:'none', },
        { y: 129.2,borderColor:'none', },
        { y: 144.0, color: '#ffe8df',borderColor:'none', },
        { y: 176.0 ,borderColor:'none',},
        { y: 148.5 ,borderColor:'none',},
        { y: 216.4, color: '#fc5185',borderColor:'none', },

      ],

    }],




  });




  areachart = new Chart ({



    chart: {

      type: 'area',
      width: 370,
      inverted: false,
      zoomType: 'xy',
      backgroundColor: 'transparent',


    },



    title: {
      text: 'Optimum Work Load'
    },
    credits: {
      enabled: false,
    },
    accessibility: {
      keyboardNavigation: {
        seriesNavigation: {
          mode: 'serialize'
        }
      }



    },


    //-------------------------- Button - Handberger Menu Styles (start)--------------------------//
    //https://api.highcharts.com/highcharts/
    navigation:{

      menuStyle:{ //menu pop-up background color
        backgroundColor:'#666666',


      },




      menuItemStyle:{//menu pop-up text color
        color:'white',

      },


      buttonOptions:{//handberger button settings
        symbolStroke:'white', //The color of the symbol's stroke or line.(inside the handbermenu)
        enabled:false, //enable and disable the menu button
        theme:{
          fill:'#666666', // for handberger button color change in "theme" in "fill"
        }

      }



    },

    //-------------------------- Button - Handberger Menu Styles (end)--------------------------//


    legend: {
      layout: 'horizontal',
      align: 'right',
      verticalAlign: 'top',
      x: -150,
      y: 100,
      floating: true,
      borderWidth: 1,
      backgroundColor: 'white'

    },
    xAxis: {
      categories: [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
      ]
    },
    yAxis: {
      title: {
        text: '',
      },
      gridLineColor:'#666666',
      allowDecimals: false,
      min: 0
    },
    plotOptions: {
      area: {
        fillOpacity: 0.8
      }
    },



    series: [{
      type: 'area',
      name: 'John',
      data: [{y:3}, {y:4}, {y:3}, {y:5}, {y:4}, {y:10}, {y:12}]
    },
     {
       type: 'area',
      name: 'Jane',
      data: [{y:1}, {y:3}, {y:4}, {y:3}, {y:3}, {y:5}, {y:4}]
    }
  ]





  });





  teamBasicDetails:any[] = [
    { name: 'Team Name', value: 'Evergreen Soldiers' },
    { name: 'Team Coacher', value: 'Thomas Edison' },
    { name: 'Team Created', value: 'DD MMM YYYY' },
    { name: 'Training Start Date', value: 'DD MMM YYYY' },
    { name: 'Training End Date', value: 'DD MMM YYYY' },
    { name: 'Training Status', value: 'Running' }
  ];

  statistics: StatisticBlockProperties[] = [{
    label: 'Total Athletes',
    statistic: 6,
    icon_name : 'settings_accessibility',
    progress_percentage: 30,
    progress_color_class: 'dashbg-1',
    color_class: 'color-white',
  },
  {
    label: 'Active Athletes',
    statistic: 7,
    icon_name : 'task_alt',
    progress_percentage: 30,
    progress_color_class: 'dashbg-4',
    color_class: 'color-white',
  },
  {
    label: 'Injured',
    statistic: 2,
    icon_name : 'personal_injury',
    progress_percentage: 70,
    progress_color_class: 'dashbg-2',
    color_class: 'color-white',
  },
  {
    label: 'Pending Invites',
    statistic: 1,
    icon_name : 'running_with_errors',
    progress_percentage: 55,
    progress_color_class: 'dashbg-3',
    color_class: 'color-white',
  }];

  userdetails: Atheleteblockproperties[] = [
    {
      order:1,
      name: 'Thomas Hector',
      body_mass:20.05,
      status:'Active',
      current_injury:1,
      training_days:5,
      missed_days:0,
      avatar:'assets/img/avatar-1.jpg',
      color_class: 'active_tag'
    },
    {
      order:2,
      name: 'Alexander Shelby',
      body_mass:88.45,
      status:'Doubt',
      current_injury:8,
      training_days:1,
      missed_days:0,
      avatar:'assets/img/avatar-2.jpg',
      color_class: 'doubt_tag'
    },
    {
      order:3,
      name: 'Alexander Shelby',
      body_mass:88.45,
      status:'Injured',
      current_injury:8,
      training_days:1,
      missed_days:0,
      avatar:'assets/img/avatar-2.jpg',
      color_class: 'injured_tag'
    }

  ];


  public add: any =
  {


    "email": "",
    "mobileNo": "",
    "athleteName":"",
    "id":"",
    "createdBy":"",
    "updatedBy":"",
    "organizationId":"",

  }





  // number='';
  // name='';
  // email='';
  resId:any=''
  orgId:any=''
  constructor(private Api:AllApiService) { }

  ngOnInit(): void {

    this.orgId=localStorage.getItem('OrgID')

    // setInterval(() => {
      this.resId=localStorage.getItem('ID')
      

  //  }, 1000);



  }

  onSubmit(data:any){
    console.log('original',data);

    const datatosend  = {
      "email": data.email,
      "mobileNo": data.mobileNo,
      "athleteName": data.athleteName,
      "teamId":this.resId,
      "organizationId": this.orgId
    }

    console.log(datatosend);

    this.Api.inviteAthlete(datatosend)
    .subscribe((data:any)=>{
      console.log('final response',data)
    })

  }



}
