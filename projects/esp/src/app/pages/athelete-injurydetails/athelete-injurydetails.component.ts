import {  Component, Input, OnInit } from '@angular/core';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import {
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe, formatDate } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;
@Component({
  selector: 'app-athelete-injurydetails',
  templateUrl: './athelete-injurydetails.component.html',
  styleUrls: ['./athelete-injurydetails.component.scss'],
})
export class AtheleteInjurydetailsComponent implements OnInit {
isCreate=true
isView=true
isDelete=true
isEdit=true
  @Input() athleteId:any;
  myDate = this.datepipe.transform(new Date(), 'yyyy-MM-ddT00:00:46.217Z');
  myReactiveForms: FormGroup = new FormGroup({});
  constructor(
    private Api: AllApiService,
    private formBuilder: FormBuilder,
    private routes: Router,
    public datepipe: DatePipe,
    private spinner: NgxSpinnerService,
  ) {
    this.myReactiveForms = this.formBuilder.group({
      injuryAssessments: this.formBuilder.array([]),
      injuryType: ['', [Validators.required]],
      dateOfInjury: ['', [Validators.required]],
      timeOfInjury: ['', [Validators.required]],
      event: ['', [Validators.required]],
      diagnosticCode: ['', [Validators.required]],
      injuryOnset: ['', [Validators.required]],
      referredTo: ['', [Validators.required]],
      incidentType: ['', [Validators.required]],
      level1Injury: ['', [Validators.required]],
      level2Injury: ['', [Validators.required]],
      swelling: ['', [Validators.required]],
      stability: ['', [Validators.required]],
      strength: ['', [Validators.required]],
      medicalCondition: ['', [Validators.required]],
      injuryDetails: ['', [Validators.required]],
      athleteId: '',
      createdDate: '',
      createdBy: localStorage.getItem('Name'),
      continueToPlay: ['', [Validators.required]],
      treatmentStatus: ['', [Validators.required]],
      expectedDateOfReturn: ['', [Validators.required]]
    });
  }
  Datess: any
  getRoleId:any
  ngOnInit(): void {
    this.addSkillsInjury();
    this.myReactiveForms.value.createdDate = this.myDate
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();
  }
  itemsList: any = [];
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {
      this.itemsList = data;
      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Athlete Data') {
          if (ele.isDelete == true) {
            this.isDelete = true;
          } else {
            this.isDelete = false;
          }
          if (ele.isEdit == true) {
            this.isEdit = true;
          } else {
            this.isEdit = false;
          }
          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }
          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
          }
        }
      });
    });
  }
  get f() {
    return this.myReactiveForms.controls;
  }
  keyPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z  ]/.test(inp)) {
      let name1: string = this.myReactiveForms.value.organisationName;
      name1 && name1.length > 0 ? this.myReactiveForms.patchValue({
        organisationName: name1[0].toUpperCase() + name1.substring(1)
      }) : '';
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  newSkillsInjury(): FormGroup {
    return this.formBuilder.group({
      danger: false,
      commentToDanger: '',
      response: false,
      commentToResponse: '',
      airway: false,
      commentToAirway: '',
      breathing: false,
      commentToBreathing: '',
      circulation: false,
      commentToCirculation: '',
    });
  }
  get abcInjury(): FormArray {
    return this.myReactiveForms.get('injuryAssessments') as FormArray;
  }
  addSkillsInjury() {
    this.abcInjury.push(this.newSkillsInjury());
  }
  dates: any
  onSubmit() {
    let data = this.myReactiveForms.value;
    data['createdDate']= new Date(Date.now())
    data['dateOfInjury'] = this.datepipe.transform(new Date(data.dateOfInjury), 'yyyy-MM-ddT06:17:51.483Z')
    data['expectedDateOfReturn'] = this.datepipe.transform(new Date(data.expectedDateOfReturn), 'yyyy-MM-ddT06:17:51.483Z')
    data['athleteId'] = this.athleteId,
    data['createdBy'] = localStorage.getItem('Name'),
    data['createdDate'] = this.datepipe.transform(new Date(data.createdDate), 'yyyy-MM-dd'),
    this.Api.addInjury(data).subscribe(
      (data: any) => {
      },
      (err) => {
        if (err.error.text === 'Injury Details added') {
          this.myReactiveForms.reset({
            diagnosticCode : '',
injuryOnset : '',
referredTo : '',
incidentType : '',
level1Injury : '',
level2Injury : '',
swelling : '',
stability : '',
strength : '',
continueToPlay : '',
treatmentStatus : '',
          }),
          (this.myReactiveForms.get('injuryAssessments') as FormArray).clear();
          $('#createInjurydata').modal('show');
          this.addSkillsInjury();
        } else {
          $('#modelError').modal('show');
        }
      }
    );
  }
  ok() {
  }
}
