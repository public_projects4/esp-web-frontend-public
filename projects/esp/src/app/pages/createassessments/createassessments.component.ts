import { Component,Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {
  FormArray,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';

import { EspPagesService } from '../../esp-pages.service';
// import { AssessmentNavService } from '../../assessment-nav.service';


declare var $: any;

@Component({
  selector: 'app-createassessments',
  templateUrl: './createassessments.component.html',
  styleUrls: ['./createassessments.component.scss']
})

export class CreateassessmentsComponent implements OnInit, OnChanges {

  modelDetails :ModalCommanService []=[{
    modelName:'Questionnaire',
  }]

  assessmId:any

  addQuestionnaireForm: FormGroup = new FormGroup({});


  orgId: any = ''
  creatBy: any = ''
  boxCount = 0
yesOrNo=false
arrayBox=false
  editProfileTabs: any;
  counter:any;

  questionsList:any
  ameen:any=[]
test: any = [];
demo: any = [];
delId:any

switchTab:any
switchTabResult:any







editQuestionnaireForm: FormGroup = new FormGroup({});








  constructor(private formBuilder: FormBuilder, private Api: AllApiService, private eventEmitterService: EspPagesService) {
    this.addQuestionnaireForm = this.formBuilder.group({
      assessmentId: this.assessmId,
      question: ['', [Validators.required]],
      type: ['', [Validators.required]],
      typedataValue: ['', [Validators.required]],
      data: this.formBuilder.array([]) ,
    }),

    this.editQuestionnaireForm = this.formBuilder.group({
      assessmentId: this.assessmId,
      question: ['', [Validators.required]],
      type: ['', [Validators.required]],
      typedataValue: ['', [Validators.required]],
      data: this.formBuilder.array([]) ,
    })
  }


  ngOnInit(): void {

    this.assessmId = localStorage.getItem('QuesAssmentId');

    this.viwQuestionsById()

    console.log('assementId',this.assessmId);

  this.switchTab =  localStorage.getItem('viewAssement');
  this.switchTabResult =  localStorage.getItem('resultAssement');

  if(this.switchTab == 'true'){
  $('.nav-tabs a[href="#view_quest"]').tab('show');

  }
  if(this.switchTabResult == 'true'){
    $('.nav-tabs a[href="#view_results"]').tab('show');

    }


  //   if (this.eventEmitterService.subsVar==undefined) {
  //       this.eventEmitterService.subsVar = this.eventEmitterService.
  //       invokeFirstComponentFunction.subscribe((name:string) => {
  //         this.openViewTab();
  // $('.nav-tabs a[href="#view_quest"]').tab('show');
  // $('#view_quest').tab('show');
  // console.log('oninit');

  //       });
  //     }

  }

  navTabs(){
  localStorage.setItem('viewAssement','false');

  }



  ngOnChanges(changes: SimpleChanges): void {
    this.viwQuestionsById()


//     if (this.eventEmitterService.subsVar==undefined) {
//       this.eventEmitterService.subsVar = this.eventEmitterService.
//       invokeFirstComponentFunction.subscribe((name:string) => {
//         this.openViewTab();
// $('.nav-tabs a[href="#view_quest"]').tab('show');
// $('#view_quest').tab('show');
// console.log('onchange');

//       });
//     }

  }



  get f() { return this.addQuestionnaireForm.controls; }


  get addInputs() : FormArray {
    return this.addQuestionnaireForm.get("data") as FormArray
  }

  newInput(): FormGroup {
    return this.formBuilder.group({
      value: '',
      name: ['', [Validators.required]],
    })
 }

 addSkills() {
  this.addInputs.push(this.newInput());
}



removeSkill(i:number) {
  this.addInputs.removeAt(i);
}





  selectedOption(data: any) {

    const val = data.target.value
    this.counter = val;

    // var boxCount = 0;

    if(val=='yesOrNo'){
      this.yesOrNo = true
      this.arrayBox = false

    let a: any = this.addQuestionnaireForm.controls['data'];

    // a['controls'].patchValue({
    //   name: 'yes',
    //   value: 1
    // });

    }
    else{
      this.yesOrNo = false
      this.arrayBox = true

      this.boxCount++

      if(this.boxCount == 1){
        this.yesOrNo = false
        this.arrayBox = true


        this.addSkills()

      }
    }



    console.log(this.boxCount);





  }


viwQuestionsById(){
  console.log(this.assessmId);

  this.Api.getQuestionsById(this.assessmId).subscribe((res:any) => {
    console.log('viewQuestinsList', res);
    this.questionsList =res
  })
}



  onSubmit(){
    const data = this.addQuestionnaireForm.value

    console.log('data',data);
    // this.orgId = localStorage.getItem('OrgID')
    // this.creatBy = localStorage.getItem('Name')

    const dataToAdd ={

      assessmentId: this.assessmId,
      question: data.question.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      type: data.type.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      typedataValue: data.typedataValue,
      data: data.data
    }


    console.log('dataToAdd',dataToAdd);




    this.Api
        .addQuestions(dataToAdd)
        .subscribe((res: any) => {

          console.log('final-response', res);

            if(res.message == 'Question Added Successfully'){

            this.eventEmitterService.onFirstComponentButtonClick();

            $('#successModal').modal('show');
    // window.location.reload()
            // $('.nav-tabs a[href="#view_quest"]').tab('show');
              this.viwQuestionsById()

            this.yesOrNo = false
        this.arrayBox = false
            this.addQuestionnaireForm.reset({
              typedataValue:''
            })





            }
            else{
              alert('something went wrong');
            }

        },(err) => {


            $('#modelError').modal('show');
            // alert("something went wrong")

          }
          );

  }

count:any = 0;

incrementCount(){
  this.count++

  localStorage.setItem('count',this.count);
}

Testing = false

openViewTab(){
  console.log('openViewTab');
 this.Testing = true
  if(this.Testing){
  $('.nav-tabs a[href="#view_quest"]').tab('show');

  console.log('openViewTab TRUE');


  }

}







editQuestion(data:any){
  console.log(data);
  this.test = data.data;
  this.ameen = []
  this.demo =[]

  this.test.forEach((element: any) => {
    console.log(element.fileName);
    this.demo.push(element);
  });

  console.log(this.demo);

  this.demo.forEach((obj:any) => {
    this.ameen.push(
    this.formBuilder.group({
    value: obj.value,
    name: obj.name,
    })
  );
})


  this.addQuestionnaireForm = this.formBuilder.group({
    assessmentId: data.assessmentId,
    id:data.id,
    question: data.question,
    type: data.type,
    typedataValue: data.typedataValue,
    data: this.formBuilder.array(this.ameen) ,
  })

  console.log(this.addQuestionnaireForm.controls['typedataValue'].value);

  var conditions =this.addQuestionnaireForm.controls['typedataValue'].value

  if(conditions == "yesOrNo"){

    this.yesOrNo = true
    this.arrayBox = false
  }
  else{
    this.yesOrNo = false
    this.arrayBox = true
  }
}

onUpdate(){
  const data =this.addQuestionnaireForm.value
  console.log('beforeApi',data);

  const dataToAdd ={

    assessmentId: this.assessmId,
    question: data.question.replace(/\b\w/g, (l:any) => l.toUpperCase()),
    type: data.type.replace(/\b\w/g, (l:any) => l.toUpperCase()),
    typedataValue: data.typedataValue,
    data: data.data,
    id:data.id
  }


  console.log('dataToAdd',dataToAdd);


  this.Api.updateQuestionsById(dataToAdd).subscribe( (res:any) =>{
    console.log('updated',res);

    if(res.message == "Questions Updated Successfully"){
      $('#updateModal').modal('show');
      this.viwQuestionsById()
      this.eventEmitterService.onFirstComponentButtonClick();

    }
    else{
      $('#modelError').modal('show');
    }

  })

}

getQueListId(id:any){
  this.delId = id
  console.log('CLICKED');
  // $('#removeQueList').modal('show');


}

deleteQueList(){
  this.Api.deleteQuessionById(this.delId).subscribe( (res:any) =>{
    console.log('deletedId',res);

    if(res.message == 'Questions Deleted Successfully'){
      $('#successfullyDeletedModal').modal('show');
      this.viwQuestionsById()
      console.log('deleted 1');
      this.eventEmitterService.onFirstComponentButtonClick();



    }
    else{
      $('#modelError').modal('show');
    }



  }
  // ,(err) => {

  //   if(err.message == 'Questions Deleted Successfully'){
  //     $('#deletedSuccessQue').modal('show');
  //     this.viwQuestionsById()
  //     console.log('deleted 1');


  //   }
  //   else{
  //     $('#modelError').modal('show');
  //   }

  // }
  )
}




































































viewsQues(data:any){
  console.log(data);
  this.test = data.data;
  this.ameen = []
  this.demo =[]

  this.test.forEach((element: any) => {
    console.log(element.fileName);
    this.demo.push(element);
  });

  console.log(this.demo);

  this.demo.forEach((obj:any) => {
    this.ameen.push(
    this.formBuilder.group({
    value: obj.value,
    name: obj.name,
    })
  );
})


  this.editQuestionnaireForm = this.formBuilder.group({
    assessmentId: data.assessmentId,
    id:data.id,
    question: data.question,
    type: data.type,
    typedataValue: data.typedataValue,
    data: this.formBuilder.array(this.ameen) ,
  })

  console.log(this.editQuestionnaireForm.controls['typedataValue'].value);

  var conditions =this.editQuestionnaireForm.controls['typedataValue'].value

  if(conditions == "yesOrNo"){

    this.yesOrNo = true
    this.arrayBox = false
  }
  else{
    this.yesOrNo = false
    this.arrayBox = true
  }
}







}
// function input() {
//   throw new Error('Function not implemented.');
// }

