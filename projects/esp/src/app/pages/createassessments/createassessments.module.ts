import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { CreateassessmentsComponent } from './createassessments.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewQuestionnaireComponent } from '../view-questionnaire/view-questionnaire.component';

const routes: Route[] = [
  {
    path: '',
    component: CreateassessmentsComponent
  }
];


@NgModule({
  declarations: [
    CreateassessmentsComponent,
    ViewQuestionnaireComponent
  ],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class CreateassessmentsModule { }
