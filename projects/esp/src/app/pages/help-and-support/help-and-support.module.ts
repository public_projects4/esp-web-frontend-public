import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelpAndSupportComponent } from './help-and-support.component';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Route[] = [
  {
    path: '',
    component: HelpAndSupportComponent
  }
];

@NgModule({
  declarations: [HelpAndSupportComponent],
  imports: [
    CommonModule,
    EspSharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class HelpAndSupportModule { }
