import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { ConfirmedValidator } from './confirmed.validator';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
declare var $: any;
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup = new FormGroup({});
  submitted: boolean = false;
  decryptedUrl: string = '';
  responseData: any;
  show_button: Boolean = false;
  show_eye: Boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private Api: AllApiService  ) {
    this.registerForm = this.formBuilder.group(
      {
        password: [
          '',
          [
            Validators.required,
            Validators.pattern(
              '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&].{8,}'
            ),
            Validators.minLength(8),
          ],
        ],
        confirmPassword: [
          '',
          [
            Validators.required,
            Validators.pattern(
              '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&].{8,}'
            ),
            Validators.minLength(8),
          ],
        ],
        code: [
          '',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(6),
            Validators.pattern('^[0-9]*$'),
          ],
        ],
      },
      {
        validator: ConfirmedValidator('password', 'confirmPassword'),
      }
    );
  }
  ngOnInit() {
    this.decryptFun();
  }
  get f() {
    return this.registerForm.controls;
  }
  decryptFun() {
    const url = window.location.href.split('=')[1];
    this.decryptedUrl = atob(url);
  }
  showPassword() {
    this.show_button = !this.show_button;
    this.show_eye = !this.show_eye;
  }
  onSubmit() {
    const data = this.registerForm.value;

    this.Api.registerApi(
      data.code,
      this.decryptedUrl,
      data.confirmPassword
    ).subscribe(
      (res: any) => {

        if (res.message === 'Password Created Successfully') {
          $('#result12').modal('show');
        } else if (
          res.message === 'Object reference not set to an instance of an object.'
        ) {
          $('#errorLoginAdmin12').modal('show');
        }
        else if(res.message === 'Password  already exists'){
          $('#alreadyExist12').modal('show');

        }
        else if(res.message == 'OTP Validation failed'){
          $('#alreadyExist12').modal('show');

        }
         else {
          $('#modelError12').modal('show');
        }
      },
      (err) => {

        if (err.error.text == 'Password Created Successfully') {
          $('#result12').modal('show');
        } else if (
          err.error.text == 'Object reference not set to an instance of an object.'
        ) {
          $('#errorLoginAdmin12').modal('show');
        }
        else if(err.error.text == 'Password  already exists'){
          $('#alreadyExist12').modal('show');

        }
        else if(err.error.text == 'OTP Validation failed'){
          $('#alreadyExist12').modal('show');

        }
         else {
          $('#modelError12').modal('show');
        }
      }
    );
  }
  eightCharLength = false;
  upperCase = false;
  lowerCase = false;
  numberCase = false;
  specialCase = false;
  isDisplay: boolean = false;
  validatePassword(data: any) {

    var valData = data.target.value;
    if (valData.length > 7) {
      this.eightCharLength = true;
    } else {
      this.eightCharLength = false;
    }
    if (valData.search(/[A-Z]/) < 0) {
      this.upperCase = false;
    } else {
      this.upperCase = true;
    }
    if (valData.search(/[a-z]/) < 0) {
      this.lowerCase = false;
    } else {
      this.lowerCase = true;
    }
    if (valData.search(/[0-9]/) < 0) {
      this.numberCase = false;
    } else {
      this.numberCase = true;
    }
    if (valData.search(/[!@#$%^&*]/) < 0) {
      this.specialCase = false;
    } else {
      this.specialCase = true;
    }
  }
  showBox(data: any) {
    this.isDisplay = true;
  }
  hideBox(data: any) {
    this.isDisplay = false;
  }
  keysPressAlphaNumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
}
