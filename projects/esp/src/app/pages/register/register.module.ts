import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { RegisterComponent } from './register.component';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
const routes: Route[] = [
  {
    path: '',
    component: RegisterComponent
  }
];
@NgModule({
  declarations: [  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatInputModule,
    ShowHidePasswordModule,
    RouterModule.forChild(routes)
  ],
})
export class RegisterModule { }
