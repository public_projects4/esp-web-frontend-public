  import { Component, OnInit } from '@angular/core';
  import { FormGroup,FormBuilder, FormControl,Validators} from '@angular/forms';

  @Component({
    selector: 'app-code-verification',
    templateUrl: './code-verification.component.html',
    styleUrls: ['./code-verification.component.scss']
  })
  export class CodeVerificationComponent implements OnInit {
    // loginForm: any;
    // constructor() { }



    // ngOnInit(): void {

    //   this.loginForm = new FormGroup({
    //     user: new FormControl('',[Validators.required]),
    //     email:new FormControl('',[Validators.required,Validators.email]),
    //   })

    // }



    // loginUser(){
    //   console.log(this.loginForm.value);

    // }

    // get user(){

    //   return this.loginForm.get('user')
    // }

    // get email(){

    //   return this.loginForm.get('email')

    // }
// public signupForm !:FormGroup;

code='';




//     constructor(private formBuilder:FormBuilder) { }

//     ngOnInit(): void {
//       this.signupForm = this.formBuilder.group({
//         code:['',Validators.required],
//         email:['',Validators.required,Validators.email]
//       })
//     }



registerForm: FormGroup = new FormGroup({});
submitted:boolean = false;

constructor(private formBuilder: FormBuilder) { }

ngOnInit() {
    this.registerForm = this.formBuilder.group({
        code: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6),Validators.pattern('^[0-9]*$')]],
        email: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        // password: ['', [Validators.required, Validators.minLength(6)]]
    });
}

// convenience getter for easy access to form fields
get f() { return this.registerForm.controls; }

onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }

    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
}



  }
