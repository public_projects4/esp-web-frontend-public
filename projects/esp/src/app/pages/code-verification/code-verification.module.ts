import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { CodeVerificationComponent } from './code-verification.component';

const routes: Route[] = [
  {
    path: '',
    component: CodeVerificationComponent
  }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class CodeVerificationModule { }
