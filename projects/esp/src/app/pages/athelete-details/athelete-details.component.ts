import { Component, Input, OnInit } from '@angular/core';
import { faWeight } from '@fortawesome/free-solid-svg-icons';
import { Chart } from 'angular-highcharts';
import { Atheleteperformance } from 'projects/esp-shared/src/lib/models/athelete-performance-block';
import { AtheleteActivecard } from 'projects/esp-shared/src/lib/models/athelete-activecard';
import { variable } from '@angular/compiler/src/output/output_ast';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import { FormGroup, FormControl,FormArray, FormBuilder, Validators } from '@angular/forms'
import { DatePipe } from '@angular/common';
import { add } from 'date-fns';

import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';


declare var $: any;

@Component({
  selector: 'app-athelete-details',
  templateUrl: './athelete-details.component.html',
  styleUrls: ['./athelete-details.component.scss']
})
export class AtheleteDetailsComponent implements OnInit {

  modelDetails :ModalCommanService []=[
    {
    modelName:'Document'
  }
  ]

  URL:any
  myVaccinationForm: FormGroup = new FormGroup({});
  myPrescriptionForm: FormGroup = new FormGroup({});

  deleted:any
  // @Input() athID:any;

  Uploaded = false;
  Deletes = false;
  Success = false;
  Deleted = false;
  Category: any;
  athlete: any[] = [];
  vaccination:any[]=[]
  id: any;
  atheId:any
  selectedAthelete: any = {};
  atheleteId: any;
  orgData: any = [];



  vaccinationBlock = true;
  viewPrescriptionData = true;
  editPrescriptionSection = false;
  viewprescriptionSection = false;


  viewVaccinationSection = false;
  editVaccinationSection = false;
  atheleteDocumentList: any = [];

  emails: any;
  mobileNo: any;
  status: any;
  athleteName: any;
  teamListByOrgId: any[] = []
  // athhiid:any = this.atheleteIiidd


// <-- original chart starts from here-->


  polarChartOptions = {
    chart: {
      polar: true,
      zoomType: 'xy',
      backgroundColor: 'transparent',
    },


    title: {
      text: 'Polar Chart'
    },
    credits: {
      enabled: false
    },

        //-------------------------- Button - Handberger Menu Styles (start)--------------------------//
    //https://api.highcharts.com/highcharts/
    navigation:{

      menuStyle:{ //menu pop-up background color
        backgroundColor:'#666666',


      },




      menuItemStyle:{//menu pop-up text color
        color:'white',

      },


      buttonOptions:{//handberger button settings
        symbolStroke:'white', //The color of the symbol's stroke or line.(inside the handbermenu)
        enabled:false, //enable and disable the menu button
        theme:{
          fill:'#666666', // for handberger button color change in "theme" in "fill"
        }

      }



    },

    //-------------------------- Button - Handberger Menu Styles (end)--------------------------//


    subtitle: {
      text: 'Also known as Radar Chart'
    },

    pane: {
      startAngle: 0,
      endAngle: 360
    },

    xAxis: {
      tickInterval: 45,
      min: 0,
      max: 360,
      labels: {
        format: '{value}°'
      }
    },

    yAxis: {
      min: 0
    },

    plotOptions: {
      series: {
        pointStart: 0,
        pointInterval: 45
      },
      column: {
        pointPadding: 0,
        groupPadding: 0
      }
    },

    series: [{
      type: 'column',
      name: 'Column',
      data: [11, 8, 10, 7, 6, 5, 4, 3, 2, 1],
      pointPlacement: 'between'
    }, {
      type: 'line',
      name: 'Line',
      data: [1, 2, 3, 4, 5, 6, 7, 8, 10]
    }, {
      type: 'area',
      name: 'Area',
      data: [1, 6, 2, 7, 3, 8, 4, 5, 9]
    }]


  };


  // <-- 2nd chart starts here--->

  columnchart = new Chart({
    chart :{
      width: 350,
      zoomType: 'xy',
      backgroundColor: 'transparent'
    },


        //-------------------------- Button - Handberger Menu Styles (start)--------------------------//
    //https://api.highcharts.com/highcharts/
    navigation:{

      menuStyle:{ //menu pop-up background color
        backgroundColor:'#666666',


      },




      menuItemStyle:{//menu pop-up text color
        color:'white',

      },


      buttonOptions:{//handberger button settings
        symbolStroke:'white', //The color of the symbol's stroke or line.(inside the handbermenu)
        enabled:false, //enable and disable the menu button
        theme:{
          fill:'#666666', // for handberger button color change in "theme" in "fill"
        }

      }



    },

    //-------------------------- Button - Handberger Menu Styles (end)--------------------------//

    title: {
      text: "Sleep Quality"
    },
    credits: {
      enabled: false
    },
    xAxis: {
      labels: {
       rotation:-45
      },

      categories: ["20/05/2021", "12/02/2020", "08/05/2021", "20/07/2021", "20/08/2020", "23/05/2021",
        "20/05/2021", "20/05/2021"]
    },
    yAxis: {
      title: {
        text: ""
      },
      gridLineColor:'#666666',
    },
    series: [{
      type: 'column',
      color: '#506ef9',
      data: [
        { y: 20.9, borderColor:'none',},
        { y: 71.5, borderColor:'none', },
        { y: 106.4, borderColor:'none', },
        { y: 129.2, borderColor:'none', },
        { y: 144.0, color: '#ffe8df', borderColor:'none',},
        { y: 176.0, borderColor:'none', },
        { y: 148.5, borderColor:'none', },
        { y: 216.4, color: '#fc5185', borderColor:'none', },
      ],

    }]
  });

  splinechart = new Chart({
    chart :{
      type:'spline',
      zoomType: 'xy',
      width: 350,
      backgroundColor: 'transparent'
    },

        //-------------------------- Button - Handberger Menu Styles (start)--------------------------//
    //https://api.highcharts.com/highcharts/
    navigation:{

      menuStyle:{ //menu pop-up background color
        backgroundColor:'#666666',


      },




      menuItemStyle:{//menu pop-up text color
        color:'white',

      },


      buttonOptions:{//handberger button settings
        symbolStroke:'white', //The color of the symbol's stroke or line.(inside the handbermenu)
        enabled:false, //enable and disable the menu button
        theme:{
          fill:'#666666', // for handberger button color change in "theme" in "fill"
        }

      }



    },

    //-------------------------- Button - Handberger Menu Styles (end)--------------------------//

    title: {
      text: "Athelete Work Load"
    },
    credits: {
      enabled: false
    },
    xAxis: {
      labels: {
       rotation:-45,

      },

      categories: ["20/05/2021", "12/02/2020", "08/05/2021", "20/07/2021", "20/08/2020", "23/05/2021",
        "20/05/2021", "20/05/2021"]
    },
    yAxis: {
      title: {
        text: ""
      }
    },
    series: [{
      type: 'spline',
      color: '#506ef9',
      data: [
        { y: 20.9},
        { y: 71.5 },
        { y: 106.4 },
        { y: 129.2 },
        { y: 144.0, color: '#ffe8df' },
        { y: 176.0 },
        { y: 148.5 },
        { y: 216.4, color: '#fc5185' },
      ],

    }]
  });

  userdetails: AtheleteActivecard[] = [
    {
    current_injury:2,
    training_days:1,
    missed_days:0,
    avatar:'',
    name:'Thomas'

    }

  ];

  statistics: Atheleteperformance[] = [
    {
      label : 'Body Weight',
      color_class: 'injured_txt',
      date_span: ' N/A',
      statistic : 'N/A',
    },
    {
      label : 'Mood Score',
      statistic : 'N/A',
      date_span: ' N/A',
      color_class: 'active_txt',
    },
    {
      label : 'Recovery Rate',
      statistic : 'N/A',
      date_span: ' N/A',
      color_class: '',
    }
  ]





  constructor(private formBuilder: FormBuilder,private Api: AllApiService,public datepipe: DatePipe) {



  }

  ngOnInit(): void {
    this.id = localStorage.getItem('OrgID');
    this.orgName = localStorage.getItem('Name');

    this.getSports()
    this.getOrganisation()
    this.getPractitioner()
this.getAthelete()
  }






  sports(data: any) {


    var selectedSports = data.target.value;
    this.Category = selectedSports;
    this.getSports();
  }

  getSports() {

    this.Api.getSportsByCat(this.Category, this.id).subscribe((data) => {
      this.athlete = data;
      console.log(this.athlete);

    });
  }

  getOrganisation() {
    this.athlete=[]
    this.Api.getAllOrganizationById(this.id).subscribe((data) => {
      this.orgData = data;
      console.log(this.orgData);

    });
  }


  getAllVaccination() {

    this.Api.getVaccinationById(this.atheId)
    .subscribe((data:any)=>{
      console.log('final response => (getVaccination)',data)
      this.vaccination=data

    })

  }






  inviteAthelete(data: any) {
    if (data.target.value == '0') {
      this.atheleteDocumentList = [];
    }

    else {
      this.atheId = data.target.value;
      console.log(this.atheId);

      this.getAthelete();
      this.getAllVaccination()
      this.getPrescription()
      this.getAssessmentListByOrgId()
      var selectedInviteAthelete = data.target.value;
      this.athlete.forEach((element) => {
        // console.log(element);

        if (selectedInviteAthelete == element.id) {
          this.URL=element.profilepic
          this.selectedAthelete = element;
          this.atheleteId = element.id;
          this.emails = element.email;
          this.athleteName = element.athleteName;
          this.mobileNo = element.mobileNo;
          this.status = element.status;
        }
      });
    }



  }

  testList: any = [];
  practitioner:any[]=[]
  newPractitioner:any[]=[]
  prescription:any[]=[]
  // pid:string=''
  // airway: any;
  // danger: any;
  // breathing: any;
  // circulation: any;
  // commentToAirway: any;
  // commentToBreathing: any;
  // commentToCirculation: any;
  // commentToDanger: any;
  // commentToResponse: any;
  // response: any;
  // dateOfInjury='abc';
  // dateFormate='dd/MM/yyyy'
  // injuryDate:any

  // injuryTime:any
  // injuryReturn:any

  public edit = {
    timeOfInjury: '',
    event: '',
  };


  consultationDate:any





  public editTreat = {
    complaints: '',
    consultationDate: '',
    consultationTime: '',
    consultationType: '',
    disease: '',
    examination: '',
    location: '',
    practitionerID: '',
    treatmentType: '',
    practitionerName: '',
    athleteId: '',
    documents: '',
  };



//   test:any=[]
demo:any=[]





// recDocData:any



  vaccinationBack(){
    this.editVaccinationSection = false
    this.viewVaccinationSection = false
    this.vaccinationBlock = true
    this.demo=[]
  }

  viewVaccinationDetails(data:any){
    console.log('viewVaccinationDetails',data);
    this.viewVaccinationSection = true
    this.editVaccinationSection = false
    this.vaccinationBlock = false


    this.myVaccinationForm = this.formBuilder.group({

      vaccinationName: data.vaccinationName,
      practionerId: data.practionerId,
      vaccinationDate: data.vaccinationDate,
      quantity: data.quantity,
      description: data.description,
      athleteId: data.athleteId
    });

  }

  getPractitioner() {
    this.Api.getPractitionerByOrganisationId(this.id)
    .subscribe((data:any)=>{
      console.log('final response => (getPractitioner)',data)
      this.practitioner=data
    })

  }

  showPractitionerData(){
    this.newPractitioner=[]

    this.practitioner.forEach((element) =>{
      if(element.id==this.editTreat.practitionerID){
        this.newPractitioner.push(element)

        console.log('this.newPractitioner',this.newPractitioner);

      }

    })

  }

  getPrescription() {

    // this.athID=localStorage.getItem('atheId')
    this.Api.getPrescriptionById(this.atheId)
    .subscribe((data:any)=>{

      this.prescription=data
      console.log('final response => (prescription)',this.prescription)

    })

  }


  viewPrescription(data:any){

    this.viewPrescriptionData = false;
    this.editPrescriptionSection = false;
  this.viewprescriptionSection = true;


    console.log('edit',data);



    this.myPrescriptionForm = this.formBuilder.group({

      drugName: data.drugName,
      practitioneerid: data.practitioneerid,
      drugStartDate: data.drugStartDate,
      duration: data.duration,
      dosage: data.dosage,
      unit: data.unit,
      frequency: data.frequency,
      routes: data.routes,
      comments: data.comments,
      atheletId: this.atheId,
      id:data.id
    });

  }


  backPrescription(){
    this.viewPrescriptionData = true;
    this.editPrescriptionSection = false;
  this.viewprescriptionSection = false;
  }




  getAssessmentListByOrgId() {
    this.Api
      .getAssessmentByAthleteId(this.athlete)
      .subscribe((res: any) => {
        this.teamListByOrgId = res;
        console.log('this.teamListByOrgId',this.teamListByOrgId);

      });
}

editVacc(data:any){

  this.viewVaccinationSection = false
    this.editVaccinationSection = true
    this.vaccinationBlock = false


  console.log('edit',data);




  this.myVaccinationForm = this.formBuilder.group({

    vaccinationName: data.vaccinationName,
    practionerId: data.practionerId,
    vaccinationDate: data.vaccinationDate,
    quantity: data.quantity,
    description: data.description,

    athleteId: data.athleteId,
    id:data.id,
    createdBy:data.createdBy,
  });


}

onEdit(){

  const edit = this.myVaccinationForm.value;



  const datatoupdate={

    vaccinationName: edit.vaccinationName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
    practionerId: edit.practionerId,
    vaccinationDate: edit.vaccinationDate,
    quantity: edit.quantity,
    description: edit.description.replace(/\b\w/g, (l:any) => l.toUpperCase()),
    id:edit.id,
    athleteId:edit.athleteId,

createdBy: edit.createdBy,

updatedBy: localStorage.getItem('Name')

  }
  console.log(datatoupdate);


  this.Api.updateVaccination(datatoupdate)
  .subscribe((data:any)=>{



    console.log('updatePrescription',data)
    this.myVaccinationForm.reset()
    this.getAllVaccination()
    this.vaccinationBack()

    if(data.message == 'Vaccination updated successfully'){
        $("#updateVaccination").modal('show');
    }

  })





}

Delete(){
  this.Api.deleteVaccination(this.deleteid).subscribe((data: any) => {
    console.log(data);

    if(data.message == 'Vaccination deleted successfully'){
      $("#deleteVaccination").modal('show');


     }
    //  this.prescription=data

      this.getAllVaccination()

    });
}
deleteid:any
deletePres(data:any){
this.deleteid=data




}


onPrescriptionEdit() {
  const edit = this.myPrescriptionForm.value;

  const datatoupdate = {
    drugName: edit.drugName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
    practitioneerid: edit.practitioneerid,
    drugStartDate: edit.drugStartDate,
    duration: edit.duration,
    dosage: edit.dosage,
    unit: edit.unit,
    frequency: edit.frequency,
    routes: edit.routes,
    comments: edit.comments.replace(/\b\w/g, (l:any) => l.toUpperCase()),
    atheletId: edit.atheletId,
    id: edit.id,
    createdBy: edit.createdBy,

updatedBy: localStorage.getItem('Name')
  };
  console.log(datatoupdate);

  this.Api.updatePrescription(datatoupdate).subscribe((data: any) => {
    console.log('final response => (updatePrescription)', data);
    console.log(data);

    this.myPrescriptionForm.reset();
    this.getPrescription();
    this.backPrescription();
    if (data) {
      $('#updatePrescription').modal('show');

    }
  });
}

editPrescription(data: any) {

  this.viewPrescriptionData = false;
  this.editPrescriptionSection = true;
this.viewprescriptionSection = false;


  console.log('edit', data);

  // this.athID = localStorage.getItem('atheId');

  this.myPrescriptionForm = this.formBuilder.group({
    drugName: data.drugName,
    practitioneerid: data.practitioneerid,
    drugStartDate: data.drugStartDate,
    duration: data.duration,
    dosage: data.dosage,
    unit: data.unit,
    frequency: data.frequency,
    routes: data.routes,
    comments: data.comments,
    atheletId: data.atheletId,
    id: data.id,
    createdBy: data.createdBy
  });
}

deletePrescriptionid:any
prescriptionDelete() {
  this.Api.deletePrescription(this.deletePrescriptionid).subscribe((data: any) => {
    //  this.prescription=data
    console.log(data);
    if (data) {
      $('#deletePrescription').modal('show');

    }
    this.getPrescription();
  });
}
getDeletePres(data: any) {
  console.log(data);
  this.deletePrescriptionid = data;
}

keyPressAlphaNumerics(event: any) {
  var inp = String.fromCharCode(event.keyCode);

  if (/[0-9   ]/.test(inp)) {
    return true;
  } else {
    event.preventDefault();
    return false;
  }
}
AtheleteId: any;

click(data: any) {
  console.log(data);
  this.AtheleteId = data;
}
t:any
Check(data: any) {
  console.log(data);

  this.getAthid = data.athleteId;
  console.log('athid', this.getAthid);

  this.t = data.documents;
  this.t.forEach((element: any) => {
    // console.log(element.documentType + '.' + element.fileName);
    this.r = element.fileName;
    console.log(this.r);
  });
  this.Api.downloadDocument(this.r, this.orgName, this.getAthid).subscribe(
    (data: any) => {
      console.log('final response==>', data);
      if (data) {
        console.log(data);


        window.open(
              data
            )
      }
    }
  );
}
getAthid:any
r:any
orgName:any

EXCEL() {
  console.log(this.getAthid);
  console.log(this.r);


}
getAthelete() {
  // this.atheleteDocumentList=[]
 // this.spinner.show();
  this.Api.getAtheleteDocument(this.atheId).subscribe((data: any) => {
    // console.log('datattatatata', data);

    this.atheleteDocumentList = data;
    //  this.rashid=data[0]
    //  this.ameen=data[1]
    //  this.ruman=data[2]

    // console.log(this.rashid);
    // console.log(this.ameen);//
    //  console.log(this.ruman);

   // this.spinner.hide();

    console.log('getDemolist', this.atheleteDocumentList);
  });
}
deleteCard() {
  this.Api.deleteDocument(this.AtheleteId).subscribe((data: any) => {
    this.getAthelete();

    if (data) {
      $('#successfullyDeletedDocumentModal').modal('show');
    }
  });
}
}
