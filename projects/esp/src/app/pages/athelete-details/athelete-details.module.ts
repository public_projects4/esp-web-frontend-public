import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { AtheleteDetailsComponent } from './athelete-details.component';
import { ChartModule } from 'angular-highcharts';
import {HighchartsChartModule} from 'highcharts-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailsTreatmentInfoComponent } from '../details-treatment-info/details-treatment-info.component';
import { DetailsInjuryDetailsComponent } from '../details-injury-details/details-injury-details.component';
import { DetailsAssessmentsComponent } from '../details-assessments/details-assessments.component';

const routes: Route[] = [
  {
    path: '',
    component: AtheleteDetailsComponent
  }
];


@NgModule({
  declarations: [
    AtheleteDetailsComponent,
    DetailsTreatmentInfoComponent,
    DetailsInjuryDetailsComponent,
    DetailsAssessmentsComponent

  ],
  imports: [
    CommonModule,
    EspSharedModule,
    ChartModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    HighchartsChartModule,
  ]
})
export class AtheleteDetailsModule { }
