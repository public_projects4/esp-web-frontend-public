import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import {  EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AtheletePrescriptionsComponent } from './athelete-prescriptions.component';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxSpinnerModule } from 'ngx-spinner';
const routes: Route[] = [
  {
    path: '',
    component: AtheletePrescriptionsComponent
  }
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
    NgxSpinnerModule,
    RouterModule.forChild(routes)
  ]
})
export class AtheletePrescriptionsModule { }
