import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AllApiService } from 'projects/esp-shared/src/lib/services/all-api.service';
import {
  FormGroup,
  FormControl,
  FormArray,
  FormBuilder,
  Validators,
} from '@angular/forms';
import Swal from 'sweetalert2';
import { Subject } from 'rxjs';
import jsPDF from 'jspdf';
import { NgxSpinnerService } from 'ngx-spinner';
import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';
export interface data {
  columnName: string;
  value: string;
}
declare var $: any;
@Component({
  selector: 'app-athelete-prescriptions',
  templateUrl: './athelete-prescriptions.component.html',
  styleUrls: ['./athelete-prescriptions.component.scss'],
})
export class AtheletePrescriptionsComponent implements OnInit ,OnChanges {
isCreate=true
isView=true
isDelete=true
isEdit=true
    modelDetails :ModalCommanService []=[
    {
    modelName:'Prescription'
  }
  ]
  @Input() athID:any;
  pdf: any;
  Id: any;
  checked = true;
  refresh: Subject<any> = new Subject();
  iD: any;
  Deleted = false;
  excel: any;
  rashid = true;
  myReactiveForms: FormGroup = new FormGroup({});
  update = true;
  added = false;
  prescription: any[] = [];
  pid: string = '';
  orgId: any = '';
  practitioner: any[] = [];
  constructor(private Api: AllApiService, private formBuilder: FormBuilder,private spinner: NgxSpinnerService,) {
    this.myReactiveForms = this.formBuilder.group({
      drugName: ['', [Validators.required]],
      practitioneerid: ['', [Validators.required]],
      drugStartDate: ['', [Validators.required]],
      duration: ['', [Validators.required]],
      dosage: ['', [Validators.required]],
      unit: ['', [Validators.required]],
      frequency: ['', [Validators.required]],
      routes: ['', [Validators.required]],
      comments: ['', [Validators.required]],
      atheletId: '',
      createdBy: '',
      updatedBy: '',
    });
  }
  formats: data[] = [
    {
      columnName: 'EXCEL',
      value: 'EXCEL',
    },
    {
      columnName: 'PDF',
      value: 'PDF',
    },
  ];
  getRoleId:any
  ngOnInit(): void {
    this.myReactiveForms.reset();
    this.iD = localStorage.getItem('OrgID');
    this.getPractitioner();
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();
  }
  itemsList: any = [];
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {

      this.itemsList = data;

      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Athlete Data') {
          if (ele.isDelete == true) {
            this.isDelete = true;
          } else {
            this.isDelete = false;
          }
          if (ele.isEdit == true) {
            this.isEdit = true;
          } else {
            this.isEdit = false;
          }
          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }
          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
            this.isCreate=false
            this.isEdit=false
            this.isDelete=false
          }
        }
      });
    });
  }
  ngOnChanges(changes:SimpleChanges) :void {
    this.getPrescription();
  }
  get f() {
    return this.myReactiveForms.controls;
  }
  passData(data: any) {
    this.pid = data.target.value;
  }
  getPractitioner() {
    this.orgId = localStorage.getItem('OrgID');
this.spinner.show()
    this.Api.getPractitionerByOrganisationId(this.orgId).subscribe(
      (data: any) => {
        this.practitioner = data;
        this.spinner.hide()
      }
    );
  }
  Type: any;
  Name = 'PRESCRIPTION';
  openPage() {
    this.getPrescription();
    this.Api.downloadOrganization(this.Id, this.Type, this.Name).subscribe(
      (data: any) => {

        if (data) {
          window.location.href = data;
        }
      }
    );
  }
  gender: any;
  reset: any;
  opens(data: any) {
    this.Type = data.target.value;

    this.reset = null;
  }
  onSubmit() {
    const data = this.myReactiveForms.value;


    const datatosend = {
      drugName: data.drugName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      practitioneerid: this.pid,
      drugStartDate: data.drugStartDate,
      duration: data.duration,
      dosage: data.dosage,
      unit: data.unit,
      frequency: data.frequency,
      routes: data.routes,
      comments: data.comments.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      atheletId: this.athID,
      createdBy: localStorage.getItem('Name')
    };

this.spinner.show()
    this.Api.addPrescription(datatosend).subscribe((data: any) => {

      if (data.message == 'Prescription created successfully') {
        $('#successModal').modal('show');

      }
      this.myReactiveForms.reset();
      this.getPrescription();
      this.spinner.hide()
    });
    this.myReactiveForms.reset();
  }
  onEdit() {
    const edit = this.myReactiveForms.value;
    const datatoupdate = {
      drugName: edit.drugName.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      practitioneerid: edit.practitioneerid,
      drugStartDate: edit.drugStartDate,
      duration: edit.duration,
      dosage: edit.dosage,
      unit: edit.unit,
      frequency: edit.frequency,
      routes: edit.routes,
      comments: edit.comments.replace(/\b\w/g, (l:any) => l.toUpperCase()),
      atheletId: this.athID,
      id: edit.id,
      createdBy: edit.createdBy,
updatedBy: localStorage.getItem('Name')
    };

this.spinner.show()
    this.Api.updatePrescription(datatoupdate).subscribe((data: any) => {


      this.myReactiveForms.reset();
      this.getPrescription();
      if (data) {
        $('#updateModal').modal('show');
      }
      this.spinner.hide()
    });
  }
  editPres(data: any) {

    this.myReactiveForms = this.formBuilder.group({
      drugName: data.drugName,
      practitioneerid: data.practitioneerid,
      drugStartDate: data.drugStartDate,
      duration: data.duration,
      dosage: data.dosage,
      unit: data.unit,
      frequency: data.frequency,
      routes: data.routes,
      comments: data.comments,
      atheletId: this.athID,
      id: data.id,
      createdBy: data.createdBy
    });
  }
  download(data: any) {
    this.Id = data;
  }
  deleteid: any;
  Delete() {
    this.Api.deletePrescription(this.deleteid).subscribe((data: any) => {
      if (data) {
        $('#successfullyDeletedModal').modal('show');
      }
      this.getPrescription();
    });
  }
  deletePres(data: any) {
    this.deleteid = data;
  }
  getPrescription() {
this.spinner.show()
    this.Api.getPrescriptionById(this.athID).subscribe((data: any) => {
      this.prescription = data;
      this.spinner.hide()
    });
  }
  EXCEL() {
    this.excel = 'EXCEL';
    this.Api.downloadOrganization(this.Id, this.excel, this.Name).subscribe(
      (data: any) => {

        if (data) {
          window.location.href = data;
        }
      }
    );
  }
  close() {
    this.myReactiveForms.reset();
  }
  keyPressAlphaNumerics(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[0-9   ]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  check(data: any) {
    this.pdf = data;
  }
  PDF() {

    const doc = new jsPDF();
    doc.text(`\n DrugName : ${this.pdf.drugName}`, 5, 17);
    doc.text(`\n Comments : ${this.pdf.comments}`, 5, 27);
    doc.text(`\n Dosage : ${this.pdf.dosage}`, 5, 37);
    doc.text(`\n Frequency : ${this.pdf.frequency}`, 5, 47);
    doc.text(`\n DrugStartDate : ${this.pdf.drugStartDate}`, 5, 57);
    doc.text(`\n Duration : ${this.pdf.duration}`, 5, 67);
    doc.text(`\n Frequency : ${this.pdf.frequency}`, 5, 77);
    doc.text(`\n State : ${this.pdf.routes}`, 5, 87);
    doc.text(`\n CountryCode : ${this.pdf.unit}`, 5, 97);
    doc.save('Prescriptions.pdf');
  }
}
