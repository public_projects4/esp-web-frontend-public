import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteMedicalhistoryComponent } from './athelete-medicalhistory.component';

describe('AtheleteMedicalhistoryComponent', () => {
  let component: AtheleteMedicalhistoryComponent;
  let fixture: ComponentFixture<AtheleteMedicalhistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheleteMedicalhistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteMedicalhistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
