import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { AtheleteMedicalhistoryComponent } from './athelete-medicalhistory.component';
import { EspSharedModule } from 'projects/esp-shared/src/public-api';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Route[] = [
  {
    path: '',
    component: AtheleteMedicalhistoryComponent
  }
];


@NgModule({
  declarations: [AtheleteMedicalhistoryComponent],
  imports: [
    CommonModule,
    EspSharedModule,
    ReactiveFormsModule,
    FormsModule,
    // BrowserModule,
    RouterModule.forChild(routes)
  ]
})
export class AtheleteMedicalhistoryModule { }
