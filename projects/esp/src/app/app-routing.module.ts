import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(o => o.LoginModule)
  },

  // {
  //   path: 'code-verification',
  //   loadChildren: () => import('./pages/code-verification/code-verification.module').then(o => o.CodeVerificationModule)
  // },

  // {
  //   path: 'code-verification',
  //   loadChildren: () => import('../../../coache/src/app/pages/code-verification/code-verification.module').then(o => o.CodeVerificationModule)
  // },

  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/forgot-password/forgot-password.module').then(o => o.ForgotPasswordModule)
   },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then(o => o.RegisterModule)
  },
  {
    path: 'register-coach',
    loadChildren: () => import('../../../coache/src/app/pages/register/register.module').then(o => o.RegisterModule)
  },
  // {
  //   path: 'coach-onboarding',
  //   loadChildren: () => import('../../../coache/src/app/pages/onboarding/onboarding.module').then(o => o.OnboardingModule)
  // },
  {
    path: 'coach-onboarding',
   loadChildren: () => import('../../../coache/src/app/pages/coach-onboarding/coach-onboarding.module').then(o => o.CoachOnboardingModule)
},
  {
    path: 'admin-onboarding',
   loadChildren: () => import('../../../esp/src/app/pages/admin-onboarding/admin-onboarding.module').then(o => o.AdminOnboardingModule)
},
//   {
// path:'onboarding',
// loadChildren: () => import('./pages/')
//   },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(o => o.HomeModule)
  },
  {
    path: '404',
    loadChildren: () => import('./pages/404/404.module').then(o => o.NotFoundModule)
  },
  {
    path: '',
    redirectTo: '/register',
    pathMatch: 'full'
  },
  {
    path: 'coach',
    loadChildren: () => import('../../../coache/src/app/pages/home/home.module').then(o => o.HomeModule)
  },
  {
    path: 'esp-super-admin',
    loadChildren: () => import('../../../esp-super-admin/src/app/pages/home/home.module').then(o => o.HomeModule)
  },
  {
    path: 'login-super-admin',
    loadChildren: () => import('../../../esp-super-admin/src/app/pages/login/login.module').then(o => o.LoginModule)
  },
  {
    path: 'login-coach',
    loadChildren: () => import('../../../coache/src/app/pages/login/login.module').then(o => o.LoginModule)
  },
  {
    path : 'print-page',
    loadChildren : () => import ('./pages/print-page/print-page.module').then(o => o.PrintPageModule)
  },

  // {
  //   path: 'register-coache',
  //   loadChildren: () => import('../../../coache/src/app/pages/register/register.module').then(o => o.RegisterModule)
  // },
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    { useHash: true }
  )],
  exports: [RouterModule],
})
export class AppRoutingModule { }
// [
//   RouterModule.forRoot(
//     appRoutes,
//     { enableTracing: true } // <-- debugging purposes only
//   )
//   // other imports here
// ],
