import { EventEmitter, Injectable } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root'
})
export class AssessmentNavService {

  navTabFunction = new EventEmitter();
  subscriptVar: Subscription;

  constructor() { }

  onNavComponentChange() {
    this.navTabFunction.emit();
  }
}
