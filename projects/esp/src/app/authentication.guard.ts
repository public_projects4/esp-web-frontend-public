import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { AuthguardServiceService } from 'projects/esp-shared/src/lib/services/authguard-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationGuard implements CanActivate {
  constructor(
    private AuthenticationService: AuthguardServiceService,
    private router: Router
  ) {}
  canActivate(): boolean {
    if (!this.AuthenticationService.gettoken()) {
      if (
        document.URL == '/login' ||
        document.URL == 'http://test.evvosports.com/#/login'
      ) {
        this.router.navigateByUrl('/login');
      } else if (
        document.URL == '/login-coach' ||
        document.URL == 'http://test.evvosports.com/#/login-coach'
      ) {
        this.router.navigateByUrl('/login-coach');
      } else if (
        document.URL == '/login-super-admin' ||
        document.URL == 'http://test.evvosports.com/#/login-super-admin'
      ) {
        this.router.navigateByUrl('/login-super-admin');
      }
    }
    return this.AuthenticationService.gettoken();
  }
}
