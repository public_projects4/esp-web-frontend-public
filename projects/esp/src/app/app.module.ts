import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { HighchartsChartModule } from 'highcharts-angular';
import { ChartModule } from 'angular-highcharts';
import { RegisterComponent } from './pages/register/register.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CodeVerificationComponent } from './pages/code-verification/code-verification.component';
import { FlatpickrModule } from 'angularx-flatpickr';
import { MatDialogModule } from '@angular/material/dialog';

import 'flatpickr/dist/flatpickr.css';
import {MatChipsModule} from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
// import { AuthService } from './auth.service';
// import { AngularFireModule } from "@angular/fire";
// import { AngularFireAuthModule } from "@angular/fire/auth";
// import { AngularFireStorageModule } from '@angular/fire/storage';
// import { AngularFirestoreModule } from '@angular/fire/firestore';
// import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from 'projects/esp/src/environments/environment';
import { DatePipe } from '@angular/common';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { EspPagesService } from './esp-pages.service';
import { AssessmentNavService } from './assessment-nav.service';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    CodeVerificationComponent,
  ],
  imports: [
    BrowserModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FlatpickrModule,
    MatInputModule,
    MatIconModule,
    FlatpickrModule.forRoot(),
    BrowserAnimationsModule,
    ChartModule,
    NgxIntlTelInputModule,
    AppRoutingModule,
    LoadingBarModule,
    LoadingBarHttpClientModule,
    HighchartsChartModule ,
    FormsModule,
    MatDialogModule,
    MatChipsModule,


    HttpClientModule ,
    // AngularFireModule.initializeApp(environment.firebasedata),
    // AngularFirestoreModule,
     // firestore
    // AngularFireAuthModule,
     // auth
    // AngularFireStorageModule,
     // storage
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
    }),
   CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ],
   providers: [
    // AuthService
    DatePipe,
    EspPagesService,
    // AssessmentNavService

    ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private library: FaIconLibrary) {
    library.addIconPacks(fas, far, fab);
  }
 }
