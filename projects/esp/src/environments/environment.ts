// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,

  baseurl: 'https://api.evvosports.com/api/',

  
  firebasedata: {
    apiKey: "AIzaSyCAQ3QGbglfmH__x38sP8FPFz0IaTIYoII",
    authDomain: "evvosports.firebaseapp.com",
    projectId: "evvosports",
    storageBucket: "evvosports.appspot.com",
    messagingSenderId: "124944705394",
    appId: "1:124944705394:web:b46713ed0bec7258471feb",
    measurementId: "G-ZYKK8100BX"
  }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
