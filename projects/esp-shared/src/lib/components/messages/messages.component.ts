import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AllApiService } from '../../services/all-api.service';
import { DatePipe } from '@angular/common';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { environment } from '../../../../../esp-super-admin/src/environments/environment'
import {ModalCommanService} from 'projects/esp-shared/src/lib/models/modal-comman-service';

declare var $: any;


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  modelDetails :ModalCommanService []=[

  ]

  selectedInbox = true;


isCreate=true
isDeletes=true
isView=true
isEdit=true
  msgCount: any;
  isEnableMsgHistory: boolean = false;
  isRowSelected: boolean = false;
  inboxList: any = [];
  sentList: any = [];
  id: any;
  orgName: any;
  orgData: any;
  sportsCategory: any;
  teamsList: any;
  athletesList: any;
  teamId: any;
  file: any;
  fileCoach: any;
  uploadStatus: String = '';
  currentAthleteId: any = '';
  uploadedFile: string = '';
  description: any;
  sentButton: Boolean = false;
  sports: string = '';
  teams: string = '';
  athelete: string = '';
  subject: string = '';
  errorMessage: string = '';
  secondBlock = false;
  firstBlock = true;
  roleId: string;
  coachId: string;
  currentRole: any;
  coachesList: any;
  tempCoachesList: any;
  selectedCoachId: any = '';
  selectedMessageId: any;
  isDelete: Boolean = false;
  selectedMessage: any[];
  selectedTeamName: any;
  fileName:any;
  fileNameCoach:any;
  enablePin = false;
  replayUploadedNewMsgBtn:boolean = false;
  hideUploadNewMsgBlock = false;
  fileUploadEnabled = false;
  readColor:any



  coach : string  = '';
  messageIdsList =[];
  replyMessageData :any;
  isReplyMsg: boolean =false;
  arrPush:string='';
  pushedData  :any =[];
  pushedDataCoach  :any =[];
  pushedDataNewMsg  :any =[];
  replayFileUploadEnabled = false
  getRoleId:any;
  receiverToken: string;
  isExists: boolean = true;
  uploadedNewMsgBtn: boolean =true;
  teamsListCoach: any=[];
  thirdBlock: boolean;
  uploadedFileArray: any=[];
  isSuperAdmin: boolean =false;
  tokenId: any;
  tokenObj: any;
  tokenObj1: any;
  senderToken: any;
  errorMessage2: string;
  inboxList2: any; //used for deleting messages
  // ,private angularFireMessaging: AngularFireMessaging

  constructor(private Api: AllApiService, private datepipe: DatePipe,private angularFireMessaging: AngularFireMessaging) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
      _messaging.onMessage = _messaging.onMessage.bind(_messaging);
      _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
      )
   }

  ngOnInit(): void {
   this.requestPermission()
   this.receiveMessage();
    this.id = localStorage.getItem('OrgID');
    this.orgName = localStorage.getItem('Name');
    this.roleId = localStorage.getItem('RoleId');
    if (localStorage.getItem('coachId') != undefined)
      this.coachId = localStorage.getItem('coachId');
    this.getOrganisation();
    this.getCoachesByOrgId(this.id);
    //this.getTeamBySportsByCoachId(this.coachId);
    this.Api.getParentNode(this.roleId).subscribe((data: any) => {
      this.currentRole = data.roleName;
      //this.currentRole = 'Coach';
      if (data.roleName == 'Admin'){
        this.getMessagesByReceiverId(this.id);
        this.getTokenById(this.id);
      }
      else if (data.roleName == 'Coach'){
        this.getMessagesByReceiverId(this.coachId);
        this.getTokenById(this.coachId);
      }
    })
    this.getRoleId = localStorage.getItem('RoleId');
    this.getSideNavItems();
    // this.requestPermission()
    // this.receiveMessage();
    //this.saveOrUpdateToken();
  }
  itemsList: any = [];
  getSideNavItems() {
    this.Api.getSideNavbarItems(this.getRoleId).subscribe((data: any) => {
      console.log('sidebar', data);
      this.itemsList = data;
      console.log(this.itemsList);
      this.itemsList.forEach((ele: any) => {
        if (ele.parentNodeName == 'Messages') {
          if (ele.isDelete == true) {
            this.isDelete = true;
          } else {
            this.isDelete = false;
          }

          if (ele.isEdit == true) {
            this.isEdit = true;
          } else {
            this.isEdit = false;
          }

          if (ele.isCreate == true) {
            this.isCreate = true;
          } else {
            this.isCreate = false;
          }

          if (ele.isView == true) {
            this.isView = true;
          } else {
            this.isView = false;
            this.isCreate=false
            this.isDelete=false
            this.isEdit=false
          }
        }
      });
    });
  }
  getCoachesByOrgId(id) {
    this.Api.getCoachesByOrgId(id).subscribe((data: any) => {
      this.coachesList = data;
      this.tempCoachesList = data;
    })
  }
  getOrganisation() {
    this.Api.getAllOrganizationById(this.id).subscribe((data) => {
      this.orgData = data;
    });
  }
  getMessagesByReceiverId(id: any) {
    //this.sentList = [];
    this.Api.getMessagesByReceiveId(id).subscribe((data: any) => {
      console.log(data);
      this.inboxList = [];
      this.inboxList2 = [];
      data.forEach(element => {
        var message = Object.create({});
        message.name = element.recieverName;
        message.teamName = element.teamName;
        message.description = element.description;
        message.subject = element.subject;
        message.date = element.date;
        message.dateToDisplay = this.datepipe.transform(new Date(), element.date);
        message.id = element.id;
        message.createdBy = element.createdBy;
        message.attachment = element.attachment;
        message.recieverId = element.recieverId;
        message.senderId = element.senderId;
        message.createdDate = element.createdDate;
        message.days = element.days;
        message.image = element.image;
        message.isArchieved = element.isArchieved;
        message.isDeleted = element.isDeleted;
        message.isRead = element.isRead;
        message.isReciverDeleted = element.isReciverDeleted;
        message.isSenderDeleted = element.isSenderDeleted;
        message.isSent = element.isSent;
        message.recieverName = element.recieverName;
        message.replyId = element.replyId;
        message.senderName = element.senderName;
        message.teamId = element.teamId;
        message.updatedBy = element.updatedBy;
        message.updatedDate = element.updatedDate;
        this.inboxList.unshift(message);
        this.inboxList2.push(message);
      })
      console.log(this.inboxList);
      for(var i=0;i<=this.inboxList.length-1;i++){
        for(var j=i+1;j<=this.inboxList.length-1;j++){
          if(this.inboxList[i].subject.toLowerCase()==this.inboxList[j].subject.toLowerCase() && (this.inboxList[j].replyId !=null || i==0) && this.inboxList[i].senderId ==this.inboxList[j].senderId && this.inboxList[i].recieverId ===this.inboxList[j].recieverId)
          this.inboxList[j].name="duplicate";
        }
      }
     this.inboxList = this.inboxList.filter(msg=>msg.name !=="duplicate");
      this.msgCount = this.inboxList.length;
    })
  }
  getMessagesBySenderId(id) {
    //this.inboxList = [];
    this.Api.getMessagesBySentId(id).subscribe((data: any) => {
      console.log(data);
      this.sentList = [];
      data.forEach(element => {
        var message = Object.create({});
        message.name = element.recieverName;
        message.teamName = element.teamName;
        message.description = element.description;
        message.subject = element.subject;
        message.date = element.date;
        message.dateToDisplay = this.datepipe.transform(new Date(), element.date);
        message.id = element.id;
        message.createdBy = element.createdBy;
        message.attachment = element.attachment;
        message.recieverId = element.recieverId;
        message.senderId = element.senderId;
        message.createdDate = element.createdDate;
        message.days = element.days;
        message.image = element.image;
        message.isArchieved = element.isArchieved;
        message.isDeleted = element.isDeleted;
        message.isRead = element.isRead;
        message.isReciverDeleted = element.isReciverDeleted;
        message.isSenderDeleted = element.isSenderDeleted;
        message.isSent = element.isSent;
        message.recieverName = element.recieverName;
        message.replyId = element.replyId;
        message.senderName = element.senderName;
        message.teamId = element.teamId;
        message.updatedBy = element.updatedBy;
        message.updatedDate = element.updatedDate;
        this.sentList.unshift(message);
      })
    })
  }
  getTeamNameById(id: any) {
    this.Api.getTeamById(id).subscribe((data: any) => {
      return data.teamName;
    })
  }

  showSentMessages() {

    this.selectedInbox = false;


    this.sentButton = true;
    this.isEnableMsgHistory = false;
    if (this.currentRole == 'Admin')
      this.getMessagesBySenderId(this.id);
    else if (this.currentRole == 'Coach')
      this.getMessagesBySenderId(this.coachId);
  }

  showInbox() {
    this.inboxList = [];
    this.selectedInbox = true;


    this.sentButton = false;
    this.isEnableMsgHistory = false;
    if (this.currentRole == 'Admin')
      this.getMessagesByReceiverId(this.id);
    else if (this.currentRole == 'Coach')
      this.getMessagesByReceiverId(this.coachId);
  }

  handleSportsCategoryChange(data: any) {
    this.teamsList = [];
    this.sportsCategory = data.target.value;
    if (this.sportsCategory != '') {
      if (this.firstBlock && this.currentRole =='Admin' || this.secondBlock && this.currentRole=='Coach')
        this.Api.getTeamBySportsCat(this.id, this.sportsCategory).subscribe((data) => {
          this.teamsList = data;
        })
      else {
        this.coachesList = this.tempCoachesList.filter(coach => coach.sportsCategory === this.sportsCategory)
      }
    } else {
      this.athelete = '';
      this.teams = '';
      this.athletesList = [];
      this.getCoachesByOrgId(this.id);
    }
  }
  handleTeamChange(data: any) {
    this.teamId = data.target.value;
    if(this.teamsList!=undefined)
    this.teamsList.forEach(team => {
      if(team.id === this.teamId)
      this.selectedTeamName = team.teamName;
    });
    if(this.teamsListCoach.length>0 || this.teamsListCoach!=undefined)
    this.teamsListCoach.forEach(team => {
      if(team.id === this.teamId)
      this.selectedTeamName = team.teamName;
    });
    console.log(this.teamId,this.selectedTeamName);
    if (this.teamId != '') {
      this.Api.getAthleteByTeamId(this.teamId).subscribe((data) => {
        this.athletesList = data;
      })
    } else {
      this.athelete = '';
      this.athletesList = [];
      this.currentAthleteId = '';
    }
  }
  handleCoachChange(data: any) {
    this.selectedCoachId = data.target.value;
  }
  onClickNewMessage() {
    console.log(this.currentRole);
    this.firstBlock = true;
    this.secondBlock = false;
    this.thirdBlock = false;
    this.resetFormData();
    this.isDelete = false;
    if(this.currentRole =='Coach')
    this.getTeamBySportsByCoachId(this.coachId)
    if (this.currentRole === 'Coach')
      $('#coachNewMessageModal').modal('show');
    else if (this.currentRole === 'Admin')
      $('#newMessageModal').modal('show');
  }

  onClickReply(data:any) {
    this.isDelete = false;
    this.resetFormData();
    this.isReplyMsg = true;
    this.replyMessageData = data[0];
    console.log(this.replyMessageData);
    $('#replyModal').modal('show');
  }


  openUploadModel() {
    console.log('working uploadpin');
    // this.enablePin = false;


    if (this.currentRole == 'Admin' && this.firstBlock && this.currentAthleteId == '' || this.currentAthleteId == undefined) {
      this.errorMessage = 'Select valid athelete';
      setTimeout(() => {
        this.errorMessage = '';
        this.enablePin = false;
      }, 5000);
    }
    else if (this.currentRole == 'Admin' && this.secondBlock && this.selectedCoachId == undefined) {
      this.errorMessage = 'Select valid coach';
      setTimeout(() => {
        this.errorMessage = '';
        this.enablePin = false;

      }, 5000);
    }
    else{
      this.enablePin = true;

    }

  }




  uploadFile() {
    console.log('its working');
    var testData = new FormData();
    var receiverId = '';
    if (this.currentRole == 'Admin')
      receiverId = this.id;
    else if (this.currentRole = 'Coach')
      receiverId = this.coachId;
    if (this.file != undefined) {
      console.log(this.file);
      console.log(this.file.name);

      // this.testData.append('file', this.file);
      testData.append('file', this.file, this.file.name);

      this.Api.uploadMessageFile(this.orgName, receiverId, testData).subscribe(result => {
        this.uploadedFileArray.push(result);
        this.uploadStatus = 'Successfully Uploaded';
        this.hideUploadNewMsgBlock = false;
        this.pushDataCoach();
      },
        error => {
          this.uploadStatus = JSON.parse(error.error).message;
        })
      setTimeout(() => {
        this.uploadStatus = '';
      }, 5000);
    }
  }
  fileChangeEvent(data: any) {
    // if (data.length > 0) {
    //   this.file = data[0];
    // }
  }

  uploadAttachment = (files: any) => {
    this.file = '';
    this.fileName = '';
    this.hideUploadNewMsgBlock = true;
    let fileToUpload = <File>files[0];
    console.log(fileToUpload);
    this.file = fileToUpload
    this.fileName = fileToUpload.name;
  }
  handleAthleteChange(data: any) {
    this.currentAthleteId = data.target.value;
    // this.openUploadModel();
  }
  sendMessage() {
    var messageData = Object.create({});
    messageData.createdDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString(),
    messageData.updatedDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString(),
    messageData.subject = this.subject+'_'+new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString();
    messageData.description = this.description;
    messageData.date = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString(),
    messageData.attachment = this.uploadedFileArray;
    messageData.createdBy = this.orgName // admin name or coach name
    messageData.senderName = this.orgName;
    messageData.isDeleted = false;
    if (this.currentRole === 'Admin') {
      messageData.senderId = this.id;
      if(this.firstBlock){
      messageData.recieverId = this.currentAthleteId;
      messageData.teamId = this.teamId;
      messageData.teamName = this.selectedTeamName;
      }
      else if(this.thirdBlock)
      messageData.recieverId = environment.superAdminId;
      else
      messageData.recieverId = this.selectedCoachId;
    } else if (this.currentRole == 'Coach') {
      messageData.senderId = this.coachId;
      if(this.secondBlock){
        messageData.recieverId = this.currentAthleteId;
        messageData.teamId = this.teamId;
        messageData.teamName = this.selectedTeamName;
        }
        else
      messageData.recieverId = this.id;
    }
    if(this.isReplyMsg){
    messageData.replyId = this.selectedMessageId;
    messageData.recieverId = this.replyMessageData.senderId;
    messageData.subject = this.replyMessageData.subject;
    messageData.role = this.currentRole;
    }
    console.log(messageData);
    this.Api.sendMessage(messageData).subscribe((data: any) => {
      if (data.message === 'Message sent successfully') {
        // $('#result').modal('show');
        $('#messageSentSuccessModal').modal('show');

        this.pushedData = []
        this.pushedDataNewMsg = []

        this.getMessagesBySenderId(this.currentRole == 'Admin' ? this.id : this.coachId);
        this.getMessagesByReceiverId(this.currentRole == 'Admin' ? this.id : this.coachId);
        this.getReceiverTokenById(messageData.recieverId);
        if(this.isReplyMsg)
        //this.selectedMessage.unshift(messageData);
        this.selectedMessage.push(messageData);
        // this.getTokenById(messageData.recieverId);
        // this.sendPushNotification();
      }
    })
  }
  deleteMessage() {
    var messageIdsList = [];
    this.isDelete = true;
    console.log(this.messageIdsList)
    console.log(this.messageIdsList.length)
    this.messageIdsList.forEach(msgObj=>{
      messageIdsList.push(msgObj.id);
    })
    if(this.messageIdsList.length <=1){
    this.Api.deleteMessageById(this.selectedMessageId,this.currentRole == 'Admin' ? this.id : this.coachId).subscribe((data: any) => {
      if (data.success ==true) {
        if (this.sentButton)
          this.getMessagesBySenderId(this.currentRole == 'Admin' ? this.id : this.coachId);
        else
          this.getMessagesByReceiverId(this.currentRole == 'Admin' ? this.id : this.coachId);
        $('#messageSuccessfullyDeletedModal').modal('show')
        this.isRowSelected = false;
        this.isEnableMsgHistory = false;
      }
    })
  }else{
    this.Api.deleteMultipleMessages(this.currentRole == 'Admin' ? this.id : this.coachId,messageIdsList).subscribe((data: any) => {
      if (data.success === true) {
        if (this.sentButton)
          this.getMessagesBySenderId(this.currentRole == 'Admin' ? this.id : this.coachId);
        else
          this.getMessagesByReceiverId(this.currentRole == 'Admin' ? this.id : this.coachId);
        $('#messageSuccessfullyDeletedModal').modal('show')
        this.isRowSelected = false;
        this.isEnableMsgHistory = false;
      }
    })
  }
  this.messageIdsList = [];
  }


  onClickMsgHistory(row: any) {

    if(row.isRead == true){
      this.readColor = true;
    }
    else{
      this.readColor = false;

    }
    if(row.recieverId !=undefined && row.recieverId ==environment.superAdminId)
    this.isSuperAdmin = true;
    else  if(row.senderId !=undefined && row.senderId ==environment.superAdminId)
    this.isSuperAdmin = true;
    else
    this.isSuperAdmin = false;
    console.log(row);

    localStorage.setItem('printSubject',row.subject)
    localStorage.setItem('printDescription',row.description)
    localStorage.setItem('printName',row.name)


    this.selectedMessage = [];
    this.isEnableMsgHistory = true;
    localStorage.setItem('isRefresh',this.isEnableMsgHistory.toString());
    this.selectedMessageId = row.id;
    var currentMessage = Object.create({});
    var files = [];
    currentMessage.id = row.id;
    currentMessage.image = row.image;
    currentMessage.description = row.description;
    currentMessage.date = this.datepipe.transform(row.date, 'dd MMM yyyy, hh:mm:a');
    localStorage.setItem('printDate',currentMessage.date)

    // currentMessage.roleName = row.name;
    currentMessage.roleName = this.sentButton == true ? row.name : row.senderName;

    currentMessage.subject = row.subject;
    row.attachment.forEach(attachment=>{
      var fileObj = Object.create({});
      fileObj.fileName = attachment.split('/')[4];
      fileObj.fileLocation = attachment;
      files.push(fileObj);
    })

    currentMessage.files= files;
    // if(row.attachment[0]!=''){
    // currentMessage.fileName = row.attachment[0].split('/')[4];
    // currentMessage.fileLocation = row.attachment[0];
    // }else{
    //   currentMessage.fileName = '';
    //   currentMessage.fileLocation = '';
    // }
    currentMessage.teamName = row.teamName;
    currentMessage.senderId = row.senderId;
    currentMessage.recieverId = row.recieverId;
    currentMessage.dateToDisplay = row.dateToDisplay;
    if(this.sentButton ==false){
    var selectedMessage = [];
    this.inboxList2.forEach(msg=>{
      if(msg.subject ===row.subject && msg.senderId ===row.senderId && msg.recieverId==row.recieverId){
        msg.roleName = (this.sentButton == true) ? row.name : row.senderName;
        this.selectedMessage.push(msg);
        selectedMessage.push({"msg":msg,"isReplyMsg":false});
        this.Api.getReplyMessagesById(msg.id).subscribe((data:any)=>{
          data.messages.forEach(element => {
            console.log(element);
            var currentMessage = Object.create({});
            currentMessage.description = element.description;
            currentMessage.date = element.date;
            var dateIST = new Date(element.date);
            dateIST.setHours(dateIST.getHours() - 5);
            dateIST.setMinutes(dateIST.getMinutes() - 30);
            currentMessage.dateToDisplay = dateIST;
            //currentMessage.date = this.datepipe.transform(element.date, 'dd MMM yyyy, hh:mm:a');
            element.attachment.forEach(attachment=>{
              var fileObj = Object.create({});
              fileObj.fileName = attachment.split('/')[4];
              fileObj.fileLocation = attachment;
              files.push(fileObj);
            })
            currentMessage.files= files;
            currentMessage.teamName = element.teamName;
            currentMessage.senderId = element.senderId;
            currentMessage.recieverId = element.recieverId;
            //currentMessage.dateToDisplay = element.dateToDisplay;
            currentMessage.replyId = element.replyId;
            //this.selectedMessage.push(currentMessage);
            currentMessage.role = this.currentRole;
            selectedMessage.push({ "msg": currentMessage, "isReplyMsg": true });
           
              this.selectedMessage.forEach((msg2,index2)=>{
                selectedMessage.some((msg,index)=>{
                  if(msg2.id ==msg.msg.replyId){
                  this.selectedMessage.splice(index2+1,0,msg.msg);
                  selectedMessage.splice(index,1);
                  return true;
                  }
              })
            })
            // for (var i = 0; i <= selectedMessage.length - 1; i++) {
            //   //this.selectedMessage.push(selectedMessage[i].msg)
            //   for (var j = i + 1; j <= selectedMessage.length - 1; j++) {
            //     if (selectedMessage[i].isReplyMsg === false && selectedMessage[i].id === selectedMessage[j].msg.replyId) {
            //       this.selectedMessage.splice(i + 1, 0, selectedMessage[j].msg)
            //       selectedMessage.splice(j, 1);
            //     }
            //     //this.selectedMessage.push(selectedMessage[j].msg)
            //   }
            // }
            console.log(this.selectedMessage);
          });
        })
      }
    })
  }else{
    this.selectedMessage.push(currentMessage);
  }
   //this.selectedMessage = [];
console.log(this.selectedMessage);

    const updateMessageRead = {
      createdBy: row.createdBy,
      createdDate: row.createdDate,
      updatedBy: row.recieverName,
      updatedDate: row.updatedDate,
      isDeleted: row.isDeleted,
      senderId: row.senderId,
      recieverId: row.recieverId,
      teamId: row.teamId,
      replyId: row.replyId,
      subject: row.subject,
      description: row.description,
      id:row.id,
      date: row.date,
      attachment: row.attachment,
      isSent: row.isSent,
      isRead: true,
      isArchieved: row.isArchieved,
      senderName: row.senderName,
      recieverName: row.recieverName,
      image: row.image,
      teamName: row.teamName,
      days: row.days,
      isSenderDeleted: row.isSenderDeleted,
      isReciverDeleted: row.isReciverDeleted
    }

    console.log(updateMessageRead);

    this.Api.updateMessage(updateMessageRead).subscribe( (data: any) => {
      console.log(data);
    })





  }
  openAttachment(url: any) {
    window.open(url);
  }

  onClickBackToList() {
    this.isEnableMsgHistory = false;
    localStorage.setItem('isRefresh',this.isEnableMsgHistory.toString())
    if(this.selectedInbox){
    this.showInbox();
    }
    else{
    // this.showSentMessages();
    }



  }

  onRowSelected(dataItem: any, event, id: any) {
    console.log(id);
    debugger
    this.isRowSelected = false;
    this.selectedMessageId = id;
    dataItem.isSelected = event.target.checked;
    if(dataItem.isSelected && this.sentButton ==false){
      this.inboxList2.forEach(msg=>{
        if(msg.subject === dataItem.subject)
        this.messageIdsList.push({"id":msg.id,"subject":msg.subject});
      })
    }
    //this.messageIdsList.push(dataItem.id);
    if(dataItem.isSelected ==false)
      this.messageIdsList = this.messageIdsList.filter(messageId => messageId.subject !==dataItem.subject)
    if (this.inboxList.filter((x) => x.isSelected == true).length > 0) {
      this.isRowSelected = true;
    }
    if (this.sentList.filter((x) => x.isSelected == true).length > 0) {
      this.isRowSelected = true;
    }
  }

  myFun() {
    this.firstBlock = true;
    this.secondBlock = false;
    this.thirdBlock = false;
    this.resetFormData();
  }
  resetFormData() {
    this.sports = '';
    this.coach = '';
    this.teams = '';
    this.athelete = '';
    this.currentAthleteId = '';
    this.selectedCoachId = '';
    this.subject = '';
    this.description = '';
    this.uploadedFile = ''
    this.pushedDataNewMsg = []
    this.pushedDataCoach = []
    this.file =''
    this.fileName =''
    this.hideUploadNewMsgBlock = false;
    this.uploadedFileArray = [];
  }
  secondFun() {
    this.firstBlock = false;
    this.secondBlock = true;
    this.thirdBlock = false;
    this.resetFormData();
  }
  cancelUpload() {
    this.file =''
    this.fileName =''
    this.hideUploadNewMsgBlock = false;
    this.uploadedFile = '';
  }
  requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
        this.senderToken = token;
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }


  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.log("new message received. ", payload);
      })
  }
  getTokenById(id:any){
    this.Api.getTokenById(id).subscribe((data:any)=>{
      if(data.length ==0)
      this.isExists = false;
      else{
      this.isExists = true;
      this.tokenObj = data;
      }
      this.saveOrUpdateToken();
  });
  }
  saveOrUpdateToken(){
    console.log(this.senderToken);
    var tokenInfo = Object.create({});
    if(this.isExists ==false){
    tokenInfo.createdBy = this.orgName;
    tokenInfo.createdDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString();
    tokenInfo.updatedDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString();
    tokenInfo.isDeleted = false;
    tokenInfo.tokenPasserId = this.currentRole =='Admin' ? this.id: this.coachId;
    tokenInfo.fcmToken = this.senderToken;
    tokenInfo.date = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString();
    if(tokenInfo.fcmToken !=undefined && tokenInfo.tokenPasserId !=undefined)
    this.Api.saveToken(tokenInfo).subscribe((data)=>{
      console.log(data);
    })
  }
    else{
      tokenInfo.id = this.tokenObj[0].id;
      tokenInfo.createdBy = this.tokenObj[0].createdBy;
      tokenInfo.createdDate = this.tokenObj[0].createdDate;
      tokenInfo.updatedDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString();
      tokenInfo.isDeleted = false;
      tokenInfo.tokenPasserId = this.tokenObj[0].tokenPasserId;
      tokenInfo.fcmToken = this.senderToken;
      tokenInfo.date = this.tokenObj[0].createdDate;
      tokenInfo.updatedBy =  this.orgName;
      console.log(tokenInfo);
      if(tokenInfo.fcmToken !=undefined && tokenInfo.tokenPasserId !=undefined)
      this.Api.updateToken(tokenInfo).subscribe((data)=>{
        console.log(data);
      })
    }
  }
  sendPushNotification(){
    console.log("push method" + this.receiverToken);
    var data =
    {
      "notification": {
       "title": this.subject,
       "body": this.description,
       "click_action": this.currentRole =='Coach' ? 'http://localhost:4200/#/home/messages' : 'http://localhost:4200/#/coach/messages'
      },
      "to" : this.receiverToken,
     }
     if(this.receiverToken!=undefined)
  this.Api.sendPushNotification(data).subscribe((data)=>{
    console.log(data);
  })
}
  pushDataCoach(){
    this.pushedDataCoach.push(this.fileName)
    this.fileNameCoach = '';
    console.log(this.pushedDataCoach);
  }

  deleteUploadedFile(i:any){
    console.log(i);
  this.pushedDataCoach.splice(i, 1);
  this.uploadedFileArray.splice(i,1);
  }

  getTeamBySportsByCoachId(id:any){
    this.Api.getTeamBySportsByCoachId(id).subscribe((data:any)=>{
      data.forEach(element => {
        this.teamsListCoach.push(element);
      });
      console.log(this.teamsListCoach);
    })
  }
  superAdminRadio(){
    this.firstBlock = false;
    this.secondBlock = false;
    this.thirdBlock = true;
    this.resetFormData();
  }

  printPage(){

   var a = window.open(
      '/#/print-page'
    )

    a.window.print()

  }
  getReceiverTokenById(id:any){
    this.Api.getTokenById(id).subscribe((data:any)=>{
      console.log(data.length);
      if(data.length === 0){
      //this.errorMessage = "receiver don't had valid token to send push notification";
      console.log(this.errorMessage);
    }
      else{
      this.receiverToken = data[0].fcmToken;
      this.sendPushNotification();
      }
    })
  }
  confirmDelete(){
     $('#removeConfirm').modal('show');
  }
}
