import { Component, OnInit,Input } from '@angular/core';
import { ModalCommanService } from '../../models/modal-comman-service';


@Component({
  selector: 'esp-modal-already-exist',
  templateUrl: './modal-already-exist.component.html',
  styleUrls: ['./modal-already-exist.component.css']
})
export class ModalAlreadyExistComponent implements OnInit {

  @Input() props: ModalCommanService = {
    modelName: '',
  };

  constructor() { }

  ngOnInit(): void {
  }

}
