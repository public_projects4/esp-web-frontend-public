import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashTeamsLeaderBoardComponent } from './dash-teams-leader-board.component';

describe('DashTeamsLeaderBoardComponent', () => {
  let component: DashTeamsLeaderBoardComponent;
  let fixture: ComponentFixture<DashTeamsLeaderBoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashTeamsLeaderBoardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashTeamsLeaderBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
