import { Component,Input, OnInit } from '@angular/core';
import { DashboardLeaderBoard } from '../../models/dashboard-leader-board';

@Component({
  selector: 'esp-dash-teams-leader-board',
  templateUrl: './dash-teams-leader-board.component.html',
  styleUrls: ['./dash-teams-leader-board.component.css']
})
export class DashTeamsLeaderBoardComponent implements OnInit {

  @Input() props: DashboardLeaderBoard = {
    position:'',
    name: '',
    which_coach: '',
    which_team:'',
    item_1:1,
    item_2:1,
    item_3:0,
    avatar:'',
    color_class:''
  }
  constructor() { }

  ngOnInit(): void {
  }

}
