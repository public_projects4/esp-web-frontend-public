import { Component, Input, OnInit } from '@angular/core';
import { Atheleteblockproperties } from '../../models/athelete-block';

@Component({
  selector: 'esp-athelete-block',
  templateUrl: './athelete-block.component.html',
  styleUrls: ['./athelete-block.component.css']
})
export class AtheleteBlockComponent implements OnInit {
  @Input() details: Atheleteblockproperties = {
    order:5,
    name: '',
    body_mass:2,
    status:'',
    current_injury:1,
    training_days:1,
    missed_days:0,
    avatar:'',
    color_class:''
  }
  constructor() { }

  ngOnInit(): void {
  }

  

}
