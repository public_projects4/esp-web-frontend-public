import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageInfoBarComponent } from './page-info-bar.component';

describe('PageInfoBarComponent', () => {
  let component: PageInfoBarComponent;
  let fixture: ComponentFixture<PageInfoBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageInfoBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageInfoBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
