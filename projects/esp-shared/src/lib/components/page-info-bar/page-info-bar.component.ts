import { Component, Input, OnInit } from '@angular/core';
import { PageInfoProperty } from '../../models';

@Component({
  selector: 'esp-page-info-bar',
  templateUrl: './page-info-bar.component.html',
  styleUrls: ['./page-info-bar.component.css']
})
export class PageInfoBarComponent {
  @Input() props: PageInfoProperty[] = [{
    name: '',
    value: ''
  }];
 
}
