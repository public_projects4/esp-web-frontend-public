import { Component, Input, OnInit } from '@angular/core';
import { CoachList } from '../../models/coach-list';
@Component({
  selector: 'esp-coaches-list',
  templateUrl: './coaches-list.component.html',
  styleUrls: ['./coaches-list.component.css']
})
export class CoachesListComponent implements OnInit {
  @Input() props: CoachList = {
    avatar:'',
    order:'',
    name:'',
    email:'',
    phone_number:0,
    label:'',
    assigned_date:0,
    coaching_start:0,
    coaching_end:0
   }
  constructor() { }

  ngOnInit(): void {
  }

}
