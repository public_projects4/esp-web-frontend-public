import { Component, Input, OnInit } from '@angular/core';
import { AtheleteInfoBlock } from '../../models/athelete-info-block';

@Component({
  selector: 'esp-athelete-info-block',
  templateUrl: './athelete-info-block.component.html',
  styleUrls: ['./athelete-info-block.component.css']
})
export class AtheleteInfoBlockComponent implements OnInit {
  @Input() hero: any;

 @Input() props: AtheleteInfoBlock = {
   avatar:'',
   name:'',
   age:0,
   email:'',
   phone_no:'',
   current_injury:0,
   dob:'',
   doj:'',
   status:''
 }

 Name:any
 Mobile:any
Email:any
Status:any
Dob:any

  constructor() { }

  ngOnInit(): void {
    
  

  }

}
