import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteInfoBlockComponent } from './athelete-info-block.component';

describe('AtheleteInfoBlockComponent', () => {
  let component: AtheleteInfoBlockComponent;
  let fixture: ComponentFixture<AtheleteInfoBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheleteInfoBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteInfoBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
