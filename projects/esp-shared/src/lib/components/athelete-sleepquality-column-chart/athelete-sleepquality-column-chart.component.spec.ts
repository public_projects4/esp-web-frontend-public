import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteSleepqualityColumnChartComponent } from './athelete-sleepquality-column-chart.component';

describe('AtheleteSleepqualityColumnChartComponent', () => {
  let component: AtheleteSleepqualityColumnChartComponent;
  let fixture: ComponentFixture<AtheleteSleepqualityColumnChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheleteSleepqualityColumnChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteSleepqualityColumnChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
