import { Component,Input, OnInit } from '@angular/core';
import { OraganisationsList } from '../../../models/Super-Admin-Models/oraganisations-list'

@Component({
  selector: 'esp-list-of-oraganisations',
  templateUrl: './list-of-oraganisations.component.html',
  styleUrls: ['./list-of-oraganisations.component.css']
})
export class ListOfOraganisationsComponent implements OnInit {
@Input() props: OraganisationsList = {
    label:'',
    short_label:'',
    subdomain:'',
    linkwill:'',
    comehere:'',
    onboarded_date:'',
    expiry_date:'',
    admin_status:'',
  }

  constructor() { }

  ngOnInit(): void {
  }

}
