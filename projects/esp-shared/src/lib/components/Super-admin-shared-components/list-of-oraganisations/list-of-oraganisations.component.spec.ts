import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfOraganisationsComponent } from './list-of-oraganisations.component';

describe('ListOfOraganisationsComponent', () => {
  let component: ListOfOraganisationsComponent;
  let fixture: ComponentFixture<ListOfOraganisationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListOfOraganisationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfOraganisationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
