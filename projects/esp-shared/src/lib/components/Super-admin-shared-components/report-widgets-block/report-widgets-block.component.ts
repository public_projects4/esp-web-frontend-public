import { Component,Input, OnInit } from '@angular/core';
import { ReportWidgetsData } from '../../../models/Super-Admin-Models/report-widgets-data';

@Component({
  selector: 'esp-report-widgets-block',
  templateUrl: './report-widgets-block.component.html',
  styleUrls: ['./report-widgets-block.component.css']
})
export class ReportWidgetsBlockComponent implements OnInit {
  @Input() props: ReportWidgetsData = {
    label:'',
    descrip_text:''
  }
  constructor() { }

  ngOnInit(): void {
  }

}
