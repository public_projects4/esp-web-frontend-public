import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportWidgetsBlockComponent } from './report-widgets-block.component';

describe('ReportWidgetsBlockComponent', () => {
  let component: ReportWidgetsBlockComponent;
  let fixture: ComponentFixture<ReportWidgetsBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportWidgetsBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportWidgetsBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
