import { Component,Input, OnInit } from '@angular/core';
import { ManageWidgets } from '../../../models/Super-Admin-Models/manage-widgets';

@Component({
  selector: 'esp-manage-widgets',
  templateUrl: './manage-widgets.component.html',
  styleUrls: ['./manage-widgets.component.css']
})
export class ManageWidgetsComponent implements OnInit {
  @Input() props: ManageWidgets = {
    label:'',
    description:'',
    widgetImage:'',
    click:''
  }

    // enable button code starts here
  // https://www.netjstech.com/2020/04/angular-disable-button-example.html#DisableButtonAfterClick  -- refer this link for click enable button
  click : boolean = true;
  count=0;
  onButtonClick(){
    this.count++
    if(this.count==1)
    this.click = !this.click;
  }
  onKey(event: KeyboardEvent) {
    // if value is not empty the set click to false otherwise true
    this.click = (event.target as HTMLInputElement).value === '' ? true:false;
  }

  // enable button code ends here

  constructor() { }

  ngOnInit(): void {
  }

}
