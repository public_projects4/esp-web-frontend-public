import { Component,Input, OnInit } from '@angular/core';
import { ManageAthleteDeviceData } from '../../../models/Super-Admin-Models/manage-athlete-device-data';

@Component({
  selector: 'esp-manage-athlete-device-block',
  templateUrl: './manage-athlete-device-block.component.html',
  styleUrls: ['./manage-athlete-device-block.component.css']
})
export class ManageAthleteDeviceBlockComponent implements OnInit {
  @Input() props: ManageAthleteDeviceData = {
    label:'',
    descrip_text:'',
    widgetImage:'',
    title:'',
    list1:'',
    list2:'',
    list3:'',
    list4:'',
    themeId:'',
  }

   // enable button code starts here
  // https://www.netjstech.com/2020/04/angular-disable-button-example.html#DisableButtonAfterClick  -- refer this link for click enable button
  click : boolean = true;
  count=0;
  onButtonClick(){
    this.count++
    if(this.count==1)
    this.click = !this.click;
  }
  onKey(event: KeyboardEvent) {
    // if value is not empty the set click to false otherwise true
    this.click = (event.target as HTMLInputElement).value === '' ? true:false;
  }

  // enable button code ends here


  constructor() { }

  ngOnInit(): void {
  }

}
