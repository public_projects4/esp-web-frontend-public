import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageAthleteDeviceBlockComponent } from './manage-athlete-device-block.component';

describe('ManageAthleteDeviceBlockComponent', () => {
  let component: ManageAthleteDeviceBlockComponent;
  let fixture: ComponentFixture<ManageAthleteDeviceBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageAthleteDeviceBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageAthleteDeviceBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
