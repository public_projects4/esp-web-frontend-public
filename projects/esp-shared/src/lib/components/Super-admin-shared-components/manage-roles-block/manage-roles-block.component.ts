import { Component,Input, OnInit } from '@angular/core';
import { ManageRoles } from '../../../models/Super-Admin-Models/manage-roles';
import { AllApiService } from '../../../services/all-api.service';

@Component({
  selector: 'esp-manage-roles-block',
  templateUrl: './manage-roles-block.component.html',
  styleUrls: ['./manage-roles-block.component.css']
})
export class ManageRolesBlockComponent implements OnInit {
  @Input() props: ManageRoles = {
    adminRoles:'',
    adminDescription:'',
    totalModules:'',
    activeModules:'',
    themeId:1,

  }








  // enable button code starts here
  // https://www.netjstech.com/2020/04/angular-disable-button-example.html#DisableButtonAfterClick  -- refer this link for click enable button
  click : boolean = true;
  count=0;
  onButtonClick(){
    this.count++
    if(this.count==1)
    this.click = !this.click;
  }
  onKey(event: KeyboardEvent) {
    // if value is not empty the set click to false otherwise true
    this.click = (event.target as HTMLInputElement).value === '' ? true:false;
  }

  // enable button code ends here

  constructor(private Api:AllApiService) { }

  ngOnInit(): void {
    this.getDetails();
  }

  getRoles:any[]=[]

  getDetails() {
    this.Api
      .getAllRoles()
      .subscribe((res: any) => {
        console.log('data', res);
        this.getRoles=res;
        // console.log(res.places[0].state);
      });
  }

}
