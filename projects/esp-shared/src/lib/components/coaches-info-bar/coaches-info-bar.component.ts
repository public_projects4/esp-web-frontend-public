import { Component, Input, OnInit } from '@angular/core';
import { CoachInfoProperty } from '../../models/coaches-info-bar';

@Component({
  selector: 'esp-coaches-info-bar',
  templateUrl: './coaches-info-bar.component.html',
  styleUrls: ['./coaches-info-bar.component.css']
})
export class CoachesInfoBarComponent implements OnInit {
  @Input()  coach: CoachInfoProperty = {
    label:'',
    addNew:'',
  };
  constructor() { }

  ngOnInit(): void {
  }

}
