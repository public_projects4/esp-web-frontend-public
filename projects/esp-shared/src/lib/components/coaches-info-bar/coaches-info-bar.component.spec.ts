import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachesInfoBarComponent } from './coaches-info-bar.component';

describe('CoachesInfoBarComponent', () => {
  let component: CoachesInfoBarComponent;
  let fixture: ComponentFixture<CoachesInfoBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoachesInfoBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoachesInfoBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
