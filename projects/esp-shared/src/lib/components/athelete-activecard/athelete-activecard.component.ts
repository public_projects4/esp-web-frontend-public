import { Component, Input, OnInit } from '@angular/core';
import { AtheleteActivecard } from '../../models/athelete-activecard';


@Component({
  selector: 'esp-athelete-activecard',
  templateUrl: './athelete-activecard.component.html',
  styleUrls: ['./athelete-activecard.component.css']
})
export class AtheleteActivecardComponent implements OnInit {
  @Input() details: AtheleteActivecard = {
    current_injury:0,
    training_days:0,
    missed_days:0,
    avatar:'',
    name:'',
  }
  constructor() { }

  ngOnInit(): void {
  }

}
