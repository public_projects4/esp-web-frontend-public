import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteActivecardComponent } from './athelete-activecard.component';

describe('AtheleteActivecardComponent', () => {
  let component: AtheleteActivecardComponent;
  let fixture: ComponentFixture<AtheleteActivecardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheleteActivecardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteActivecardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
