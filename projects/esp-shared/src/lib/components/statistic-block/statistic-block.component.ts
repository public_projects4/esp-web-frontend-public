import { Component, Input, OnInit } from '@angular/core';
import { StatisticBlockProperties } from '../../models';
import { AllApiService } from '../../services/all-api.service';

@Component({
  selector: 'esp-statistic-block',
  templateUrl: './statistic-block.component.html',
  styleUrls: ['./statistic-block.component.css']
})
export class StatisticBlockComponent implements OnInit {
 @Input()  props: StatisticBlockProperties = {
    label: '',
    statistic: 0,
    icon_name : '',
    progress_percentage: 0,
    progress_color_class: '',
    color_class: '',
  };
  constructor(private api : AllApiService) { }

  ngOnInit(): void {
    this.getWidget()
  
  }

getWidget(){
  // this.api.getWidget().subscribe((data:any)=>{
  //   console.log('widget',data);
  //   this.props.label=data.dates
  //   this.props.statistic=data.inviteCount
    
  // })
}
}
