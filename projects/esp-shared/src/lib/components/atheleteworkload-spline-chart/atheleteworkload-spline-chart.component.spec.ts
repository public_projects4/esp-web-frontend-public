import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteworkloadSplineChartComponent } from './atheleteworkload-spline-chart.component';

describe('AtheleteworkloadSplineChartComponent', () => {
  let component: AtheleteworkloadSplineChartComponent;
  let fixture: ComponentFixture<AtheleteworkloadSplineChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheleteworkloadSplineChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteworkloadSplineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
