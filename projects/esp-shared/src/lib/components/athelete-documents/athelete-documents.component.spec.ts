import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteDocumentsComponent } from './athelete-documents.component';

describe('AtheleteDocumentsComponent', () => {
  let component: AtheleteDocumentsComponent;
  let fixture: ComponentFixture<AtheleteDocumentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheleteDocumentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
