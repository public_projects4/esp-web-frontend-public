import { Component, Input, OnInit } from '@angular/core';
import { AtheleteDocuments } from '../../models/athelete-documents';

@Component({
  selector: 'esp-athelete-documents',
  templateUrl: './athelete-documents.component.html',
  styleUrls: ['./athelete-documents.component.css']
})
export class AtheleteDocumentsComponent implements OnInit {
@Input() props: AtheleteDocuments = {
  title:'',
  description:'',
  label:'',
  date:''
}
  constructor() { }

  ngOnInit(): void {
  }

}
