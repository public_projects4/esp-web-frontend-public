import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteDocumentInfoComponent } from './athelete-document-info.component';

describe('AtheleteDocumentInfoComponent', () => {
  let component: AtheleteDocumentInfoComponent;
  let fixture: ComponentFixture<AtheleteDocumentInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheleteDocumentInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteDocumentInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
