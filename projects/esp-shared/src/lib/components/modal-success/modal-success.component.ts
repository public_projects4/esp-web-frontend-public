import { Component, OnInit, Input } from '@angular/core';
import { ModalCommanService } from '../../models/modal-comman-service';

@Component({
  selector: 'esp-modal-success',
  templateUrl: './modal-success.component.html',
  styleUrls: ['./modal-success.component.css']
})
export class ModalSuccessComponent implements OnInit {


  @Input() props: ModalCommanService = {
    modelName: '',

  };

  constructor() { }

  ngOnInit(): void {
  }

}
