import { Component, OnInit, Input } from '@angular/core';
import { ModalCommanService } from '../../models/modal-comman-service';

@Component({
  selector: 'esp-modal-error',
  templateUrl: './modal-error.component.html',
  styleUrls: ['./modal-error.component.css']
})
export class ModalErrorComponent implements OnInit {


  @Input() props: ModalCommanService = {
    modelName: '',

  };

  constructor() { }

  ngOnInit(): void {
  }

}
