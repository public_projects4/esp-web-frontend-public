import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteNavtabItemsComponent } from './athelete-navtab-items.component';

describe('AtheleteNavtabItemsComponent', () => {
  let component: AtheleteNavtabItemsComponent;
  let fixture: ComponentFixture<AtheleteNavtabItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheleteNavtabItemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteNavtabItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
