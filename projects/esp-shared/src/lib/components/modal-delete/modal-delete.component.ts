import { Component, Input, OnInit } from '@angular/core';
import { ModalCommanService } from '../../models/modal-comman-service';

@Component({
  selector: 'esp-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.css']
})
export class ModalDeleteComponent implements OnInit {


  @Input() props: ModalCommanService = {
    modelName: '',
  };

  constructor() { }

  ngOnInit(): void {
  }

}
