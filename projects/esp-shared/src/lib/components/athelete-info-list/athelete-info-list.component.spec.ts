import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteInfoListComponent } from './athelete-info-list.component';

describe('AtheleteInfoListComponent', () => {
  let component: AtheleteInfoListComponent;
  let fixture: ComponentFixture<AtheleteInfoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheleteInfoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteInfoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
