import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
import { ModalCommanService } from '../../models/modal-comman-service';

@Component({
  selector: 'esp-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.css']
})
export class ModalConfirmComponent implements OnInit {

  @Output() deleteMsg = new EventEmitter();
  @Input() props: ModalCommanService = {
    modelName: '',

  };

  constructor() { }

  ngOnInit(): void {
  }

  callDeleteMsg(){
    this.deleteMsg.emit();
  }



}
