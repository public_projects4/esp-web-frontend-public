import { Component, OnInit, Input } from '@angular/core';
import { ModalCommanService } from '../../models/modal-comman-service';

@Component({
  selector: 'esp-modal-update',
  templateUrl: './modal-update.component.html',
  styleUrls: ['./modal-update.component.css']
})
export class ModalUpdateComponent implements OnInit {


  @Input() props: ModalCommanService = {
    modelName: '',

  };

  constructor() { }

  ngOnInit(): void {
  }

}
