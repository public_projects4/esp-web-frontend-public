import { Component, Input, OnInit } from '@angular/core';
import { Team } from '../../models';

@Component({
  selector: 'esp-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.css']
})
export class TeamCardComponent implements OnInit {


  @Input() team?: Team


  coachName :string="Add coach";
count=0;
  onChange(data:any){
    // console.log("ameen",data.target.value)
this.count++;
if(this.count==1){
  if(data.target.value == "Coach1"){

    this.coachName = "Coach Name 1";
  }
  else if(data.target.value == "Coach2"){

    this.coachName = "Coach Name 2";
  }
  else if(data.target.value == "Coach3"){

    this.coachName = "Coach Name 3";
  }
  else if(data.target.value == "Coach4"){

    this.coachName = "Coach Name 4";
  }
  this.count--;
}

  }

  constructor() { }

  ngOnInit(): void {
  }

}
