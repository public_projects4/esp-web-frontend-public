import { Component, Input, OnInit } from '@angular/core';
import { Atheleteperformance } from '../../models/athelete-performance-block';

@Component({
  selector: 'esp-athelete-performance-block',
  templateUrl: './athelete-performance-block.component.html',
  styleUrls: ['./athelete-performance-block.component.css']
})
export class AtheletePerformanceBlockComponent implements OnInit {
  @Input() props: Atheleteperformance = {
    label:'',
    color_class:'',
    statistic:'',
    date_span:''
  }
  constructor() { }

  ngOnInit(): void {
  }

}
