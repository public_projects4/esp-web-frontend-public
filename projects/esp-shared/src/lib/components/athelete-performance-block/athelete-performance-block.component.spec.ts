import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheletePerformanceBlockComponent } from './athelete-performance-block.component';

describe('AtheletePerformanceBlockComponent', () => {
  let component: AtheletePerformanceBlockComponent;
  let fixture: ComponentFixture<AtheletePerformanceBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheletePerformanceBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheletePerformanceBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
