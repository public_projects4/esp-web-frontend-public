import { Component, OnInit } from '@angular/core';
import { AllApiService } from '../../services/all-api.service';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);
@Component({
  selector: 'esp-dashboard-charts',
  templateUrl: './dashboard-charts.component.html',
  styleUrls: ['./dashboard-charts.component.css']
})
export class DashboardChartsComponent implements OnInit {
  ras: any = []
  recivedData: any
  recivedInnerData: any = []
orgId:any
  constructor(private Api: AllApiService) { }
  ngOnInit(): void {
this.orgId= localStorage.getItem('OrgID')
      this.getBar();
      this.getLine();
      this.getPie();
      this.getBubble();
      this.getRadar();
      this.getPolarArea();
      this.getScatter();
      this.getHorizantalBar();
      this.getDounut();
  }
  chart:any
  chart1:any
  chart2:any
  chartss:any
  chartt:any
  getHorizantalBar() {
    this.Api.getAthletesInviteCount().subscribe((data: any) => {
      let allData = data;
      let mylabel: any = [];
      let mydata: any = [];
      allData.map((temp: any) => {
        mylabel.push(temp.date)
        mydata.push(temp.athleteInvited)
      })
      this.chart = new Chart("horizontalBar", {
        type: 'bar',
        data: {
          labels: mylabel,
          datasets: [{
            label: 'athleteInvited',
            data: mydata,
            hoverBorderColor: "orange",
            // backgroundColor: [
            //   'rgb(166, 120, 235)',
            //   'rgb(166, 120, 235)',
            //   'rgb(166, 120, 235)',
            //   'rgb(166, 120, 235)',
            //   'rgb(166, 120, 235)',
            //   'rgb(166, 120, 235)'
            // ],
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 205, 86)'
            ],
            borderColor: [
              'rgb(166, 120, 235)',
              'rgb(166, 120, 235)',
              'rgb(166, 120, 235)',
              'rgb(166, 120, 235)',
              'rgb(166, 120, 235)',
              'rgb(166, 120, 235)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          indexAxis: 'y',
          plugins: {
            legend: {
              display: false,
              position:'bottom'
            },
            title: {
              display: true,
              text: 'Bar Graph',
              color:'white'
            }
          },
          scales: {
            y: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }            },
              x: {
                grid:{
                  borderColor:"white"
                },
                ticks: {
                  color: "white",
                  font: {
                    size: 12,
                  }
                }
               }
          }
        }
      });
    })
  }
  getBar() {
    this.Api.getAthletesInviteCount().subscribe((data: any) => {
      let allData = data;
      let mylabel: any = [];
      let mydata: any = [];
      allData.map((temp: any) => {
        mylabel.push(temp.date)
        mydata.push(temp.athleteInvited)
      })
      this.chart = new Chart("bar", {
        type: 'bar',
        data: {
          labels: mylabel,
          datasets: [{
            label: 'athleteInvited',
            data: mydata,
            hoverBorderColor: "orange",
            // backgroundColor: [
            //   'rgb(166, 120, 235)',
            //   'rgb(166, 120, 235)',
            //   'rgb(166, 120, 235)',
            //   'rgb(166, 120, 235)',
            //   'rgb(166, 120, 235)',
            //   'rgb(166, 120, 235)'
            // ],
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 205, 86)'
            ],
            borderColor: [
              'rgb(166, 120, 235)',
              'rgb(166, 120, 235)',
              'rgb(166, 120, 235)',
              'rgb(166, 120, 235)',
              'rgb(166, 120, 235)',
              'rgb(166, 120, 235)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              display: false,
              position:'top'
            },
            title: {
              display: true,
              text: 'Bar Graph',
              color:'white'
            }
          },
          scales: {
            y: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }            },
              x: {
                grid:{
                  borderColor:"white"
                },
                ticks: {
                  color: "white",
                  font: {
                    size: 12,
                  }
                }
               }
          }
        }
      });
    })
  }
  getLine(){
    this.Api.getAthletesInviteCount().subscribe((data:any) => {
      let allData = data;
      let mylabel: any = [];
      let mydata: any = [];
      allData.map((temp: any) => {
        mylabel.push(temp.date)
        mydata.push(temp.athleteInvited)
      })
      this.chart1 = new Chart("line", {
        type: 'line',
        data: {
          labels: mylabel,
          datasets: [{
            label: 'AthleteInvited',
            data: mydata,
            borderColor: 'rgb(166, 120, 235)',
            backgroundColor: 'rgb(255, 99, 132)',
            borderWidth: 1
          }]
        },
        options: {
          plugins: {
            legend: {
              display: false,
              position:'bottom'
            },
            title: {
              display: true,
              text: 'Line Graph',
              color:'white'
            }
          },
          scales: {
            y: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }
             },
             x: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }
             }
          }
        }
      });
    })
  }
  getRadar(){
    this.Api.getAthletesInviteCount().subscribe((data:any) => {
      let allData = data;

      let mylabel: any = [];
      let mydata: any = [];
      allData.map((temp: any) => {
        mylabel.push(temp.date)
        mydata.push(temp.athleteInvited)
      })
      this.chart1 = new Chart("radar", {
        type: 'radar',
        data: {
          labels: mylabel,
          datasets: [{
            label: 'AthleteInvited',
            data: mydata,
            fill:true,
            hoverBorderColor:'red',
            pointHoverBackgroundColor:'green',
            borderColor: 'white',
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 205, 86)'
            ],
            // backgroundColor: 'rgb(166,120,235)',
            hoverBackgroundColor: 'rgb(141, 65, 255)',
            borderWidth: 0.5
          }]
        },
        options: {
          elements: {
            line: {
              // borderWidth: 3,
            }
          },
          plugins: {
            legend: {
              display: false,
              position:'bottom',
            },
            title: {
              display: true,
              text: 'Radar Graph',
              color:'white'
            }
          },
          scales: {
            // r:{
            //   angleLines:{
            //     display:true,
            //     color:"white"
            //   },
            // },
            y: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }
             },
             x: {
               grid:{
                 borderColor:"white"
               },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }
             }
          }
        }
      });
    })
  }
  getPolarArea(){
    this.Api.getAthletesInviteCount().subscribe((data:any) => {
      let allData = data;

      let mylabel: any = [];
      let mydata: any = [];
      allData.map((temp: any) => {
        mylabel.push(temp.date)
        mydata.push(temp.athleteInvited)
      })
      this.chart1 = new Chart("polarArea", {
        type: 'polarArea',
        data: {
          labels: mylabel,
          datasets: [{
            label: 'AthleteInvited',
            data: mydata,
            borderColor: 'black',
            // backgroundColor: 'rgb(166,120,235)',
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 205, 86)'
            ],
            hoverBackgroundColor: 'rgb(141, 65, 255)',
            borderWidth: 0.5
          }]
        },
        options: {
          plugins: {
            legend: {
              display: false,
              position:'bottom'
            },
            title: {
              display: true,
              text: 'Polar Area Graph',
              color:'white'
            }
          },
          scales: {
            y: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }
             },
             x: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }
             }
          }
        }
      });
    })
  }
  getDounut(){
    this.Api.getAthletesInviteCount().subscribe((data:any) => {
      let allData = data;

      let mylabel: any = [];
      let mydata: any = [];
      allData.map((temp: any) => {
        mylabel.push(temp.date)
        mydata.push(temp.athleteInvited)
      })
      this.chart1 = new Chart("doughnut", {
        type: 'doughnut',
        data: {
          labels: mylabel,
          datasets: [{
            label: 'AthleteInvited',
            data: mydata,
            borderColor: 'black',
            // backgroundColor: 'rgb(166,120,235)',
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 205, 86)'
            ],
            // hoverOffset: 4,
            hoverBackgroundColor: 'rgb(141, 65, 255)',
            borderWidth: 0.5
          }]
        },
        options: {
          plugins: {
            legend: {
              display: false,
              position:'bottom'
            },
            title: {
              display: true,
              text: 'Doughnut Graph',
              color:'white'
            }
          },
          scales: {
            y: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }
             },
             x: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }
             }
          }
        }
      });
    })
  }
  getPie(){
    this.Api.getAthletesInviteCount().subscribe((data:any) => {
      let allData = data;

      let mylabel: any = [];
      let mydata: any = [];
      allData.map((temp: any) => {
        mylabel.push(temp.date)
        mydata.push(temp.athleteInvited)
      })
      this.chart1 = new Chart("myPie", {
        type: 'pie',
        data: {
          labels: mylabel,
          datasets: [{
            label: 'AthleteInvited',
            data: mydata,
            borderColor: 'black',
            // backgroundColor: 'rgb(166,120,235)',
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 205, 86)'
            ],
            hoverBackgroundColor: 'rgb(141, 65, 255)',
            borderWidth: 0.5
          }]
        },
        options: {
          plugins: {
            legend: {
              display: false,
              position:'bottom'
            },
            title: {
              display: true,
              text: 'Pie Graph',
              color:'white'
            }
          },
          scales: {
            y: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }
             },
             x: {
              grid:{
                borderColor:"white"
              },
              ticks: {
                color: "white",
                font: {
                  size: 12,
                }
              }
             }
          }
        }
      });
    })
  }
  getScatter(){
    this.Api.getAthletesInviteCount().subscribe((data:any) => {
      let allData = data;

      let mylabel: any = [];
      let mydata: any = [];
      allData.map((temp: any) => {
        mylabel.push(temp.date)
        mydata.push({x:temp.athleteInvited, y:temp.athleteInvited})
      })

      this.chart1 = new Chart("scatter", {
        type: 'scatter',
        data: {
          labels: mylabel,
          datasets: [{
            label: 'AthleteInvited',
            data: mydata,
            borderColor: 'black',
            backgroundColor: 'rgb(166,120,235)',
            hoverBackgroundColor: 'rgb(141, 65, 255)',
            borderWidth: 0.5
          }]
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              display: false,
              position:'top'
            },
            title: {
              display: true,
              text: 'Scatter Graph',
              color:'white'
            }
          },
          scales: {
            x: {grid:{
              borderColor:"white"
            }},
            y: {
              grid:{
              borderColor:"white"
            },
              type: 'linear',
              position: 'bottom'
          }
          }
        }
      });
    })
  }
  getBubble(){
    this.Api.getAthletesInviteCount().subscribe((data:any) => {
      let allData = data;

      let mylabel: any = [];
      let mydata: any = [];
      allData.map((temp: any) => {
        mylabel.push(temp.date)
        mydata.push({x:temp.athleteInvited, y:temp.athleteInvited})
      })

      this.chart1 = new Chart("bubble", {
        type: 'bubble',
        data: {
          labels: mylabel,
          datasets: [{
            label: 'AthleteInvited',
            data: mydata,
            borderColor: 'black',
            backgroundColor: 'rgb(166,120,235)',
            hoverBackgroundColor: 'rgb(141, 65, 255)',
            borderWidth: 0.5
          }]
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              display: false,
              position:'top'
            },
            title: {
              display: true,
              text: 'Bubble Graph',
              color:'white'
            }
          },
          scales: {
            x: {
              grid:{
                borderColor:"white"
              },
              type: 'linear',
              position: 'bottom'
          },
            y: {
              grid:{
                borderColor:"white"
              },
              type: 'linear',
              position: 'bottom'
          }
          }
        }

      });
    })
  }
}
