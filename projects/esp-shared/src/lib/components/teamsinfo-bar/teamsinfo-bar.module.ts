import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsinfoBarComponent } from './teamsinfo-bar.component';
import { Route, RouterModule } from '@angular/router';
const routes: Route[] = [
  {
    path: '',
    component: TeamsinfoBarComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class TeamsinfoBarModule { }
