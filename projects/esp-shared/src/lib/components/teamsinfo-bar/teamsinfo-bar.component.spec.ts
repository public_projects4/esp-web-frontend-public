import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsinfoBarComponent } from './teamsinfo-bar.component';

describe('TeamsinfoBarComponent', () => {
  let component: TeamsinfoBarComponent;
  let fixture: ComponentFixture<TeamsinfoBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamsinfoBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsinfoBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
