import { Component, Input, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { TeamsInfoBar } from '../../models/teamsinfobar';

@Component({
  selector: 'esp-teamsinfo-bar',
  templateUrl: './teamsinfo-bar.component.html',
  styleUrls: ['./teamsinfo-bar.component.css']
})
export class TeamsinfoBarComponent implements OnInit {
@Input() props: TeamsInfoBar = {
  label:'',
  list:'',
  clickOption:'',
  iconOption:'',
  
}

  constructor() { }

  ngOnInit(): void {
  }



}
