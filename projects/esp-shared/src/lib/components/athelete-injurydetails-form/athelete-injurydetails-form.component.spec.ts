import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheleteInjurydetailsFormComponent } from './athelete-injurydetails-form.component';

describe('AtheleteInjurydetailsFormComponent', () => {
  let component: AtheleteInjurydetailsFormComponent;
  let fixture: ComponentFixture<AtheleteInjurydetailsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheleteInjurydetailsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheleteInjurydetailsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
