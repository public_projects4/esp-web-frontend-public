import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtheletedataNavtabComponent } from './atheletedata-navtab.component';

describe('AtheletedataNavtabComponent', () => {
  let component: AtheletedataNavtabComponent;
  let fixture: ComponentFixture<AtheletedataNavtabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtheletedataNavtabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtheletedataNavtabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
