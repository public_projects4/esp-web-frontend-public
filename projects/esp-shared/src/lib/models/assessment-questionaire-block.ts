export interface AssessmentQuestionaireBlock {
    label:string;
    content:string;
    shared_for:string;
    questions:number;
    response:number;
}