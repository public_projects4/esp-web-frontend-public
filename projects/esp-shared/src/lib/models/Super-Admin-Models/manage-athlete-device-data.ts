export interface ManageAthleteDeviceData {
    label:string;
    widgetImage:string;
    title:string;
    descrip_text:string;
    list1:string;
    list2:string;
    list3:string;
    list4:string;
    themeId:string;
}
