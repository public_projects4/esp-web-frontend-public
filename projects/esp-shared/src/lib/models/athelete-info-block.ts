export interface AtheleteInfoBlock {
    avatar:string;
    name:string;
    age:number;
    email:string;
    phone_no:string;
    current_injury:number;
    dob:string;
    doj:string;
    status:string
}