export interface Atheleteblockproperties{
    order:number;
    name: string;
    body_mass:number;
    status:string;
    current_injury:number;
    training_days:number;
    missed_days:number;
    avatar:string;
    color_class:string;
}