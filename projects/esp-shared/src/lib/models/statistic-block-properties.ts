export interface StatisticBlockProperties {
    label: string;
    statistic: number;
    icon_name:string;
    progress_percentage: number;
    progress_color_class: string;
    color_class: string;
  }