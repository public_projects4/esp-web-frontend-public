export interface Team {
    name: string;
    coach: string;
    total_atheletes: number;
    active: number;
    injured: number;
    invited: number;
    avatar: string;


}
