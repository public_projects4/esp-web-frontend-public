export interface AtheleteActivecard{
    current_injury:number;
    training_days:number;
    missed_days:number;
    avatar:string;
    name:string;
}