export interface AtheleteVaccination {
     label:string;
     description:string;
     vacc_date:string;
     frequency:string;
     vacc_unit:number;
     quantity:string;
}