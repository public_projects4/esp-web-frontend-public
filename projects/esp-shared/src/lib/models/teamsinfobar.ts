export interface TeamsInfoBar {
    label:string;
    list:string;
    clickOption:string;
    iconOption:string;
}