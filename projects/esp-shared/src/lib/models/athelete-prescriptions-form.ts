export interface AtheletePrescription {
label:string;
name:string;
status:string;
days:string;
from:string;
to:string;
route:string;
total_quantity:string;
}