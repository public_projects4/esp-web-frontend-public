export interface AtheleteTreatmentInfo {
    practitioner_name:string;
    practitioner_role:string;
    practitioner_no:string;
    practitioner_email:string;
    practice_name:string;
    practice_address:string;

}