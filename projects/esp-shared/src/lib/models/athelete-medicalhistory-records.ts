export interface AtheleteMedicalHistory {
  title:string;
  description:string;
  name:string;
  date:string;
  status:string;
}