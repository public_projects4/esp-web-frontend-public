import { NgModule } from '@angular/core';
import { SidePanelComponent } from './components/side-panel/side-panel.component';
import { HeaderComponent } from './components/header/header.component';
import { ContentComponent } from './components/content/content.component';
import { MainComponent } from './components/main/main.component';
import { TeamCardComponent } from './components/team-card/team-card.component';
import { LoadingComponent } from './components/loading/loading.component';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StatisticBlockComponent } from './components/statistic-block/statistic-block.component';
import { PageInfoBarComponent } from './components/page-info-bar/page-info-bar.component';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
import { PolarChartComponent } from './components/polar-chart/polar-chart.component';
import { ChartModule } from 'angular-highcharts';
import { AreaChartComponent } from './components/area-chart/area-chart.component';
import { AtheleteworkloadSplineChartComponent } from './components/atheleteworkload-spline-chart/atheleteworkload-spline-chart.component';
import { CoachesInfoBarComponent } from './components/coaches-info-bar/coaches-info-bar.component';
import { CoachesListComponent } from './components/coaches-list/coaches-list.component';
import { FooterComponent } from './components/footer/footer.component';
import { AtheleteNavtabItemsComponent } from './components/athelete-navtab-items/athelete-navtab-items.component';
import { TeamsinfoBarComponent } from './components/teamsinfo-bar/teamsinfo-bar.component';import { AtheleteSleepqualityColumnChartComponent } from './components/athelete-sleepquality-column-chart/athelete-sleepquality-column-chart.component';
import { AtheleteBlockComponent } from './components/athelete-block/athelete-block.component';
import { AtheletePerformanceBlockComponent } from './components/athelete-performance-block/athelete-performance-block.component';
import { AtheleteActivecardComponent } from './components/athelete-activecard/athelete-activecard.component';
import { AtheleteInfoListComponent } from './components/athelete-info-list/athelete-info-list.component';
import { AtheleteInfoBlockComponent } from './components/athelete-info-block/athelete-info-block.component';
import { AtheletedataNavtabComponent } from './components/atheletedata-navtab/atheletedata-navtab.component';
import { AtheleteInjurydetailsFormComponent } from './components/athelete-injurydetails-form/athelete-injurydetails-form.component';
import { AtheleteDocumentInfoComponent } from './components/athelete-document-info/athelete-document-info.component';
import { AtheleteDocumentsComponent } from './components/athelete-documents/athelete-documents.component';
import { ListOfOraganisationsComponent } from './components/Super-admin-shared-components/list-of-oraganisations/list-of-oraganisations.component';
import { ManageRolesBlockComponent } from './components/Super-admin-shared-components/manage-roles-block/manage-roles-block.component';
import { ManageWidgetsComponent } from './components/Super-admin-shared-components/manage-widgets/manage-widgets.component';
import { ManageAthleteDeviceBlockComponent } from './components/Super-admin-shared-components/manage-athlete-device-block/manage-athlete-device-block.component';
import { ReportWidgetsBlockComponent } from './components/Super-admin-shared-components/report-widgets-block/report-widgets-block.component';
import { DashTeamsLeaderBoardComponent } from './components/dash-teams-leader-board/dash-teams-leader-board.component';
import { DashboardChartsComponent } from './components/dashboard-charts/dashboard-charts.component';
import { AddNewPractitionerInfoComponent } from './components/add-new-practitioner-info/add-new-practitioner-info.component';
import { ModalSuccessComponent } from './components/modal-success/modal-success.component';
import { ModalDeleteComponent } from './components/modal-delete/modal-delete.component';
import { ModalUpdateComponent } from './components/modal-update/modal-update.component';
import { ModalConfirmComponent } from './components/modal-confirm/modal-confirm.component';
import { ModalErrorComponent } from './components/modal-error/modal-error.component';
import { ModalAlreadyExistComponent } from './components/modal-already-exist/modal-already-exist.component';
import { MessagesComponent } from './components/messages/messages.component';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'projects/esp/src/environments/environment';



@NgModule({
  declarations: [
    SidePanelComponent,
    HeaderComponent,
    ContentComponent,
    MainComponent,
    TeamCardComponent,
    LoadingComponent,
    StatisticBlockComponent,
    PageInfoBarComponent,
    BarChartComponent,
    PolarChartComponent,
    AreaChartComponent,
    AtheleteworkloadSplineChartComponent,
    CoachesInfoBarComponent,
    CoachesListComponent,
    FooterComponent,
    AtheleteNavtabItemsComponent,
    TeamsinfoBarComponent,
    AtheleteSleepqualityColumnChartComponent,
    AtheleteBlockComponent,
    AtheletePerformanceBlockComponent,
    AtheleteActivecardComponent,
    AtheleteInfoListComponent,
    AtheleteInfoBlockComponent,
    AtheletedataNavtabComponent,
    AtheleteInjurydetailsFormComponent,
    AtheleteDocumentInfoComponent,
    AtheleteDocumentsComponent,
    ListOfOraganisationsComponent,
    ManageRolesBlockComponent,
    ManageWidgetsComponent,
    ManageAthleteDeviceBlockComponent,
    ReportWidgetsBlockComponent,
    DashTeamsLeaderBoardComponent,
    DashboardChartsComponent,
    AddNewPractitionerInfoComponent,
    ModalSuccessComponent,
    ModalDeleteComponent,
    ModalUpdateComponent,
    ModalConfirmComponent,
    ModalErrorComponent,
    ModalAlreadyExistComponent,
    MessagesComponent


  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule,
    ReactiveFormsModule,
    ChartModule,
    FormsModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebasedata),
  ],
  exports: [
    SidePanelComponent,
    HeaderComponent,
    ContentComponent,
    MainComponent,
    TeamCardComponent,
    LoadingComponent,
    StatisticBlockComponent,
    PageInfoBarComponent,
    BarChartComponent,
    AtheleteInfoListComponent,
    AtheleteActivecardComponent,
    AtheletePerformanceBlockComponent,
    PolarChartComponent,
    AreaChartComponent,
    AtheleteworkloadSplineChartComponent,
    CoachesInfoBarComponent,
    CoachesListComponent,
    FooterComponent,
    AtheleteNavtabItemsComponent,
    TeamsinfoBarComponent,
    AtheleteSleepqualityColumnChartComponent,
    AtheleteBlockComponent,
    AtheleteInfoBlockComponent,
    AtheletedataNavtabComponent,
    AtheleteInjurydetailsFormComponent,
    AtheleteDocumentInfoComponent,
    AtheleteDocumentsComponent,
    ListOfOraganisationsComponent,
    ManageRolesBlockComponent,
    ManageWidgetsComponent,
    ManageAthleteDeviceBlockComponent,
    ReportWidgetsBlockComponent,
    DashTeamsLeaderBoardComponent,
    DashboardChartsComponent,
    AddNewPractitionerInfoComponent,
    ModalSuccessComponent,
    ModalDeleteComponent,
    ModalUpdateComponent,
    ModalConfirmComponent,
    ModalErrorComponent,
    ModalAlreadyExistComponent,
    MessagesComponent
  ]
})
export class EspSharedModule { }
