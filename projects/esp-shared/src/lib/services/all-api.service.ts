import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'projects/esp/src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AllApiService {
  constructor(private http: HttpClient) {}

  // getLogin(data:any){
  //   return this.http.get(`http://api.evvosports.com/api/Login/AuthenticateUser`,data);
  // }

  // callingCodes() {
  //   return this.http.get(
  //     `http://country.io/phone.json`
  //   );
  // }

  callingCodesApi() {
    return this.http.get(
      `https://api.evvosports.com/api/Coaches/GetAllCountryCode`
    );
  }

  getLogin(user: any, pass: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Login/AuthenticateUser?Email=${user}&Password=${pass}`
    );
  }

  registerApi(otp: any, email: any, pass: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Otp/ValidateOTPCreatePassword?otp=${otp}&email=${email}&Password=${pass}`
    );
  }

  // organization api's (super-admin)

  addorganization(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/Organisation/AddOrganisation`,
      data
    );
  }

  getAllOrganization() {
    return this.http.get(
      `https://api.evvosports.com/api/Organisation/GetAllOrganisations`
    );
  }

  getAllOrganizationById(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Organisation/GetOrganisationById/${id}`
    );
  }

  updateOrganization(data: any) {
    return this.http.put(
      `https://api.evvosports.com/api/Organisation/UpdateOrganisation`,
      data
    );
  }

  deleteOrganization(id: any) {
    return this.http.delete(
      `https://api.evvosports.com/api/Organisation/Delete?id=${id}`
    );
  }

  downloadOrganization(id: any, excel: any, Name: any): Observable<any> {
    return this.http.get(
      environment.baseurl +
        'Exportdata/ExportData?Id=' +
        id +
        '&DownloadType=' +
        excel +
        '&DataName=' +
        Name,
      { responseType: 'text' }
    );
  }

  download(Ids: any, Types: any, Names: any): Observable<any> {
    return this.http.get(
      environment.baseurl +
        'Exportdata/ExportData?Id=' +
        Ids +
        '&DownloadType=' +
        Types +
        '&DataName=' +
        Names,
      { responseType: 'text' }
    );
  }

  GetInjury() {
    return this.http.get(environment.baseurl + 'InjuryDetails/GetAllInjury');
  }
  GetTreatment() {
    return this.http.get(
      environment.baseurl + 'TreatmentInfo/GetAllTreatmentInfo'
    );
  }

  // downloadVaccination(id: any,Type:any,Name:any):Observable<any> {

  //  return this.http.get(environment.baseurl + "Exportdata/ExportData?Id="+id+"&DownloadType="+Type+"&DataName="+Name,{responseType:'text'})
  // }

  // manage-roles (super Admin)

  addRoles(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/Roles/AddRoles`,
      data
    );
  }

  getAllRoles() {
    return this.http.get(`https://api.evvosports.com/api/Roles/GetAllRoles`);
  }

  getParentNode(id: any) {
    return this.http.get(environment.baseurl + 'Roles/GetRolesById?id=' + id);
  }

  getAdminLogin(user: any, pass: any) {
    // console.log(user, pass);

    return this.http.get(
      `https://api.evvosports.com/api/Login/AuthenticateUser?Email=${user}&Password=${pass}`
    );
  }

  //----------------------------------- Parent-Node (super Admin) --- start-------------------------------

  getAllParentNode() {
    return this.http.get(
      `https://api.evvosports.com/api/ParentNode/GetAllParentNode`
    );
  }

  getParentNodeById(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/ParentNode/GetParentNodeById?id=${id}`
    );
  }

  addParentNode(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/ParentNode/AddParentNode`,
      data
    );
  }

  updateParentNode(data: any) {
    return this.http.put(
      `https://api.evvosports.com/api/ParentNode/UpdateParentNode`,
      data
    );
  }

  deleteParentNode(id: any) {
    return this.http.delete(
      `https://api.evvosports.com/api/ParentNode/DeleteParentNode?Id=${id}`
    );
  }

  //----------------------------------- Parent-Node (super Admin) --- end-------------------------------

  //----------------------------------- GetAllInviteAthlete (Admin) --- start-------------------------------

  inviteAthlete(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/InviteAthlete/AddInviteNewAthlete`,
      data
    );
  }

  getAllInviteAthlete() {
    return this.http.get(
      `https://api.evvosports.com/api/InviteAthlete/GetAllInviteAthlete`
    );
  }

  getAthleteByTeamId(id: any) {
    return this.http.get(
      `  https://api.evvosports.com/api/InviteAthlete/GetAthleteByTeamId?teamid=${id}`
    );
  }

  //----------------------------------- InviteAthlete (Admin) --- end-------------------------------

  createTeam(data: any) {
    return this.http.post(`https://api.evvosports.com/api/Team/AddTeam`, data);
  }

  getAllTeam() {
    return this.http.get(`https://api.evvosports.com/api/Team/GetAllTeam`);
  }

  getAllTeamBySportsCategory(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Team/GetAllTeamBySportsCategory?organsationid=${id}`
    );
  }

  getAllTeamSportsCategoryByCoachId(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Team/GetAllTeamSportsCategoryByCoachId?coachid=${id}`
    );
  }

  getTeamById(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Team/GetTeamById/${id}`
    );
  }

  updateTeam(data: any) {
    return this.http.put(
      `https://api.evvosports.com/api/Team/UpdateTeam`,
      data
    );
  }

  // --------------------------------------------     Coaches api's   ----------------------------------------------

  getAllCoaches() {
    return this.http.get(
      `https://api.evvosports.com/api/Coaches/GetAllCoaches`
    );
  }

  // coaches (Admin)

  addCoaches(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/Coaches/AddCoaches`,
      data
    );
  }

  getAllCoachBySportsCategory(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Coaches/GetAllCoachBySportsCategory?organsationid=${id}`
    );
  }

  getCoachesByOrgId(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Coaches/GetCoachesByOrgId?id=${id}`
    );
  }

  deleteCoaches(id: any) {
    return this.http.delete(
      `https://api.evvosports.com/api/Coaches/DeleteCoaches?coachid=${id}`
    );
  }

  updateCoaches(data: any) {
    return this.http.put(
      `https://api.evvosports.com/api/Coaches/UpdateCoaches`,
      data
    );
  }

  // --------------------------------------------    SportsCategory api's   ----------------------------------------------

  getAllSportsCategory() {
    return this.http.get(
      `https://api.evvosports.com/api/SportsCategory/GetAllSportsCategory`
    );
  }

  // --------------------------------------------     Athlete api's   ----------------------------------------------

  addInjury(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/InjuryDetails/AddInjury`,
      data
    );
  }

  updateInjury(data: any) {
    return this.http.put(
      `https://api.evvosports.com/api/InjuryDetails/UpdateInjuryDetails`,
      data
    );
  }

  deleteInjury(id: any) {
    return this.http.delete(
      `https://api.evvosports.com/api/InjuryDetails/DeleteInjuryDetails?injuryid=${id}`
    );
  }

  addTreatmentInfo(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/TreatmentInfo/AddTreatmentInfo`,
      data
    );
  }

  updateTreatmentInfo(data: any) {
    return this.http.put(
      `https://api.evvosports.com/api/TreatmentInfo/UpdateTreatmentInfo`,
      data
    );
  }

  deleteTreatmentInfo(id: any) {
    return this.http.delete(
      `https://api.evvosports.com/api/TreatmentInfo/DeleteTreatmentInfo?treatmentinfoid=${id}`
    );
  }

  // --------------------------------------------    Practitioner api's   ----------------------------------------------

  addPractitioner(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/Practitioner/AddPractitioner`,
      data
    );
  }

  getAllPractitioner() {
    return this.http.get(
      `https://api.evvosports.com/api/Practitioner/GetAllPractitioner`
    );
  }

  getMedicalPractitioner(id: any) {
    console.log(id);

    return this.http.get(
      environment.baseurl +
        'Practitioner/GetPractitionerByPractitionerRole?organsationid=' +
        id
    );
    //return this.http.get(environment.baseurl + "MedicalHistory?athleteid=6165589f217c6d98238fd963&startdate=10-12-2021&enddate=10-14-2021&type=Injury%20Data")
  }

  getMedicalHistory(id: any, start: any, end: any, type: any) {
    // console.log(id, start, end, type);

    return this.http.get(
      environment.baseurl +
        'MedicalHistory?athleteid=' +
        id +
        '&startdate=' +
        start +
        '&enddate=' +
        end +
        '&type=' +
        type
    );
    // return this.http.get(environment.baseurl + "MedicalHistory?athleteid=6165589f217c6d98238fd963&startdate=10-12-2021&enddate=10-14-2021&type=Injury%20Data")
  }

  getPractitionerByPractitionerRole(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Practitioner/GetPractitionerByPractitionerRole?organsationid=${id}`
    );
  }

  getPractitionerByOrganisationId(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Practitioner/GetPractitionerByOrganisationId?organsationid=${id}`
    );
  }

  deletePractitionerData(id: any) {
    return this.http.delete(
      `https://api.evvosports.com/api/Practitioner/DeletePractitioner?id=${id}`
    );
  }

  UpdatePractitioner(data: any) {
    return this.http.put(
      `https://api.evvosports.com/api/Practitioner/UpdatePractitioner`,
      data
    );
  }

  // --------------------------------------------    Prescription api's   ----------------------------------------------

  addPrescription(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/Prescription/AddPrescription`,
      data
    );
  }

  getAllPrescription() {
    return this.http.get(
      `https://api.evvosports.com/api/Prescription/GetAllPrescription`
    );
  }

  getPrescriptionById(id: any) {
    // console.log('getPrescriptionById',id);

    return this.http.get(
      `https://api.evvosports.com/api/Prescription/GetPrescriptionById?id=${id}`
    );
  }

  updatePrescription(data: any) {
    return this.http.put(
      `https://api.evvosports.com/api/Prescription/UpdatePrescription`,
      data
    );
  }

  deletePrescription(id: any) {
    return this.http.delete(
      `https://api.evvosports.com/api/Prescription/Delete?id=${id}`
    );
  }

  // --------------------------------------------    Forget password api's   ----------------------------------------------

  forgetPassword(email: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Password/ForgetPassword?email=${email}`
    );
  }

  // --------------------------------------------    Vaccination api's   ----------------------------------------------

  addVaccination(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/Vaccination/AddVaccination
  `,
      data
    );
  }

  getVaccinationById(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Vaccination/GetVaccinationById?id=${id}`
    );
  }

  getVaccination() {
    return this.http.get(
      `https://api.evvosports.com/api/Vaccination/GetVaccination`
    );
  }

  updateVaccination(data: any) {
    return this.http.put(
      `https://api.evvosports.com/api/Vaccination/UpdateCoaches`,
      data
    );
  }

  deleteVaccination(id: any) {
    return this.http.delete(
      `https://api.evvosports.com/api/Vaccination/DeleteCoaches?Id=${id}`
    );
  }

  // downloadApi(id: any, type:any, team:any) {
  //   return this.http.get(`https://api.evvosports.com/api/Exportdata/ExportData?Id=${id}&DownloadType=${type}&DataName=${team}`)

  // }

  downloadApi(id: any, type: any, team: any): Observable<any> {
    return this.http.get(
      environment.baseurl +
        'Exportdata/ExportData?Id=' +
        id +
        '&DownloadType=' +
        type +
        '&DataName=' +
        team,
      { responseType: 'text' }
    );
  }
  updateRoles(Datatosend: any): Observable<any> {
    return this.http.put(environment.baseurl + 'Roles/UpdateRoles', Datatosend);
  }

  getAthleteStatus(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Team/GetAthleteStatus?teamid=${id}`
    );
  }

  getTeamBySports(id: any): Observable<any> {
    return this.http.get(
      `https://api.evvosports.com/api/Team/GetTeamByOrgId?orgid=${id}`
    );
  }
  getTeamBySportsByCoachId(id: any): Observable<any> {
    return this.http.get(
      `https://api.evvosports.com/api/Team/GetTeamByCoachId/${id}`
    );
  }
  getAllTraining(orgId: any, month: any) {
    //return this.http.get(environment.baseurl + 'TrainingEvents/GetAllTraining');
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetTrainingByOrganisationId?organisationid=' +
        orgId +
        '&month=' +
        month
    );
  }
  getAllTrainingByCoachId(orgId: any, month: any) {
    //return this.http.get(environment.baseurl + 'TrainingEvents/GetAllTraining');
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetTrainingByCoachId?coachid=' +
        orgId +
        '&month=' +
        month
    );
  }
  addTraining(Datatosend: any): Observable<any> {
    return this.http.post(
      environment.baseurl + 'TrainingEvents/AddTraining',
      Datatosend
    );
  }
  updateTraining(Datatosend: any): Observable<any> {
    return this.http.put(
      environment.baseurl + 'TrainingEvents/UpdateTraining',
      Datatosend
    );
  }

  addEvents(postData: any): Observable<any> {
    return this.http.post(
      environment.baseurl + 'TrainingEvents/AddEvents',
      postData
    );
  }
  getAllEvents(orgId: any, month: any) {
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetEventByOrganisationId?organisationid=' +
        orgId +
        '&month=' +
        month
    );
  }
  getAllEventsByCoachId(orgId: any, month: any) {
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetEventByCoachId?coachid=' +
        orgId +
        '&month=' +
        month
    );
  }
  updateEvent(Datatosend: any): Observable<any> {
    return this.http.put(
      environment.baseurl + 'TrainingEvents/UpdateEvents',
      Datatosend
    );
  }
  deleteTraining(id: any): Observable<any> {
    return this.http.delete(
      environment.baseurl + 'TrainingEvents/DeleteTraining?id=' + id
    );
  }
  deleteEvent(id: any): Observable<any> {
    return this.http.delete(
      environment.baseurl + 'TrainingEvents/DeleteEvents?id=' + id
    );
  }
  getAllTrainingsByDay(orgId: any, date: any) {
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetTrainingByDay?organisationid=' +
        orgId +
        '&date=' +
        date
    );
  }
  getAllTrainingsByDayByCoachId(orgId: any, date: any) {
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetTrainingByDayByCoachId?coachid=' +
        orgId +
        '&date=' +
        date
    );
  }
  getAllEventsByDay(orgId: any, date: any) {
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetEventByDay?organisationid=' +
        orgId +
        '&date=' +
        date
    );
  }

  getAllEventsByDayByCoachId(orgId: any, date: any) {
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetEventByDayByCoachId?coachid=' +
        orgId +
        '&date=' +
        date
    );
  }
  getAllTrainingsByWeekly(orgId: any, fromDate: any, toDate: any) {
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetTrainingByWeekly?organisationid=' +
        orgId +
        '&fromdate=' +
        fromDate +
        '&todate=' +
        toDate
    );
  }
  getAllTrainingsByWeeklyByCoachId(orgId: any, fromDate: any, toDate: any) {
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetTrainingByWeeklyByCoachId?coachid=' +
        orgId +
        '&fromdate=' +
        fromDate +
        '&todate=' +
        toDate
    );
  }
  getAllEventsByWeekly(orgId: any, fromDate: any, toDate: any) {
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetEventsByWeekly?organisationid=' +
        orgId +
        '&fromdate=' +
        fromDate +
        '&todate=' +
        toDate
    );
  }
  getAllEventsByWeeklyByCoachId(orgId: any, fromDate: any, toDate: any) {
    return this.http.get(
      environment.baseurl +
        'TrainingEvents/GetEventsByWeeklyByCoachId?coachid=' +
        orgId +
        '&fromdate=' +
        fromDate +
        '&todate=' +
        toDate
    );
  }
  //---------------------------------------Message Module---------------------------------------------------------------------
  sendMessage(data: any): Observable<Object> {
    return this.http.post(
      'https://api.evvosports.com/api/Messages/SendMessage',
      data
    );
  }
  uploadMessageFile(name: any, id: any, formdata: any) {
    return this.http.post(
      environment.baseurl +
        'Messages/DocumentUpload?OrganisationName=' +
        name +
        '&Id=' +
        id,
      formdata,
      { responseType: 'text' }
    );
  }
  getMessagesByReceiverId(id: any) {
    //only athelete
    return this.http.get(
      environment.baseurl + 'Messages/GetByRecieverId?recieverid=' + id
    );
  }
  getMessagesBySenderId(id: any) {
    //only athelete
    return this.http.get(
      environment.baseurl + 'Messages/GetBySenderId?senderid=' + id
    );
  }
  getMessagesByReceiveId(id: any) {
    return this.http.get(
      environment.baseurl + 'Messages/GetRecieved?recieverid=' + id
    );
  }
  getMessagesBySentId(id: any) {
    return this.http.get(
      environment.baseurl + 'Messages/GetSent?senderid=' + id
    );
  }
  deleteMessageById(messageId: any, id: any) {
    return this.http.delete(
      environment.baseurl +
        'Messages/DeleteMessage?messageid=' +
        messageId +
        '&id=' +
        id
    );
  }
  deleteMultipleMessages(id: any, messageIds: any) {
    console.log(messageIds);
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: messageIds,
    };
    return this.http.delete(
      environment.baseurl + 'Messages/DeleteMany?id=' + id,
      options
    );
  }
  getTokenById(id: any) {
    return this.http.get(
      environment.baseurl + 'Messages/GetTokenById?id=' + id
    );
  }
  saveToken(data: any) {
    return this.http.post(
      'https://api.evvosports.com/api/Messages/AddTokens',
      data
    );
  }

  updateToken(data: any) {
    return this.http.post(
      'https://api.evvosports.com/api/Messages/UpdateTokens',
      data
    );
  }

  updateMessage(data: any) {
    return this.http.put(
      'https://api.evvosports.com/api/Messages/Update',
      data
    );
  }
  sendPushNotification(data: any) {
    return this.http.post('https://fcm.googleapis.com/fcm/send', data, {
      headers: new HttpHeaders({
        Authorization:
          'Bearer AAAAHRdI53I:APA91bEo7vsyNs5L_yF97twrd_LHogVykApu9MD5xq1eq82vmjJWOAnN9zDR1p0m6pH6-WHoUGSUb3Gtk8jY5Qf3zzYl2lAHaOt1aDr9NPpNSwH9KGM_WV-ekg0bk79EGQMT7HfAWaqQ',
      }),
    });
  }
  getReplyMessagesById(id:any):Observable<any>{
  return this.http.get(
    environment.baseurl + 'Messages/GetmessagesById/' + id
  );
  }
  // --------------------------------------------    Assessment api's   ----------------------------------------------

  addAssessment(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/Assessment/AddAssessment`,
      data
    );
  }

  addQuestions(data: any) {
    return this.http.post(
      `https://api.evvosports.com/api/Assessment/AddQuestions`,
      data
    );
  }

  getQuestionsById(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Assessment/GetQuestionsById/${id}`
    );
  }

  updateQuestionsById(data: any) {
    return this.http.put(
      `https://api.evvosports.com/api/Assessment/UpdateQuestion`,
      data
    );
  }

  deleteQuessionById(id: any) {
    return this.http.delete(
      `https://api.evvosports.com/api/Assessment/DeleteQuestions?id=${id}`
    );
  }

  deleteAssessment(id: any) {
    return this.http.delete(
      `https://api.evvosports.com/api/Assessment/Delete?id=${id}`
    );
  }

  getAssessmentByOrgId(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Assessment/GetAssessmentByOrgId/${id}`
    );
  }

  getAssessmentByAthleteId(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Assessment/GetAssessmentByAthleteId?athleteid=${id}`
    );
  }
  getAssessmentByAthleteIdWeb(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Assessment/GetAssessmentByAthleteIdWeb?athleteid=${id}`
    );
  }

  getSportsByCat(sports: any, id: any): Observable<any> {
    return this.http.get(
      environment.baseurl +
        'InviteAthlete/GetAthleteBySportsCat?sports=' +
        sports +
        '&orgid=' +
        id
    );
  }

  uploadFile(name: any, id: any, formdata: any) {
    // fileName='637438190998421372.xlsx';
    console.log(name, id, formdata);

    return this.http.post(
      environment.baseurl +
        'AthleteDocumentUpload/AthleteDocumentUpload?OrganisationName=' +
        name +
        '&Id=' +
        id,
      formdata,
      { responseType: 'text' }
    );
  }

  uploadTreatmentFile(name: any, id: any, formdata: any) {
    // fileName='637438190998421372.xlsx';

    console.log('upload treatmernt filr', name + id + formdata);

    return this.http.post(
      environment.baseurl +
        'TreatmentInfo/TreatmentInfoUpload?OrganisationName=' +
        name +
        '&Id=' +
        id,
      formdata,
      { responseType: 'text' }
    );
  }

  addAtheleteDocument(data: any) {
    // fileName='637438190998421372.xlsx';

    return this.http.post(
      environment.baseurl + 'AthleteDocumentUpload/AddAthleteDocumentUpload',
      data
    );
  }
  // getAtheleteDocument() {
  //   return this.http.get(
  //     environment.baseurl + 'AthleteDocumentUpload/GetAllAthleteDocumentUpload'
  //   );
  // }
  getAtheleteDocument(id: any) {
    // console.log(id);

    // return this.http.get(
    //   environment.baseurl + 'AthleteDocumentUpload/GetAthleteDocuments?athleteid=' + id
    // );

    return this.http.get(
      environment.baseurl +
        'AthleteDocumentUpload/GetAthleteDocument?athleteid=' +
        id
    );
  }
  deleteDocument(id: any): Observable<any> {
    return this.http.delete(
      environment.baseurl + 'AthleteDocumentUpload/Delete?id=' + id
    );
  }

  // --------------------------------------------    athlete-details api's   ----------------------------------------------

  getInjuryById(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/InjuryDetails/GetInjuryById?id=${id}`
    );
  }

  getTreatmentInfoByAthleteId(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/TreatmentInfo/GetTreatmentInfoByAthleteId?athleteid=${id}`
    );
  }
  getTeamBySportsCat(id: any, type: any) {
    return this.http.get(
      environment.baseurl +
        'Team/GetTeamBySportsCat?orgid=' +
        id +
        '&sports=' +
        type
    );
  }
  getListEventTraining(id: any, type: any) {
    console.log(id, type);

    return this.http.get(
      environment.baseurl +
        'Team/GetListEventsTraining?teamid=' +
        id +
        '&type=' +
        type
    );
  }

  getAthleteAttendance(id: any, type: any, start: any, end: any) {
    console.log(id, type, start, end);

    return this.http.get(
      environment.baseurl +
        'Team/GetAthleteAttendance?teamid=' +
        id +
        '&typeid=' +
        type +
        '&fromdate=' +
        start +
        '&todate=' +
        end
    );
  }

  exportToExcel(): Observable<any> {
    return this.http.get(
      environment.baseurl +
        'Exportdata/ExportAllOrgaisationData?DownloadType=EXCEL',
      { responseType: 'text' }
    );
  }
  exportToPdF(): Observable<any> {
    return this.http.get(
      environment.baseurl +
        'Exportdata/ExportAllOrgaisationData?DownloadType=PDF',
      { responseType: 'text' }
    );
  }
  getAthletesInviteCount() {
    return this.http.get(
      environment.baseurl + 'Widgets/GetAthletesInviteCount'
    );
  }

  getAthletesInviteCountByOrgId(data: any) {
    return this.http.get(
      environment.baseurl +
        'Widgets/GetAthletesInviteCountByOrgId?organisationid=' +
        data
    );
  }

  getTeamSizeCountOrgId(data: any) {
    return this.http.get(
      environment.baseurl + 'Widgets/TeamSizeCount?organisationid=' + data
    );
  }

  getAthleteCountByCountryByOrgId(data: any) {
    // return this.http.get(
    //   environment.baseurl + 'Widgets/GetAthleteCountByCountryByOrgId?organisationid='+data
    // );
  }

  getVaccinationCountByOrgId(data: any) {
    return this.http.get(
      environment.baseurl +
        'Widgets/GetVaccinationCountByOrgId?organisationid=' +
        data
    );
  }

  getWidgetCollections() {
    return this.http.get(environment.baseurl + 'Widgets/GetCollections');
  }
  getAllWidgetTypes() {
    return this.http.get(environment.baseurl + 'Widgets/GetAllWidgetTypes');
  }

  addWidgets(data: any) {
    return this.http.post(environment.baseurl + 'Widgets/AddWidget', data);
  }

  downloadDocument(fileName: any, name: any, Ids: any): Observable<any> {
    console.log(name, fileName, Ids);

    // return this.http.get(environment.baseurl + 'AthleteDocumentUpload/DownloadFile?=NameFile='+name+'&OrganisationName='+fileName+'&Id='+Ids,{ responseType: 'text' });
    return this.http.get(
      environment.baseurl +
        'AthleteDocumentUpload/DownloadFile?NameFile=' +
        fileName +
        '&OrganisationName=' +
        name +
        '&Id=' +
        Ids,
      { responseType: 'text' }
    );
  }

  numVerify(code: any, phoneNo: any) {
    return this.http.get(
      `http://apilayer.net/api/validate?access_key=f65107cf703957b700b62651250f56f9&number=${phoneNo}&format=1&country_code=${code}`
    );
  }

  downloadTreatmentInfo(fileName: any, orgName: any, athleteId: any) {
    return this.http.get(
      environment.baseurl +
        'TreatmentInfo/DownloadFile?NameFile=' +
        fileName +
        '&OrganisationName=' +
        orgName +
        '&Id=' +
        athleteId,
      { responseType: 'text' }
    );
  }

  addCoachOnboard(data: any) {
    // fileName='637438190998421372.xlsx';

    return this.http.put(environment.baseurl + 'Coaches/UpdateCoaches', data);
  }

  addAdminOnboard(data: any) {
    return this.http.put(
      environment.baseurl + 'Organisation/CompleteProfile',
      data
    );
  }

  getOrgById(id: any) {
    return this.http.get(
      `https://api.evvosports.com/api/Organisation/GetOrganisationById/${id}`
    );
  }

  uploadImage(name: any, id: any, formdata: any) {
    console.log(name, id, formdata);
    const formdatas: FormData = new FormData();
    formdatas.append('file', formdata, formdata.name);
    return this.http.post(
      environment.baseurl +
        'Coaches/ProfilePicUpload?organisationname=' +
        name +
        '&coachid=' +
        id,
      formdatas
    );
  }

  // https://api.evvosports.com/api/Organisation/AdminProfilePicUpload

  uploadAdminImage(name: any, id: any, formdata: any) {
    console.log(name, id, formdata);
    const formdatas: FormData = new FormData();
    formdatas.append('file', formdata, formdata.name);
    return this.http.post(
      environment.baseurl +
        'Organisation/AdminProfilePicUpload?organisationname=' +
        name +
        '&organisationid' +
        id,
      formdatas
    );
  }

  getCoachById(id: any) {
    return this.http.get(
      environment.baseurl + 'Coaches/GetCoachesById?id=' + id
    );
  }
  getSideNavbarItems(id: any) {
    return this.http.get(
      environment.baseurl + 'Roles/SideNavByRoleId?Id=' + id
    );
  }
  get() {
    return this.http.get(
      'https://31du67c2x5.execute-api.us-east-1.amazonaws.com/dev/data/getuser'
    );
  }

  addAttendance(data: any) {
    return this.http.post(environment.baseurl + 'Team/AddManyAttendance', data);
  }
  updateAttendance(data:any){
    return this.http.post(environment.baseurl+ 'Team/UpdateManyAttendance', data)
  }
  getOrganisationCount(){
    return this.http.get(environment.baseurl + 'Organisation/OrganisationCount')
  }


  getCountByOrgId(data: any) {
    return this.http.get(
      environment.baseurl + 'InviteAthlete/AthleteCounts?orgid=' + data
    );
  }

  getDataSet(id: any) {
    return this.http.get(
      environment.baseurl +
        'DataSet/GetDatasetByCollectionId?collectionid=' +
        id
    );
  }

  updateDataSet(data: any) {
    return this.http.put(environment.baseurl + 'DataSet/UpdateDataSets', data);
  }

  getZipCodeBase(zipCode: any) {
    return this.http.get(
      `https://app.zipcodebase.com/api/v1/search?apikey=b82fdf10-24db-11ec-9a5a-a3b728e6df7d&codes=${zipCode}`
    );
  }
}
