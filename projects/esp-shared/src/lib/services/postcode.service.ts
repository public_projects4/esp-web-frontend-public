import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostcodeService {

  constructor(private http: HttpClient) { }

  // getData():Observable<>{
  //   return this.http.get<any>(https://api.zippopotam.us/in/560045);
  // }

//   getPost(cd:any,pin:any) {
//     return this.http.get(`https://api.zippopotam.us/${cd}/${pin}`);
//  }

  getPost(pin:any) {
    return this.http.get(`https://api.postalpincode.in/pincode/${pin}`);
 }

  getzipCodeBase(pin:any) {
    return this.http.get(`https://app.zipcodebase.com/api/v1/search?apikey=b82fdf10-24db-11ec-9a5a-a3b728e6df7d&codes=${pin}`);
 }


 addorganization(){
   return this.http.get(`https://api.hostip.info`);
 }

 getLogin(user:any,pass:any){
   console.log('user======>',user);
   console.log('pass======>',pass);


   return this.http.get(`http://api.evvosports.com/api/Login/AuthenticateUser?Email=${user}&Password=${pass}`)
 }

}


