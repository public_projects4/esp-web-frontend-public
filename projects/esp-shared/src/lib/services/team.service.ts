import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { delay } from "rxjs/operators";
import { Team } from "../models";

@Injectable({
    providedIn: "root"
})
export class TeamService {
    getTeams(): Observable<Team[]> {
        const teams: Team[] = [
            {
                name: 'Evergreen Soldiers',
                coach: '',
                total_atheletes: 12,
                active: 10,
                injured: 2,
                invited: 2,
                avatar: 'assets/img/icons/team_icon.svg'
            },
            {
                name: 'Evergreen Soldiers',
                coach: '',
                total_atheletes: 12,
                active: 10,
                injured: 2,
                invited: 2,
                avatar: 'assets/img/icons/team_icon.svg'
            },
            {
                name: 'Evergreen Soldiers',
                coach: '',
                total_atheletes: 12,
                active: 10,
                injured: 2,
                invited: 2,
                avatar: 'assets/img/icons/team_icon.svg'
            },
            {
                name: 'Evergreen Soldiers',
                coach: '',
                total_atheletes: 12,
                active: 10,
                injured: 2,
                invited: 2,
                avatar: 'assets/img/icons/team_icon.svg'
            }
            
        ];
        return of(teams).pipe(delay(200));
    }
}